﻿namespace Peeza.RPG.Wiki.Extensions
{
    public static class TypeExtensions
    {
        private static IEnumerable<Type> allTypes;
        public static IEnumerable<Type> GetAllRPGTypes()
        {
            allTypes ??= typeof(RPG).Assembly
                .GetReferencedAssemblies()
                .Where(assemblyName => assemblyName.Name.StartsWith("Peeza.RPG"))
                .Select(assemblyName => AppDomain.CurrentDomain.GetAssemblies().First(assembly => assembly.GetName().Name == assemblyName.Name))
                .SelectMany(assembly => assembly.GetExportedTypes())
                .Where(t => !t.IsAbstract);

            return allTypes;
        }

        public static IEnumerable<Type> GetAllTypes<T>()
        {
            var types = GetAllRPGTypes().Where(t => typeof(T).IsAssignableFrom(t));
            return types;
        }

        public static IEnumerable<T> GetAndConstruct<T>(params object[] constructorArgs)
        {
            var constructed = GetAllTypes<T>().Select(t => (T)Activator.CreateInstance(t, constructorArgs));
            return constructed;
        }
    }
}
