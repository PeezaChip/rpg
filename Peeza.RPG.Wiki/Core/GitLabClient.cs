﻿using Peeza.RPG.Wiki.Models;
using System.Net;
using System.Net.Http.Json;

namespace Peeza.RPG.Wiki.Core
{
    public class GitLabClient
    {
        private readonly HttpClient httpClient;

        public GitLabClient(string token)
        {
            httpClient = new HttpClient
            {
                BaseAddress = new Uri("https://gitlab.com/api/v4/projects/PeezaChip%2Frpg/")
            };
            httpClient.DefaultRequestHeaders.Add("PRIVATE-TOKEN", token);
        }

        public async Task<WikiPageResponse> GetPage(string slug)
        {
            try
            {
                return await httpClient.GetFromJsonAsync<WikiPageResponse>($"wikis/{slug}");
            }
            catch (HttpRequestException ex)
            {
                if (ex.StatusCode == HttpStatusCode.NotFound) return null;
                throw;
            }
        }

        public async Task<WikiPageResponse> CreatePage(WikiPageRequest body)
        {
            var response = await httpClient.PostAsJsonAsync($"wikis", body);
            return response.IsSuccessStatusCode ? await response.Content.ReadFromJsonAsync<WikiPageResponse>() : null;
        }

        public async Task<WikiPageResponse> EditPage(string slug, WikiPageRequest body)
        {
            var response = await httpClient.PutAsJsonAsync($"wikis/{slug}", body);
            return response.IsSuccessStatusCode ? await response.Content.ReadFromJsonAsync<WikiPageResponse>() : null;
        }
    }
}
