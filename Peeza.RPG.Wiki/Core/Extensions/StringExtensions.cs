﻿namespace Peeza.RPG.Wiki.Core.Extensions
{
    public static  class StringExtensions
    {
        public static string ToSlug(this string s) => s.Replace("/", "%2F");
    }
}
