﻿using Peeza.RPG.Core.Basic.Generic;
using Peeza.RPG.Wiki.Core.Extensions;
using Peeza.RPG.Wiki.Models;
using Peeza.RPG.Wiki.PageBuilders;

namespace Peeza.RPG.Wiki.Core
{
    public class CollectionPageUpdater<T>(ICollectionPageBuilder<T> collectionPageBuilder, GitLabClient gitLabClient) : IPageUpdater where T : IRPGKey
    {
        public async Task Update()
        {
            await UpdatePage(collectionPageBuilder.GetNavPagePath(), collectionPageBuilder.GetNavPage());

            foreach (var item in collectionPageBuilder.Items)
            {
                await UpdatePage(collectionPageBuilder.GetPagePath(item), collectionPageBuilder.GetPage(item));
            }
        }

        private async Task UpdatePage(string title, string content)
        {
            if (gitLabClient == null)
            {
                DryRun(title, content);
                return;
            }

            Console.WriteLine($"Checking if {title} needs to be updated");
            var page = await gitLabClient.GetPage(title.ToSlug());
            if (page == null)
            {
                Console.WriteLine($"Page doesn't exist, creating");
                await gitLabClient.CreatePage(new WikiPageRequest() { Title = title, Content = content, Format = "markdown" });
            }
            else if (page.Content != content)
            {
                Console.WriteLine($"Page content doesn't match, updating");
                await gitLabClient.EditPage(title.ToSlug(), new WikiPageRequest() { Title = title, Content = content, Format = "markdown" });
            }
        }

        private void DryRun(string title, string content)
        {
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine(title);
            Console.WriteLine();
            Console.WriteLine(content);
        }
    }
}
