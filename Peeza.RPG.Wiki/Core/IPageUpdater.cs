﻿
namespace Peeza.RPG.Wiki.Core
{
    public interface IPageUpdater
    {
        Task Update();
    }
}