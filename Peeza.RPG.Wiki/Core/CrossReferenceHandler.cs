﻿using Peeza.RPG.Wiki.PageBuilders;

namespace Peeza.RPG.Wiki.Core
{
    public class CrossReferenceHandler
    {
        private readonly Dictionary<string, CrossReferencePageBuilder> pageBuilders = [];

        public void Register(string keyword, CrossReferencePageBuilder pageBuilder)
        {
            pageBuilders.Add(keyword, pageBuilder);
        }

        public string GetLink(string keyword, string key)
        {
            if (!pageBuilders.TryGetValue(keyword, out CrossReferencePageBuilder pageBuilder)) return null;
            return pageBuilder.GetPagePath(key);
        }
    }
}
