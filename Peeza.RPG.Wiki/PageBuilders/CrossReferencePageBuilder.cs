﻿using Peeza.RPG.Core.Basic.Generic;
using Peeza.RPG.Wiki.Core;
using System.Text.RegularExpressions;

namespace Peeza.RPG.Wiki.PageBuilders
{
    public class CrossReferencePageBuilder<T>(CollectionPageBuilder<T> pageBuilder, string keyword, CrossReferenceHandler crossReferenceHandler) : CrossReferencePageBuilder(keyword, crossReferenceHandler), ICollectionPageBuilder<T> where T : IRPGKey
    {
        public IEnumerable<T> Items => pageBuilder.Items;

        public string GetNavPage()
        {
            return pageBuilder.GetNavPage();
        }

        public string GetNavPagePath()
        {
            return pageBuilder.GetNavPagePath();
        }

        public string GetPage(T item)
        {
            var content = pageBuilder.GetPage(item);

            var crossReferences = GetCrossReferences(content);

            foreach(var (key, keyword) in crossReferences)
            {
                var link = crossReferenceHandler.GetLink(keyword, key);
                if (link == null) continue;

                content = content.Replace($"[{keyword}:{key}]", $"[{key}]({link})");
            }

            return content;
        }

        public string GetPagePath(T item)
        {
            return pageBuilder.GetPagePath(item);
        }

        public override string GetPagePath(string itemKey)
        {
            var item = pageBuilder.Items.FirstOrDefault(item => item.Key == itemKey);
            return item == null ? null : GetPagePath(item);
        }
    }

    public abstract partial class CrossReferencePageBuilder
    {
        protected readonly CrossReferenceHandler crossReferenceHandler;

        public CrossReferencePageBuilder(string keyword, CrossReferenceHandler crossReferenceHandler)
        {
            this.crossReferenceHandler = crossReferenceHandler;
            crossReferenceHandler.Register(keyword, this);
        }

        public abstract string GetPagePath(string itemKey);

        [GeneratedRegex(@"\[([\w]+):([\w ]+)\]")]
        private static partial Regex CrossReferenceRegex();
        protected Dictionary<string, string> GetCrossReferences(string content)
        {
            return CrossReferenceRegex().Matches(content)
                .DistinctBy(m => m.Groups[2].Value)
                .ToDictionary(m => m.Groups[2].Value, m => m.Groups[1].Value);
        }
    }
}
