﻿using Peeza.RPG.Core.Heroes.Races;
using System.Text;

namespace Peeza.RPG.Wiki.PageBuilders
{
    public class RacePageBuilder(IEnumerable<RaceDeclaration> races) : CollectionPageBuilder<RaceDeclaration>(races)
    {
        public override string GetNavPagePath()
        {
            return string.Join("/", "Peeza-RPG", "Race-List");
        }

        public override string GetNavPage()
        {
            var ordered = Items.OrderBy(races => races.Key);

            var sb = new StringBuilder();
            sb.AppendLine("---");
            sb.AppendLine("title: Race List");
            sb.AppendLine("---");

            foreach (var race in ordered)
            {
                sb.AppendLine($"- ![{race.Key}][{race.Key}]{{width=24}} [{race.Key}](Peeza-RPG/Race-List/{race.Key})");
            }

            sb.AppendLine();

            foreach (var race in ordered)
            {
                sb.AppendLine($"[{race.Key}]: https://gitlab.com/PeezaChip/rpg.assets/-/raw/master/0.5x/{race.Key}.webp \"{race.Key}\"");
            }

            return sb.ToString();
        }

        public override string GetPagePath(RaceDeclaration race)
        {
            return string.Join("/", "Peeza-RPG", "Race-List", race.Key.Replace(' ', '-'));
        }

        public override string GetPage(RaceDeclaration race)
        {
            var sb = new StringBuilder();

            sb.AppendLine("---");
            sb.AppendLine($"title: {race.Key}");
            sb.AppendLine("---");
            sb.AppendLine($"![{race.Key}](https://gitlab.com/PeezaChip/rpg.assets/-/raw/master/0.5x/{race.Key}.webp \"{race.Key}\"){{width=128}}");
            sb.AppendLine($"# {race.Key} - **{race.AbilityName}**");
            sb.AppendLine(race.Description);

            return sb.ToString();
        }
    }
}
