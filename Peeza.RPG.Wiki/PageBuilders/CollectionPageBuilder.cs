﻿using Peeza.RPG.Core.Basic.Generic;

namespace Peeza.RPG.Wiki.PageBuilders
{
    public abstract class CollectionPageBuilder<T>(IEnumerable<T> items) : ICollectionPageBuilder<T> where T : IRPGKey
    {
        public IEnumerable<T> Items => items;

        public abstract string GetNavPage();
        public abstract string GetNavPagePath();
        public abstract string GetPage(T item);
        public abstract string GetPagePath(T item);
    }
}
