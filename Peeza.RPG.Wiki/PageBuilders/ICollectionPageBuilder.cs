﻿using Peeza.RPG.Core.Basic.Generic;

namespace Peeza.RPG.Wiki.PageBuilders
{
    public interface ICollectionPageBuilder<T> where T : IRPGKey
    {
        string GetNavPage();
        string GetNavPagePath();
        string GetPage(T item);
        string GetPagePath(T item);

        IEnumerable<T> Items { get; }
    }
}
