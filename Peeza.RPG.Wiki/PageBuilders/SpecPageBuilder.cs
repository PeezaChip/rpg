﻿using Peeza.RPG.Core.Heroes.Specs;
using System.Text;

namespace Peeza.RPG.Wiki.PageBuilders
{
    public class SpecPageBuilder(IEnumerable<SpecDeclaration> specs) : CollectionPageBuilder<SpecDeclaration>(specs)
    {
        public override string GetNavPagePath()
        {
            return string.Join("/", "Peeza-RPG", "Class-List");
        }

        public override string GetNavPage()
        {
            var ordered = Items.OrderBy(spec => spec.Key);

            var sb = new StringBuilder();
            sb.AppendLine("---");
            sb.AppendLine("title: Class List");
            sb.AppendLine("---");

            foreach (var spec in ordered)
            {
                sb.AppendLine($"- ![{spec.Key}][{spec.Key}]{{width=24}} [{spec.Key}](Peeza-RPG/Class-List/{spec.Key})");
            }

            sb.AppendLine();

            foreach (var spec in ordered)
            {
                sb.AppendLine($"[{spec.Key}]: https://gitlab.com/PeezaChip/rpg.assets/-/raw/master/0.5x/{spec.Key}.webp \"{spec.Key}\"");
            }

            return sb.ToString();
        }

        public override string GetPagePath(SpecDeclaration spec)
        {
            return string.Join("/", "Peeza-RPG", "Class-List", spec.Key.Replace(' ', '-'));
        }

        public override string GetPage(SpecDeclaration spec)
        {
            var sb = new StringBuilder();

            sb.AppendLine("---");
            sb.AppendLine($"title: {spec.Key}");
            sb.AppendLine("---");
            sb.AppendLine($"![{spec.Key}](https://gitlab.com/PeezaChip/rpg.assets/-/raw/master/0.5x/{spec.Key}.webp \"{spec.Key}\"){{width=128}}");
            sb.AppendLine($"# {spec.Key} - **{spec.AbilityName}**");
            sb.AppendLine(spec.Description);

            return sb.ToString();
        }
    }
}
