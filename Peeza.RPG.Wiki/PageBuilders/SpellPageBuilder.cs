﻿using Peeza.RPG.Core.Basic.Spells;
using System.Text;

namespace Peeza.RPG.Wiki.PageBuilders
{
    public class SpellPageBuilder(IEnumerable<Spell> spells) : CollectionPageBuilder<Spell>(spells)
    {
        public override string GetNavPagePath()
        {
            return string.Join("/", "Peeza-RPG", "Spell-List");
        }

        public override string GetNavPage()
        {
            var ordered = Items.OrderBy(spell => spell.Key);

            var sb = new StringBuilder();
            sb.AppendLine("---");
            sb.AppendLine("title: Spell List");
            sb.AppendLine("---");

            foreach (var spec in ordered)
            {
                sb.AppendLine($"- [{spec.Key}](Peeza-RPG/Spell-List/{spec.Key})");
            }

            return sb.ToString();
        }

        public override string GetPagePath(Spell spell)
        {
            return string.Join("/", "Peeza-RPG", "Spell-List", spell.Key.Replace(' ', '-'));
        }

        public override string GetPage(Spell spell)
        {
            var sb = new StringBuilder();

            sb.AppendLine("---");
            sb.AppendLine($"title: {spell.Key}");
            sb.AppendLine("---");
            sb.AppendLine($"# {spell.Key}");
            sb.AppendLine(spell.Description);

            if (!string.IsNullOrWhiteSpace(spell.Details))
            {
                sb.AppendLine();
                sb.AppendLine("_Details:_");
                sb.AppendLine(spell.Details);
            }

            return sb.ToString();
        }
    }
}
