﻿namespace Peeza.RPG.Wiki.Models
{
    public class WikiPageResponse : WikiPageRequest
    {
        public string Encoding { get; set; }
        public string Slug { get; set; }
    }

    public class WikiPageRequest
    {
        public string Content { get; set; }
        public string Format { get; set; }
        public string Title { get; set; }
    }
}
