﻿using Peeza.RPG;
using Peeza.RPG.Core.Basic.Spells;
using Peeza.RPG.Core.Heroes.Races;
using Peeza.RPG.Core.Heroes.Specs;
using Peeza.RPG.Wiki.Core;
using Peeza.RPG.Wiki.Extensions;
using Peeza.RPG.Wiki.PageBuilders;

var gitlab_token = Environment.GetEnvironmentVariable("gitlab_token");

var client = string.IsNullOrEmpty(gitlab_token) ? null : new GitLabClient(gitlab_token);

var crossReferenceHandler = new CrossReferenceHandler();

var updaters = new IPageUpdater[] {
    new CollectionPageUpdater<RaceDeclaration>(new CrossReferencePageBuilder<RaceDeclaration>(new RacePageBuilder(RPG.RaceDeclarations), "Race", crossReferenceHandler), client),
    new CollectionPageUpdater<SpecDeclaration>(new CrossReferencePageBuilder<SpecDeclaration>(new SpecPageBuilder(RPG.SpecDeclarations), "Class", crossReferenceHandler), client),
    new CollectionPageUpdater<Spell>(new CrossReferencePageBuilder<Spell>(new SpellPageBuilder(TypeExtensions.GetAndConstruct<Spell>([null])), "Spell", crossReferenceHandler), client),
};

foreach (var updater in updaters)
{
    Console.WriteLine($"Updating {updater}");

    await updater.Update();
}

Console.WriteLine("All done. Press any key to continue..");
Console.ReadKey();