﻿using Peeza.RPG.Core.Heroes.Races;
using Peeza.RPG.Core.Realizations.Generic;

namespace Peeza.RPG.Core.Realizations.Heroes.Races
{
    public class RacesDictionary : KeyedEntityDictionaryAccessor<RaceDeclaration>, IRaceDeclarationAccessor { }
}
