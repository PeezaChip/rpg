﻿using Peeza.RPG.Core.Heroes.Quests;
using Peeza.RPG.Core.Realizations.Generic;

namespace Peeza.RPG.Core.Realizations.Heroes.Quests
{
    public class QuestList : KeyedEntityDictionaryAccessor<Quest>, IQuestsAccessor { }
}
