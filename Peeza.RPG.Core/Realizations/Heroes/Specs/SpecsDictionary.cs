﻿using Peeza.RPG.Core.Heroes.Specs;
using Peeza.RPG.Core.Realizations.Generic;

namespace Peeza.RPG.Core.Realizations.Heroes.Specs
{
    public class SpecsDictionary : KeyedEntityDictionaryAccessor<SpecDeclaration>, ISpecDeclarationAccessor { }
}
