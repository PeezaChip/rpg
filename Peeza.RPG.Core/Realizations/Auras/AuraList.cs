﻿using Peeza.RPG.Core.Basic.Auras;
using Peeza.RPG.Core.Realizations.Generic;

namespace Peeza.RPG.Core.Realizations.Auras
{
    public class AuraList : KeyedEntityListAccessor<Aura>, IAurasAccessor { }
}
