﻿using Peeza.RPG.Core.Basic.Cooldowns;
using Peeza.RPG.Core.Realizations.Generic;
using System.Linq;

namespace Peeza.RPG.Core.Realizations.Cooldowns
{
    public class CooldownList : KeyedEntityDictionaryAccessor<Cooldown>, ICooldownAccessor
    {
        public void DecreaseCooldown(Cooldown cooldown)
        {
            cooldown.Counter--;
            if (cooldown.Counter <= 0)
            {
                Remove(cooldown);
            }
        }

        public void DecreaseCooldownsOfType(CooldownTypeFlags cooldownType)
        {
            var cooldownsOfType = GetAll(c => c.Type.HasFlag(cooldownType)).ToList();
            foreach (var cooldown in cooldownsOfType)
            {
                DecreaseCooldown(cooldown);
            }
        }
    }
}
