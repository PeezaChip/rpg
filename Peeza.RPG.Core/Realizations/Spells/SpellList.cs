﻿using Peeza.RPG.Core.Basic.Spells;
using Peeza.RPG.Core.Realizations.Generic;

namespace Peeza.RPG.Core.Realizations.Spells
{
    public class SpellList : KeyedEntityDictionaryAccessor<Spell>, ISpellsAccessor { }
}
