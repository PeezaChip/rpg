﻿using Peeza.RPG.Core.Basic.BattleFields;

namespace Peeza.RPG.Core.Realizations.BattleFields
{
    public class BattleFieldAccessor : IBattleFieldAccessor
    {
        protected IBattleField field;

        public IBattleField GetField()
        {
            return field;
        }
    }
}
