﻿using Peeza.RPG.Core.Basic.BattleFields;

namespace Peeza.RPG.Core.Realizations.BattleFields
{
    public class BattleFieldProvider : BattleFieldAccessor
    {
        public void SetField(IBattleField field)
        {
            this.field = field;
        }
    }
}
