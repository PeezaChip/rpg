﻿using Peeza.RPG.Core.Basic.Resources;
using Peeza.RPG.Core.Realizations.Generic;

namespace Peeza.RPG.Core.Realizations.Resources
{
    public class ResourceList : KeyedEntityDictionaryAccessor<Resource>, IResourcesAccessor { }
}
