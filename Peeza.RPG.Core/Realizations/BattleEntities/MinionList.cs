﻿using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Minions;
using Peeza.RPG.Core.Realizations.Generic;

namespace Peeza.RPG.Core.Realizations.BattleEntities
{
    public class MinionList : EntityListAccessor<Minion>, IMinionsAccessor { }
}
