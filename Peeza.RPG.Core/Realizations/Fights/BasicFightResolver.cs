﻿using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.BattleFields;
using Peeza.RPG.Core.Basic.Interfaces;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Peeza.RPG.Core.Realizations.Fights
{
    public class BasicFightResolver : IFightResolver
    {
        protected IBattleSidesAccessor battleSides;
        protected IRoundHandler roundHandler;
        protected IBattleFieldAccessor fieldAccessor;
        protected IBattleLogger logger;
        private readonly int roundsHardLimit;

        public BasicFightResolver(IBattleSidesAccessor battleSides, IRoundHandler roundHandler, IBattleFieldAccessor fieldAccessor = null, int roundsHardLimit = 1000, IBattleLogger logger = null)
        {
            this.battleSides = battleSides;
            this.roundHandler = roundHandler;
            this.fieldAccessor = fieldAccessor;

            this.roundsHardLimit = roundsHardLimit;
            this.logger = logger;
        }

        public virtual async Task ProccessFight(CancellationToken cancellationToken)
        {
            int roundCount = 1;
            // ToDo: this is incorrect if we got two friendly sides left for example
            logger?.Log($"Starting a fight");
            LogSides();

            while (battleSides.GetAll().Count(side => side.BattleEntities.Any(e => e.CanFight)) > 1)
            {
                if (cancellationToken.IsCancellationRequested) break;

                logger?.Log($"Round {roundCount}");
                using var indent = logger.IndentLog();
                await roundHandler.ProccessRound(cancellationToken, battleSides.GetAll().SelectMany(side => side.BattleEntities.Where(entity => entity.CanFight && entity is IBattleQueable).Cast<IBattleQueable>().Distinct().ToList()));
                logger?.Log();

                if (roundCount++ >= roundsHardLimit)
                {
                    break;
                }
            }

            logger?.Log("Fight ended");
            LogSides();
        }

        private void LogSides()
        {
            using var indent = logger.IndentLog();
            int side = 1;
            foreach (var battleSide in battleSides)
            {
                logger?.Log($"Party #{side++}", BattleLogVerbosity.Verbose);
                using var partyIndent = logger.IndentLog();
                foreach (var entity in battleSide.BattleEntities)
                {
                    logger?.Log(entity.ToString(), BattleLogVerbosity.Verbose);
                }
                logger?.Log(null, BattleLogVerbosity.Verbose);
            }
        }
    }
}
