﻿using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.BattleFields;
using Peeza.RPG.Core.Basic.Interfaces;
using System.Threading;
using System.Threading.Tasks;

namespace Peeza.RPG.Core.Realizations.Fights
{
    public class BasicTurnHandler : ITurnHandler
    {
        protected IBattleFieldAccessor fieldAccessor;

        public BasicTurnHandler(IBattleFieldAccessor fieldAccessor = null)
        {
            this.fieldAccessor = fieldAccessor;
        }

        public virtual Task ProccessTurn(CancellationToken cancellationToken, IBattleQueable queueable)
        {
            queueable.TurnAction();
            return Task.CompletedTask;
        }
    }
}
