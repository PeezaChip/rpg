﻿using Peeza.RPG.Core.Basic.Interfaces;
using Peeza.RPG.Core.Realizations.Battle;

namespace Peeza.RPG.Core.Realizations.Fights
{
    public class BasicRoundQueueAccessor : IQueueAccessor
    {
        private readonly BattleQueue queue;

        public BattleQueue GetQueue()
        {
            return queue;
        }

        public BasicRoundQueueAccessor(BattleQueue queue)
        {
            this.queue = queue;
        }
    }
}
