﻿using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Peeza.RPG.Core.Realizations.Fights.RelationshipManagers
{
    public class BrawlRelationshipManager : IBattleSideRelationshipManager
    {
        public IBattleSidesAccessor Sides { get; private set; }

        public IEnumerable<IBattleSide> GetFriendlies(IBattleSide side)
        {
            return GetBattleSidesByRelationship(side, BattleSideRelationshipEnum.Friendly);
        }

        public IEnumerable<IBattleSide> GetHostiles(IBattleSide side)
        {
            return GetBattleSidesByRelationship(side, BattleSideRelationshipEnum.Hostile);
        }

        public IEnumerable<IBattleSide> GetNeutrals(IBattleSide side)
        {
            return GetBattleSidesByRelationship(side, BattleSideRelationshipEnum.Neutral);
        }

        public BattleSideRelationshipEnum GetRelationship(IBattleSide side1, IBattleSide side2)
        {
            if (side1 == side2) return BattleSideRelationshipEnum.Friendly;
            return BattleSideRelationshipEnum.Hostile;
        }

        public BrawlRelationshipManager(IBattleSidesAccessor battleSides)
        {
            Sides = battleSides;
        }

        private IEnumerable<IBattleSide> GetBattleSidesByRelationship(IBattleSide side, BattleSideRelationshipEnum relationship)
        {
            return Sides.GetAll().Where(s => s != side && GetRelationship(side, s) == relationship);
        }
    }
}
