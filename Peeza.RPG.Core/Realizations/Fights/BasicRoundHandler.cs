﻿using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.BattleFields;
using Peeza.RPG.Core.Basic.Interfaces;
using Peeza.RPG.Core.Extensions;
using Peeza.RPG.Core.Realizations.Battle;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Peeza.RPG.Core.Realizations.Fights
{
    public class BasicRoundHandler : IRoundHandler
    {
        protected ITurnHandler turnHandler;
        protected Random random;
        protected BattleQueue battleQueue;
        protected IBattleFieldAccessor fieldAccessor;
        protected IBattleLogger logger;
        private BasicRoundQueueAccessor queueAccessor;

        public BasicRoundHandler(ITurnHandler turnHandler, Random random, IBattleFieldAccessor fieldAccessor = null, IBattleLogger logger = null)
        {
            this.turnHandler = turnHandler;
            this.random = random;
            this.logger = logger;
            this.fieldAccessor = fieldAccessor;
            battleQueue = new BattleQueue();
            queueAccessor = new BasicRoundQueueAccessor(battleQueue);
        }

        public virtual async Task ProccessRound(CancellationToken cancellationToken, IEnumerable<IBattleQueable> queables)
        {
            cancellationToken.ThrowIfCancellationRequested();
            battleQueue.Clear();
            battleQueue.EnqueueRange(queables.OrderRandomly(random));

            var turn = 0;
            while (battleQueue.TryDequeue(out var queable))
            {
                logger?.Log($"Turn {++turn}");
                using var indent = logger.IndentLog();
                await turnHandler.ProccessTurn(cancellationToken, queable);
                cancellationToken.ThrowIfCancellationRequested();
            }
        }

        public IQueueAccessor GetQueueAccessor()
        {
            return queueAccessor;
        }
    }
}
