﻿using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.BattleFields;
using Peeza.RPG.Core.Basic.Interfaces;
using Peeza.RPG.Core.Battle.Events;
using System;

namespace Peeza.RPG.Core.Realizations.Fights
{
    public class BasicBattleEventArgsMaker : IBattleEventArgsMaker
    {
        private readonly IQueueAccessor queueAccessor;
        private readonly IBattleFieldAccessor fieldAccessor;
        private readonly IBattleLogger logger;
        private readonly Random random;

        public BasicBattleEventArgsMaker(IQueueAccessor queueAccessor, IBattleLogger logger, Random random, IBattleFieldAccessor fieldAccessor = null)
        {
            this.queueAccessor = queueAccessor;
            this.fieldAccessor = fieldAccessor;
            this.logger = logger;
            this.random = random;
        }

        public virtual BattleEventArgs<T> FromData<T>(T data, IBattleEntity self = null, IBattleEntity target = null)
        {
            return new BattleEventArgs<T>(queueAccessor.GetQueue(), logger, random, self, target, data, fieldAccessor?.GetField());
        }
    }
}
