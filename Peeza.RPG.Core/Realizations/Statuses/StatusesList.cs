﻿using Peeza.RPG.Core.Basic.Statuses;
using Peeza.RPG.Core.Realizations.Generic;

namespace Peeza.RPG.Core.Realizations.Statuses
{
    public class StatusesList : KeyedEntityListAccessor<Status>, IStatusAccessor { }
}
