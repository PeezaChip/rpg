﻿using Peeza.RPG.Core.Basic.Ailments;
using Peeza.RPG.Core.Realizations.Generic;

namespace Peeza.RPG.Core.Realizations.Ailments
{
    public class AilmentList : KeyedEntityListAccessor<Ailment>, IAilmentsAccessor { }
}
