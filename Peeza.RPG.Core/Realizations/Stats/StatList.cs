﻿using Peeza.RPG.Core.Basic.Stats;
using Peeza.RPG.Core.Realizations.Generic;
using System.Numerics;

namespace Peeza.RPG.Core.Realizations.Stats
{
    public class StatList : KeyedEntityDictionaryAccessor<Stat>, IStatsAccessor
    {
        public Stat<T> GetByKey<T>(string key) where T : INumber<T>
        {
            var stat = GetByKey(key);
            if (stat != null && stat is Stat<T> typedStat) return typedStat;
            return null;
        }
    }
}
