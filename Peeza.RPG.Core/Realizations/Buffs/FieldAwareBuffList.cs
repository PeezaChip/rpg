﻿using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.BattleFields;
using Peeza.RPG.Core.Basic.Buffs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Peeza.RPG.Core.Realizations.Buffs
{
    public class FieldAwareBuffList : BuffList
    {
        protected IBattleSide battleSide;
        protected IBattleFieldAccessor fieldAccessor;

        public FieldAwareBuffList(IBattleSide battleSide, IBattleFieldAccessor fieldAccessor)
        {
            this.battleSide = battleSide;
            this.fieldAccessor = fieldAccessor;
        }

        public override IEnumerable<Buff> GetAll()
        {
            if (fieldAccessor == null || (!fieldAccessor.GetField()?.SupportsAuras ?? true)) return base.GetAll();

            return entities.Concat(GetFilteredBuffs()).ToList();
        }

        public override IEnumerable<Buff> GetAll(Func<Buff, bool> predicate)
        {
            if (fieldAccessor == null || (!fieldAccessor.GetField()?.SupportsAuras ?? true)) return base.GetAll(predicate);

            return entities.Concat(GetFilteredBuffs().Where(predicate)).ToList();
        }

        public override IEnumerable<Buff> GetAll(string key)
        {
            if (fieldAccessor == null || (!fieldAccessor.GetField()?.SupportsAuras ?? true)) return base.GetAll(key);

            return entities.Concat(GetFilteredBuffs().Where(buff => buff.Key == key)).ToList();
        }

        protected IEnumerable<Buff> GetFilteredBuffs()
        {
            if (fieldAccessor == null || (!fieldAccessor.GetField()?.SupportsAuras ?? true)) return [];

            return fieldAccessor.GetField().Auras
                .Where(aura => !aura.Filter.Any() || (battleSide != null
                    && (
                        (!aura.Flags.HasFlag(Basic.Auras.AuraFlags.FilterBlacklistMode) && aura.Filter.Contains(battleSide))
                        || (aura.Flags.HasFlag(Basic.Auras.AuraFlags.FilterBlacklistMode) && !aura.Filter.Contains(battleSide))
                    )
                ))
                .SelectMany(aura => aura.Buffs);
        }
    }
}
