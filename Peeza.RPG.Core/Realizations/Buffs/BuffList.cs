﻿using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Realizations.Generic;

namespace Peeza.RPG.Core.Realizations.Buffs
{
    public class BuffList : KeyedEntityListAccessor<Buff>, IBuffsAccessor { }
}
