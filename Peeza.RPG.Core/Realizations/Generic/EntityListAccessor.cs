﻿using Peeza.RPG.Core.Basic.Generic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Peeza.RPG.Core.Realizations.Generic
{
    public class EntityListAccessor<T> : IAccessor<T>
    {
        protected readonly List<T> entities;

        public EntityListAccessor()
        {
            entities = [];
        }

        public EntityListAccessor(params T[] values)
        {
            entities = [.. values];
        }

        public virtual void Add(T entity) => entities.Add(entity);

        public virtual void Add(params T[] entities) => this.entities.AddRange(entities);

        public virtual void Remove(T entity) => entities.Remove(entity);

        public virtual void Clear() => entities.Clear();

        public virtual IEnumerable<T> GetAll() => entities.ToList();

        public virtual IEnumerable<T> GetAll(Func<T, bool> predicate) => entities.Where(predicate).ToList();

        public IEnumerator<T> GetEnumerator() => GetAll().GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
