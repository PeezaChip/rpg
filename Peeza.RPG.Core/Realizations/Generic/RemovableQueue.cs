﻿using System.Collections.Generic;
using System.Linq;

namespace Peeza.RPG.Core.Realizations.Generic
{
    public class RemovableQueue<T> : Queue<T>
    {
        public RemovableQueue() : base() { }
        public RemovableQueue(IEnumerable<T> collection) : base(collection) { }
        public RemovableQueue(int capacity) : base(capacity) { }

        public void EnqueueRange(IEnumerable<T> itemsToEnqueue) => EnqueueRange(itemsToEnqueue.ToArray());
        public void EnqueueRange(params T[] itemsToEnqueue)
        {
            foreach(var item in itemsToEnqueue)
            {
                Enqueue(item);
            }
        }

        public void Remove(T item) => RemoveRange(item);
        public void RemoveRange(IEnumerable<T> itemsToRemove) => RemoveRange(itemsToRemove.ToArray());
        public void RemoveRange(params T[] itemsToRemove)
        {
            if (itemsToRemove == null || !itemsToRemove.Any()) return;

            var copy = new List<T>(this.Except(itemsToRemove));

            Clear();

            foreach (var item in copy)
            {
                Enqueue(item);
            }
        }
    }
}
