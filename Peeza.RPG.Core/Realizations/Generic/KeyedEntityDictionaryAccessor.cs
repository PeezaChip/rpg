﻿using Peeza.RPG.Core.Basic.Generic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Peeza.RPG.Core.Realizations.Generic
{
    public class KeyedEntityDictionaryAccessor<T> : IUniqueKeyedAccessor<T> where T : IRPGKey
    {
        private readonly Dictionary<string, T> entities;

        public KeyedEntityDictionaryAccessor()
        {
            entities = new Dictionary<string, T>();
        }

        public void Add(T entity)
        {
            if (entities.ContainsKey(entity.Key))
            {
                entities[entity.Key] = entity;
            }
            else
            {
                entities.Add(entity.Key, entity);
            }
        }

        public void Add(params T[] entities)
        {
            foreach (var item in entities)
            {
                Add(item);
            }
        }

        public void Clear()
        {
            entities.Clear();
        }

        public IEnumerable<T> GetAll()
        {
            return entities.Values;
        }

        public IEnumerable<T> GetAll(Func<T, bool> predicate)
        {
            return entities.Values.Where(predicate);
        }

        public T GetByKey(string key)
        {
            if (!entities.TryGetValue(key, out T entity)) return default;
            return entity;
        }

        public void Remove(T entity) => RemoveByKey(entity.Key);

        public void RemoveByKey(string key)
        {
            entities.Remove(key);
        }

        public IEnumerator<T> GetEnumerator() => GetAll().GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
