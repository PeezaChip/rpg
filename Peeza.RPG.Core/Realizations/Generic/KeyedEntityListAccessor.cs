﻿using Peeza.RPG.Core.Basic.Generic;
using System.Collections.Generic;

namespace Peeza.RPG.Core.Realizations.Generic
{
    public class KeyedEntityListAccessor<T> : EntityListAccessor<T>, IKeyedAccessor<T> where T : IRPGKey
    {
        public virtual IEnumerable<T> GetAll(string key) => GetAll(e => e.Key == key);
    }
}
