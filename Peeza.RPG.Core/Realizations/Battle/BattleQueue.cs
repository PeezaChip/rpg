﻿using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Realizations.Generic;
using System.Collections.Generic;

namespace Peeza.RPG.Core.Realizations.Battle
{
    public class BattleQueue : RemovableQueue<IBattleQueable>
    {
        public BattleQueue() : base() { }
        public BattleQueue(IEnumerable<IBattleQueable> collection) : base(collection) { }
        public BattleQueue(int capacity) : base(capacity) { }
    }
}
