﻿using Peeza.RPG.Core.Basic.Battle;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Peeza.RPG.Core.Realizations.Battle
{
    public class BasicBattleLogger : IBattleLogger
    {
        private readonly List<LogIndent> indents = [];
        private readonly List<BattleLogMessage> logs = [];

        public LogIndent IndentLog()
        {
            var loader = new LogIndent((l) =>
            {
                indents.Remove(l);
            });
            indents.Add(loader);
            return loader;
        }

        public void Log(string log, BattleLogVerbosity level = BattleLogVerbosity.Info)
        {
            logs.Add(new BattleLogMessage(log, indents.Count, level));
        }

        public string GetBattleLog(BattleLogVerbosity level = BattleLogVerbosity.Info)
        {
            var sb = new StringBuilder();

            foreach (var log in logs)
            {
                if (log.Level > level) continue;

                if (string.IsNullOrWhiteSpace(log.Log))
                {
                    sb.AppendLine();
                    continue;
                }
                if (log.Indent > 0)
                {
                    sb.Append(new string(' ', log.Indent * 2));
                }
                sb.AppendLine(log.Log);
            }

            return sb.ToString();
        }
    }

    [DebuggerDisplay("{DebuggerDisplay,nq}")]
    public readonly struct BattleLogMessage(string log, int indent, BattleLogVerbosity level)
    {
        public readonly string Log = log;
        public readonly int Indent = indent;
        public readonly BattleLogVerbosity Level = level;

        private string DebuggerDisplay => $"{Level,-9}{new string(' ', 2)}{Log}";
    }
}
