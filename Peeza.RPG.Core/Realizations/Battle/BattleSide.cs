﻿using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.BattleEntities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Peeza.RPG.Core.Realizations.Battle
{
    public class BattleSide : IBattleSide, ICollection<IBattleEntity>, IEnumerable<IBattleEntity>, IEnumerable, IList<IBattleEntity>, IReadOnlyCollection<IBattleEntity>, IReadOnlyList<IBattleEntity>, ICollection, IList
    {
        public List<IBattleEntity> BattleEntities { get; }

        public BattleSide()
        {
            BattleEntities = [];
        }

        public BattleSide(IEnumerable<IBattleEntity> battleEntities)
        {
            BattleEntities = battleEntities.ToList();
        }

        #region ICollection
        public int Count => BattleEntities.Count;
        public bool IsReadOnly => ((ICollection<IBattleEntity>)BattleEntities).IsReadOnly;
        public bool IsSynchronized => ((ICollection)BattleEntities).IsSynchronized;
        public object SyncRoot => ((ICollection)BattleEntities).SyncRoot;
        public void Clear() => BattleEntities.Clear();
        public void Add(IBattleEntity item) => BattleEntities.Add(item);
        public bool Contains(IBattleEntity item) => BattleEntities.Contains(item);
        public void CopyTo(IBattleEntity[] array, int arrayIndex) => BattleEntities.CopyTo(array, arrayIndex);
        public void CopyTo(Array array, int index) => ((ICollection)BattleEntities).CopyTo(array, index);
        public bool Remove(IBattleEntity item) => BattleEntities.Remove(item);
        public IEnumerator<IBattleEntity> GetEnumerator() => BattleEntities.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => BattleEntities.GetEnumerator();
        #endregion
        #region IList
        public IBattleEntity this[int index] { get => BattleEntities[index]; set => BattleEntities[index] = value; }
        public int IndexOf(IBattleEntity item) => BattleEntities.IndexOf(item);
        public void Insert(int index, IBattleEntity item) => BattleEntities.Insert(index, item);
        public void RemoveAt(int index) => BattleEntities.RemoveAt(index);
        public bool IsFixedSize => ((IList)BattleEntities).IsFixedSize;
        object IList.this[int index] { get => BattleEntities[index]; set => ((IList)BattleEntities)[index] = value; }
        public int Add(object value) => ((IList)BattleEntities).Add(value);
        public bool Contains(object value) => ((IList)BattleEntities).Contains(value);
        public int IndexOf(object value) => ((IList)BattleEntities).IndexOf(value);
        public void Insert(int index, object value) => ((IList)BattleEntities).Insert(index, value);
        public void Remove(object value) => ((IList)BattleEntities).Remove(value);
        #endregion
    }
}
