﻿using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Realizations.Generic;

namespace Peeza.RPG.Core.Realizations.Battle
{
    public class BasicBattleSidesAccessor : EntityListAccessor<IBattleSide>, IBattleSidesAccessor
    {
        public BasicBattleSidesAccessor()
        {
        }

        public BasicBattleSidesAccessor(params IBattleSide[] values) : base(values)
        {
        }
    }
}
