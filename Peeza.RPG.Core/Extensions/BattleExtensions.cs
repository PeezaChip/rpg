﻿using Peeza.RPG.Core.Battle.Events;

namespace Peeza.RPG.Core.Extensions
{
    public static class BattleExtensions
    {
        public static BattleEventArgs<T> Clone<T>(this BattleEventArgs<T> args, bool reverse = false, bool includeData = false)
        {
            return new BattleEventArgs<T>(args, reverse, includeData);
        }
    }
}
