﻿using System;
using System.Text;

namespace Peeza.RPG.Core.Extensions
{
    public static class StringEnumExtensions
    {
        public static string ToText(this Enum value)
        {
            return InsertSpaces(value.ToString());
        }

        private static string InsertSpaces(string input)
        {
            var result = new StringBuilder();
            for (var i = 0; i < input.Length; i++)
            {
                var c = input[i];
                if (char.IsUpper(c) && i != 0) result.Append(' ');
                result.Append(c);
            }
            return result.ToString();
        }
    }
}
