﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Peeza.RPG.Core.Extensions
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<Tuple<T, T>> GetAllPairs<T>(this IEnumerable<T> items)
        {
            var result = new List<Tuple<T, T>>();
            var list = items.ToList();

            for (var i = 0; i < list.Count; i++)
            {
                for (var j = i + 1; j < list.Count; j++)
                {
                    result.Add(new Tuple<T, T>(list[i], list[j]));
                }
            }

            return result;
        }
    }
}
