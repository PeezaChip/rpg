﻿using System;

namespace Peeza.RPG.Core.Extensions
{
    public static class RandomExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rng"></param>
        /// <param name="percent">% to hit positive outcome</param>
        /// <returns>true if roll outcome is positive</returns>
        public static bool Roll(this Random rng, double percent)
        {
            return rng.NextDouble() < percent;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rng"></param>
        /// <param name="percent">% to hit positive outcome</param>
        /// <param name="pseudoPercent">% that is added to base %</param>
        /// <returns>true if roll outcome is positive</returns>
        public static bool PseudoRoll(this Random rng, double percent, ref double pseudoPercent)
        {
            var sucess = rng.Roll(percent + pseudoPercent);
            if (sucess)
            {
                pseudoPercent = 0;
            }
            else
            {
                pseudoPercent += percent;
            }
            return sucess;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rng"></param>
        /// <param name="value">value</param>
        /// <param name="deviation">value change %</param>
        /// <returns>returns value +- (value*deviation)</returns>
        public static double Deviation(this Random rng, double value, double deviation)
        {
            var edge = value * deviation;
            return value + (rng.NextDouble() * edge * 2 - edge);
        }
    }
}
