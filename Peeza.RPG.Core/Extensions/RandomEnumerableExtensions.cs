﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Peeza.RPG.Core.Extensions
{
    public static class RandomEnumerableExtensions
    {
        public static T RandomByWeight<T>(this IEnumerable<T> sequence, Func<T, double> weightSelector, Random random)
        {
            var totalWeight = sequence.Sum(weightSelector);
            var itemWeightIndex = random.NextDouble() * totalWeight;
            double currentWeightIndex = 0;

            foreach (var item in sequence)
            {
                currentWeightIndex += weightSelector(item);
                if (currentWeightIndex >= itemWeightIndex) return item;
            }

            // that shouldn't happen
            return default;
        }

        public static IEnumerable<T> OrderRandomly<T>(this IEnumerable<T> sequence, Random random)
        {
            var copy = sequence.ToList();

            while (copy.Count > 0)
            {
                var index = random.Next(copy.Count);
                yield return copy[index];
                copy.RemoveAt(index);
            }
        }

        public static T TakeRandom<T>(this IEnumerable<T> sequence, Random random)
        {
            return OrderRandomly(sequence, random).FirstOrDefault();
        }

        public static IEnumerable<T> TakeRandom<T>(this IEnumerable<T> sequence, int itemsToTake, Random random)
        {
            return OrderRandomly(sequence, random).Take(itemsToTake);
        }
    }
}
