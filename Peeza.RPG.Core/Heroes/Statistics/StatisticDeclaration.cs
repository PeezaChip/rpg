﻿using Peeza.RPG.Core.Basic.Generic;

namespace Peeza.RPG.Core.Heroes.Statistics
{
    public abstract class StatisticDeclaration : IRPGKey
    {
        public abstract string Key { get; }
        public abstract StatisticAbilityFactory CreateStatisticAbilityFactory();
        /// <summary>
        /// Should return true if <see cref="CreateStatisticAbilityFactory"/> is not null
        /// </summary>
        /// <returns></returns>
        public abstract bool HasBattleEvent();
    }
}
