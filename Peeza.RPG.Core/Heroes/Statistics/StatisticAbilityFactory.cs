﻿using Peeza.RPG.Core.Basic.BattleEntities;

namespace Peeza.RPG.Core.Heroes.Statistics
{
    public abstract class StatisticAbilityFactory : IBattleEntityEventActionsProvider
    {
        public abstract void Hook(IBattleEntityEventHandlersProvider hookTo);
        public abstract void Unhook(IBattleEntityEventHandlersProvider hookTo);
    }
}
