﻿using Peeza.RPG.Core.Basic.Generic;

namespace Peeza.RPG.Core.Heroes.Specs
{
    public interface ISpecDeclarationAccessor : IUniqueKeyedAccessor<SpecDeclaration> { }
}
