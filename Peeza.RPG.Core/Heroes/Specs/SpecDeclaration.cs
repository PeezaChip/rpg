﻿using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Generic;
using Peeza.RPG.Core.Basic.Interfaces;
using Peeza.RPG.Core.Players;

namespace Peeza.RPG.Core.Heroes.Specs
{
    public abstract class SpecDeclaration : IRPGKey
    {
        public abstract string Key { get; }
        public abstract string AbilityName { get; }
        public abstract string Description { get; }
        public abstract string ShortDescription { get; }
        public abstract string Icon { get; }

        public abstract bool IsAvailable(PlayerAccount playerAccount, Hero hero);

        public abstract IBattleEntityEventActionsProvider CreateAbilityFactory(Hero hero, IBuffsAccessor buffs);
        public abstract IStatsFactory CreateStatsFactory(Hero hero, IBuffsAccessor buffs);
        public abstract IResourcesFactory CreateResourcesFactory(Hero hero, IBuffsAccessor buffs);
        public abstract ISpellsFactory CreateSpellsFactory(Hero hero, IBuffsAccessor buffs);
    }
}
