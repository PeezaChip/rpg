﻿using Peeza.RPG.Core.Basic.Generic;

namespace Peeza.RPG.Core.Heroes.Races
{
    public interface IRaceDeclarationAccessor : IUniqueKeyedAccessor<RaceDeclaration> { }
}
