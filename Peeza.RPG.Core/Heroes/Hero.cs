﻿using Peeza.RPG.Core.Heroes.Items.Enums;
using Peeza.RPG.Core.Heroes.Quests;
using System.Collections.Generic;

namespace Peeza.RPG.Core.Heroes
{
    public class Hero
    {
        public Dictionary<string, QuestStatus> QuestData = new();
        public Dictionary<string, long> Statistics = new();
        public Dictionary<ItemSlotEnum, long> Equipment = new();

        public string Name { get; set; }

        public ulong Experience { get; set; }
        public uint Level { get; set; } = 1;
        public string Avatar { get; set; }

        public string Race { get; set; }
        public string Spec { get; set; }

        public override string ToString()
        {
            return $"{Name} - {Race} {Spec} {Level}lvl";
        }
    }
}
