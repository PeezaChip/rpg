﻿using Peeza.RPG.Core.Heroes.Items;
using System.Collections.Generic;

namespace Peeza.RPG.Core.Heroes.Quests
{
    public class QuestReward
    {
        public ulong Exp { get; private set; }
        public long Points { get; private set; }
        public IEnumerable<Item> Items { get; private set; }

        public QuestReward()
        {
            Items = new List<Item>();
        }

        public QuestReward(ulong exp, long points, IEnumerable<Item> items)
        {
            Exp = exp;
            Points = points;
            Items = items;
        }
    }
}
