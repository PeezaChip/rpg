﻿using System;

namespace Peeza.RPG.Core.Heroes.Quests
{
    public class QuestStatus
    {
        public int Counter { get; set; }
        public bool GotRewards { get; set; }
        public DateTimeOffset? CompletedAt { get; set; }
    }
}
