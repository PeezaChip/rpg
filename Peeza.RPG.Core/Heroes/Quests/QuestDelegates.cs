﻿using Peeza.RPG.Core.Players;

namespace Peeza.RPG.Core.Heroes.Quests
{
    public delegate bool GetQuestCondition(IQuestsAccessor questsAccessor, PlayerAccount player, Hero hero);
    public delegate string GetQuestDescription(IQuestsAccessor questsAccessor, PlayerAccount player, Hero hero);
    public delegate QuestReward GetQuestReward(PlayerAccount player, Hero hero);
}
