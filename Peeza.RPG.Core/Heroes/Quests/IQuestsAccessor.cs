﻿using Peeza.RPG.Core.Basic.Generic;

namespace Peeza.RPG.Core.Heroes.Quests
{
    public interface IQuestsAccessor : IUniqueKeyedAccessor<Quest> { }
}
