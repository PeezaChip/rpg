﻿using Peeza.RPG.Core.Basic.Generic;
using Peeza.RPG.Core.Players;
using System.Collections.Generic;

namespace Peeza.RPG.Core.Heroes.Quests
{
    public abstract class Quest : IRPGKey
    {
        public abstract string Key { get; }

        public abstract GetQuestDescription Description { get; }
        public abstract GetQuestDescription ProgressDescription { get; }

        public virtual List<string> PrerequisiteQuests { get; } = new List<string>();

        public abstract GetQuestDescription RewardDescription { get; }
        public abstract GetQuestReward Reward { get; }

        public abstract GetQuestCondition IsComplete { get; }
        public abstract GetQuestCondition IsHidden { get; }
        public abstract GetQuestCondition IsActive { get; }

        public abstract bool IsAccountWide { get; }

        public virtual QuestStatus GetStatus(PlayerAccount account, Hero hero)
        {
            var questData = IsAccountWide ? account.QuestData : hero.QuestData;
            if (!questData.TryGetValue(Key, out var status)) return CreateData(questData);
            return status;
        }

        private QuestStatus CreateData(Dictionary<string, QuestStatus> questData)
        {
            var status = new QuestStatus();
            if (!questData.ContainsKey(Key)) questData.Add(Key, status);
            return status;
        }
    }
}
