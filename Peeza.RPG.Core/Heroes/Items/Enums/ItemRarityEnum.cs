﻿namespace Peeza.RPG.Core.Heroes.Items.Enums
{
    public enum ItemRarityEnum
    {
        Common = 0,
        Uncommon = 1,
        Rare = 2,
        Unique = 4,
        Legendary = 8,
    }
}
