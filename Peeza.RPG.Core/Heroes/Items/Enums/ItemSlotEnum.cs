﻿namespace Peeza.RPG.Core.Heroes.Items.Enums
{
    public enum ItemSlotEnum
    {
        None = 0,

        // jewelry
        LeftRing = 10,
        RightRing = 11,
        Amulet = 20,
        Trinket = 21,

        // armor
        Head = 30,
        Hands = 31,
        Body = 32,
        Legs = 33,

        // other
        Cape = 40,
        Belt = 41,

        // weapons
        LeftHand = 50,
        RightHand = 51,
    }
}
