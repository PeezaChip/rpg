﻿namespace Peeza.RPG.Core.Heroes.Items.Enums
{
    public enum ItemTypeEnum
    {
        None = 0,

        // jewelry
        Ring = 10,
        Amulet = 11,
        Trinket = 12,

        // armor
        Helmet = 20,
        Hands = 21,
        Body = 22,
        Legs = 23,

        // other
        Cape = 30,
        Belt = 31,
        Shield = 32,

        // weapons
        Dagger = 100,
        Sword = 101,
        TwoHandedSword = 102,
        Lance = 103,
        Bow = 104,
        Crossbow = 105,
        Scythe = 106,
        Wand = 107,
        Scepter = 108,
        Staff = 109
    }
}
