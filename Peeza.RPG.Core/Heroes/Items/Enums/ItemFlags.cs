﻿using System;

namespace Peeza.RPG.Core.Heroes.Items.Enums
{
    [Flags]
    public enum ItemFlags
    {
        None = 0,
        Legacy = 1,
        Antique = 2,
    }
}
