﻿using Peeza.RPG.Core.Extensions;
using Peeza.RPG.Core.Heroes.Items.Enums;
using Peeza.RPG.Core.Realizations.Stats;
using System.Text;

namespace Peeza.RPG.Core.Heroes.Items
{
    public class Item
    {
        public ItemTypeEnum ItemType { get; private set; }
        public ItemRarityEnum Rarity { get; private set; }
        public ItemFlags Flags { get; private set; }

        public StatList Stats { get; private set; }
        public StatList ItemStats { get; private set; }
        public string ItemAbilityDeclarationKey { get; private set; }

        public string Prefix { get; private set; }
        public string Postfix { get; private set; }
        public string Sigil { get; private set; }

        public Item() { }

        public Item(ItemTypeEnum itemType, ItemRarityEnum rarity, ItemFlags flags, StatList stats, StatList itemStats, string itemAbilityDeclarationKey, string prefix, string postfix, string sigil)
        {
            ItemType = itemType;
            Rarity = rarity;
            Flags = flags;

            Stats = stats ?? [];
            ItemStats = itemStats ?? [];
            ItemAbilityDeclarationKey = itemAbilityDeclarationKey;

            Prefix = prefix;
            Postfix = postfix;
            Sigil = sigil;
        }

        public string GetItemName()
        {
            var sb = new StringBuilder();

            if (!string.IsNullOrEmpty(Prefix))
            {
                sb.Append(Prefix);
                sb.Append(' ');
            }

            sb.Append(ItemType.ToText());

            if (!string.IsNullOrEmpty(Postfix))
            {
                sb.Append(" of ");
                sb.Append(Postfix);
            }

            if (!string.IsNullOrEmpty(Sigil))
            {
                sb.Append(" with ");
                sb.Append(Sigil);
                sb.Append(" sigil");
            }

            return sb.ToString();
        }
    }
}
