﻿using Peeza.RPG.Core.Basic.Generic;
using System.Collections.Generic;

namespace Peeza.RPG.Core.Heroes.Items.Abstract
{
    public abstract class ItemAbilityDeclaration : IRPGKey
    {
        public string Key => this.GetType().Name;

        public abstract string GetAbilityDescription(Item item);

        public abstract ItemAbilityFactory CreateItemAbilityFactory(IEnumerable<Item> items);
        public virtual ItemAbilityFactory CreateItemAbilityFactory(Item item) => CreateItemAbilityFactory(new List<Item>() { item });
    }
}
