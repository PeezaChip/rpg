﻿using Peeza.RPG.Core.Basic.BattleEntities;
using System.Collections.Generic;

namespace Peeza.RPG.Core.Heroes.Items.Abstract
{
    public abstract class ItemAbilityFactory : IBattleEntityEventActionsProvider
    {
        protected IEnumerable<Item> items;

        protected ItemAbilityFactory(IEnumerable<Item> items)
        {
            this.items = items;
        }

        public abstract void Hook(IBattleEntityEventHandlersProvider hookTo);
        public abstract void Unhook(IBattleEntityEventHandlersProvider hookTo);
    }
}
