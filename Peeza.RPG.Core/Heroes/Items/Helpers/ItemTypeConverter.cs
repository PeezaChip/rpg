﻿using Peeza.RPG.Core.Heroes.Items.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Peeza.RPG.Core.Heroes.Items.Helpers
{
    public static class ItemTypeConverter
    {
        private static readonly List<ItemSlotEnum> invalidSlot = [ItemSlotEnum.None];
        private static readonly Dictionary<ItemTypeEnum, List<ItemSlotEnum>> validSlots = new()
        {
            { ItemTypeEnum.Ring, [ ItemSlotEnum.LeftRing, ItemSlotEnum.RightRing ] },
            { ItemTypeEnum.Amulet, [ ItemSlotEnum.Amulet ] },
            { ItemTypeEnum.Trinket, [ ItemSlotEnum.Trinket ] },

            { ItemTypeEnum.Helmet, [ ItemSlotEnum.Head ] },
            { ItemTypeEnum.Hands, [ ItemSlotEnum.Hands ] },
            { ItemTypeEnum.Body, [ ItemSlotEnum.Body ] },
            { ItemTypeEnum.Legs, [ ItemSlotEnum.Legs ] },

            { ItemTypeEnum.Cape, [ ItemSlotEnum.Cape ] },
            { ItemTypeEnum.Belt, [ ItemSlotEnum.Belt ] },
            { ItemTypeEnum.Shield, [ItemSlotEnum.RightHand] },

            { ItemTypeEnum.Dagger, [ ItemSlotEnum.LeftHand, ItemSlotEnum.RightHand ] },
            { ItemTypeEnum.Sword, [ ItemSlotEnum.LeftHand, ItemSlotEnum.RightHand ] },
            { ItemTypeEnum.TwoHandedSword, [ ItemSlotEnum.LeftHand ] },
            { ItemTypeEnum.Lance, [ ItemSlotEnum.LeftHand ] },
            { ItemTypeEnum.Bow, [ ItemSlotEnum.LeftHand ] },
            { ItemTypeEnum.Crossbow, [ ItemSlotEnum.LeftHand ] },
            { ItemTypeEnum.Scythe, [ ItemSlotEnum.LeftHand ] },
            { ItemTypeEnum.Wand, [ ItemSlotEnum.LeftHand, ItemSlotEnum.RightHand ] },
            { ItemTypeEnum.Scepter, [ ItemSlotEnum.LeftHand, ItemSlotEnum.RightHand ] },
            { ItemTypeEnum.Staff, [ ItemSlotEnum.LeftHand ] },
        };

        public static IEnumerable<ItemSlotEnum> GetValidSlots(ItemTypeEnum itemType)
        {
            if (validSlots.TryGetValue(itemType, out List<ItemSlotEnum> value)) return value;
            return invalidSlot;
        }

        public static bool IsTwoHanded(ItemTypeEnum itemType)
        {
            var validSlots = GetValidSlots(itemType);
            return validSlots.Count() == 1 && validSlots.First() == ItemSlotEnum.LeftHand;
        }
    }
}
