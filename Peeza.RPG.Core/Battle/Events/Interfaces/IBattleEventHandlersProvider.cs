﻿namespace Peeza.RPG.Core.Battle.Events.Interfaces
{
    public interface IBattleEventHandlersProvider
    {
        void Add(BattleEventDeclaration eventDeclaration);
        void Add<T>(BattleEventDeclaration<T> eventDeclaration);
        void Add(string key);
        void Add<T>(string key);

        BattleEvent Get(BattleEventDeclaration eventDeclaration);
        BattleEvent<T> Get<T>(BattleEventDeclaration<T> eventDeclaration);
        BattleEvent Get(string key);
        BattleEvent<T> Get<T>(string key);

        void HookEvent(BattleEventDeclaration eventDeclaration, BattleEventAction action, int? priority = null);
        void HookEvent<T>(BattleEventDeclaration<T> eventDeclaration, BattleEventAction<T> action, int? priority = null);
        void HookEvent(string key, BattleEventAction action, int? priority = null);
        void HookEvent<T>(string key, BattleEventAction<T> action, int? priority = null);

        void UnhookEvent(BattleEventDeclaration eventDeclaration, BattleEventAction action);
        void UnhookEvent<T>(BattleEventDeclaration<T> eventDeclaration, BattleEventAction<T> action);
        void UnhookEvent(string key, BattleEventAction action);
        void UnhookEvent<T>(string key, BattleEventAction<T> action);

        void RaiseEvent(BattleEventDeclaration eventDeclaration, BattleEventArgs args);
        void RaiseEvent<T>(BattleEventDeclaration<T> eventDeclaration, BattleEventArgs<T> args);
        void RaiseEvent(string key, BattleEventArgs args);
        void RaiseEvent<T>(string key, BattleEventArgs<T> args);
    }
}
