﻿namespace Peeza.RPG.Core.Battle.Events.Interfaces
{
    public interface IBattleEventActionsProvider
    {
        void Hook(IBattleEventHandlersProvider hookTo);
        void Unhook(IBattleEventHandlersProvider hookTo);
    }
}
