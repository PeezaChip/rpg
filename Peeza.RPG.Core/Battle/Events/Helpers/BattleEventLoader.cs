﻿using Peeza.RPG.Core.Battle.Events.Attributes;
using Peeza.RPG.Core.Battle.Events.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Peeza.RPG.Core.Battle.Events.Helpers
{
    public static class BattleEventLoader
    {
        public static void LoadEventActionsFromType(Type typeToLoadFrom, IBattleEventHandlersProvider eventHandlersProvider)
        {
            var eventActionsToLoad = GetActions(typeToLoadFrom);

            foreach (var group in eventActionsToLoad)
            {
                var eventDeclaration = (BattleEventDeclaration)Activator.CreateInstance(group.Key);
                foreach (var eventAction in group)
                {
                    eventDeclaration.Hook(eventHandlersProvider, eventAction);
                }
            }
        }

        public static void UnloadEventActionsFromType(Type typeToUnloadFrom, IBattleEventHandlersProvider eventHandlersProvider)
        {
            var eventActionsToUnload = GetActions(typeToUnloadFrom);

            foreach (var group in eventActionsToUnload)
            {
                var eventDeclaration = (BattleEventDeclaration)Activator.CreateInstance(group.Key);
                foreach (var eventAction in group)
                {
                    eventDeclaration.Unhook(eventHandlersProvider, eventAction);
                }
            }
        }

        public static void LoadEventHandlersFromActionProvider(IBattleEventActionsProvider loadFrom, IBattleEventHandlersProvider eventHandlersProvider)
        {
            loadFrom.Hook(eventHandlersProvider);
        }

        public static void UnloadEventHandlersFromActionProvider(IBattleEventActionsProvider loadFrom, IBattleEventHandlersProvider eventHandlersProvider)
        {
            loadFrom.Unhook(eventHandlersProvider);
        }

        private static IEnumerable<IGrouping<Type, MethodInfo>> GetActions(Type type)
        {
            return type
                .GetMethods(BindingFlags.Public | BindingFlags.Static)
                .Where(m => m.GetCustomAttribute<BattleEventActionAttribute>() != null)
                .GroupBy(m => m.GetCustomAttribute<BattleEventActionAttribute>().EventDeclarationType);
        }
    }
}
