﻿using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.BattleFields;
using Peeza.RPG.Core.Realizations.Battle;
using System;

namespace Peeza.RPG.Core.Battle.Events
{
    public class BattleEventArgs : BattleEventArgs<object>
    {
        public BattleEventArgs() : base() { }

        public BattleEventArgs(BattleQueue battleQueue, IBattleLogger logger, Random random, IBattleEntity self, IBattleEntity target, IBattleField field) : base(battleQueue, logger, random, self, target, field) { }
        public BattleEventArgs(BattleQueue battleQueue, IBattleLogger logger, Random random, IBattleEntity self, IBattleEntity target, object data, IBattleField field) : base(battleQueue, logger, random, self, target, data, field) { }

        public static BattleEventArgs CloneFrom<T>(BattleEventArgs<T> args, bool reverse = false, bool includeData = false)
        {
            return new BattleEventArgs(args.BattleQueue, args.Logger, args.Random, reverse ? args.Target : args.Self, reverse ? args.Self : args.Target, includeData ? args.Data : null, args.Field);
        }
    }

    public class BattleEventArgs<T>
    {
        public BattleQueue BattleQueue { get; }

        public IBattleLogger Logger { get; private set; }
        public Random Random { get; }

        public IBattleEntity Self { get; }
        public IBattleEntity Target { get; }

        public IBattleField Field { get; }

        public T Data { get; set; }

        public void Log(string log, BattleLogVerbosity level = BattleLogVerbosity.Info) => Logger?.Log(log, level);

        public BattleEventArgs() { }

        public BattleEventArgs(BattleQueue battleQueue, IBattleLogger logger, Random random, IBattleEntity self, IBattleEntity target, IBattleField field)
        {
            BattleQueue = battleQueue;
            Logger = logger;
            Random = random;
            Self = self;
            Target = target;
            Field = field;
        }

        public BattleEventArgs(BattleQueue battleQueue, IBattleLogger logger, Random random, IBattleEntity self, IBattleEntity target, T data, IBattleField field) : this(battleQueue, logger, random, self, target, field)
        {
            Data = data;
        }

        public BattleEventArgs(BattleEventArgs<T> args, bool reverse = false, bool includeData = false)
        {
            Logger = args.Logger;
            Random = args.Random;
            BattleQueue = args.BattleQueue;
            Field = args.Field;

            if (reverse)
            {
                Self = args.Target;
                Target = args.Self;
            }
            else
            {
                Self = args.Self;
                Target = args.Target;
            }

            if (includeData)
            {
                Data = args.Data;
            }
        }

        public static BattleEventArgs<T> CloneFrom(BattleEventArgs<T> args, bool reverse = false, bool includeData = false)
        {
            return new BattleEventArgs<T>(args, reverse, includeData);
        }

        public static BattleEventArgs<T> CloneFrom<T2>(BattleEventArgs<T2> args, bool reverse = false, T data = default)
        {
            return new BattleEventArgs<T>(args.BattleQueue, args.Logger, args.Random, reverse ? args.Target : args.Self, reverse ? args.Self : args.Target, data, args.Field);
        }

        public static BattleEventArgs<T> CloneFrom<T2>(BattleEventArgs<T2> args, IBattleEntity self, IBattleEntity target, T data = default)
        {
            return new BattleEventArgs<T>(args.BattleQueue, args.Logger, args.Random, self, target, data, args.Field);
        }
    }
}
