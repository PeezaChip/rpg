﻿using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.BattleFields;
using System.Collections.Generic;

namespace Peeza.RPG.Core.Battle.Events
{
    public class DictionaryEventHandlerProvider : IBattleEntityEventHandlersProvider, IBattleFieldEventHandlersProvider
    {
        private readonly Dictionary<string, object> events;

        public DictionaryEventHandlerProvider()
        {
            events = [];
        }

        public void Add(BattleEventDeclaration eventDeclaration) => Add(eventDeclaration.Key);
        public void Add(string key)
        {
            events.Add(key, new BattleEvent());
        }

        public void Add<T>(BattleEventDeclaration<T> eventDeclaration) => Add<T>(eventDeclaration.Key);
        public void Add<T>(string key)
        {
            events.Add(key, new BattleEvent<T>());
        }

        public BattleEvent Get(BattleEventDeclaration eventDeclaration) => Get(eventDeclaration.Key);
        public BattleEvent Get(string key)
        {
            if (!events.ContainsKey(key))
            {
                Add(key);
            }
            return (BattleEvent)events[key];
        }

        public BattleEvent<T> Get<T>(BattleEventDeclaration<T> eventDeclaration) => Get<T>(eventDeclaration.Key);
        public BattleEvent<T> Get<T>(string key)
        {
            if (!events.ContainsKey(key))
            {
                Add<T>(key);
            }
            return (BattleEvent<T>)events[key];
        }

        public void HookEvent(BattleEventDeclaration eventDeclaration, BattleEventAction action, int? priority = null) => HookEvent(eventDeclaration.Key, action, priority);
        public void HookEvent(string key, BattleEventAction action, int? priority = null)
        {
            Get(key).Hook(action);
        }

        public void HookEvent<T>(BattleEventDeclaration<T> eventDeclaration, BattleEventAction<T> action, int? priority = null) => HookEvent(eventDeclaration.Key, action, priority);
        public void HookEvent<T>(string key, BattleEventAction<T> action, int? priority = null)
        {
            Get<T>(key).Hook(action);
        }

        public void UnhookEvent(BattleEventDeclaration eventDeclaration, BattleEventAction action) => UnhookEvent(eventDeclaration.Key, action);
        public void UnhookEvent(string key, BattleEventAction action)
        {
            Get(key).Unhook(action);
        }

        public void UnhookEvent<T>(BattleEventDeclaration<T> eventDeclaration, BattleEventAction<T> action) => UnhookEvent(eventDeclaration.Key, action);
        public void UnhookEvent<T>(string key, BattleEventAction<T> action)
        {
            Get<T>(key).Unhook(action);
        }


        public void RaiseEvent(BattleEventDeclaration eventDeclaration, BattleEventArgs args) => RaiseEvent(eventDeclaration.Key, args);
        public virtual void RaiseEvent(string key, BattleEventArgs args)
        {
            Get(key).Raise(args);
        }

        public void RaiseEvent<T>(BattleEventDeclaration<T> eventDeclaration, BattleEventArgs<T> args) => RaiseEvent(eventDeclaration.Key, args);
        public virtual void RaiseEvent<T>(string key, BattleEventArgs<T> args)
        {
            Get<T>(key).Raise(args);
        }
    }
}
