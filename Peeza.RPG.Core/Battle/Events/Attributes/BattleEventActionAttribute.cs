﻿using System;

namespace Peeza.RPG.Core.Battle.Events.Attributes
{
    public class BattleEventFilterActionAttribute : BattleEventActionAttribute
    {
        /// <summary>
        /// Will check arguments and if key is not equals filer then action will not run
        /// </summary>
        public string Filter { get; private set; }

        public BattleEventFilterActionAttribute(string filter, Type eventDeclarationType)
            : base(eventDeclarationType)
        {
            Filter = filter;
        }
    }

    public class BattleEventActionAttribute : Attribute
    {
        public string Key => EventDeclarationType.Name;
        public Type EventDeclarationType { get; private set; }

        /// <summary>
        /// The higher is the priority the sooner the action will be invoked. Default is 100 (defined in <see cref="BattleEvent{T}.Raise(BattleEventArgs{T})"/>)
        /// </summary>
        public int Priority { get; set; }

        public BattleEventActionAttribute(Type eventDeclarationType)
        {
            EventDeclarationType = eventDeclarationType;
        }
    }
}
