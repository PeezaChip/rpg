﻿using Peeza.RPG.Core.Basic.Generic;
using Peeza.RPG.Core.Battle.Events.Interfaces;
using System;
using System.Linq;
using System.Reflection;

namespace Peeza.RPG.Core.Battle.Events
{
    public abstract class BattleEventDeclaration<T> : BattleEventDeclaration
    {
        public override void Hook(IBattleEventHandlersProvider battleEventHandlersProvider, MethodInfo action)
        {
            HookInternal(battleEventHandlersProvider, typeof(BattleEventAction<T>), action);
        }

        public override void Unhook(IBattleEventHandlersProvider battleEventHandlersProvider, MethodInfo action)
        {
            UnhookInternal(battleEventHandlersProvider, typeof(BattleEventAction<T>), action);
        }

        protected override MethodInfo GetHookMethod()
        {
            return GetMethod(nameof(IBattleEventHandlersProvider.HookEvent));
        }

        protected override MethodInfo GetUnhookMethod()
        {
            return GetMethod(nameof(IBattleEventHandlersProvider.UnhookEvent));
        }

        private static MethodInfo GetMethod(string name)
        {
            return typeof(IBattleEventHandlersProvider)
                .GetMethods()
                .Where(m => m.Name == name && m.IsGenericMethod && m.GetParameters().First().ParameterType == typeof(string))
                .First()
                .MakeGenericMethod(typeof(T));
        }
    }

    public abstract class BattleEventDeclaration : IRPGKey
    {
        public string Key { get; }

        public BattleEventDeclaration()
        {
            Key = GetType().Name;
        }

        public virtual void Hook(IBattleEventHandlersProvider battleEventHandlersProvider, MethodInfo action)
        {
            HookInternal(battleEventHandlersProvider, typeof(BattleEventAction), action);
        }

        protected void HookInternal(IBattleEventHandlersProvider battleEventHandlersProvider, Type delegateType, MethodInfo action)
        {
            var handler = Delegate.CreateDelegate(delegateType, action);
            GetHookMethod().Invoke(battleEventHandlersProvider, [Key, handler, null]);
        }

        public virtual void Unhook(IBattleEventHandlersProvider battleEventHandlersProvider, MethodInfo action)
        {
            UnhookInternal(battleEventHandlersProvider, typeof(BattleEventAction), action);
        }

        protected void UnhookInternal(IBattleEventHandlersProvider battleEventHandlersProvider, Type delegateType, MethodInfo action)
        {
            var handler = Delegate.CreateDelegate(delegateType, action);
            GetUnhookMethod().Invoke(battleEventHandlersProvider, [Key, handler]);
        }

        protected virtual MethodInfo GetHookMethod()
        {
            return GetMethod(nameof(IBattleEventHandlersProvider.HookEvent));
        }

        protected virtual MethodInfo GetUnhookMethod()
        {
            return GetMethod(nameof(IBattleEventHandlersProvider.UnhookEvent));
        }

        private static MethodInfo GetMethod(string name)
        {
            return typeof(IBattleEventHandlersProvider)
                .GetMethods()
                .Where(m => m.Name == name && !m.IsGenericMethod && m.GetParameters().First().ParameterType == typeof(string))
                .First();
        }
    }
}
