﻿namespace Peeza.RPG.Core.Battle.Events
{
    public delegate bool BattleEventAction<T>(BattleEventArgs<T> args);
    public delegate bool BattleEventAction(BattleEventArgs args);
}
