﻿using Peeza.RPG.Core.Basic.Generic;
using Peeza.RPG.Core.Battle.Events.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Peeza.RPG.Core.Battle.Events
{
    public class BattleEvent<T>
    {
        private readonly List<BattleEventAction<T>> handlers;
        private readonly Dictionary<BattleEventAction<T>, int> priorities;

        public BattleEvent()
        {
            handlers = [];
            priorities = [];
        }

        public void Hook(BattleEventAction<T> handler, int? priority = null)
        {
            handlers.Add(handler);
            if (priority.HasValue) priorities.Add(handler, priority.Value);
        }

        public void Unhook(BattleEventAction<T> handler)
        {
            handlers.Remove(handler);
            priorities.Remove(handler);
        }

        public void Raise(BattleEventArgs<T> args)
        {
            var queue = handlers
                .OrderBy(handler => GetAttribute(handler)?.Priority ?? (priorities.TryGetValue(handler, out int value) ? value : 100))
                .AsEnumerable().Reverse();

            foreach (var e in queue)
            {
                var attr = GetAttribute(e);
                if (attr is BattleEventFilterActionAttribute filter
                    && args.Data is IRPGKey key
                    && filter.Filter != key.Key) continue;

                var res = e(args);
                if (res)
                {
                    break;
                }
            }
        }

        protected virtual BattleEventActionAttribute GetAttribute(BattleEventAction<T> handler)
        {
            return GetAttribute(handler.Method);
        }

        protected BattleEventActionAttribute GetAttribute(MethodInfo methodInfo)
        {
            return methodInfo.GetCustomAttribute<BattleEventActionAttribute>();
        }
    }

    public class BattleEvent : BattleEvent<object>
    {
        private readonly Dictionary<BattleEventAction<object>, BattleEventAction> wrapperDict = [];

        public void Hook(BattleEventAction handler)
        {
            var wrapper = new BattleEventAction<object>((args) => handler((BattleEventArgs)args));
            wrapperDict.Add(wrapper, handler);
            Hook(wrapper);
        }

        public void Unhook(BattleEventAction handler)
        {
            var wrappers = wrapperDict.Where(kv => kv.Value == handler).Select(kv => kv.Key).ToList();
            foreach(var wrapper in wrappers)
            {
                Unhook(wrapper);
                wrapperDict.Remove(wrapper);
            }
        }

        protected override BattleEventActionAttribute GetAttribute(BattleEventAction<object> handler)
        {
            if (wrapperDict.TryGetValue(handler, out var ogHandler))
            {
                return GetAttribute(ogHandler.Method);
            }
            return null;
        }
    }
}
