﻿using Peeza.RPG.Core.Heroes;
using Peeza.RPG.Core.Heroes.Items;
using Peeza.RPG.Core.Heroes.Quests;
using System.Collections.Generic;

namespace Peeza.RPG.Core.Players
{
    public class PlayerAccount
    {
        public Dictionary<string, QuestStatus> QuestData = new();
        public Dictionary<string, long> Statistics = new();
        public Dictionary<long, Hero> Heroes = new();
        public Dictionary<long, Item> Inventory = new();
    }
}
