﻿using Peeza.RPG.Core.Basic.Generic;
using System.Numerics;

namespace Peeza.RPG.Core.Basic.Stats
{
    public class Stat<T> : Stat where T : INumber<T>
    {
        public virtual T BaseValue { get; protected set; }
        public virtual T Value => BaseValue < MinValue ? MinValue : BaseValue;

        protected virtual T MinValue { get; private set; }

        public Stat(string key, T value = default, T minValue = default) : base(key)
        {
            BaseValue = value;
            MinValue = minValue;
        }

        public static implicit operator T(Stat<T> b) => b.Value;
    }

    public abstract class Stat : IRPGKey
    {
        public string Key { get; private set; }

        public Stat(string key)
        {
            Key = key;
        }
    }
}
