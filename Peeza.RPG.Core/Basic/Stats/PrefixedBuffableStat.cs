﻿using Peeza.RPG.Core.Basic.Buffs;
using System.Collections.Generic;
using System.Numerics;

namespace Peeza.RPG.Core.Basic.Stats
{
    public class PrefixedBuffableStat<T> : BuffableStat<T> where T : INumber<T>
    {
        private readonly string prefix;

        public PrefixedBuffableStat(string key, string prefix, IBuffsAccessor buffs, T value = default, T minValue = default) : base(key, buffs, value, minValue)
        {
            this.prefix = prefix;
        }

        protected override IEnumerable<Buff> GetApplicableBuffs()
        {
            return buffs.GetAll(buff => buff is not ResourceBuff && (buff.Key == Key || buff.Key == $"{prefix}{Key}"));
        }
    }
}
