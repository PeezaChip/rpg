﻿using Peeza.RPG.Core.Basic.Buffs;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Peeza.RPG.Core.Basic.Stats
{
    public class BuffableStat<T> : Stat<T>, IModifiableStat<T> where T : INumber<T>
    {
        protected readonly IBuffsAccessor buffs;

        public override T Value
        {
            get
            {
                var value = ApplyBuffs();
                return value < MinValue ? MinValue : value;
            }
        }

        public BuffableStat(string key, IBuffsAccessor buffs, T value = default, T minValue = default) : base(key, value, minValue)
        {
            this.buffs = buffs;
        }

        public void Modify(T value)
        {
            BaseValue = value;
        }

        protected virtual IEnumerable<Buff> GetApplicableBuffs()
        {
            return buffs.GetAll(buff => buff is not ResourceBuff && (buff.Key == Key || buff.Key == "*"));
        }

        private T ApplyBuffs()
        {
            if (buffs == null) return BaseValue;
            var applicableBuffs = GetApplicableBuffs().OrderBy(buff => buff.Priority);
            if (!applicableBuffs.Any()) return BaseValue;

            var value = double.CreateChecked(BaseValue);

            foreach (var buff in applicableBuffs)
            {
                if (buff.Flags.HasFlag(BuffFlags.Multiplicative))
                {
                    value *= buff.DoubleValue;
                }
                else
                {
                    value += buff.DoubleValue;
                }
            }

            return T.CreateChecked(value);
        }

        public static implicit operator T(BuffableStat<T> b) => b.Value;
    }
}
