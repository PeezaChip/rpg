﻿using Peeza.RPG.Core.Basic.Generic;

namespace Peeza.RPG.Core.Basic.Stats
{
    public abstract class StatDeclaration<T> : IRPGKey
    {
        public string Key { get; }

        public T DefaultValue { get; } = default;

        public bool SupportsBuff { get; } = false;

        public StatDeclaration()
        {
            var typeName = GetType().Name;
            if (typeName.EndsWith("StatDeclaration"))
            {
                typeName = typeName[..^"StatDeclaration".Length];
            }
            Key = typeName;
        }

        public StatDeclaration(T defaultValue, bool supportsBuff) : this()
        {
            DefaultValue = defaultValue;
            SupportsBuff = supportsBuff;
        }
    }
}
