﻿using System.Numerics;

namespace Peeza.RPG.Core.Basic.Stats
{
    public interface IModifiableStat<T> where T : INumber<T>
    {
        void Modify(T value);
    }
}
