﻿using Peeza.RPG.Core.Basic.Generic;
using System.Numerics;

namespace Peeza.RPG.Core.Basic.Stats
{
    public interface IStatsAccessor : IUniqueKeyedAccessor<Stat>
    {
        Stat<T> GetByKey<T>(string key) where T : INumber<T>;
    }
}