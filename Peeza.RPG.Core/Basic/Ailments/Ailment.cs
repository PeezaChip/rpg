﻿using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Statuses;
using Peeza.RPG.Core.Realizations.Buffs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Peeza.RPG.Core.Basic.Ailments
{
    public class Ailment : DecayingStatus, IBuffsAccessor
    {
        public double Power { get; private set; }
        public BuffList Buffs { get; private set; }
        public AilmentFlags Flags { get; private set; }

        public Ailment(string key, IBattleEntity inflicter, double power, AilmentFlags flags = AilmentFlags.None, params Buff[] initialBuffs) : base(key, inflicter)
        {
            Buffs = new BuffList();

            Power = power;
            Flags = flags;

            if (initialBuffs.Any()) Buffs.Add(initialBuffs);
        }

        public IEnumerable<Buff> GetAll(string key) => Buffs.GetAll(key);
        public void Add(Buff entity) => Buffs.Add(entity);
        public void Add(params Buff[] entities) => Buffs.Add(entities);
        public void Remove(Buff entity) => Buffs.Remove(entity);
        public void Clear() => Buffs.Clear();
        public IEnumerable<Buff> GetAll() => Buffs.GetAll();
        public IEnumerable<Buff> GetAll(Func<Buff, bool> predicate) => Buffs.GetAll(predicate);
        public IEnumerator<Buff> GetEnumerator() => GetAll().GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
