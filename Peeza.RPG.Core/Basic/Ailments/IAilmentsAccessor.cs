﻿using Peeza.RPG.Core.Basic.Generic;

namespace Peeza.RPG.Core.Basic.Ailments
{
    public interface IAilmentsAccessor : IKeyedAccessor<Ailment> { }
}
