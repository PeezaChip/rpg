﻿using System;

namespace Peeza.RPG.Core.Basic.Statuses
{
    [Flags]
    public enum StatusFlags
    {
        None = 0,
        Silent = 1,
    }
}
