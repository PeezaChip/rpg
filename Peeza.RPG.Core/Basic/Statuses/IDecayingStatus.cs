﻿namespace Peeza.RPG.Core.Basic.Statuses
{
    public interface IDecayingStatus
    {
        int TurnsLeft { get; set; }
    }
}
