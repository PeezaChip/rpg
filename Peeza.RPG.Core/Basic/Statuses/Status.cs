﻿using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Generic;

namespace Peeza.RPG.Core.Basic.Statuses
{
    public class Status : IRPGKey
    {
        public string Key { get; private set; }

        public IBattleEntity Inflicter { get; private set; }

        public StatusFlags StatusFlags { get; private set; }

        public Status(string key, IBattleEntity inflicter, StatusFlags flags = StatusFlags.None)
        {
            Key = key;
            Inflicter = inflicter;
        }
    }
}
