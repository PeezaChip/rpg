﻿using Peeza.RPG.Core.Basic.BattleEntities;

namespace Peeza.RPG.Core.Basic.Statuses
{
    public class DecayingStatus : Status, IDecayingStatus
    {
        public int TurnsLeft { get; set; }

        public DecayingStatus(string key, IBattleEntity inflicter) : base(key, inflicter) { }
    }
}
