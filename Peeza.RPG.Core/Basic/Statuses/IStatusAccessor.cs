﻿using Peeza.RPG.Core.Basic.Generic;

namespace Peeza.RPG.Core.Basic.Statuses
{
    public interface IStatusAccessor : IKeyedAccessor<Status> { }
}
