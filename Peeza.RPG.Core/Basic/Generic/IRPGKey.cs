﻿namespace Peeza.RPG.Core.Basic.Generic
{
    public interface IRPGKey
    {
        string Key { get; }
    }
}
