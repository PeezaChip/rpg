﻿using System.Collections.Generic;

namespace Peeza.RPG.Core.Basic.Generic
{
    public interface IKeyedAccessor<T> : IAccessor<T> where T : IRPGKey
    {
        IEnumerable<T> GetAll(string key);
    }
}
