﻿namespace Peeza.RPG.Core.Basic.Generic
{
    public interface IUniqueKeyedAccessor<T> : IAccessor<T> where T : IRPGKey
    {
        T GetByKey(string key);
        void RemoveByKey(string key);
    }
}
