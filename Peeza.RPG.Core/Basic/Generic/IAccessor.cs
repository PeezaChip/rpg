﻿using System;
using System.Collections.Generic;

namespace Peeza.RPG.Core.Basic.Generic
{
    public interface IAccessor<T> : IEnumerable<T>
    {
        void Add(T entity);
        void Add(params T[] entities);
        void Remove(T entity);
        void Clear();

        IEnumerable<T> GetAll();
        IEnumerable<T> GetAll(Func<T, bool> predicate);
    }
}
