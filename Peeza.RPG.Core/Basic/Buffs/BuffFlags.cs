﻿using System;

namespace Peeza.RPG.Core.Basic.Buffs
{
    [Flags]
    public enum BuffFlags
    {
        None = 0,

        Positive = 1,
        Negative = 2,
        Neutral = 3,

        Multiplicative = 4,
        Dispellable = 8,

        InfiniteDuration = 16,
    }
}
