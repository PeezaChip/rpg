﻿using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Statuses;
using System.Numerics;
using System.Text;

namespace Peeza.RPG.Core.Basic.Buffs
{
    public class Buff<T> : Buff where T : INumber<T>
    {
        public T Value { get; private set; }
        public override double DoubleValue => Value is double doubleValue ? doubleValue : double.CreateChecked(Value);

        public Buff(string key, IBattleEntity inflicter, T value = default, BuffFlags flags = BuffFlags.None, int turnsLeft = 0, int priority = 100) : base(key, inflicter, flags, turnsLeft, priority)
        {
            Value = value;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append($"{Key} buff for {Value} ({Flags})");
            if (TurnsLeft > 0 && !Flags.HasFlag(BuffFlags.InfiniteDuration))
            {
                sb.Append($" {TurnsLeft} turns left");
            }

            return sb.ToString();
        }
    }

    public abstract class Buff : DecayingStatus
    {
        public abstract double DoubleValue { get; }

        public int Priority { get; }

        public BuffFlags Flags { get; private set; }

        public Buff(string key, IBattleEntity inflicter, BuffFlags flags = BuffFlags.None, int turnsLeft = 0, int priority = 100) : base(key, inflicter)
        {
            Flags = flags;
            TurnsLeft = turnsLeft;
            Priority = priority;
        }
    }
}
