﻿using Peeza.RPG.Core.Basic.BattleEntities;

namespace Peeza.RPG.Core.Basic.Buffs
{
    public class ResourceBuff : Buff<double>
    {
        public ResourceBuffEnum ResourceType { get; private set; }

        public ResourceBuff(string key, IBattleEntity inflicter, ResourceBuffEnum resourceType, double value = 0, BuffFlags flags = BuffFlags.None, int turnsLeft = 0, int priority = 100) : base(key, inflicter, value, flags, turnsLeft, priority)
        {
            ResourceType = resourceType;
        }
    }
}
