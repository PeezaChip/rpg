﻿using Peeza.RPG.Core.Basic.Generic;

namespace Peeza.RPG.Core.Basic.Buffs
{
    public interface IBuffsAccessor : IKeyedAccessor<Buff> { }
}
