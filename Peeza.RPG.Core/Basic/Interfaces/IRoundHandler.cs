﻿using Peeza.RPG.Core.Basic.BattleEntities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Peeza.RPG.Core.Basic.Interfaces
{
    public interface IRoundHandler
    {
        Task ProccessRound(CancellationToken cancellationToken, IEnumerable<IBattleQueable> queables);
        IQueueAccessor GetQueueAccessor();
    }
}
