﻿namespace Peeza.RPG.Core.Basic.Interfaces
{
    public interface IExpansion
    {
        public string Name { get; }
    }
}
