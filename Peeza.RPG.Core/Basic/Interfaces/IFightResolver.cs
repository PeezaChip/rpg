﻿using System.Threading;
using System.Threading.Tasks;

namespace Peeza.RPG.Core.Basic.Interfaces
{
    public interface IFightResolver
    {
        Task ProccessFight(CancellationToken cancellationToken);
    }
}
