﻿using System.Threading;
using System.Threading.Tasks;

namespace Peeza.RPG.Core.Basic.Interfaces
{
    public interface IDungeonResolver
    {
        Task ProccessDungeon(CancellationToken cancellationToken);
    }
}
