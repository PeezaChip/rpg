﻿using Peeza.RPG.Core.Basic.Stats;

namespace Peeza.RPG.Core.Basic.Interfaces
{
    public interface IStatsFactory
    {
        void Calculate(IStatsAccessor stats);
    }
}