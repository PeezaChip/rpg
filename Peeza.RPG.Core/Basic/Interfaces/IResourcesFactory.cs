﻿using Peeza.RPG.Core.Basic.Resources;

namespace Peeza.RPG.Core.Basic.Interfaces
{
    public interface IResourcesFactory
    {
        void Calculate(IResourcesAccessor resources);
    }
}