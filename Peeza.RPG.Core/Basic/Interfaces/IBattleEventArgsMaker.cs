﻿using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Battle.Events;

namespace Peeza.RPG.Core.Basic.Interfaces
{
    public interface IBattleEventArgsMaker
    {
        BattleEventArgs<T> FromData<T>(T data, IBattleEntity self = null, IBattleEntity target = null);
    }
}
