﻿using Peeza.RPG.Core.Basic.Spells;

namespace Peeza.RPG.Core.Basic.Interfaces
{
    public interface ISpellsFactory
    {
        void Learn(ISpellsAccessor spells);
    }
}