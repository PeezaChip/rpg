﻿using Peeza.RPG.Core.Realizations.Battle;

namespace Peeza.RPG.Core.Basic.Interfaces
{
    public interface IQueueAccessor
    {
        BattleQueue GetQueue();
    }
}
