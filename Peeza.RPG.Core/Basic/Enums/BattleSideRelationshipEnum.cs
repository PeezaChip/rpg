﻿namespace Peeza.RPG.Core.Basic.Enums
{
    public enum BattleSideRelationshipEnum
    {
        Neutral,
        Friendly,
        Hostile
    }
}
