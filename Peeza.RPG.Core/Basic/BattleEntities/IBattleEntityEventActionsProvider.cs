﻿namespace Peeza.RPG.Core.Basic.BattleEntities
{
    public interface IBattleEntityEventActionsProvider
    {
        void Hook(IBattleEntityEventHandlersProvider hookTo);
        void Unhook(IBattleEntityEventHandlersProvider hookTo);
    }
}
