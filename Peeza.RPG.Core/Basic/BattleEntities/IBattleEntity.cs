﻿using Peeza.RPG.Core.Basic.Ailments;
using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Cooldowns;
using Peeza.RPG.Core.Basic.Resources;
using Peeza.RPG.Core.Basic.Spells;
using Peeza.RPG.Core.Basic.Stats;
using Peeza.RPG.Core.Basic.Statuses;

namespace Peeza.RPG.Core.Basic.BattleEntities
{
    public interface IBattleEntity
    {
        string Name { get; }

        bool CanFight { get; }

        IBattleSide BattleSide { get; }


        bool SupportsEvents { get; }
        IBattleEntityEventHandlersProvider EventHandler { get; }

        bool HaveMinions { get; }
        IMinionsAccessor Minions { get; }

        bool SupportsCooldowns { get; }
        ICooldownAccessor Cooldowns { get; }

        bool SupportsAilments { get; }
        IAilmentsAccessor Ailments { get; }

        bool SupportsBuffs { get; }
        IBuffsAccessor Buffs { get; }

        bool SupportsStatuses { get; }
        IStatusAccessor Statuses { get; }

        bool SupportStats { get; }
        IStatsAccessor Stats { get; }

        bool SupportResources { get; }
        IResourcesAccessor Resources { get; }

        bool SupportSpells { get; }
        ISpellsAccessor Spells { get; }
    }
}
