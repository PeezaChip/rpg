﻿using Peeza.RPG.Core.Battle.Events.Interfaces;

namespace Peeza.RPG.Core.Basic.BattleEntities
{
    public interface IBattleEntityEventHandlersProvider : IBattleEventHandlersProvider { }
}
