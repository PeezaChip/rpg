﻿using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Interfaces;
using Peeza.RPG.Core.Basic.Stats;

namespace Peeza.RPG.Core.Basic.BattleEntities
{
    public abstract class BattleEntityStatsFactory : IStatsFactory
    {
        protected readonly IBuffsAccessor buffs;

        protected BattleEntityStatsFactory(IBuffsAccessor buffs)
        {
            this.buffs = buffs;
        }

        public abstract void Calculate(IStatsAccessor stats);
    }
}
