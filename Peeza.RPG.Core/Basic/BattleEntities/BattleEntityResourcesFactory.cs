﻿using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Interfaces;
using Peeza.RPG.Core.Basic.Resources;

namespace Peeza.RPG.Core.Basic.BattleEntities
{
    public abstract class BattleEntityResourcesFactory : IResourcesFactory
    {
        protected readonly IBuffsAccessor buffs;

        protected BattleEntityResourcesFactory(IBuffsAccessor buffs)
        {
            this.buffs = buffs;
        }

        public abstract void Calculate(IResourcesAccessor resources);
    }
}
