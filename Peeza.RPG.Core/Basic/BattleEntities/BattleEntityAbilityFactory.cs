﻿namespace Peeza.RPG.Core.Basic.BattleEntities
{
    public abstract class BattleEntityAbilityFactory : IBattleEntityEventActionsProvider
    {
        public abstract void Hook(IBattleEntityEventHandlersProvider hookTo);
        public abstract void Unhook(IBattleEntityEventHandlersProvider hookTo);
    }
}
