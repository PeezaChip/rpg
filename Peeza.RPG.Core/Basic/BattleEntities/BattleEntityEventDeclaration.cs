﻿using Peeza.RPG.Core.Battle.Events;

namespace Peeza.RPG.Core.Basic.BattleEntities
{
    public abstract class BattleEntityEventDeclaration<T> : BattleEventDeclaration<T> { }

    public abstract class BattleEntityEventDeclaration : BattleEventDeclaration { }
}
