﻿using Peeza.RPG.Core.Basic.Generic;
using Peeza.RPG.Core.Basic.Minions;

namespace Peeza.RPG.Core.Basic.BattleEntities
{
    public interface IMinionsAccessor : IAccessor<Minion> { }
}
