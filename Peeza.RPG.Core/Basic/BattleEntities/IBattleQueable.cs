﻿namespace Peeza.RPG.Core.Basic.BattleEntities
{
    public interface IBattleQueable
    {
        void TurnAction();
    }
}
