﻿namespace Peeza.RPG.Core.Basic.BattleFields
{
    public interface IBattleFieldEventActionsProvider
    {
        void Hook(IBattleFieldEventHandlersProvider hookTo);
        void Unhook(IBattleFieldEventHandlersProvider hookTo);
    }
}
