﻿using Peeza.RPG.Core.Basic.Auras;
using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Cooldowns;

namespace Peeza.RPG.Core.Basic.BattleFields
{
    public interface IBattleField : IBattleEntityEventActionsProvider
    {
        string Name { get; }

        IBattleSidesAccessor AllBattleSides { get; }

        bool SupportsAuras { get; }
        IAurasAccessor Auras { get; }

        bool SupportsCooldowns { get; }
        ICooldownAccessor Cooldowns { get; }

        bool SupportsEvents { get; }
        IBattleFieldEventHandlersProvider EventHandler { get; }
    }
}
