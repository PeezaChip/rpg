﻿using Peeza.RPG.Core.Battle.Events.Interfaces;

namespace Peeza.RPG.Core.Basic.BattleFields
{
    public interface IBattleFieldEventHandlersProvider : IBattleEventHandlersProvider { }
}
