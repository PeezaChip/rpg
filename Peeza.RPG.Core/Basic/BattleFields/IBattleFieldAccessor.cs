﻿namespace Peeza.RPG.Core.Basic.BattleFields
{
    public interface IBattleFieldAccessor
    {
        IBattleField GetField();
    }
}
