﻿using Peeza.RPG.Core.Battle.Events;

namespace Peeza.RPG.Core.Basic.BattleFields
{
    public abstract class BattleFieldEventDeclaration<T> : BattleEventDeclaration<T> { }

    public abstract class BattleFieldEventDeclaration : BattleEventDeclaration { }
}
