﻿namespace Peeza.RPG.Core.Basic.Resources
{
    public class SimpleResource : Resource
    {
        private long _value;
        public override long Value
        {
            get
            {
                FixValue();
                return _value;
            }
            set
            {
                _value = value;
                FixValue();
            }
        }

        private readonly long _maxValue;
        public override long MaxValue => _maxValue;

        private readonly long _minValue;
        public override long MinValue => _minValue;

        public SimpleResource(string key) : base(key) { }

        public SimpleResource(string key, long max, long val, long min = 0) : base(key)
        {
            _maxValue = max;
            _minValue = min;

            if (_maxValue < _minValue)
            {
                _maxValue = _minValue;
            }

            _value = val;
        }

        public SimpleResource(string key, long max, long min = 0, bool setCurrentToMax = true) : this(key, max, setCurrentToMax ? max : min, min) { }

        private void FixValue()
        {
            if (_value > _maxValue)
            {
                _value = _maxValue;
            }
            else if (_value < _minValue)
            {
                _value = _minValue;
            }
        }
    }
}
