﻿using Peeza.RPG.Core.Basic.Generic;

namespace Peeza.RPG.Core.Basic.Resources
{
    public abstract class ResourceDeclaration : IRPGKey
    {
        public string Key { get; }

        public long DefaultValue { get; } = default;

        public bool SupportsBuff { get; } = false;

        public ResourceDeclaration()
        {
            var typeName = GetType().Name;
            if (typeName.EndsWith("ResourceDeclaration"))
            {
                typeName = typeName[..^"ResourceDeclaration".Length];
            }
            Key = typeName;
        }

        public ResourceDeclaration(long defaultValue, bool supportsBuff) : this()
        {
            DefaultValue = defaultValue;
            SupportsBuff = supportsBuff;
        }
    }
}
