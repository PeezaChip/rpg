﻿using Peeza.RPG.Core.Basic.Generic;

namespace Peeza.RPG.Core.Basic.Resources
{
    public interface IResourcesAccessor : IUniqueKeyedAccessor<Resource> { }
}