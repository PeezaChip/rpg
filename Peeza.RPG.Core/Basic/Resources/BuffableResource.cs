﻿using Peeza.RPG.Core.Basic.Buffs;
using System.Linq;

namespace Peeza.RPG.Core.Basic.Resources
{
    public class BuffableResource : Resource, IModifiableResource
    {
        private readonly IBuffsAccessor buffs;

        private long _value;
        private long _maxValue;
        private long _minValue;

        public override long Value
        {
            get
            {
                FixValue();
                return _value;
            }
            set
            {
                _value = value;
                FixValue();
            }
        }

        public override long MaxValue
        {
            get
            {
                var maxValue = ApplyBuffs(_maxValue, ResourceBuffEnum.Max);
                var minValue = ApplyBuffs(_minValue, ResourceBuffEnum.Min);

                if (maxValue < minValue)
                {
                    maxValue = minValue;
                }

                return maxValue;
            }
        }
        public override long MinValue => ApplyBuffs(_minValue, ResourceBuffEnum.Min);

        public void ModifyMin(long value)
        {
            _minValue = value;
            if (_maxValue < _minValue)
            {
                _maxValue = _minValue;
            }
        }

        public void ModifyMax(long value)
        {
            _maxValue = value;
            if (_maxValue < _minValue)
            {
                _maxValue = _minValue;
            }
        }

        public BuffableResource(string key, IBuffsAccessor buffs) : base(key)
        {
            this.buffs = buffs;
        }

        public BuffableResource(string key, IBuffsAccessor buffs, long max, long val, long min = 0) : this(key, buffs)
        {
            _maxValue = max;
            _minValue = min;

            if (_maxValue < _minValue)
            {
                _maxValue = _minValue;
            }

            _value = val;
        }

        public BuffableResource(string key, IBuffsAccessor buffs, long max, long min = 0, bool setCurrentToMax = true) : this(key, buffs, max, setCurrentToMax ? max : min, min) { }

        private long ApplyBuffs(long baseValue, ResourceBuffEnum type)
        {
            if (buffs == null) return baseValue;
            var applicableBuffs = buffs.GetAll(buff => buff is ResourceBuff resBuff && resBuff.ResourceType == type && (buff.Key == Key || buff.Key == "*")).OrderBy(buff => buff.Priority); ;
            if (!applicableBuffs.Any()) return baseValue;

            var value = double.CreateChecked(baseValue);

            foreach (var buff in applicableBuffs)
            {
                if (buff.Flags.HasFlag(BuffFlags.Multiplicative))
                {
                    value *= buff.DoubleValue;
                }
                else
                {
                    value += buff.DoubleValue;
                }
            }

            return long.CreateChecked(value);
        }

        private void FixValue()
        {
            if (_value > MaxValue)
            {
                _value = MaxValue;
            }
            else if (_value < MinValue)
            {
                _value = MinValue;
            }
        }
    }
}
