﻿namespace Peeza.RPG.Core.Basic.Resources
{
    public interface IModifiableResource
    {
        void ModifyMin(long value);
        void ModifyMax(long value);
    }
}
