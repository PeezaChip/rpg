﻿using Peeza.RPG.Core.Basic.Generic;

namespace Peeza.RPG.Core.Basic.Resources
{
    public abstract class Resource : IRPGKey
    {
        public string Key { get; private set; }

        public abstract long Value { get; set; }
        public abstract long MaxValue { get; }
        public abstract long MinValue { get; }

        public virtual bool IsEmpty => Value <= 0;
        public virtual double Percentage => Value * 1.0 / MaxValue;
        public virtual double PercentageRelativeToMin => (Value - MinValue) * 1.0 / (MaxValue - MinValue);

        public Resource(string key)
        {
            Key = key;
        }
    }
}
