﻿using System;

namespace Peeza.RPG.Core.Basic.Battle
{
    public interface IBattleLogger
    {
        void Log(string log = null) => Log(log, BattleLogVerbosity.None);
        void Log(string log, BattleLogVerbosity level = BattleLogVerbosity.Info);
        LogIndent IndentLog();
    }

    public enum BattleLogVerbosity
    {
        None,
        Summary,
        Info,
        Verbose,
        Technical
    }

    public class LogIndent : IDisposable
    {
        private readonly Action<LogIndent> action;

        private bool disposed = false;

        public LogIndent(Action<LogIndent> action)
            => this.action = action;

        public void Dispose()
        {
            if (disposed) return;
            disposed = true;
            action.Invoke(this);
            GC.SuppressFinalize(this);
        }
    }
}
