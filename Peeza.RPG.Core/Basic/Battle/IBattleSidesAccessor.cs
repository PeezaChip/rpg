﻿using Peeza.RPG.Core.Basic.Generic;

namespace Peeza.RPG.Core.Basic.Battle
{
    public interface IBattleSidesAccessor<T> : IAccessor<T> where T : IBattleSide { }
    public interface IBattleSidesAccessor : IAccessor<IBattleSide> { }
}
