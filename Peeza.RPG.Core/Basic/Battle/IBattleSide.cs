﻿using Peeza.RPG.Core.Basic.BattleEntities;
using System.Collections.Generic;

namespace Peeza.RPG.Core.Basic.Battle
{
    public interface IBattleSide
    {
        List<IBattleEntity> BattleEntities { get; }
    }
}
