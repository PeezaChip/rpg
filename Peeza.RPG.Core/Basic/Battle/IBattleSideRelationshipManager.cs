﻿using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Enums;
using System.Collections.Generic;

namespace Peeza.RPG.Core.Basic.Battle
{
    public interface IBattleSideRelationshipManager
    {
        IBattleSidesAccessor Sides { get; }

        IEnumerable<IBattleSide> GetHostiles(IBattleSide side);
        IEnumerable<IBattleSide> GetFriendlies(IBattleSide side);
        IEnumerable<IBattleSide> GetNeutrals(IBattleSide side);

        BattleSideRelationshipEnum GetRelationship(IBattleSide side1, IBattleSide side2);
        BattleSideRelationshipEnum GetRelationship(IBattleEntity entity, IBattleSide side) => GetRelationship(entity.BattleSide, side);
        BattleSideRelationshipEnum GetRelationship(IBattleEntity entity1, IBattleEntity entity2) => GetRelationship(entity1, entity2);
    }
}
