﻿using Peeza.RPG.Core.Basic.BattleEntities;

namespace Peeza.RPG.Core.Basic.Minions
{
    public class Minion
    {
        public MinionFlags Flags { get; set; }
        public int TimeLeft { get; set; }
        public IBattleEntity BattleEntity { get; set; }
        public IBattleEntity Owner { get; set; }

        public Minion(IBattleEntity battleEntity, MinionFlags flags = MinionFlags.None, int timeLeft = 0)
        {
            BattleEntity = battleEntity;
            Flags = flags;
            TimeLeft = timeLeft;
        }
    }
}
