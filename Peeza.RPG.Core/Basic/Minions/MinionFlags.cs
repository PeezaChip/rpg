﻿using System;

namespace Peeza.RPG.Core.Basic.Minions
{
    [Flags]
    public enum MinionFlags
    {
        None = 0,

        InfiniteDuration = 1,
    }
}
