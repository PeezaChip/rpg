﻿using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Generic;
using Peeza.RPG.Core.Realizations.Buffs;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Peeza.RPG.Core.Basic.Auras
{
    public class Aura : IRPGKey, IBuffsAccessor
    {
        public string Key { get; private set; }

        public IBattleEntity Owner { get; private set; }

        /// <summary>
        /// Aura will be applied to all entities if filter is empty, defualt filter mode is white list, modifiable via <see cref="AuraFlags.FilterBlacklistMode"/>
        /// </summary>
        public IEnumerable<IBattleSide> Filter { get; private set; }

        public BuffList Buffs { get; private set; }

        public AuraFlags Flags { get; private set; }

        public Aura(string key, IBattleEntity owner, IEnumerable<IBattleSide> filter = null, AuraFlags flags = AuraFlags.None, params Buff[] initialBuffs)
        {
            Key = key;

            Owner = owner;
            Flags = flags;

            Filter = filter ?? [];

            Buffs = [.. initialBuffs];
        }

        public IEnumerable<Buff> GetAll(string key) => Buffs.GetAll(key);
        public void Add(Buff entity) => Buffs.Add(entity);
        public void Add(params Buff[] entities) => Buffs.Add(entities);
        public void Remove(Buff entity) => Buffs.Remove(entity);
        public void Clear() => Buffs.Clear();
        public IEnumerable<Buff> GetAll() => Buffs.GetAll();
        public IEnumerable<Buff> GetAll(Func<Buff, bool> predicate) => Buffs.GetAll(predicate);
        public IEnumerator<Buff> GetEnumerator() => GetAll().GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
