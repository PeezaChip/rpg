﻿using Peeza.RPG.Core.Basic.Generic;

namespace Peeza.RPG.Core.Basic.Auras
{
    public interface IAurasAccessor : IKeyedAccessor<Aura> { }
}
