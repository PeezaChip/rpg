﻿using System;

namespace Peeza.RPG.Core.Basic.Auras
{
    [Flags]
    public enum AuraFlags
    {
        None = 0,

        Positive = 1,
        Negative = 2,
        Neutral = 3,

        Reserved = 4,
        Dispellable = 8,

        Reserved2 = 16,

        AllowMultiple = 32,

        FilterBlacklistMode = 64,
    }
}
