﻿using Peeza.RPG.Core.Basic.Generic;

namespace Peeza.RPG.Core.Basic.Spells
{
    public interface ISpellsAccessor : IUniqueKeyedAccessor<Spell> { }
}