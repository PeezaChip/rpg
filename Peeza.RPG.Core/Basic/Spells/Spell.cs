﻿using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Cooldowns;
using Peeza.RPG.Core.Basic.Generic;
using Peeza.RPG.Core.Realizations.Stats;

namespace Peeza.RPG.Core.Basic.Spells
{
    public abstract class Spell : IBattleEntityEventActionsProvider, IRPGKey
    {
        public abstract string Key { get; }
        public abstract string Description { get; }
        public abstract string Details { get; }

        public StatList Stats { get; private set; }
        public string ResourceKey { get; }
        public CooldownTypeFlags? CooldownType { get; }
        public int? Priority { get; }

        public Spell(StatList stats, string resourceKey = null, CooldownTypeFlags? cooldownType = null, int? priority = null)
        {
            Stats = stats;
            ResourceKey = resourceKey;
            CooldownType = cooldownType;
            Priority = priority;
        }

        public abstract void Hook(IBattleEntityEventHandlersProvider hookTo);
        public abstract void Unhook(IBattleEntityEventHandlersProvider hookTo);
    }
}
