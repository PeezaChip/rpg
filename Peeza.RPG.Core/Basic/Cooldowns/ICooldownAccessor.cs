﻿using Peeza.RPG.Core.Basic.Generic;

namespace Peeza.RPG.Core.Basic.Cooldowns
{
    public interface ICooldownAccessor : IUniqueKeyedAccessor<Cooldown>
    {
        public void DecreaseCooldown(Cooldown cooldown);
        public void DecreaseCooldownsOfType(CooldownTypeFlags cooldownType);
    }
}
