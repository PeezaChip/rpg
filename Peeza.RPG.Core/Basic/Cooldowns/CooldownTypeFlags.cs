﻿using System;

namespace Peeza.RPG.Core.Basic.Cooldowns
{
    [Flags]
    public enum CooldownTypeFlags
    {
        Never = 0,

        OnDungeonStart = 1,
        OnDungeonEnd = 2,
        OnDungeon = 3,

        OnFightStart = 4,
        OnFightEnd = 8,
        OnFight = 12,

        OnRoundStart = 16,
        OnRoundEnd = 32,
        OnRound = 48,

        OnTurnStart = 64,
        OnTurnEnd = 128,
        OnTurn = 192,
    }
}
