﻿using Peeza.RPG.Core.Basic.Generic;

namespace Peeza.RPG.Core.Basic.Cooldowns
{
    public class Cooldown : IRPGKey
    {
        public string Key { get; private set; }
        public CooldownTypeFlags Type { get; private set; }
        public int Counter { get; set; }

        public Cooldown(string key, CooldownTypeFlags type = CooldownTypeFlags.OnDungeon)
        {
            Key = key;
            Type = type;
        }

        public Cooldown(string key, int initialCounter, CooldownTypeFlags type = CooldownTypeFlags.OnDungeon) : this(key, type)
        {
            Counter = initialCounter;
        }
    }
}
