﻿using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Heroes.Items;
using Peeza.RPG.Core.Heroes.Items.Abstract;
using Peeza.RPG.Core.Heroes.Items.Enums;
using Peeza.RPG.Tests.Battle;

namespace Peeza.RPG.Tests.Items
{
    [TestClass]
    public class BasicItemAbilityTest
    {
        [TestMethod]
        public void TestItemAbilityPipeline()
        {
            var dict = new DictionaryEventHandlerProvider();

            var declaration = new TestItemAbility();
            var item = new Item(ItemTypeEnum.None, ItemRarityEnum.Common, ItemFlags.None, null, null, nameof(TestItemAbility), "pref", "suff", "sig");

            declaration.CreateItemAbilityFactory(item).Hook(dict);

            var args = new BattleEventArgs<string>();
            dict.OnTest1(args);
            Assert.AreEqual("test", args.Data);
        }
    }

    public class TestItemAbility : ItemAbilityDeclaration
    {
        public override string GetAbilityDescription(Item item)
        {
            return $"TestItem";
        }

        public override ItemAbilityFactory CreateItemAbilityFactory(IEnumerable<Item> items)
        {
            return new TestItemAbilityFactory(items);
        }

        private class TestItemAbilityFactory : ItemAbilityFactory
        {
            public TestItemAbilityFactory(IEnumerable<Item> items) : base(items) { }

            public override void Hook(IBattleEntityEventHandlersProvider hookTo)
            {
                hookTo.HookEvent(BattleEventHooksTestExtensions.OnTest1Declaration, TestItemAbilityGenerator());
            }

            public override void Unhook(IBattleEntityEventHandlersProvider hookTo)
            {
                throw new NotImplementedException("Unhooking that is not supported");
            }

            public BattleEventAction<string> TestItemAbilityGenerator()
            {
                return args =>
                {
                    args.Data = "test";
                    return false;
                };
            }
        }
    }
}
