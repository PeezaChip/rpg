﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Base.Battle.Arguments.Damage;
using Peeza.RPG.Base.Battle.BattleEntities;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Base.Resources;
using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Stats;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Heroes;
using Peeza.RPG.Core.Realizations.Resources;
using Peeza.RPG.Core.Realizations.Stats;
using Peeza.RPG.Tests.Core;

namespace Peeza.RPG.Tests.Expansions.Base.Specs.T1
{
    [TestClass]
    public class MageTests
    {
        [TestMethod]
        public void TestPassivesAbility()
        {
            var hero = new Hero() { Level = 1, Name = "A" };
            var entity = new HeroBattleEntity(hero, null, RPG.SpecDeclarations.GetByKey("Mage"), null, null, null, null);
            var logger = new ConsoleBattleLogger();

            Assert.AreEqual(2.1, entity.Stats.GetCritDamage());
            Assert.AreEqual(BaseResourceScalings.CalculateMana(1, null).Value, entity.Resources.GetMana()?.Value);
        }

        [TestMethod]
        public void TestManaRegenAbility()
        {
            var hero = new Hero() { Level = 1, Name = "A" };
            var entity = new HeroBattleEntity(hero, null, RPG.SpecDeclarations.GetByKey("Mage"), null, null, null, null);
            var logger = new ConsoleBattleLogger();

            entity.Resources.GetMana().Value = 0;

            entity.EventHandler.OnAttackResolve(new BattleEventArgs<DamageInfo>(null, logger, null, entity, null, new(), null));

            Assert.AreEqual(10, entity.Resources.GetMana().Value);
        }

        [TestMethod]
        public void TestDispellAbility()
        {
            var hero = new Hero() { Level = 1, Name = "A" };
            var entity = new HeroBattleEntity(hero, null, RPG.SpecDeclarations.GetByKey("Mage"), null, null, null, null);
            var logger = new ConsoleBattleLogger();
            var rng = new Random(1);

            entity.Resources.GetMana().Value = 0;

            entity.Buffs.Add(new Buff<int>("testDebuff", entity, 10, BuffFlags.Negative | BuffFlags.Dispellable));

            entity.EventHandler.OnTurn(new BattleEventArgs<OnTimeEnum>(null, logger, rng, entity, null, OnTimeEnum.Start, null));

            Assert.AreEqual(1, entity.Buffs.GetAll().Count());

            entity.Resources.GetMana().Value = 30;

            entity.EventHandler.OnTurn(new BattleEventArgs<OnTimeEnum>(null, logger, rng, entity, null, OnTimeEnum.Start, null));

            Assert.AreEqual(0, entity.Buffs.GetAll().Count());
            Assert.AreEqual(10, entity.Resources.GetMana().Value);
        }

        [TestMethod]
        public void TestFireballAbility()
        {
            var hero = new Hero() { Level = 1, Name = "A" };
            var entity = new HeroBattleEntity(hero, null, RPG.SpecDeclarations.GetByKey("Mage"), null, null, null, null);
            var logger = new ConsoleBattleLogger();
            var rng = new Random(2);

            var target = new TestBattleEntity() { EventHandler = new DictionaryEventHandlerProvider(), Stats = new StatList(), Resources = new ResourceList() };
            target.Resources.Add(new HealthResource(100));

            entity.Resources.GetMana().Value = 0;
            (entity.Stats.GetByKey(BasicStatsDeclarationsExtensions.CritChanceStatKey) as IModifiableStat<double>).Modify(0);
            (entity.Stats.GetByKey(BasicStatsDeclarationsExtensions.AccuracyStatKey) as IModifiableStat<double>).Modify(1);
            (entity.Stats.GetByKey(BasicStatsDeclarationsExtensions.AttackStatKey) as IModifiableStat<long>).Modify(10);

            var args = new BattleEventArgs<DamageInfo>(null, logger, rng, entity, target, null, null);
            entity.EventHandler.OnCalculateDamage(args);

            Assert.AreEqual(10, args.Data.TotalAttack);

            entity.Resources.GetMana().Value = 60;

            args = new BattleEventArgs<DamageInfo>(null, logger, rng, entity, target, null, null);
            entity.EventHandler.OnCalculateDamage(args);

            Assert.AreEqual(20, args.Data.TotalAttack);
            Assert.AreEqual(10, entity.Resources.GetMana().Value);
        }
    }
}
