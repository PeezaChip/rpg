﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Base.Battle.BattleEntities;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Heroes;

namespace Peeza.RPG.Tests.Expansions.Base.Specs.T1
{
    [TestClass]
    public class WarriorTests
    {
        [TestMethod]
        public void TestWarriorAbility()
        {
            var hero = new Hero() { Level = 1, Name = "A" };
            var entity = new HeroBattleEntity(hero, null, RPG.SpecDeclarations.GetByKey("Warrior"), null, null, null, null);

            Assert.AreEqual((long)(BaseStatScalings.CalculateArmor(1).Value * 1.1), entity.Stats.GetArmor());
            Assert.AreEqual((long)(BaseStatScalings.CalculateMagicResistance(1).Value * 1.1), entity.Stats.GetMagicResistance());

            Assert.AreEqual(10, entity.Stats.GetAggro());

            var duration = 2;
            var cooldown = 3;

            for (int i = 0; i <= cooldown; i++)
            {
                entity.EventHandler.OnRound(new BattleEventArgs<OnTimeEnum>(null, null, null, entity, null, OnTimeEnum.Start, null));
                entity.EventHandler.OnTurn(new BattleEventArgs<OnTimeEnum>(null, null, null, entity, null, OnTimeEnum.Start, null));

                if (i < duration || cooldown == i)
                {
                    Assert.AreEqual(15, entity.Stats.GetAggro());
                }
                else
                {
                    Assert.AreEqual(10, entity.Stats.GetAggro());
                }

                if (i < cooldown)
                {
                    Assert.IsTrue(entity.Cooldowns.GetByKey("Warrior Shout").Counter == cooldown - i);
                }

                if (i == cooldown)
                {
                    Assert.IsNotNull(entity.Cooldowns.GetByKey("Warrior Shout").Counter == cooldown);
                }
            }
        }
    }
}
