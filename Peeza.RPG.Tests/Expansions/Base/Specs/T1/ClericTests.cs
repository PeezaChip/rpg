﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Base.Battle.Arguments.Damage;
using Peeza.RPG.Base.Battle.BattleEntities;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Base.Resources;
using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Stats;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Heroes;
using Peeza.RPG.Core.Realizations.Battle;
using Peeza.RPG.Tests.Core;

namespace Peeza.RPG.Tests.Expansions.Base.Specs.T1
{
    [TestClass]
    public class ClericTests
    {
        [TestMethod]
        public void TestPassivesAbility()
        {
            var hero = new Hero() { Level = 1, Name = "A" };
            var entity = new HeroBattleEntity(hero, null, RPG.SpecDeclarations.GetByKey("Cleric"), null, null, null, null);
            var logger = new ConsoleBattleLogger();

            Assert.AreEqual(10 * 0.9, entity.Stats.GetAggro());
            Assert.AreEqual(BaseResourceScalings.CalculateMana(1, null).Value, entity.Resources.GetMana()?.Value);
        }

        [TestMethod]
        public void TestManaRegenAbility()
        {
            var hero = new Hero() { Level = 1, Name = "A" };
            var entity = new HeroBattleEntity(hero, null, RPG.SpecDeclarations.GetByKey("Cleric"), null, null, null, null);
            var logger = new ConsoleBattleLogger();

            entity.Resources.GetMana().Value = 0;

            entity.EventHandler.OnAttackResolve(new BattleEventArgs<DamageInfo>(null, logger, null, entity, null, new(), null));

            Assert.AreEqual(10, entity.Resources.GetMana().Value);
        }

        [TestMethod]
        public void TestDispellAbility()
        {
            var bs = new BattleSide();

            var hero = new Hero() { Level = 1, Name = "A" };
            var entity = new HeroBattleEntity(hero, null, RPG.SpecDeclarations.GetByKey("Cleric"), bs, null, null, null);
            var ally = new HeroBattleEntity(new Hero() { Level = 1, Name = "B" }, null, null, bs, null, null, null);

            bs.Add(ally);
            bs.Add(entity);

            var logger = new ConsoleBattleLogger();
            var rng = new Random(1);

            entity.Resources.GetMana().Value = 0;

            entity.Buffs.Add(new Buff<int>("testDebuff", entity, 10, BuffFlags.Negative | BuffFlags.Dispellable));

            entity.EventHandler.OnTurn(new BattleEventArgs<OnTimeEnum>(null, logger, rng, entity, null, OnTimeEnum.Start, null));

            Assert.AreEqual(1, entity.Buffs.GetAll().Count());

            entity.Resources.GetMana().Value = 40;

            entity.EventHandler.OnTurn(new BattleEventArgs<OnTimeEnum>(null, logger, rng, entity, null, OnTimeEnum.Start, null));

            Assert.AreEqual(0, entity.Buffs.GetAll().Count());
            Assert.AreEqual(10, entity.Resources.GetMana().Value);

            entity.Resources.GetMana().Value = 40;


            entity.Cooldowns.Clear();

            ally.Buffs.Add(new Buff<int>("testDebuff", entity, 10, BuffFlags.Negative | BuffFlags.Dispellable));

            entity.EventHandler.OnTurn(new BattleEventArgs<OnTimeEnum>(null, logger, rng, entity, null, OnTimeEnum.Start, null));

            Assert.AreEqual(0, ally.Buffs.GetAll().Count());
            Assert.AreEqual(10, entity.Resources.GetMana().Value);
        }

        [TestMethod]
        public void TestHealAbility()
        {
            var bs = new BattleSide();

            var hero = new Hero() { Level = 1, Name = "A" };
            var entity = new HeroBattleEntity(hero, null, RPG.SpecDeclarations.GetByKey("Cleric"), bs, null, null, null);
            var logger = new ConsoleBattleLogger();
            var rng = new Random(2);

            var ally = new HeroBattleEntity(new Hero() { Level = 1, Name = "B" }, null, null, bs, null, null, null);

            bs.Add(ally);
            bs.Add(entity);

            ally.Resources.GetHealth().Value = 1;
            entity.Resources.GetHealth().Value = 100;

            entity.Resources.GetMana().Value = 60;
            (entity.Stats.GetByKey(BasicStatsDeclarationsExtensions.CritChanceStatKey) as IModifiableStat<double>).Modify(0);

            var args = new BattleEventArgs<DamageInfo>(null, logger, rng, entity, null, null, null);
            entity.EventHandler.OnCalculateDamage(args);

            Assert.AreEqual(75, ally.Resources.GetHealth().Value);
            Assert.AreEqual(20, entity.Resources.GetMana().Value);
        }
    }
}
