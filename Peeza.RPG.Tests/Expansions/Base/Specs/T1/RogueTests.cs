﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Base.Battle.Arguments.Damage;
using Peeza.RPG.Base.Battle.BattleEntities;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Heroes;
using Peeza.RPG.Tests.Core;

namespace Peeza.RPG.Tests.Expansions.Base.Specs.T1
{
    [TestClass]
    public class RogueTests
    {
        [TestMethod]
        public void TestRogueAbility()
        {
            var hero = new Hero() { Level = 1, Name = "A" };
            var entity = new HeroBattleEntity(hero, null, RPG.SpecDeclarations.GetByKey("Rogue"), null, null, null, null);
            var logger = new ConsoleBattleLogger();

            Assert.AreEqual(0.05, entity.Stats.GetDodgeChance());
            Assert.AreEqual(0, entity.Stats.GetCritChance());

            for (int i = 0; i <= 6; i++)
            {
                entity.EventHandler.OnRound(new BattleEventArgs<OnTimeEnum>(null, logger, null, entity, null, OnTimeEnum.End, null));

                var buff = 0.02 * (i + 1);

                Assert.AreEqual(buff > 0.1 ? 0.1 : buff, entity.Stats.GetCritChance());
            }

            entity.EventHandler.OnGetHitResult(new BattleEventArgs<DamageInfo>(null, logger, null, entity, null, new() { DamageInstances = [new() { Damage = 10, DamageDealt = 10 }] }, null));

            Assert.AreEqual(0, entity.Stats.GetCritChance());
        }
    }
}
