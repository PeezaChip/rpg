﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Base.Battle.BattleEntities;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Heroes;
using Peeza.RPG.Tests.Core;

namespace Peeza.RPG.Tests.Expansions.Base.Races
{
    [TestClass]
    public class HumanTests
    {
        [TestMethod]
        public void TestHumanAbility()
        {
            var hero = new Hero() { Level = 1, Name = "A" };
            var entity = new HeroBattleEntity(hero, RPG.RaceDeclarations.GetByKey("Human"), null, null, null, null, null);

            entity.EventHandler.OnFight(new BattleEventArgs<OnTimeEnum>(null, new ConsoleBattleLogger(), new Random(), entity, null, null));

            Assert.AreEqual(2, entity.Buffs.Count());
        }
    }
}
