﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Base.Battle.Arguments.Heal;
using Peeza.RPG.Base.Battle.BattleEntities;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Base.Resources;
using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Heroes;
using Peeza.RPG.Tests.Core;

namespace Peeza.RPG.Tests.Expansions.Base.Races
{
    [TestClass]
    public class UndeadTests
    {
        [TestMethod]
        public void TestUndeadAbility()
        {
            var hero = new Hero() { Level = 1, Name = "A" };
            var entity = new HeroBattleEntity(hero, RPG.RaceDeclarations.GetByKey("Undead"), null, null, null, null, null);

            Assert.AreEqual(BaseResourceScalings.CalculateHealth(1).MaxValue * 2, entity.Resources.GetHealth().MaxValue);
            Assert.AreEqual(0.2, entity.Stats.GetLifesteal());

            entity.EventHandler.OnKill(new BattleEventArgs<ResourceChangeInfo>(null, new ConsoleBattleLogger(), new Random(), entity, null, null));

            Assert.AreEqual(0.2 + 0.1, entity.Stats.GetLifesteal());

            entity.EventHandler.OnKill(new BattleEventArgs<ResourceChangeInfo>(null, new ConsoleBattleLogger(), new Random(), entity, null, null));

            Assert.AreEqual(0.2 + 0.1 + 0.1, entity.Stats.GetLifesteal());

            entity.Resources.GetHealth().Value = 10;

            entity.EventHandler.OnGetHeal(new BattleEventArgs<HealInfo>(null, new ConsoleBattleLogger(), new Random(), entity, null, new() { HealInstances = [ new() {  Heal = 10 } ] }, null));

            Assert.AreEqual(10, entity.Resources.GetHealth().Value);
        }
    }
}
