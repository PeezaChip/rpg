﻿using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Stats;
using Peeza.RPG.Core.Realizations.Buffs;

namespace Peeza.RPG.Tests.Basic
{
    [TestClass]
    public class StatsTests
    {
        [TestMethod]
        public void TestConstructor()
        {
            var stat = new Stat<int>("test", 1);
            var stat2 = new Stat<int>("test");

            Assert.AreEqual(1, stat.Value);
            Assert.AreEqual(0, stat2.Value);
        }

        [TestMethod]
        public void TestImplicitCast()
        {
            var stat = new Stat<int>("test", 1);
            var stat2 = new Stat<int>("test");

            Assert.AreEqual(1, stat);
            Assert.AreEqual(0, stat2);
        }

        [TestMethod]
        public void TestBaseValue()
        {
            var stat = new Stat<int>("test", 1);

            Assert.AreEqual(stat.BaseValue, stat.Value);
        }

        [TestMethod]
        public void TestMinimum()
        {
            var stat = new Stat<int>("test", -5);

            Assert.AreEqual(0, stat);
        }

        [TestMethod]
        public void TestBuffableConstructor()
        {
            var buffValue = 5;
            var buffs = new BuffList();

            var stat = new BuffableStat<int>("test", buffs, 1) as Stat<int>;
            var stat2 = new BuffableStat<int>("test", buffs);

            buffs.Add(new Buff<int>("test", null, buffValue));

            Assert.AreEqual(1 + buffValue, stat.Value);
            Assert.AreEqual(1, stat.BaseValue);
            Assert.AreEqual(0 + buffValue, stat2.Value);
            Assert.AreEqual(0, stat2.BaseValue);
        }

        [TestMethod]
        public void TestMultiplicativeBuff()
        {
            var buffs = new BuffList();

            var stat = new BuffableStat<int>("test", buffs, 10) as Stat<int>;

            buffs.Add(new Buff<int>("test", null, 10));

            Assert.AreEqual(10 + 10, stat.Value);

            buffs.Add(new Buff<double>("test", null, 1.2, BuffFlags.Multiplicative));

            Assert.AreEqual((10 + 10) * 1.2, stat.Value);
        }

        [TestMethod]
        public void TestBuffableImplicitCast()
        {
            var buffValue = 5;
            var buffs = new BuffList();

            var stat = new BuffableStat<int>("test", buffs, 1) as Stat<int>;
            var stat2 = new BuffableStat<int>("test", buffs);

            buffs.Add(new Buff<int>("test", null, buffValue));

            Assert.AreEqual(1 + buffValue, stat);
            Assert.AreEqual(0 + buffValue, stat2);
        }

        [TestMethod]
        public void TestBuffableMinimum()
        {
            var buffValue = 5;
            var buffs = new BuffList();

            var stat = new BuffableStat<int>("test", buffs, 1) as Stat<int>;
            var stat2 = new BuffableStat<int>("test", buffs, 1);

            buffs.Add(new Buff<int>("test", null, -buffValue));

            Assert.AreEqual(0, stat);
            Assert.AreEqual(0, stat2);
        }

        [TestMethod]
        public void TestWildcardBuffs()
        {
            var baseValue = 100;
            var buffValue = 10;
            var wildcardBuffValue = 30;

            var buffs = new BuffList();

            var stat = new BuffableStat<int>("test", buffs, baseValue);
            var stat2 = new BuffableStat<int>("test2", buffs, baseValue);
            buffs.Add(new ResourceBuff("test", null, ResourceBuffEnum.Max, buffValue, BuffFlags.None, 0));
            buffs.Add(new ResourceBuff("*", null, ResourceBuffEnum.Max, wildcardBuffValue, BuffFlags.None, 0));
            buffs.Add(new Buff<long>("test", null, buffValue, BuffFlags.None, 0));
            buffs.Add(new Buff<long>("*", null, wildcardBuffValue, BuffFlags.None, 0));

            Assert.AreEqual(baseValue + buffValue + wildcardBuffValue, stat.Value);
            Assert.AreEqual(baseValue + wildcardBuffValue, stat2.Value);
        }

        [TestMethod]
        public void TestPrefixedBuffs()
        {
            var baseValue = 100;
            var buffValue = 10;
            var wildcardBuffValue = 30;

            var buffs = new BuffList();

            var stat = new PrefixedBuffableStat<int>("Test", "prefix", buffs, baseValue);

            buffs.Add(new Buff<int>("Test", null, buffValue, BuffFlags.None, 0));
            buffs.Add(new Buff<int>("*", null, wildcardBuffValue, BuffFlags.None, 0));

            Assert.AreEqual(baseValue + buffValue, stat.Value); // wildcards buffs should not work on prefixed stats

            buffs.Add(new Buff<int>("prefixTest", null, wildcardBuffValue, BuffFlags.None, 0));

            Assert.AreEqual(baseValue + buffValue + wildcardBuffValue, stat.Value);
        }
    }
}
