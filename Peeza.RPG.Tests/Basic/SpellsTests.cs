﻿using Peeza.RPG.Base.Battle.Arguments.Damage;
using Peeza.RPG.Base.Battle.BattleEntities;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Base.Resources;
using Peeza.RPG.Base.Spells;
using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Cooldowns;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Realizations.Battle;
using Peeza.RPG.Core.Realizations.Buffs;

namespace Peeza.RPG.Tests.Basic
{
    [TestClass]
    public class SpellsTests
    {
        [TestMethod]
        public void TestSpell()
        {
            var entity = new BaseBattleEntity("Test", 1, null, null, null, null);
            var spell = new TestSpell();
            var logger = new BasicBattleLogger();
            var rng = new Random(1);

            spell.Hook(entity.EventHandler);

            Assert.AreEqual(0, entity.Cooldowns.Count());
            Assert.AreEqual(entity.Resources.GetHealth().MaxValue, entity.Resources.GetHealth().Value);

            entity.EventHandler.OnGetHit(new BattleEventArgs<DamageInfo>(null, logger, rng, entity, null, new DamageInfo() { DamageInstances = [ new DamageInstanceInfo() { Damage = 10 } ] }, null));

            Assert.AreEqual(1, entity.Cooldowns.Count());
            Assert.AreEqual(entity.Resources.GetHealth().MaxValue - spell.Stats.GetSpellCost(), entity.Resources.GetHealth().Value);
            Assert.IsTrue(logger.GetBattleLog().Trim() == "20");

            entity.EventHandler.OnGetHit(new BattleEventArgs<DamageInfo>(null, logger, rng, entity, null, new DamageInfo() { DamageInstances = [new DamageInstanceInfo() { Damage = 10 }] }, null));

            Assert.AreEqual(entity.Resources.GetHealth().MaxValue - spell.Stats.GetSpellCost() - 10, entity.Resources.GetHealth().Value);

            Console.WriteLine(logger.GetBattleLog(BattleLogVerbosity.Technical));
        }

        [TestMethod]
        public void TestSpellUnhook()
        {
            var entity = new BaseBattleEntity("Test", 1, null, null, null, null);
            var spell = new TestSpell();
            var logger = new BasicBattleLogger();
            var rng = new Random(1);

            spell.Hook(entity.EventHandler);
            spell.Unhook(entity.EventHandler);

            Assert.AreEqual(0, entity.Cooldowns.Count());
            Assert.AreEqual(entity.Resources.GetHealth().MaxValue, entity.Resources.GetHealth().Value);

            entity.EventHandler.OnGetHit(new BattleEventArgs<DamageInfo>(null, logger, rng, entity, null, new DamageInfo() { DamageInstances = [new DamageInstanceInfo() { Damage = 10 }] }, null));

            Assert.AreEqual(entity.Resources.GetHealth().MaxValue - 10, entity.Resources.GetHealth().Value);

            Console.WriteLine(logger.GetBattleLog(BattleLogVerbosity.Technical));
        }

        [TestMethod]
        public void TestSpellNoCooldown()
        {
            var entity = new BaseBattleEntity("Test", 1, null, null, null, null);
            var spell = new TestSpellNoCooldown();
            var logger = new BasicBattleLogger();
            var rng = new Random(1);

            spell.Hook(entity.EventHandler);

            Assert.AreEqual(0, entity.Cooldowns.Count());
            Assert.AreEqual(entity.Resources.GetHealth().MaxValue, entity.Resources.GetHealth().Value);

            entity.EventHandler.OnGetHit(new BattleEventArgs<DamageInfo>(null, logger, rng, entity, null, new DamageInfo() { DamageInstances = [new DamageInstanceInfo() { Damage = 10 }] }, null));

            Assert.AreEqual(0, entity.Cooldowns.Count());
            Assert.AreEqual(entity.Resources.GetHealth().MaxValue - spell.Stats.GetSpellCost(), entity.Resources.GetHealth().Value);
            Assert.IsTrue(logger.GetBattleLog().Trim() == "20");

            entity.EventHandler.OnGetHit(new BattleEventArgs<DamageInfo>(null, logger, rng, entity, null, new DamageInfo() { DamageInstances = [new DamageInstanceInfo() { Damage = 10 }] }, null));

            Assert.AreEqual(entity.Resources.GetHealth().MaxValue - spell.Stats.GetSpellCost() * 2, entity.Resources.GetHealth().Value);
            Assert.IsTrue(logger.GetBattleLog().Trim() == $"20{Environment.NewLine}20");

            Console.WriteLine(logger.GetBattleLog(BattleLogVerbosity.Technical));
        }

        [TestMethod]
        public void TestSpellNoCost()
        {
            var entity = new BaseBattleEntity("Test", 1, null, null, null, null);
            var spell = new TestSpellNoCost();
            var logger = new BasicBattleLogger();
            var rng = new Random(1);

            spell.Hook(entity.EventHandler);

            Assert.AreEqual(0, entity.Cooldowns.Count());
            Assert.AreEqual(entity.Resources.GetHealth().MaxValue, entity.Resources.GetHealth().Value);

            entity.EventHandler.OnGetHit(new BattleEventArgs<DamageInfo>(null, logger, rng, entity, null, new DamageInfo() { DamageInstances = [new DamageInstanceInfo() { Damage = 10 }] }, null));

            Assert.AreEqual(1, entity.Cooldowns.Count());
            Assert.AreEqual(entity.Resources.GetHealth().MaxValue, entity.Resources.GetHealth().Value);
            Assert.IsTrue(logger.GetBattleLog().Trim() == "20");

            entity.EventHandler.OnGetHit(new BattleEventArgs<DamageInfo>(null, logger, rng, entity, null, new DamageInfo() { DamageInstances = [new DamageInstanceInfo() { Damage = 10 }] }, null));

            Assert.AreEqual(entity.Resources.GetHealth().MaxValue - 10, entity.Resources.GetHealth().Value);

            Console.WriteLine(logger.GetBattleLog(BattleLogVerbosity.Technical));
        }

        [TestMethod]
        public void TestSpellBuff()
        {
            var entity = new BaseBattleEntity("Test", 1, null, null, null, null);
            var buffs = new BuffList();
            var spell = new TestSpell(buffs);
            var logger = new BasicBattleLogger();
            var rng = new Random(1);

            buffs.Add(new SpellPowerBuff(null, 2, BuffFlags.Multiplicative));
            buffs.Add(new MultiplicativeSpellCostBuff(null, 2));

            spell.Hook(entity.EventHandler);

            Assert.AreEqual(0, entity.Cooldowns.Count());
            Assert.AreEqual(entity.Resources.GetHealth().MaxValue, entity.Resources.GetHealth().Value);

            entity.EventHandler.OnGetHit(new BattleEventArgs<DamageInfo>(null, logger, rng, entity, null, new DamageInfo() { DamageInstances = [new DamageInstanceInfo() { Damage = 10 }] }, null));

            Assert.AreEqual(1, entity.Cooldowns.Count());
            Assert.AreEqual(entity.Resources.GetHealth().MaxValue - 70, entity.Resources.GetHealth().Value);
            Assert.IsTrue(logger.GetBattleLog().Trim() == "40");

            entity.EventHandler.OnGetHit(new BattleEventArgs<DamageInfo>(null, logger, rng, entity, null, new DamageInfo() { DamageInstances = [new DamageInstanceInfo() { Damage = 10 }] }, null));

            Assert.AreEqual(entity.Resources.GetHealth().MaxValue - 70 - 10, entity.Resources.GetHealth().Value);

            Console.WriteLine(logger.GetBattleLog(BattleLogVerbosity.Technical));
        }

        [TestMethod]
        public void TestSpellCustomPrecon()
        {
            var entity = new BaseBattleEntity("Test", 1, null, null, null, null);
            var spell = new TestSpellCustomPrecon();
            var logger = new BasicBattleLogger();
            var rng = new Random(1);

            spell.Hook(entity.EventHandler);

            Assert.AreEqual(0, entity.Cooldowns.Count());
            Assert.AreEqual(entity.Resources.GetHealth().MaxValue, entity.Resources.GetHealth().Value);

            entity.EventHandler.OnGetHit(new BattleEventArgs<DamageInfo>(null, logger, rng, entity, null, new DamageInfo() { DamageInstances = [new DamageInstanceInfo() { Damage = 10 }] }, null));

            Assert.AreEqual(0, entity.Cooldowns.Count());
            Assert.AreEqual(entity.Resources.GetHealth().MaxValue - 10, entity.Resources.GetHealth().Value);

            Console.WriteLine(logger.GetBattleLog(BattleLogVerbosity.Technical));
        }
    }

    public class TestSpell : BaseSpell<DamageInfo>
    {
        public TestSpell(IBuffsAccessor buffs = null) : base(BasicBattleEntityEventsDeclarationsExtensions.OnGetHitDeclaration, [], BasicResourcesDeclarationsExtensions.HealthResourceKey, CooldownTypeFlags.OnTurnStart, 250)
        {
            Stats.Add(new SpellCooldownStat(Key, 5, buffs));
            Stats.Add(new SpellCostStat(Key, 35, buffs));
            Stats.Add(new SpellPowerStat(Key, 2, buffs));
        }

        public override string Key => "Test";

        protected override string SpellDescription => "";

        public override string Details => "";

        protected override bool BattleEventAction(BattleEventArgs<DamageInfo> args)
        {
            args.Log($"{args.Data.TotalAttack * Stats.GetSpellPower()}");
            return true;
        }
    }

    public class TestSpellNoCooldown : BaseSpell<DamageInfo>
    {
        public TestSpellNoCooldown() : base(BasicBattleEntityEventsDeclarationsExtensions.OnGetHitDeclaration, [], BasicResourcesDeclarationsExtensions.HealthResourceKey, null, 250)
        {
            Stats.Add(new SpellCooldownStat(Key, 5));
            Stats.Add(new SpellCostStat(Key, 35));
            Stats.Add(new SpellPowerStat(Key, 2));
        }

        public override string Key => "Test";

        protected override string SpellDescription => "";

        public override string Details => "";

        protected override bool BattleEventAction(BattleEventArgs<DamageInfo> args)
        {
            args.Log($"{args.Data.TotalAttack * Stats.GetSpellPower()}");
            return true;
        }
    }

    public class TestSpellNoCost : BaseSpell<DamageInfo>
    {
        public TestSpellNoCost() : base(BasicBattleEntityEventsDeclarationsExtensions.OnGetHitDeclaration, [], null, CooldownTypeFlags.OnTurnStart, 250)
        {
            Stats.Add(new SpellCooldownStat(Key, 5));
            Stats.Add(new SpellCostStat(Key, 35));
            Stats.Add(new SpellPowerStat(Key, 2));
        }

        public override string Key => "Test";

        protected override string SpellDescription => "";

        public override string Details => "";

        protected override bool BattleEventAction(BattleEventArgs<DamageInfo> args)
        {
            args.Log($"{args.Data.TotalAttack * Stats.GetSpellPower()}");
            return true;
        }
    }

    public class TestSpellCustomPrecon : BaseSpell<DamageInfo>
    {
        public TestSpellCustomPrecon() : base(BasicBattleEntityEventsDeclarationsExtensions.OnGetHitDeclaration, [], BasicResourcesDeclarationsExtensions.HealthResourceKey, CooldownTypeFlags.OnTurnStart, 250)
        {
            Stats.Add(new SpellCooldownStat(Key, 5));
            Stats.Add(new SpellCostStat(Key, 35));
            Stats.Add(new SpellPowerStat(Key, 2));
        }

        public override string Key => "Test";

        protected override string SpellDescription => "";

        public override string Details => "";

        protected override bool BattleEventAction(BattleEventArgs<DamageInfo> args)
        {
            args.Log($"{args.Data.TotalAttack * Stats.GetSpellPower()}");
            return true;
        }
        protected override bool Precondition(BattleEventArgs<DamageInfo> args) => false;
    }
}
