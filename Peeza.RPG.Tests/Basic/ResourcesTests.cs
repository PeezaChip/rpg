﻿using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Resources;
using Peeza.RPG.Core.Basic.Stats;
using Peeza.RPG.Core.Realizations.Buffs;

namespace Peeza.RPG.Tests.Basic
{
    [TestClass]
    public class ResourcesTests
    {
        [TestMethod]
        public void TestBarPercentage()
        {
            var minValue = 10;
            var maxValue = 110;
            var percentage = 0.8;

            var bar = new SimpleResource("test", maxValue, minValue, true) as Resource;
            Assert.AreEqual(1, bar.Percentage, "Full bar percentage is wrong");

            bar.Value -= maxValue;
            Assert.AreEqual(0, bar.PercentageRelativeToMin, "Empty bar percentage is wrong");
            Assert.AreEqual(10 * 1.0 / maxValue, bar.Percentage, "Empty bar percentage is wrong");

            var bar2 = new SimpleResource("test", maxValue, minValue + (int)((maxValue - minValue) * percentage), minValue) as Resource;
            Assert.AreEqual(percentage, bar2.PercentageRelativeToMin, "Bar percentage is wrong");
        }

        [TestMethod]
        public void TestEdges()
        {
            var maxValue = 10;
            var minValue = 1;

            var bar = new SimpleResource("test", maxValue, minValue, true) as Resource;

            bar.Value += int.MaxValue;
            Assert.AreEqual(maxValue, bar.Value, "Bar value exceeds maximum value");

            bar.Value -= int.MaxValue;
            Assert.AreEqual(minValue, bar.Value, "Bar value exceeds minimum value");
        }

        [TestMethod]
        public void TestEmpty()
        {
            var maxValue = 10;
            var minValue = 1;

            var bar = new SimpleResource("test", maxValue, minValue, true) as Resource;

            bar.Value -= int.MaxValue;
            Assert.IsFalse(bar.IsEmpty, "Bar IsEmpty ignores min value");

            bar = new SimpleResource("test", maxValue, 0, true);

            bar.Value -= int.MaxValue;
            Assert.IsTrue(bar.IsEmpty, "Bar IsEmpty wrong value");

            var negativeBar = new SimpleResource("test", maxValue, -maxValue, true) as Resource;
            negativeBar.Value -= int.MaxValue;

            Assert.IsTrue(bar.IsEmpty, "Bar IsEmpty wrong value");
        }

        [TestMethod]
        public void TestIncorrectMinMax()
        {
            var maxValue = 10;
            var minValue = 100;

            var bar = new SimpleResource("test", maxValue, minValue, true) as Resource;

            // alawys set to minimum value
            Assert.AreEqual(bar.MaxValue, bar.MinValue);
            Assert.AreEqual(bar.Value, bar.MinValue);
            Assert.AreEqual(bar.Value, minValue);
        }

        [TestMethod]
        public void TestBuffableMaxValue()
        {
            var maxValue = 100;
            var buffValue = 50;

            var buffs = new BuffList();

            var bar = new BuffableResource("test", buffs, maxValue, 0, true) as Resource;

            // positive buff test
            Assert.AreEqual(1, bar.Percentage, "Full bar percentage is wrong");
            buffs.Add(new ResourceBuff("test", null, ResourceBuffEnum.Max, buffValue, BuffFlags.None, 0));
            Assert.AreEqual(maxValue * 1.0 / (maxValue + buffValue), bar.Percentage);

            // if value was read (or written) while under buff, real value SHOULD be updated
            bar.Value += int.MaxValue;
            Assert.AreEqual(maxValue + buffValue, bar.Value);
            buffs.Clear();
            Assert.AreEqual(maxValue, bar.Value);

            // if value was not read (or written) while under buff, real value SHOULD NOT be updated
            bar.Value += int.MaxValue;
            buffs.Add(new ResourceBuff("test", null, ResourceBuffEnum.Min, buffValue, BuffFlags.None, 0));
            buffs.Clear();
            Assert.AreEqual(maxValue, bar.Value);

            // negative buff test
            Assert.AreEqual(1, bar.Percentage, "Full bar percentage is wrong");
            buffs.Add(new ResourceBuff("test", null, ResourceBuffEnum.Max, -buffValue, BuffFlags.None, 0));
            Assert.AreEqual(1, bar.Percentage, "Full bar percentage is wrong");

            // if value was read (or written) while under buff, real value SHOULD be updated
            bar.Value += int.MaxValue;
            Assert.AreEqual(maxValue - buffValue, bar.Value);
            buffs.Clear();
            Assert.AreEqual(maxValue - buffValue, bar.Value);

            // if value was read (or written) while under buff, real value SHOULD be updated
            buffs.Add(new ResourceBuff("test", null, ResourceBuffEnum.Min, -buffValue, BuffFlags.None, 0));
            buffs.Clear();
            Assert.AreEqual(maxValue - buffValue, bar.Value);
        }

        [TestMethod]
        public void TestWildcardBuffs()
        {
            var maxValue = 100;
            var buffValue = 10;
            var wildcardBuffValue = 30;

            var buffs = new BuffList();

            var bar = new BuffableResource("test", buffs, maxValue, 0, true) as Resource;
            var bar2 = new BuffableResource("test2", buffs, maxValue, 0, true) as Resource;
            buffs.Add(new ResourceBuff("test", null, ResourceBuffEnum.Max, buffValue, BuffFlags.None, 0));
            buffs.Add(new ResourceBuff("*", null, ResourceBuffEnum.Max, wildcardBuffValue, BuffFlags.None, 0));
            buffs.Add(new Buff<long>("test", null, buffValue, BuffFlags.None, 0));
            buffs.Add(new Buff<long>("*", null, wildcardBuffValue, BuffFlags.None, 0));

            Assert.AreEqual(maxValue + buffValue + wildcardBuffValue, bar.MaxValue);
            Assert.AreEqual(maxValue + wildcardBuffValue, bar2.MaxValue);
        }

        [TestMethod]
        public void TestBuffableMinValue()
        {
            var maxValue = 100;
            var minValue = 50;

            var buffValue = 25;

            var buffs = new BuffList();

            var bar = new BuffableResource("test", buffs, maxValue, maxValue, minValue) as Resource;

            // positive buff test
            Assert.AreEqual(1, bar.PercentageRelativeToMin, "Full bar percentage is wrong");
            buffs.Add(new ResourceBuff("test", null, ResourceBuffEnum.Min, buffValue, BuffFlags.None, 0));
            Assert.AreEqual(1, bar.PercentageRelativeToMin);

            // if value was read (or written) while under buff, real value SHOULD be updated
            bar.Value -= int.MaxValue;
            Assert.AreEqual(minValue + buffValue, bar.Value);
            buffs.Clear();
            Assert.AreEqual(minValue + buffValue, bar.Value);

            // if value was not read (or written) while under buff, real value SHOULD NOT be updated
            bar.Value -= int.MaxValue;
            buffs.Add(new ResourceBuff("test", null, ResourceBuffEnum.Min, buffValue, BuffFlags.None, 0));
            buffs.Clear();
            Assert.AreEqual(minValue, bar.Value);

            // negative buff test
            buffs.Add(new ResourceBuff("test", null, ResourceBuffEnum.Min, -buffValue, BuffFlags.None, 0));

            // if value was read (or written) while under buff, real value SHOULD be updated
            bar.Value -= int.MaxValue;
            Assert.AreEqual(minValue - buffValue, bar.Value);
            buffs.Clear();
            Assert.AreEqual(minValue, bar.Value);

            // if value was not read (or written) while under buff, real value SHOULD NOT be updated
            buffs.Add(new ResourceBuff("test", null, ResourceBuffEnum.Min, -buffValue, BuffFlags.None, 0));
            buffs.Clear();
            Assert.AreEqual(minValue, bar.Value);
        }

        [TestMethod]
        public void TestMultiplicativeBuff()
        {
            var maxValue = 100;
            var buffValue = 1.5;

            var buffs = new BuffList();

            var bar = new BuffableResource("test", buffs, maxValue, 0, true) as Resource;

            // positive buff test
            Assert.AreEqual(1, bar.Percentage, "Full bar percentage is wrong");
            buffs.Add(new ResourceBuff("test", null, ResourceBuffEnum.Max, buffValue, BuffFlags.Multiplicative, 0));
            Assert.AreEqual(maxValue * 1.0 / (maxValue * buffValue), bar.Percentage);

            // if value was read (or written) while under buff, real value SHOULD be updated
            bar.Value += int.MaxValue;
            Assert.AreEqual(maxValue * buffValue, bar.Value);
            buffs.Clear();
            Assert.AreEqual(maxValue, bar.Value);

            // if value was not read (or written) while under buff, real value SHOULD NOT be updated
            bar.Value += int.MaxValue;
            buffs.Add(new ResourceBuff("test", null, ResourceBuffEnum.Min, buffValue, BuffFlags.Multiplicative, 0));
            buffs.Clear();
            Assert.AreEqual(maxValue, bar.Value);

            buffValue = 0.5;

            // negative buff test
            Assert.AreEqual(1, bar.Percentage, "Full bar percentage is wrong");
            buffs.Add(new ResourceBuff("test", null, ResourceBuffEnum.Max, buffValue, BuffFlags.Multiplicative, 0));
            Assert.AreEqual(1, bar.Percentage, "Full bar percentage is wrong");

            // if value was read (or written) while under buff, real value SHOULD be updated
            bar.Value += int.MaxValue;
            Assert.AreEqual(maxValue * buffValue, bar.Value);
            buffs.Clear();
            Assert.AreEqual(maxValue * buffValue, bar.Value);

            // if value was read (or written) while under buff, real value SHOULD be updated
            buffs.Add(new ResourceBuff("test", null, ResourceBuffEnum.Min, buffValue, BuffFlags.Multiplicative, 0));
            buffs.Clear();
            Assert.AreEqual(maxValue * buffValue, bar.Value);
        }

        [TestMethod]
        public void TestBuffableIncorrectMinMax()
        {
            var maxValue = 10;
            var minValue = 100;
            var buffValue = 50;

            var buffs = new BuffList();

            var bar = new BuffableResource("test", buffs, maxValue, minValue, true) as Resource;

            // alawys set to minimum value
            Assert.AreEqual(bar.MaxValue, bar.MinValue);
            Assert.AreEqual(bar.Value, bar.MinValue);
            Assert.AreEqual(bar.Value, minValue);

            buffs.Add(new ResourceBuff("test", null, ResourceBuffEnum.Min, buffValue, BuffFlags.None, 0));
            // alawys set to minimum value
            Assert.AreEqual(bar.MaxValue, bar.MinValue);
            Assert.AreEqual(bar.Value, bar.MinValue);
            Assert.AreEqual(bar.Value, minValue + buffValue);

            buffs.Clear();

            buffs.Add(new ResourceBuff("test", null, ResourceBuffEnum.Max, -buffValue, BuffFlags.None, 0));
            // alawys set to minimum value
            Assert.AreEqual(bar.MaxValue, bar.MinValue);
            Assert.AreEqual(bar.Value, bar.MinValue);
            Assert.AreEqual(bar.Value, minValue);
        }
    }
}
