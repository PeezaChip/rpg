﻿using Peeza.RPG.Base.Battle;
using Peeza.RPG.Base.Battle.BattleEntities;
using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.BattleFields;
using Peeza.RPG.Core.Basic.Interfaces;
using Peeza.RPG.Core.Heroes;
using Peeza.RPG.Core.Realizations.Battle;
using Peeza.RPG.Core.Realizations.BattleFields;
using Peeza.RPG.Core.Realizations.Fights;
using Peeza.RPG.Core.Realizations.Fights.RelationshipManagers;
using Peeza.RPG.Tests.Core;

namespace Peeza.RPG.Tests.LongRunning.Fights
{
    [TestClass]
    public class FightTest
    {
        [TestMethod]
        public async Task TestFightBig()
        {
            var random = new Random(222);
            var logger = new ConsoleBattleLogger();

            var fieldAccessor = new BattleFieldAccessor();
            var roundHandler = new BasicRoundHandler(new BasicTurnHandler(fieldAccessor), random, fieldAccessor, logger);
            var battleEventArgsMaker = new BasicBattleEventArgsMaker(roundHandler.GetQueueAccessor(), logger, random, new BattleFieldAccessor());
            var battleSidesAccessor = new BasicBattleSidesAccessor();
            var relationshipManager = new BrawlRelationshipManager(battleSidesAccessor);
            battleSidesAccessor.Add([GenerateHeroBattleSide("Human", battleEventArgsMaker, relationshipManager, fieldAccessor), GenerateHeroBattleSide("Undead", battleEventArgsMaker, relationshipManager, fieldAccessor)]);
            var fightResolver = new BasicFightResolver(battleSidesAccessor, roundHandler, fieldAccessor, logger: logger);

            var cts = new CancellationTokenSource();
            await fightResolver.ProccessFight(cts.Token);
        }

        [TestMethod]
        public async Task TestFightBigRandom()
        {
            var undeadWon = 0;
            var humanWon = 0;

            var sharedRandom = new Random();

            await Parallel.ForAsync(0, 10000, async (i, ct) => {
                var random = new Random(sharedRandom.Next(int.MaxValue));
                var logger = new BasicBattleLogger();

                var fieldAccessor = new BattleFieldAccessor();
                var roundHandler = new BasicRoundHandler(new BasicTurnHandler(fieldAccessor), random, fieldAccessor, logger);
                var battleEventArgsMaker = new BasicBattleEventArgsMaker(roundHandler.GetQueueAccessor(), logger, random, new BattleFieldAccessor());
                var battleSidesAccessor = new BasicBattleSidesAccessor();
                var relationshipManager = new BrawlRelationshipManager(battleSidesAccessor);
                battleSidesAccessor.Add([GenerateHeroBattleSide("Human", battleEventArgsMaker, relationshipManager, fieldAccessor), GenerateHeroBattleSide("Undead", battleEventArgsMaker, relationshipManager, fieldAccessor)]);
                var fightResolver = new BasicFightResolver(battleSidesAccessor, roundHandler, fieldAccessor, logger: logger);

                await fightResolver.ProccessFight(ct);

                var bs = battleSidesAccessor.GetAll().First(bs => bs.BattleEntities.Any(e => e.CanFight));
                if ((bs as NamedBattleSide).Name == "Undead")
                {
                    undeadWon++;
                }
                else
                {
                    humanWon++;
                }
            });
            Console.WriteLine($"H: {humanWon}; U: {undeadWon}");
        }

        private static IBattleSide GenerateHeroBattleSide(string race, IBattleEventArgsMaker battleEventArgsMaker, IBattleSideRelationshipManager relationshipManager, IBattleFieldAccessor battleFieldAccessor)
        {
            var battleSide = new NamedBattleSide(race);
            foreach (var spec in RPG.SpecDeclarations)
            {
                if (spec.Key == "Cleric" && race == "Undead")
                {
                    var be = new HeroBattleEntity(new Hero() { Level = 1, Name = $"{race} King" }, RPG.RaceDeclarations.GetByKey(race), RPG.SpecDeclarations.GetByKey("Mage"), battleSide, battleEventArgsMaker, relationshipManager, battleFieldAccessor);
                    battleSide.Add(be);
                }
                else
                {
                    var be = new HeroBattleEntity(new Hero() { Level = 1, Name = $"{race} {spec.Key}" }, RPG.RaceDeclarations.GetByKey(race), spec, battleSide, battleEventArgsMaker, relationshipManager, battleFieldAccessor);
                    battleSide.Add(be);
                }
            }
            return battleSide;
        }
    }
}
