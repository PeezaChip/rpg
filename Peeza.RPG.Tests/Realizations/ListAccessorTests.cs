﻿using Peeza.RPG.Core.Basic.Generic;
using Peeza.RPG.Core.Realizations.Generic;

namespace Peeza.RPG.Tests.Realizations
{
    [TestClass]
    public class ListAccessorTests
    {
        [TestMethod]
        public void TestListAccessor()
        {
            var list = new EntityListAccessor<Entity>();

            Assert.IsFalse(list.GetAll().Any());

            var entity = new Entity();

            list.Clear();
            list.Remove(entity);

            list.Add(entity);

            Assert.AreSame(entity, list.GetAll().First());

            list.Clear();

            Assert.IsFalse(list.GetAll().Any());

            list.Add(entity);
            list.Remove(entity);

            Assert.IsFalse(list.GetAll().Any());
        }

        [TestMethod]
        public void TestKeyedListAccessor()
        {
            var list = new KeyedEntityListAccessor<KeyedEntity>();

            Assert.IsFalse(list.GetAll().Any());

            var entityA = new KeyedEntity("a");
            var entityB = new KeyedEntity("b");

            list.Clear();
            list.Remove(entityA);

            list.Add(entityA);
            list.Add(entityB);

            Assert.AreSame(entityA, list.GetAll().First());
            Assert.AreSame(entityB, list.GetAll().Last());

            Assert.AreSame(entityA, list.GetAll("a").First());
            Assert.AreSame(entityB, list.GetAll("b").First());

            list.Clear();

            Assert.IsFalse(list.GetAll().Any());

            list.Add(entityA);
            list.Remove(entityA);

            Assert.IsFalse(list.GetAll().Any());
        }

        private class Entity
        {

        }

        private class KeyedEntity : IRPGKey
        {
            public string Key { get; private set; }

            public KeyedEntity(string key)
            {
                Key = key;
            }
        }
    }
}
