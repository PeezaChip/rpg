﻿using Peeza.RPG.Core.Basic.Generic;
using Peeza.RPG.Core.Realizations.Generic;

namespace Peeza.RPG.Tests.Realizations
{
    [TestClass]
    public class RemovableQueueTests
    {
        [TestMethod]
        public void TestRemovingFromQueue()
        {
            var queue = new RemovableQueue<KeyedEntity>();

            var a = new KeyedEntity("a");
            var b = new KeyedEntity("b");
            var c = new KeyedEntity("c");

            queue.Enqueue(a);
            queue.Enqueue(b);
            queue.Enqueue(c);

            Assert.AreEqual(a, queue.Dequeue());
            Assert.AreEqual(b, queue.Dequeue());
            Assert.AreEqual(c, queue.Dequeue());
            Assert.ThrowsException<InvalidOperationException>(() => queue.Dequeue());

            queue.Enqueue(b);
            queue.Enqueue(a);
            queue.Enqueue(b);
            queue.Enqueue(c);
            queue.Enqueue(b);

            queue.Remove(b);
            Assert.AreEqual(a, queue.Dequeue());
            Assert.AreEqual(c, queue.Dequeue());
            Assert.ThrowsException<InvalidOperationException>(() => queue.Dequeue());

            queue.Enqueue(a);
            queue.Enqueue(b);
            queue.Enqueue(c);

            queue.RemoveRange(a, c);
            Assert.AreEqual(b, queue.Dequeue());
            Assert.ThrowsException<InvalidOperationException>(() => queue.Dequeue());
        }

        private class KeyedEntity : IRPGKey
        {
            public string Key { get; private set; }

            public KeyedEntity(string key)
            {
                Key = key;
            }
        }
    }
}
