﻿using Peeza.RPG.Base.Battle.Events;

namespace Peeza.RPG.Tests.Battle.Formulas
{
    [TestClass]
    public class DefenseFormulasTest
    {
        [TestMethod]
        public void TestZeroArmor()
        {
            Assert.AreEqual(1, BasicBattleEntityEventsImplementations.CalculateArmorMitigation(0, 1, 1));
        }

        [TestMethod]
        public void TestSoftCapArmor()
        {
            Assert.AreEqual(0.5, BasicBattleEntityEventsImplementations.CalculateArmorMitigation(150, 1, 1));
        }

        [TestMethod]
        public void TestLevelDiffArmor()
        {
            Assert.IsTrue(BasicBattleEntityEventsImplementations.CalculateArmorMitigation(150, 1, 2) > 0.5);
            Assert.IsTrue(BasicBattleEntityEventsImplementations.CalculateArmorMitigation(150, 2, 1) < 0.5);
        }

        [TestMethod]
        public void TestNegativeArmor()
        {
            Assert.AreEqual(1.4, BasicBattleEntityEventsImplementations.CalculateArmorMitigation(-200, 1, 1));
        }

        [TestMethod]
        public void TestZeroMR()
        {
            Assert.AreEqual(1, BasicBattleEntityEventsImplementations.CalculateMagicMitigation(0, 1, 1));
        }

        [TestMethod]
        public void TestSoftCapMR()
        {
            Assert.AreEqual(0.5, BasicBattleEntityEventsImplementations.CalculateMagicMitigation(250, 1, 1));
        }

        [TestMethod]
        public void TestLevelDiffMR()
        {
            Assert.IsTrue(BasicBattleEntityEventsImplementations.CalculateMagicMitigation(250, 1, 2) > 0.5);
            Assert.IsTrue(BasicBattleEntityEventsImplementations.CalculateMagicMitigation(250, 2, 1) < 0.5);
        }

        [TestMethod]
        public void TestNegativeMR()
        {
            Assert.AreEqual(1.6, BasicBattleEntityEventsImplementations.CalculateMagicMitigation(-300, 1, 1));
        }
    }
}
