﻿using Peeza.RPG.Base.Battle.Arguments.Damage;
using Peeza.RPG.Base.Battle.Arguments.Heal;
using Peeza.RPG.Base.Battle.BattleEntities;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Base.Resources;
using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Battle.Events.Attributes;
using Peeza.RPG.Core.Battle.Events.Helpers;
using Peeza.RPG.Core.Battle.Events.Interfaces;
using Peeza.RPG.Core.Realizations.Battle;

namespace Peeza.RPG.Tests.Battle.BaseExpansionEvents
{
    [TestClass]
    public class BattleDefenseTests
    {
        [TestMethod]
        public void OnProcessDamageInstanceCheckResult()
        {
            var entity = CreateFullEntity("Entity");
            BattleEventLoader.LoadEventActionsFromType(typeof(BattleEventDefenseDontDamage), entity.EventHandler);
            var log = new BasicBattleLogger();

            var damageInfo = new DamageInstanceInfo() { Damage = 10, Result = DamageInstanceInfoResultFlags.Done };
            var args = CreateArgs(damageInfo, log, entity);
            entity.EventHandler.OnProcessDamageInstance(args);
            Assert.IsTrue(log.GetBattleLog().Length == 0);
            damageInfo.Result = DamageInstanceInfoResultFlags.None;

            entity.Stats.Add(new DodgeChanceStat(1));
            entity.EventHandler.OnProcessDamageInstance(args);
            Assert.IsTrue(damageInfo.Result == DamageInstanceInfoResultFlags.Dodge);
            Assert.IsTrue(damageInfo.Damage == 0);
            damageInfo.Damage = 10;
            damageInfo.Result = DamageInstanceInfoResultFlags.None;

            damageInfo.Modifiers = DamageInstanceInfoModifierFlags.CantDodge;
            entity.EventHandler.OnProcessDamageInstance(args);
            Assert.IsTrue(damageInfo.Result == DamageInstanceInfoResultFlags.None);
            Assert.IsTrue(damageInfo.Damage == 10);
            damageInfo.Modifiers = DamageInstanceInfoModifierFlags.None;
            entity.Stats.RemoveByKey(BasicStatsDeclarationsExtensions.DodgeChanceStatKey);

            entity.Stats.Add(new BlockChanceStat(1));
            entity.EventHandler.OnProcessDamageInstance(args);
            Assert.IsTrue(damageInfo.Result == DamageInstanceInfoResultFlags.Block);
            Assert.IsTrue(damageInfo.Damage == 0);
            damageInfo.Damage = 10;
            damageInfo.Result = DamageInstanceInfoResultFlags.None;

            damageInfo.Modifiers = DamageInstanceInfoModifierFlags.CantBlock;
            entity.EventHandler.OnProcessDamageInstance(args);
            Assert.IsTrue(damageInfo.Result == DamageInstanceInfoResultFlags.None);
            Assert.IsTrue(damageInfo.Damage == 10);
            entity.Stats.RemoveByKey(BasicStatsDeclarationsExtensions.BlockChanceStatKey);

            entity.EventHandler.OnProcessDamageInstance(args);
            Assert.IsTrue(damageInfo.Damage == 10);
            Assert.IsTrue(damageInfo.Status.HasFlag(DamageInstanceInfoStatusFlags.DefenseCalculated));
        }

        [TestMethod]
        public void OnDoDamage()
        {
            var entity = CreateFullEntity("Entity");
            var log = new BasicBattleLogger();

            entity.Resources.GetHealth().Value = 50;
            entity.Resources.GetShield().Value = 50;
            var damageInfo = new DamageInstanceInfo() { Damage = 10, Status = DamageInstanceInfoStatusFlags.DefenseCalculated | DamageInstanceInfoStatusFlags.DamageDone };
            var args = CreateArgs(damageInfo, log, entity);
            entity.EventHandler.OnDoDamage(args);

            Assert.IsTrue(entity.Resources.GetHealth().Value == 50);
            Assert.IsTrue(entity.Resources.GetShield().Value == 50);
            damageInfo.Status = DamageInstanceInfoStatusFlags.DefenseCalculated;
            damageInfo.Damage = 0;

            entity.EventHandler.OnDoDamage(args);
            Assert.IsTrue(entity.Resources.GetHealth().Value == 50);
            Assert.IsTrue(entity.Resources.GetShield().Value == 50);
            Assert.IsTrue(damageInfo.Status.HasFlag(DamageInstanceInfoStatusFlags.DamageDone));
            Assert.IsTrue(damageInfo.Result.HasFlag(DamageInstanceInfoResultFlags.Done));
            damageInfo.Damage = 10;
            damageInfo.Status = DamageInstanceInfoStatusFlags.DefenseCalculated;
            damageInfo.Result = DamageInstanceInfoResultFlags.None;

            entity.EventHandler.OnDoDamage(args);
            Assert.IsTrue(entity.Resources.GetHealth().Value == 50);
            Assert.IsTrue(entity.Resources.GetShield().Value == 40);
            Assert.IsTrue(damageInfo.Status.HasFlag(DamageInstanceInfoStatusFlags.DamageDone));
            Assert.IsTrue(damageInfo.Result.HasFlag(DamageInstanceInfoResultFlags.Done));
            Assert.IsTrue(damageInfo.DamageDealt == 10);
            Assert.IsTrue(damageInfo.DamageLeft == 0);
            Assert.IsTrue(damageInfo.Damage == 10);
            damageInfo.Damage = 10;
            damageInfo.DamageDealt = 0;
            damageInfo.Result = DamageInstanceInfoResultFlags.None;
            damageInfo.Status = DamageInstanceInfoStatusFlags.DefenseCalculated;
            entity.Resources.GetShield().Value = 5;

            entity.EventHandler.OnDoDamage(args);
            Assert.IsTrue(entity.Resources.GetHealth().Value == 45);
            Assert.IsTrue(entity.Resources.GetShield().Value == 0);
            Assert.IsTrue(damageInfo.Status.HasFlag(DamageInstanceInfoStatusFlags.DamageDone));
            Assert.IsTrue(damageInfo.Result.HasFlag(DamageInstanceInfoResultFlags.Done));
            Assert.IsTrue(damageInfo.DamageDealt == 10);
            Assert.IsTrue(damageInfo.DamageLeft == 0);
            Assert.IsTrue(damageInfo.Damage == 10);

            Console.WriteLine(log.GetBattleLog());
        }

        [TestMethod]
        public void OnDoHeal()
        {
            var entity = CreateFullEntity("Entity");
            var log = new BasicBattleLogger();

            entity.Resources.GetHealth().Value = 50;
            entity.Resources.GetShield().Value = 50;
            var healInfo = new HealInstanceInfo() { Heal = 10, Result = HealInstanceInfoResultFlags.Done };
            var args = CreateArgs(healInfo, log, entity);
            entity.EventHandler.OnProcessHealInstance(args);

            Assert.IsTrue(entity.Resources.GetHealth().Value == 50);
            Assert.IsTrue(entity.Resources.GetShield().Value == 50);
            Assert.IsTrue(healInfo.HealDealt == 0);
            args.Data.Result = HealInstanceInfoResultFlags.None;

            // this is correct since we dont have any flags to specify which resource to heal
            entity.EventHandler.OnDoHeal(args);
            Assert.IsTrue(entity.Resources.GetHealth().Value == 50);
            Assert.IsTrue(entity.Resources.GetShield().Value == 50);
            Assert.IsTrue(healInfo.HealDealt == 0);
            args.Data.Flags = HealInstanceInfoFlags.Health;
            args.Data.Heal = 0;

            entity.EventHandler.OnDoHeal(args);
            Assert.IsTrue(entity.Resources.GetHealth().Value == 50);
            Assert.IsTrue(entity.Resources.GetShield().Value == 50);
            Assert.IsTrue(healInfo.HealDealt == 0);
            Assert.IsTrue(healInfo.Result == HealInstanceInfoResultFlags.Done);
            args.Data.Heal = 10;
            args.Data.Result = HealInstanceInfoResultFlags.None;

            entity.EventHandler.OnDoHeal(args);
            Assert.IsTrue(entity.Resources.GetHealth().Value == 60);
            Assert.IsTrue(entity.Resources.GetShield().Value == 50);
            Assert.IsTrue(healInfo.HealDealt == 10);
            Assert.IsTrue(healInfo.Result == HealInstanceInfoResultFlags.Done);
            args.Data.Heal = 10;
            args.Data.HealDealt = 0;
            args.Data.Result = HealInstanceInfoResultFlags.None;
            entity.Resources.GetHealth().Value = 95;

            entity.EventHandler.OnDoHeal(args);
            Assert.IsTrue(entity.Resources.GetHealth().Value == 100);
            Assert.IsTrue(entity.Resources.GetShield().Value == 50);
            Assert.IsTrue(healInfo.HealDealt == 5);
            Assert.IsTrue(healInfo.HealLeft == 5);
            Assert.IsTrue(healInfo.Result == HealInstanceInfoResultFlags.None);
            args.Data.Heal = 10;
            args.Data.HealDealt = 0;
            args.Data.Result = HealInstanceInfoResultFlags.None;
            entity.Resources.GetHealth().Value = 95;
            args.Data.Flags |= HealInstanceInfoFlags.Shield;

            entity.EventHandler.OnDoHeal(args);
            Assert.IsTrue(entity.Resources.GetHealth().Value == 100);
            Assert.IsTrue(entity.Resources.GetShield().Value == 55);
            Assert.IsTrue(healInfo.HealDealt == 10);
            Assert.IsTrue(healInfo.HealLeft == 0);
            Assert.IsTrue(healInfo.Result == HealInstanceInfoResultFlags.Done);
            args.Data.Heal = 10;
            args.Data.HealDealt = 0;
            args.Data.Result = HealInstanceInfoResultFlags.None;
            entity.Resources.GetHealth().Value = 95;
            entity.Resources.GetShield().Value = 50;
            args.Data.Flags = HealInstanceInfoFlags.Shield;

            entity.EventHandler.OnDoHeal(args);
            Assert.IsTrue(entity.Resources.GetHealth().Value == 95);
            Assert.IsTrue(entity.Resources.GetShield().Value == 60);
            Assert.IsTrue(healInfo.HealDealt == 10);
            Assert.IsTrue(healInfo.HealLeft == 0);
            Assert.IsTrue(healInfo.Result == HealInstanceInfoResultFlags.Done);

            Console.WriteLine(log.GetBattleLog());
        }

        [TestMethod]
        public void OnGetHit()
        {
            var entity = CreateFullEntity("Entity");
            var log = new BasicBattleLogger();

            entity.Resources.GetHealth().Value = 50;
            entity.Resources.GetShield().Value = 50;
            var damageInfo = new DamageInfo()
            {
                DamageInstances = [
                    new() { Damage = 10, Status = DamageInstanceInfoStatusFlags.DefenseCalculated },
                    new() { Damage = 20, Status = DamageInstanceInfoStatusFlags.DefenseCalculated },
                ]
            };
            var args = CreateArgs(damageInfo, log, entity);

            entity.EventHandler.OnGetHit(args);
            Assert.IsTrue(entity.Resources.GetShield().Value == 20);
        }

        [TestMethod]
        public void OnGetHeal()
        {
            var entity = CreateFullEntity("Entity");
            var log = new BasicBattleLogger();

            entity.Resources.GetHealth().Value = 50;
            entity.Resources.GetShield().Value = 50;
            var healInfo = new HealInfo()
            {
                HealInstances = [
                    new() { Heal = 10, Flags = HealInstanceInfoFlags.Health },
                    new() { Heal = 20, Flags = HealInstanceInfoFlags.Shield },
                ]
            };
            var args = CreateArgs(healInfo, log, entity);

            entity.EventHandler.OnGetHeal(args);
            Assert.IsTrue(entity.Resources.GetShield().Value == 70);
            Assert.IsTrue(entity.Resources.GetHealth().Value == 60);
        }

        private static BasicBattleEntity CreateFullEntity(string name)
        {
            var entity = new BasicBattleEntity(name, null, null, null, null);
            entity.Resources.Add(new HealthResource(100));
            entity.Resources.Add(new ShieldResource(100));
            HookBaseEvents(entity.EventHandler);
            return entity;
        }

        private static void HookBaseEvents(IBattleEventHandlersProvider hookTo)
        {
            BattleEventLoader.LoadEventActionsFromType(typeof(BasicBattleEntityEventsImplementations), hookTo);
        }

        private static readonly Random random = new(222);
        private static BattleEventArgs<T> CreateArgs<T>(T data, IBattleLogger logger, IBattleEntity self, BattleQueue queue = null, IBattleEntity target = null)
        {
            return new BattleEventArgs<T>(queue, logger, random, self, target, data, null);
        }

        private class BattleEventDefenseDontDamage
        {
            [BattleEventAction(typeof(OnDoDamage), Priority = 1000)]
            public static bool OnSelectedAsTargetChangeTest(BattleEventArgs<DamageInstanceInfo> args)
            {
                return true;
            }
        }
    }
}
