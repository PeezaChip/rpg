﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Base.Battle.Arguments.Damage;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Core.Basic.Ailments;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.BattleFields;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Cooldowns;
using Peeza.RPG.Core.Basic.Minions;
using Peeza.RPG.Core.Basic.Statuses;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Battle.Events.Helpers;
using Peeza.RPG.Core.Realizations.Ailments;
using Peeza.RPG.Core.Realizations.Auras;
using Peeza.RPG.Core.Realizations.Battle;
using Peeza.RPG.Core.Realizations.BattleEntities;
using Peeza.RPG.Core.Realizations.Buffs;
using Peeza.RPG.Core.Realizations.Cooldowns;
using Peeza.RPG.Core.Realizations.Statuses;
using Peeza.RPG.Tests.Core;

namespace Peeza.RPG.Tests.Battle.BaseExpansionEvents
{
    [TestClass]
    public class GlobalEventTests
    {
        [TestMethod]
        public void OnAnyTest()
        {
            var emptyEntity = CreateEmptyEntity();
            emptyEntity.EventHandler.OnAny(new BattleEventArgs(null, null, null, emptyEntity, null, null, null));


            var entity = CreateFullEntity();
            entity.EventHandler = new BaseEventHandlerProvider();
            HookBaseEvents(entity.EventHandler);

            var counter = 0;
            entity.EventHandler.HookEvent(BasicBattleSharedEventsDeclarationsExtensions.OnAnyDeclaration, (args) =>
            {
                counter++;
                return false;
            });

            entity.EventHandler.OnDungeon(CreateArgs(OnTimeEnum.Start, entity));

            Assert.AreEqual(1, counter);

            entity.EventHandler.OnGetHit(CreateArgs(new DamageInfo() { DamageInstances = [ new DamageInstanceInfo() { Damage = 10 } ] }, entity));

            Assert.AreEqual(6, counter);
        }

        [TestMethod]
        public void OnDungeonClearMinionsTest()
        {
            var emptyEntity = CreateEmptyEntity();
            emptyEntity.EventHandler.OnDungeon(CreateArgs(OnTimeEnum.End, emptyEntity));
            emptyEntity.EventHandler.OnDungeon(CreateArgs(OnTimeEnum.Start, emptyEntity));


            var entity = CreateFullEntity();

            entity.Minions.Add(new Minion(emptyEntity));

            entity.EventHandler.OnDungeon(CreateArgs(OnTimeEnum.End, entity));
            Assert.IsFalse(entity.Minions.Any());

            entity.Minions.Add(new Minion(emptyEntity));

            entity.EventHandler.OnDungeon(CreateArgs(OnTimeEnum.Start, entity));
            Assert.IsFalse(entity.Minions.Any());
        }

        [TestMethod]
        public void OnDungeonClearCooldowns()
        {
            var emptyEntity = CreateEmptyEntity();
            emptyEntity.EventHandler.OnDungeon(CreateArgs(OnTimeEnum.End, emptyEntity));
            emptyEntity.EventHandler.OnDungeon(CreateArgs(OnTimeEnum.Start, emptyEntity));

            var entity = CreateFullEntity();

            var cooldownStart = new Cooldown("cStart", CooldownTypeFlags.OnDungeonStart);
            var cooldownEnd = new Cooldown("cEnd", CooldownTypeFlags.OnDungeonEnd);
            var cooldownAny = new Cooldown("cAny", CooldownTypeFlags.OnDungeon);
            var cooldownWrong = new Cooldown("cW", CooldownTypeFlags.Never);

            entity.Cooldowns.Add(cooldownStart);
            entity.Cooldowns.Add(cooldownEnd);
            entity.Cooldowns.Add(cooldownAny);

            entity.EventHandler.OnDungeon(CreateArgs(OnTimeEnum.Start, entity));
            Assert.IsTrue(entity.Cooldowns.Count() == 1 && entity.Cooldowns.First() == cooldownEnd);

            entity.Cooldowns.Clear();
            entity.Cooldowns.Add(cooldownStart);
            entity.Cooldowns.Add(cooldownEnd);
            entity.Cooldowns.Add(cooldownAny);

            entity.EventHandler.OnDungeon(CreateArgs(OnTimeEnum.End, entity));
            Assert.IsTrue(entity.Cooldowns.Count() == 1 && entity.Cooldowns.First() == cooldownStart);

            entity.Cooldowns.Clear();
            entity.Cooldowns.Add(cooldownWrong);
            entity.EventHandler.OnDungeon(CreateArgs(OnTimeEnum.Start, entity));
            entity.EventHandler.OnDungeon(CreateArgs(OnTimeEnum.End, entity));
            Assert.IsTrue(entity.Cooldowns.Count() == 1 && entity.Cooldowns.First() == cooldownWrong);
        }

        [TestMethod]
        public void OnFightClearCooldowns()
        {
            var emptyEntity = CreateEmptyEntity();
            emptyEntity.EventHandler.OnFight(CreateArgs(OnTimeEnum.End, emptyEntity));
            emptyEntity.EventHandler.OnFight(CreateArgs(OnTimeEnum.Start, emptyEntity));

            var emptyField = CreateEmptyField();
            emptyField.EventHandler.OnFight(CreateArgs(OnTimeEnum.End, null, emptyField));
            emptyField.EventHandler.OnFight(CreateArgs(OnTimeEnum.Start, null, emptyField));

            var entity = CreateFullEntity();
            var field = CreateFullField();

            var cooldownStart = new Cooldown("cStart", CooldownTypeFlags.OnFightStart);
            var cooldownEnd = new Cooldown("cEnd", CooldownTypeFlags.OnFightEnd);
            var cooldownAny = new Cooldown("cAny", CooldownTypeFlags.OnFight);
            var cooldownWrong = new Cooldown("cW", CooldownTypeFlags.Never);

            entity.Cooldowns.Add(cooldownStart);
            entity.Cooldowns.Add(cooldownEnd);
            entity.Cooldowns.Add(cooldownAny);
            field.Cooldowns.Add(cooldownStart);
            field.Cooldowns.Add(cooldownEnd);
            field.Cooldowns.Add(cooldownAny);

            entity.EventHandler.OnFight(CreateArgs(OnTimeEnum.Start, entity));
            Assert.IsTrue(entity.Cooldowns.Count() == 1 && entity.Cooldowns.First() == cooldownEnd);
            field.EventHandler.OnFight(CreateArgs(OnTimeEnum.Start, null, field));
            Assert.IsTrue(field.Cooldowns.Count() == 1 && field.Cooldowns.First() == cooldownEnd);

            entity.Cooldowns.Clear();
            field.Cooldowns.Clear();
            entity.Cooldowns.Add(cooldownStart);
            entity.Cooldowns.Add(cooldownEnd);
            entity.Cooldowns.Add(cooldownAny);
            field.Cooldowns.Add(cooldownStart);
            field.Cooldowns.Add(cooldownEnd);
            field.Cooldowns.Add(cooldownAny);

            entity.EventHandler.OnFight(CreateArgs(OnTimeEnum.End, entity));
            Assert.IsTrue(entity.Cooldowns.Count() == 1 && entity.Cooldowns.First() == cooldownStart);
            field.EventHandler.OnFight(CreateArgs(OnTimeEnum.End, null, field));
            Assert.IsTrue(field.Cooldowns.Count() == 1 && field.Cooldowns.First() == cooldownStart);

            entity.Cooldowns.Clear();
            field.Cooldowns.Clear();
            entity.Cooldowns.Add(cooldownWrong);
            field.Cooldowns.Add(cooldownWrong);
            entity.EventHandler.OnFight(CreateArgs(OnTimeEnum.Start, entity));
            entity.EventHandler.OnFight(CreateArgs(OnTimeEnum.End, entity));
            field.EventHandler.OnFight(CreateArgs(OnTimeEnum.Start, null, field));
            field.EventHandler.OnFight(CreateArgs(OnTimeEnum.End, null, field));
            Assert.IsTrue(entity.Cooldowns.Count() == 1 && entity.Cooldowns.First() == cooldownWrong);
            Assert.IsTrue(field.Cooldowns.Count() == 1 && field.Cooldowns.First() == cooldownWrong);
        }

        [TestMethod]
        public void OnRoundClearCooldowns()
        {
            var emptyEntity = CreateEmptyEntity();
            emptyEntity.EventHandler.OnRound(CreateArgs(OnTimeEnum.End, emptyEntity));
            emptyEntity.EventHandler.OnRound(CreateArgs(OnTimeEnum.Start, emptyEntity));

            var emptyField = CreateEmptyField();
            emptyField.EventHandler.OnRound(CreateArgs(OnTimeEnum.End, null, emptyField));
            emptyField.EventHandler.OnRound(CreateArgs(OnTimeEnum.Start, null, emptyField));

            var entity = CreateFullEntity();
            var field = CreateFullField();

            var cooldownStart = new Cooldown("cStart", CooldownTypeFlags.OnRoundStart);
            var cooldownEnd = new Cooldown("cEnd", CooldownTypeFlags.OnRoundEnd);
            var cooldownAny = new Cooldown("cAny", CooldownTypeFlags.OnRound);
            var cooldownWrong = new Cooldown("cW", CooldownTypeFlags.Never);

            entity.Cooldowns.Add(cooldownStart);
            entity.Cooldowns.Add(cooldownEnd);
            entity.Cooldowns.Add(cooldownAny);
            field.Cooldowns.Add(cooldownStart);
            field.Cooldowns.Add(cooldownEnd);
            field.Cooldowns.Add(cooldownAny);

            entity.EventHandler.OnRound(CreateArgs(OnTimeEnum.Start, entity));
            Assert.IsTrue(entity.Cooldowns.Count() == 1 && entity.Cooldowns.First() == cooldownEnd);
            field.EventHandler.OnRound(CreateArgs(OnTimeEnum.Start, null, field));
            Assert.IsTrue(field.Cooldowns.Count() == 1 && field.Cooldowns.First() == cooldownEnd);

            entity.Cooldowns.Clear();
            field.Cooldowns.Clear();
            entity.Cooldowns.Add(cooldownStart);
            entity.Cooldowns.Add(cooldownEnd);
            entity.Cooldowns.Add(cooldownAny);
            field.Cooldowns.Add(cooldownStart);
            field.Cooldowns.Add(cooldownEnd);
            field.Cooldowns.Add(cooldownAny);

            entity.EventHandler.OnRound(CreateArgs(OnTimeEnum.End, entity));
            Assert.IsTrue(entity.Cooldowns.Count() == 1 && entity.Cooldowns.First() == cooldownStart);
            field.EventHandler.OnRound(CreateArgs(OnTimeEnum.End, null, field));
            Assert.IsTrue(field.Cooldowns.Count() == 1 && field.Cooldowns.First() == cooldownStart);

            entity.Cooldowns.Clear();
            field.Cooldowns.Clear();
            entity.Cooldowns.Add(cooldownWrong);
            field.Cooldowns.Add(cooldownWrong);
            entity.EventHandler.OnRound(CreateArgs(OnTimeEnum.Start, entity));
            entity.EventHandler.OnRound(CreateArgs(OnTimeEnum.End, entity));
            field.EventHandler.OnRound(CreateArgs(OnTimeEnum.Start, null, field));
            field.EventHandler.OnRound(CreateArgs(OnTimeEnum.End, null, field));
            Assert.IsTrue(entity.Cooldowns.Count() == 1 && entity.Cooldowns.First() == cooldownWrong);
            Assert.IsTrue(field.Cooldowns.Count() == 1 && field.Cooldowns.First() == cooldownWrong);
        }

        [TestMethod]
        public void OnTurnClearCooldowns()
        {
            var emptyEntity = CreateEmptyEntity();
            emptyEntity.EventHandler.OnTurn(CreateArgs(OnTimeEnum.End, emptyEntity));
            emptyEntity.EventHandler.OnTurn(CreateArgs(OnTimeEnum.Start, emptyEntity));

            var emptyField = CreateEmptyField();
            emptyField.EventHandler.OnTurn(CreateArgs(OnTimeEnum.End, null, emptyField));
            emptyField.EventHandler.OnTurn(CreateArgs(OnTimeEnum.Start, null, emptyField));

            var entity = CreateFullEntity();
            var field = CreateFullField();

            var cooldownStart = new Cooldown("cStart", CooldownTypeFlags.OnTurnStart);
            var cooldownEnd = new Cooldown("cEnd", CooldownTypeFlags.OnTurnEnd);
            var cooldownAny = new Cooldown("cAny", CooldownTypeFlags.OnTurn);
            var cooldownWrong = new Cooldown("cW", CooldownTypeFlags.Never);

            entity.Cooldowns.Add(cooldownStart);
            entity.Cooldowns.Add(cooldownEnd);
            entity.Cooldowns.Add(cooldownAny);
            field.Cooldowns.Add(cooldownStart);
            field.Cooldowns.Add(cooldownEnd);
            field.Cooldowns.Add(cooldownAny);

            entity.EventHandler.OnTurn(CreateArgs(OnTimeEnum.Start, entity));
            Assert.IsTrue(entity.Cooldowns.Count() == 1 && entity.Cooldowns.First() == cooldownEnd);
            field.EventHandler.OnTurn(CreateArgs(OnTimeEnum.Start, null, field));
            Assert.IsTrue(field.Cooldowns.Count() == 1 && field.Cooldowns.First() == cooldownEnd);

            entity.Cooldowns.Clear();
            field.Cooldowns.Clear();
            entity.Cooldowns.Add(cooldownStart);
            entity.Cooldowns.Add(cooldownEnd);
            entity.Cooldowns.Add(cooldownAny);
            field.Cooldowns.Add(cooldownStart);
            field.Cooldowns.Add(cooldownEnd);
            field.Cooldowns.Add(cooldownAny);

            entity.EventHandler.OnTurn(CreateArgs(OnTimeEnum.End, entity));
            Assert.IsTrue(entity.Cooldowns.Count() == 1 && entity.Cooldowns.First() == cooldownStart);
            field.EventHandler.OnTurn(CreateArgs(OnTimeEnum.End, null, field));
            Assert.IsTrue(field.Cooldowns.Count() == 1 && field.Cooldowns.First() == cooldownStart);

            entity.Cooldowns.Clear();
            field.Cooldowns.Clear();
            entity.Cooldowns.Add(cooldownWrong);
            field.Cooldowns.Add(cooldownWrong);
            entity.EventHandler.OnTurn(CreateArgs(OnTimeEnum.Start, entity));
            entity.EventHandler.OnTurn(CreateArgs(OnTimeEnum.End, entity));
            field.EventHandler.OnTurn(CreateArgs(OnTimeEnum.Start, null, field));
            field.EventHandler.OnTurn(CreateArgs(OnTimeEnum.End, null, field));
            Assert.IsTrue(entity.Cooldowns.Count() == 1 && entity.Cooldowns.First() == cooldownWrong);
            Assert.IsTrue(field.Cooldowns.Count() == 1 && field.Cooldowns.First() == cooldownWrong);
        }

        [TestMethod]
        public void OnRoundProcessMinions()
        {
            var emptyEntity = CreateEmptyEntity();
            emptyEntity.EventHandler.OnRound(CreateArgs(OnTimeEnum.End, emptyEntity));
            emptyEntity.EventHandler.OnRound(CreateArgs(OnTimeEnum.Start, emptyEntity));

            var battleSide = new BattleSide();
            var queue = new BattleQueue();
            var entity = CreateFullEntity();

            var minion = new Minion(CreateEmptyEntity());
            var battleSidedMinion = new Minion(CreateEmptyEntity(e => e.BattleSide = battleSide));

            battleSide.Add(entity);
            battleSide.Add(battleSidedMinion.BattleEntity);
            queue.Enqueue(entity);
            queue.Enqueue(minion.BattleEntity as IBattleQueable);
            queue.Enqueue(battleSidedMinion.BattleEntity as IBattleQueable);

            entity.Minions.Add(minion);
            entity.Minions.Add(battleSidedMinion);

            entity.EventHandler.OnRound(CreateArgs(OnTimeEnum.End, entity, null, queue));
            Assert.AreEqual(2, entity.Minions.Count());
            Assert.AreEqual(2, battleSide.Count);
            Assert.AreEqual(3, queue.Count);

            entity.EventHandler.OnRound(CreateArgs(OnTimeEnum.Start, entity, null, queue));
            Assert.AreEqual(0, entity.Minions.Count());
            Assert.IsTrue(battleSide.Count == 1 && battleSide.First() == entity);
            Assert.IsTrue(queue.Count == 1 && queue.Peek() == entity);
        }

        [TestMethod]
        public void OnRoundProcessStatues()
        {
            var emptyEntity = CreateEmptyEntity();
            emptyEntity.EventHandler.OnRound(CreateArgs(OnTimeEnum.End, emptyEntity));
            emptyEntity.EventHandler.OnRound(CreateArgs(OnTimeEnum.Start, emptyEntity));

            var entity = CreateFullEntity();

            var simpleStatus = new Status("testSS", emptyEntity);
            var decayingStatus = new DecayingStatus("testSD", emptyEntity);
            var buffInfinite = new Buff<int>("testBI", emptyEntity, flags: BuffFlags.InfiniteDuration);
            var buffNormal = new Buff<int>("testBN", emptyEntity);
            var ailmentInfinite = new Ailment("testAI", emptyEntity, 1, AilmentFlags.InfiniteDuration);
            var ailmentNormal = new Ailment("testAN", emptyEntity, 1);

            entity.Statuses.Add(simpleStatus);
            entity.Statuses.Add(decayingStatus);
            entity.Buffs.Add(buffInfinite);
            entity.Buffs.Add(buffNormal);
            entity.Ailments.Add(ailmentInfinite);
            entity.Ailments.Add(ailmentNormal);

            entity.EventHandler.OnRound(CreateArgs(OnTimeEnum.End, entity));
            Assert.AreEqual(6, entity.Statuses.Count() + entity.Buffs.Count() + entity.Ailments.Count());

            entity.EventHandler.OnRound(CreateArgs(OnTimeEnum.Start, entity));
            Assert.AreEqual(3, entity.Statuses.Count() + entity.Buffs.Count() + entity.Ailments.Count());
            Assert.AreEqual(entity.Statuses.First(), simpleStatus);
            Assert.AreEqual(entity.Buffs.First(), buffInfinite);
            Assert.AreEqual(entity.Ailments.First(), ailmentInfinite);
        }

        private static TestBattleEntity CreateEmptyEntity(Action<TestBattleEntity> action)
        {
            var entity = CreateEmptyEntity();
            action(entity);
            return entity;
        }

        private static TestBattleEntity CreateEmptyEntity()
        {
            var entity = new TestBattleEntity()
            {
                EventHandler = new DictionaryEventHandlerProvider()
            };
            HookBaseEvents(entity.EventHandler);
            return entity;
        }

        private static TestBattleField CreateEmptyField()
        {
            var field = new TestBattleField()
            {
                EventHandler = new DictionaryEventHandlerProvider()
            };
            HookBaseEvents(field.EventHandler);
            return field;
        }

        private static TestBattleEntity CreateFullEntity()
        {
            var entity = new TestBattleEntity()
            {
                EventHandler = new DictionaryEventHandlerProvider(),
                Ailments = new AilmentList(),
                Buffs = new BuffList(),
                Statuses = new StatusesList(),
                Cooldowns = new CooldownList(),
                Minions = new MinionList(),
            };
            HookBaseEvents(entity.EventHandler);
            return entity;
        }

        private static TestBattleField CreateFullField()
        {
            var field = new TestBattleField()
            {
                EventHandler = new DictionaryEventHandlerProvider(),
                Auras = new AuraList(),
                Cooldowns = new CooldownList(),
            };
            HookBaseEvents(field.EventHandler);
            return field;
        }

        private static void HookBaseEvents(IBattleEntityEventHandlersProvider hookTo)
        {
            BattleEventLoader.LoadEventActionsFromType(typeof(BasicBattleEntityEventsImplementations), hookTo);
        }

        private static void HookBaseEvents(IBattleFieldEventHandlersProvider hookTo)
        {
            BattleEventLoader.LoadEventActionsFromType(typeof(BasicBattleFieldEventsImplementations), hookTo);
        }

        private static BattleEventArgs<T> CreateArgs<T>(T data, IBattleEntity self, IBattleField field = null, BattleQueue queue = null)
        {
            return new BattleEventArgs<T>(queue, null, null, self, null, data, field);
        }
    }
}
