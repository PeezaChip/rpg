﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Basic.Auras;
using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.BattleFields;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Battle.Events.Helpers;
using Peeza.RPG.Core.Realizations.Ailments;
using Peeza.RPG.Core.Realizations.Auras;
using Peeza.RPG.Core.Realizations.Battle;
using Peeza.RPG.Core.Realizations.BattleEntities;
using Peeza.RPG.Core.Realizations.BattleFields;
using Peeza.RPG.Core.Realizations.Buffs;
using Peeza.RPG.Core.Realizations.Cooldowns;
using Peeza.RPG.Core.Realizations.Stats;
using Peeza.RPG.Core.Realizations.Statuses;
using Peeza.RPG.Tests.Core;

namespace Peeza.RPG.Tests.Battle.BaseExpansionEvents
{
    [TestClass]
    public class AurasEventTests
    {
        [TestMethod]
        public void FilterTests()
        {
            var bs1 = new BattleSide();
            var bs2 = new BattleSide();
            var bs3 = new BattleSide();

            var field = CreateField(bs1, bs2, bs3);
            var fieldAccessor = new BattleFieldProvider();
            fieldAccessor.SetField(field);

            var bs1buffs = new FieldAwareBuffList(bs1, fieldAccessor);
            var bs2buffs = new FieldAwareBuffList(bs2, fieldAccessor);
            var bs3buffs = new FieldAwareBuffList(bs3, fieldAccessor);

            Assert.AreEqual(0, bs1buffs.GetAll().Count());
            Assert.AreEqual(0, bs2buffs.GetAll().Count());
            Assert.AreEqual(0, bs3buffs.GetAll().Count());

            field.Auras.Add(new Aura("no filter aura", null, null, initialBuffs: new Buff<int>("b", null)));

            Assert.AreEqual(1, bs1buffs.GetAll().Count());
            Assert.AreEqual(1, bs2buffs.GetAll().Count());
            Assert.AreEqual(1, bs3buffs.GetAll().Count());

            field.Auras.Add(new Aura("wl filter aura", null, [bs1, bs2], initialBuffs: new Buff<int>("b", null)));

            Assert.AreEqual(2, bs1buffs.GetAll().Count());
            Assert.AreEqual(2, bs2buffs.GetAll().Count());
            Assert.AreEqual(1, bs3buffs.GetAll().Count());

            field.Auras.Add(new Aura("bl filter aura", null, [bs2, bs3], AuraFlags.FilterBlacklistMode, new Buff<int>("b", null)));

            Assert.AreEqual(3, bs1buffs.GetAll().Count());
            Assert.AreEqual(2, bs2buffs.GetAll().Count());
            Assert.AreEqual(1, bs3buffs.GetAll().Count());
        }

        [TestMethod]
        public void OnAffectedWithStatusBase()
        {
            var bs1 = new BattleSide();
            var bs2 = new BattleSide();
            var field = CreateField(bs1, bs2);
            var fieldAccessor = new BattleFieldProvider();
            fieldAccessor.SetField(field);
            var entity1 = CreateEntity(fieldAccessor, bs1);
            var entity2 = CreateEntity(fieldAccessor, bs2);

            var atkBuf = new FlatAttackBuff(null, 10);

            Assert.AreEqual(100, entity1.Stats.GetAttack());
            Assert.AreEqual(100, entity2.Stats.GetAttack());

            entity1.EventHandler.OnAffectedWithStatus(CreateArgs(new OnStatusData() { NewStatuses = [atkBuf] }, entity1, field));

            Assert.AreEqual(110, entity1.Stats.GetAttack());
            Assert.AreEqual(1, entity1.Buffs.GetAll(BasicStatsDeclarationsExtensions.AttackStatKey).Count());
            Assert.AreEqual(100, entity2.Stats.GetAttack());

            var atkAura = new Aura("ATK AURA BS2", entity2, [bs2], initialBuffs: [new FlatAttackBuff(entity2, 20)]);
            var atkAuraShared = new Aura("ATK AURA SHARED", entity1, initialBuffs: [new FlatAttackBuff(entity1, 50)]);

            Assert.AreEqual(0, field.Auras.Count());
            field.EventHandler.OnAuraApplied(CreateArgs(new OnAuraData() { NewAuras = [atkAura, atkAuraShared] }, null, field));
            Assert.AreEqual(2, field.Auras.Count());

            Assert.AreEqual(160, entity1.Stats.GetAttack());
            Assert.AreEqual(2, entity1.Buffs.GetAll(BasicStatsDeclarationsExtensions.AttackStatKey).Count());
            Assert.AreEqual(170, entity2.Stats.GetAttack());
            Assert.AreEqual(2, entity2.Buffs.GetAll(BasicStatsDeclarationsExtensions.AttackStatKey).Count());

            Assert.AreEqual(2, field.Auras.Count());
            field.EventHandler.OnAuraApplied(CreateArgs(new OnAuraData() { LiftedAuras = [atkAura, atkAuraShared] }, null, field));
            Assert.AreEqual(0, field.Auras.Count());

            Assert.AreEqual(110, entity1.Stats.GetAttack());
            Assert.AreEqual(1, entity1.Buffs.GetAll(BasicStatsDeclarationsExtensions.AttackStatKey).Count());
            Assert.AreEqual(100, entity2.Stats.GetAttack());
            Assert.AreEqual(0, entity2.Buffs.GetAll(BasicStatsDeclarationsExtensions.AttackStatKey).Count());
        }

        private static TestBattleEntity CreateEntity(IBattleFieldAccessor fieldAccessor, IBattleSide side)
        {
            var entity = new TestBattleEntity()
            {
                BattleSide = side,
                EventHandler = new DictionaryEventHandlerProvider(),
                Ailments = new AilmentList(),
                Buffs = new FieldAwareBuffList(side, fieldAccessor),
                Statuses = new StatusesList(),
                Cooldowns = new CooldownList(),
                Minions = new MinionList(),
                Stats = new StatList(),
            };
            entity.Stats.Add(new AttackStat(100, entity.Buffs));
            side.BattleEntities.Add(entity);
            HookBaseEvents(entity.EventHandler);
            return entity;
        }

        private static TestBattleField CreateField(params IBattleSide[] battleSides)
        {
            var field = new TestBattleField()
            {
                AllBattleSides = new BasicBattleSidesAccessor(battleSides),
                EventHandler = new DictionaryEventHandlerProvider(),
                Auras = new AuraList(),
                Cooldowns = new CooldownList(),
            };
            HookBaseEvents(field.EventHandler);
            return field;
        }

        private static void HookBaseEvents(IBattleEntityEventHandlersProvider hookTo)
        {
            BattleEventLoader.LoadEventActionsFromType(typeof(BasicBattleEntityEventsImplementations), hookTo);
        }

        private static void HookBaseEvents(IBattleFieldEventHandlersProvider hookTo)
        {
            BattleEventLoader.LoadEventActionsFromType(typeof(BasicBattleFieldEventsImplementations), hookTo);
        }

        private static BattleEventArgs<T> CreateArgs<T>(T data, IBattleEntity self, IBattleField field = null)
        {
            return new BattleEventArgs<T>(null, null, null, self, null, data, field);
        }
    }
}
