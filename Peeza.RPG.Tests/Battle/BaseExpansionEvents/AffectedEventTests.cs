﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Core.Basic.Ailments;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Minions;
using Peeza.RPG.Core.Basic.Statuses;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Battle.Events.Helpers;
using Peeza.RPG.Core.Battle.Events.Interfaces;
using Peeza.RPG.Core.Realizations.Ailments;
using Peeza.RPG.Core.Realizations.Battle;
using Peeza.RPG.Core.Realizations.BattleEntities;
using Peeza.RPG.Core.Realizations.Buffs;
using Peeza.RPG.Core.Realizations.Cooldowns;
using Peeza.RPG.Core.Realizations.Statuses;
using Peeza.RPG.Tests.Core;

namespace Peeza.RPG.Tests.Battle.BaseExpansionEvents
{
    [TestClass]
    public class AffectedEventTests
    {
        [TestMethod]
        public void OnAffectedWithStatusBase()
        {
            var emptyEntity = CreateEmptyEntity();
            var entity = CreateFullEntity();

            var simpleStatus = new Status("testSS", emptyEntity);
            var decayingStatus = new DecayingStatus("testSD", emptyEntity);
            var buffInfinite = new Buff<int>("testBI", emptyEntity, flags: BuffFlags.InfiniteDuration);
            var buffNormal = new Buff<int>("testBN", emptyEntity);
            var ailmentInfinite = new Ailment("testAI", emptyEntity, 1, AilmentFlags.InfiniteDuration);
            var ailmentNormal = new Ailment("testAN", emptyEntity, 1);

            var args = new OnStatusData();
            args.NewStatuses.Add(simpleStatus);
            args.NewStatuses.Add(decayingStatus);
            args.NewStatuses.Add(buffInfinite);
            args.NewStatuses.Add(buffNormal);
            args.NewStatuses.Add(ailmentInfinite);
            args.NewStatuses.Add(ailmentNormal);

            emptyEntity.EventHandler.OnAffectedWithStatus(CreateArgs(args, emptyEntity));
            entity.EventHandler.OnAffectedWithStatus(CreateArgs(args, entity));
            Assert.AreEqual(2, entity.Statuses.Count());
            Assert.AreEqual(2, entity.Buffs.Count());
            Assert.AreEqual(2, entity.Ailments.Count());

            var removedArgs = new OnStatusData();
            removedArgs.LiftedStatuses.AddRange(args.NewStatuses);
            emptyEntity.EventHandler.OnAffectedWithStatus(CreateArgs(removedArgs, emptyEntity));
            entity.EventHandler.OnAffectedWithStatus(CreateArgs(removedArgs, entity));
            Assert.AreEqual(0, entity.Statuses.Count() + entity.Buffs.Count() + entity.Ailments.Count());
        }

        [TestMethod]
        public void OnMinionSummoned()
        {
            var battleSide = new BattleSide();

            var emptyEntity = CreateEmptyEntity(battleSide);
            var entity = CreateFullEntity(battleSide);

            battleSide.Add(emptyEntity);
            battleSide.Add(entity);

            var minion = new Minion(CreateEmptyEntity(battleSide));
            var minion2 = new Minion(CreateEmptyEntity(battleSide));

            var args = new OnMinionData();
            args.NewMinions.Add(minion);
            args.NewMinions.Add(minion2);

            emptyEntity.EventHandler.OnMinionSummoned(CreateArgs(args, emptyEntity));
            entity.EventHandler.OnMinionSummoned(CreateArgs(args, entity));
            Assert.AreEqual(2, entity.Minions.Count());
            Assert.AreEqual(4, battleSide.BattleEntities.Count);

            var removedArgs = new OnMinionData();
            removedArgs.DeadMinions.AddRange(args.NewMinions);
            emptyEntity.EventHandler.OnMinionSummoned(CreateArgs(removedArgs, emptyEntity));
            entity.EventHandler.OnMinionSummoned(CreateArgs(removedArgs, entity));
            Assert.AreEqual(0, entity.Minions.Count());
            Assert.AreEqual(2, battleSide.BattleEntities.Count);
        }

        private static TestBattleEntity CreateEmptyEntity(BattleSide battleSide = null)
        {
            var entity = new TestBattleEntity()
            {
                EventHandler = new DictionaryEventHandlerProvider(),

                BattleSide = battleSide
            };
            HookBaseEvents(entity.EventHandler);
            return entity;
        }

        private static TestBattleEntity CreateFullEntity(BattleSide battleSide = null)
        {
            var entity = new TestBattleEntity()
            {
                EventHandler = new DictionaryEventHandlerProvider(),
                Ailments = new AilmentList(),
                Buffs = new BuffList(),
                Statuses = new StatusesList(),
                Cooldowns = new CooldownList(),
                Minions = new MinionList(),

                BattleSide = battleSide
            };
            HookBaseEvents(entity.EventHandler);
            return entity;
        }

        private static void HookBaseEvents(IBattleEventHandlersProvider hookTo)
        {
            BattleEventLoader.LoadEventActionsFromType(typeof(BasicBattleEntityEventsImplementations), hookTo);
        }

        private static BattleEventArgs<T> CreateArgs<T>(T data, IBattleEntity self)
        {
            return new BattleEventArgs<T>(null, null, null, self, null, data, null);
        }
    }
}
