﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Base.Battle.Arguments.Damage;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Base.Resources;
using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Battle.Events.Attributes;
using Peeza.RPG.Core.Battle.Events.Helpers;
using Peeza.RPG.Core.Battle.Events.Interfaces;
using Peeza.RPG.Core.Realizations.Ailments;
using Peeza.RPG.Core.Realizations.Battle;
using Peeza.RPG.Core.Realizations.BattleEntities;
using Peeza.RPG.Core.Realizations.Buffs;
using Peeza.RPG.Core.Realizations.Cooldowns;
using Peeza.RPG.Core.Realizations.Resources;
using Peeza.RPG.Core.Realizations.Stats;
using Peeza.RPG.Core.Realizations.Statuses;
using Peeza.RPG.Tests.Core;

namespace Peeza.RPG.Tests.Battle.BaseExpansionEvents
{
    [TestClass]
    public class BattleOffenseTests
    {
        [TestMethod]
        public void OnPickTargetSimple()
        {
            var target1 = CreateFullEntity();
            var target2 = CreateFullEntity();

            TargetData createTargetData() => new() { PossibleTargets = new() { target1, target2 } };

            var fullEntity = CreateFullEntity();
            var fullArgs = createTargetData();
            fullEntity.EventHandler.OnPickTarget(CreateArgs(fullArgs, null, fullEntity, null));
            Assert.IsTrue(fullArgs.Target == target1 || fullArgs.Target == target2);
        }

        [TestMethod]
        public void OnPickTargetCheckDrop()
        {
            var target1 = CreateFullEntity();
            BattleEventLoader.LoadEventActionsFromType(typeof(BattleEventOffenseHookActionSelectedAsTarget), target1.EventHandler);
            BattleEventLoader.LoadEventActionsFromType(typeof(BattleEventOffenseHookActionDropTarget), target1.EventHandler);
            var target2 = CreateFullEntity();
            BattleEventLoader.LoadEventActionsFromType(typeof(BattleEventOffenseHookActionSelectedAsTarget), target2.EventHandler);

            TargetData createTargetData() => new() { PossibleTargets = new() { target1, target2 } };

            var log = new BasicBattleLogger();

            var fullEntity = CreateFullEntity();
            BattleEventLoader.LoadEventActionsFromType(typeof(BattleEventOffenseHookActions), fullEntity.EventHandler);
            var fullArgs = createTargetData();
            fullEntity.EventHandler.OnPickTarget(CreateArgs(fullArgs, log, fullEntity, null));
            Assert.IsTrue(log.GetBattleLog().Trim().Length == 1);
        }

        [TestMethod]
        public void OnPickTargetCheckSelectedAsCall()
        {
            var target1 = CreateFullEntity();
            BattleEventLoader.LoadEventActionsFromType(typeof(BattleEventOffenseHookActionSelectedAsTarget), target1.EventHandler);
            BattleEventLoader.LoadEventActionsFromType(typeof(BattleEventOffenseHookActionChangeTarget), target1.EventHandler);
            var target2 = CreateFullEntity();
            BattleEventLoader.LoadEventActionsFromType(typeof(BattleEventOffenseHookActionSelectedAsTarget), target2.EventHandler);

            TargetData createTargetData() => new() { PossibleTargets = new() { target1, target2 } };

            var log = new BasicBattleLogger();

            var fullEntity = CreateFullEntity();
            BattleEventLoader.LoadEventActionsFromType(typeof(BattleEventOffenseHookActions), fullEntity.EventHandler);
            var fullArgs = createTargetData();
            fullEntity.EventHandler.OnPickTarget(CreateArgs(fullArgs, log, fullEntity, null));
            Assert.IsTrue(log.GetBattleLog().Replace("\r", "").Replace("\n", "").Length == 2);
        }

        [TestMethod]
        public void OnCalculateDamage()
        {
            var entity = CreateFullEntity();
            var args = CreateArgs<DamageInfo>(null, new ConsoleBattleLogger(), entity);

            entity.EventHandler.OnCalculateDamage(args);

            Assert.AreEqual(args.Data.DamageInstances.Count, 1, "Incorrect damage instances count");
            Assert.AreEqual(args.Data.DamageInstances.First().Result, DamageInstanceInfoResultFlags.Miss, "Incorrect damage instance result");

            entity.Stats.Add(new AccuracyStat(1));
            entity.Stats.Add(new AttackStat(5));
            entity.EventHandler.OnCalculateDamage(args);

            Assert.AreEqual(args.Data.DamageInstances.Count, 1, "Incorrect damage instances count");
            Assert.AreEqual(args.Data.DamageInstances.First().Result, DamageInstanceInfoResultFlags.None, "Incorrect damage instance result");
            Assert.AreEqual(args.Data.DamageInstances.First().Damage, 5, "Incorrect damage instance damage");

            entity.Stats.Add(new CritChanceStat(1));
            entity.Stats.Add(new CritDamageStat(2));
            entity.EventHandler.OnCalculateDamage(args);

            Assert.AreEqual(args.Data.DamageInstances.Count, 1, "Incorrect damage instances count");
            Assert.AreEqual(args.Data.DamageInstances.First().Result, DamageInstanceInfoResultFlags.None, "Incorrect damage instance result");
            Assert.AreEqual(args.Data.DamageInstances.First().Modifiers, DamageInstanceInfoModifierFlags.Crit, "Incorrect damage instance modifiers");
            Assert.AreEqual(args.Data.DamageInstances.First().Damage, 5 * 2, "Incorrect damage instance damage");
        }

        [TestMethod]
        public void OnDoAttack()
        {
            var target = CreateFullEntity();
            BattleEventLoader.LoadEventActionsFromType(typeof(BattleEventOffenseHookActionGetHit), target.EventHandler);

            var log = new BasicBattleLogger();

            var attacker = CreateFullEntity();
            var args = CreateArgs(new DamageInfo() { DamageInstances = [new DamageInstanceInfo() { Damage = 5 }] }, log, attacker, target: target);

            attacker.EventHandler.OnDoAttack(args);
            Assert.IsTrue(log.GetBattleLog().Trim().Split(Environment.NewLine).Last() == "  5");
        }

        [TestMethod]
        public void OnDoAttackLifesteal()
        {
            var target = CreateFullEntity();
            target.Resources.Add(new HealthResource(100));
            var log = new BasicBattleLogger();

            var attacker = CreateFullEntity();
            attacker.Resources.Add(new HealthResource(100));
            attacker.Resources.GetHealth().Value = 50;
            attacker.Stats.Add(new LifestealStat(0.5));

            var args = CreateArgs(new DamageInfo() { DamageInstances = [new DamageInstanceInfo() { Damage = 5 }] }, log, attacker, target: target);
            attacker.EventHandler.OnDoAttack(args);

            Console.WriteLine(log.GetBattleLog().Trim().Split(Environment.NewLine).Last().Trim() == "TestEntity-2 healed 2 HP");
        }

        [TestMethod]
        public void OnAttackResolve()
        {
            var target = CreateFullEntity();
            BattleEventLoader.LoadEventActionsFromType(typeof(BattleEventOffenseHookActionGetHit), target.EventHandler);

            var log = new BasicBattleLogger();

            var attacker = CreateFullEntity();
            var args = CreateArgs(new DamageInfo() { DamageInstances = [new DamageInstanceInfo() { DamageDealt = 5 }] }, log, attacker, target: target);

            attacker.EventHandler.OnAttackResolve(args);
            Assert.IsTrue(log.GetBattleLog().Trim().Length > 0);
            Console.Write(log.GetBattleLog());
        }

        private static TestBattleEntity CreateFullEntity()
        {
            var entity = new TestBattleEntity()
            {
                EventHandler = new DictionaryEventHandlerProvider(),
                Ailments = new AilmentList(),
                Buffs = new BuffList(),
                Statuses = new StatusesList(),
                Cooldowns = new CooldownList(),
                Minions = new MinionList(),
                Stats = new StatList(),
                Resources = new ResourceList(),
            };
            HookBaseEvents(entity.EventHandler);
            return entity;
        }

        private static void HookBaseEvents(IBattleEventHandlersProvider hookTo)
        {
            BattleEventLoader.LoadEventActionsFromType(typeof(BasicBattleEntityEventsImplementations), hookTo);
        }

        private static readonly Random random = new(111);
        private static BattleEventArgs<T> CreateArgs<T>(T data, IBattleLogger logger, IBattleEntity self, BattleQueue queue = null, IBattleEntity target = null)
        {
            return new BattleEventArgs<T>(queue, logger, random, self, target, data, null);
        }

        private class BattleEventOffenseHookActions
        {
            [BattleEventAction(typeof(OnPickTarget), Priority = 50)]
            public static bool OnPickTargetTest(BattleEventArgs<TargetData> args)
            {
                args.Data.Target = args.Data.PossibleTargets.First();
                return false;
            }
        }

        private class BattleEventOffenseHookActionDropTarget
        {
            [BattleEventAction(typeof(OnSelectedAsTarget), Priority = 50)]
            public static bool OnSelectedAsTargetDropTest(BattleEventArgs<TargetData> args)
            {
                args.Data.Target = null;
                return false;
            }
        }

        private class BattleEventOffenseHookActionChangeTarget
        {
            [BattleEventAction(typeof(OnSelectedAsTarget), Priority = 50)]
            public static bool OnSelectedAsTargetChangeTest(BattleEventArgs<TargetData> args)
            {
                args.Data.Target = args.Data.PossibleTargets.Except(new List<IBattleEntity>() { args.Data.Target }).First();
                return false;
            }
        }

        private class BattleEventOffenseHookActionSelectedAsTarget
        {
            [BattleEventAction(typeof(OnSelectedAsTarget), Priority = 100)]
            public static bool OnSelectedAsTargetLogTest(BattleEventArgs<TargetData> args)
            {
                args.Log("a");
                return false;
            }
        }

        private class BattleEventOffenseHookActionGetHit
        {
            [BattleEventAction(typeof(OnGetHit), Priority = 200)]
            public static bool OnGetHit(BattleEventArgs<DamageInfo> args)
            {
                args.Log(args.Data.TotalAttack.ToString());
                return true;
            }
        }
    }
}
