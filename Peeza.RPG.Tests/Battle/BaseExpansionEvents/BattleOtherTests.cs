﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Base.Battle.BattleEntities;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Base.Resources;
using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Battle.Events.Helpers;
using Peeza.RPG.Core.Battle.Events.Interfaces;
using Peeza.RPG.Core.Realizations.Battle;

namespace Peeza.RPG.Tests.Battle.BaseExpansionEvents
{
    [TestClass]
    public class BattleOtherTests
    {
        [TestMethod]
        public void OnHealthChangeDeathTrigger()
        {
            var entity = CreateFullEntity("Entity");

            var log = new BasicBattleLogger();

            entity.Resources.GetHealth().Value = 50;
            var args = CreateArgs(new ResourceChangeInfo(BasicResourcesDeclarationsExtensions.HealthResourceKey, 100, 50), log, entity);
            entity.EventHandler.OnResourceChange(args);
            Assert.IsTrue(entity.CanFight);
            Assert.IsFalse(log.GetBattleLog().EndsWith("died"));

            entity.Resources.GetHealth().Value = 0;
            args = CreateArgs(new ResourceChangeInfo(BasicResourcesDeclarationsExtensions.HealthResourceKey, 50, 0), log, entity);
            entity.EventHandler.OnResourceChange(args);
            Assert.IsFalse(entity.CanFight);
            Assert.IsTrue(log.GetBattleLog().Trim().EndsWith("died"));

            Console.WriteLine(log.GetBattleLog());
        }

        [TestMethod]
        public void OnRessurectedTrigger()
        {
            var entity = CreateFullEntity("Entity");

            var log = new BasicBattleLogger();

            entity.Resources.GetHealth().Value = 0;
            Assert.IsFalse(entity.CanFight);
            var args = CreateArgs(new RessurectionData() { HealthToRestore = 10, Source = entity }, log, entity);
            entity.EventHandler.OnRessurection(args);
            Assert.IsTrue(entity.CanFight);
            Assert.IsTrue(log.GetBattleLog().Trim().EndsWith("ressurected"));

            Console.WriteLine(log.GetBattleLog());
        }

        private static BasicBattleEntity CreateFullEntity(string name)
        {
            var entity = new BasicBattleEntity(name, null, null, null, null);
            entity.Resources.Add(new HealthResource(100));
            HookBaseEvents(entity.EventHandler);
            return entity;
        }

        private static void HookBaseEvents(IBattleEventHandlersProvider hookTo)
        {
            BattleEventLoader.LoadEventActionsFromType(typeof(BasicBattleEntityEventsImplementations), hookTo);
        }

        private static readonly Random random = new(222);
        private static BattleEventArgs<T> CreateArgs<T>(T data, IBattleLogger logger, IBattleEntity self, BattleQueue queue = null, IBattleEntity target = null)
        {
            return new BattleEventArgs<T>(queue, logger, random, self, target, data, null);
        }
    }
}
