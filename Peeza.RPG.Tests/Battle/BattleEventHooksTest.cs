﻿using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Battle.Events.Attributes;
using Peeza.RPG.Core.Battle.Events.Helpers;
using Peeza.RPG.Core.Battle.Events.Interfaces;
using Peeza.RPG.Core.Realizations.Battle;

namespace Peeza.RPG.Tests.Battle
{
    [TestClass]
    public class BattleEventHooksTest
    {
        [TestMethod]
        public void TestLoadingFromAttribute()
        {
            var dict = new DictionaryEventHandlerProvider();

            BattleEventLoader.LoadEventActionsFromType(typeof(BattleEventHookActions), dict);

            var args1 = new BattleEventArgs<string>() { Data = "start_" };
            dict.OnTest1(args1);
            Assert.AreEqual("start_cab", args1.Data);

            var args2 = new BattleEventArgs<int>() { Data = 2 };
            dict.OnTest2(args2);
            Assert.AreEqual(2 + 3 + 1, args2.Data);

            var log = new BasicBattleLogger();
            var args3 = new BattleEventArgs(null, log, null, null, null, null);
            dict.OnTest3(args3);
            Assert.AreEqual("3", log.GetBattleLog().Trim());

            BattleEventLoader.UnloadEventActionsFromType(typeof(BattleEventHookActions), dict);

            var args1a = new BattleEventArgs<string>() { Data = "start_" };
            dict.OnTest1(args1a);
            Assert.AreEqual("start_", args1a.Data);

            var args2a = new BattleEventArgs<int>() { Data = 2 };
            dict.OnTest2(args2a);
            Assert.AreEqual(2, args2a.Data);

            var loga = new BasicBattleLogger();
            var args3a = new BattleEventArgs(null, loga, null, null, null, null);
            dict.OnTest3(args3a);
            Assert.AreEqual("", loga.GetBattleLog());
        }

        [TestMethod]
        public void TestLoadingFromInterface()
        {
            var dict = new DictionaryEventHandlerProvider();

            var actionProvider = new BattleEventHookActions();

            actionProvider.Hook(dict);

            var args1 = new BattleEventArgs<string>() { Data = "start_" };
            dict.OnTest1(args1);
            Assert.AreEqual("start_cab", args1.Data);

            var args2 = new BattleEventArgs<int>() { Data = 2 };
            dict.OnTest2(args2);
            Assert.AreEqual(2 + 3 + 1, args2.Data);

            var log = new BasicBattleLogger();
            var args3 = new BattleEventArgs(null, log, null, null, null, null);
            dict.OnTest3(args3);
            Assert.AreEqual("3", log.GetBattleLog().Trim());

            actionProvider.Unhook(dict);

            var args1a = new BattleEventArgs<string>() { Data = "start_" };
            dict.OnTest1(args1a);
            Assert.AreEqual("start_", args1a.Data);

            var args2a = new BattleEventArgs<int>() { Data = 2 };
            dict.OnTest2(args2a);
            Assert.AreEqual(2, args2a.Data);

            var loga = new BasicBattleLogger();
            var args3a = new BattleEventArgs(null, loga, null, null, null, null);
            dict.OnTest3(args3a);
            Assert.AreEqual("", loga.GetBattleLog());
        }

        private class BattleEventHookActions : IBattleEventActionsProvider
        {
            [BattleEventAction(typeof(OnTest3))]
            public static bool Log(BattleEventArgs args)
            {
                args.Log("3");
                return false;
            }

            [BattleEventAction(typeof(OnTest2))]
            public static bool AddOne(BattleEventArgs<int> args)
            {
                args.Data += 1;
                return false;
            }

            [BattleEventAction(typeof(OnTest2))]
            public static bool AddTwo(BattleEventArgs<int> args)
            {
                args.Data += 3;
                return false;
            }

            [BattleEventAction(typeof(OnTest1), Priority = 160)]
            public static bool AddC(BattleEventArgs<string> args)
            {
                args.Data += "c";
                return false;
            }

            [BattleEventAction(typeof(OnTest1), Priority = 50)]
            public static bool AddB(BattleEventArgs<string> args)
            {
                args.Data += "b";
                return false;
            }

            [BattleEventAction(typeof(OnTest1), Priority = 150)]
            public static bool AddA(BattleEventArgs<string> args)
            {
                args.Data += "a";
                return false;
            }

            public void Hook(IBattleEventHandlersProvider hookTo)
            {
                hookTo.HookEvent(BattleEventHooksTestExtensions.OnTest3Declaration, Log);

                hookTo.HookEvent(BattleEventHooksTestExtensions.OnTest2Declaration, AddOne);
                hookTo.HookEvent(BattleEventHooksTestExtensions.OnTest2Declaration, AddTwo);

                hookTo.HookEvent(BattleEventHooksTestExtensions.OnTest1Declaration, AddC);
                hookTo.HookEvent(BattleEventHooksTestExtensions.OnTest1Declaration, AddB);
                hookTo.HookEvent(BattleEventHooksTestExtensions.OnTest1Declaration, AddA);
            }

            public void Unhook(IBattleEventHandlersProvider hookTo)
            {
                hookTo.UnhookEvent(BattleEventHooksTestExtensions.OnTest3Declaration, Log);

                hookTo.UnhookEvent(BattleEventHooksTestExtensions.OnTest2Declaration, AddOne);
                hookTo.UnhookEvent(BattleEventHooksTestExtensions.OnTest2Declaration, AddTwo);

                hookTo.UnhookEvent(BattleEventHooksTestExtensions.OnTest1Declaration, AddC);
                hookTo.UnhookEvent(BattleEventHooksTestExtensions.OnTest1Declaration, AddB);
                hookTo.UnhookEvent(BattleEventHooksTestExtensions.OnTest1Declaration, AddA);
            }
        }
    }

    public class OnTest1 : BattleEventDeclaration<string> { }
    public class OnTest2 : BattleEventDeclaration<int> { }
    public class OnTest3 : BattleEventDeclaration { }
}
