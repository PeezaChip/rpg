﻿using Peeza.RPG.Core.Basic.Generic;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Battle.Events.Attributes;

namespace Peeza.RPG.Tests.Battle
{
    [TestClass]
    public class CoreBattleTests
    {
        [TestMethod]
        public void EventOrder()
        {
            BattleEvent<string> handler = new();

            handler.Hook(BattleEventActions.AddE);
            handler.Hook(BattleEventActions.AddQ);

            var args = new BattleEventArgs<string>() { Data = "" };
            handler.Raise(args);

            Assert.AreEqual("qe", args.Data);
        }

        [TestMethod]
        public void EventOrderPriority()
        {
            BattleEvent<string> handler = new();

            handler.Hook(BattleEventActions.AddB);
            handler.Hook(BattleEventActions.AddC);
            handler.Hook(BattleEventActions.AddA);
            handler.Hook(BattleEventActions.AddB);
            handler.Hook(BattleEventActions.AddC);
            handler.Hook(BattleEventActions.AddA);
            handler.Hook(BattleEventActions.AddB);

            var args = new BattleEventArgs<string>() { Data = "" };
            handler.Raise(args);

            Assert.AreEqual("aabbbcc", args.Data);
        }

        [TestMethod]
        public void EventOrderPriorityNoData()
        {
            BattleEvent handler = new();

            handler.Hook(BattleEventActions.Add2);
            handler.Hook(BattleEventActions.Add3);
            handler.Hook(BattleEventActions.Add1);
            handler.Hook(BattleEventActions.Add2);
            handler.Hook(BattleEventActions.Add3);
            handler.Hook(BattleEventActions.Add1);
            handler.Hook(BattleEventActions.Add2);

            var args = new BattleEventArgs() { Data = "" };
            handler.Raise(args);

            Assert.AreEqual("1122233", args.Data);
        }

        [TestMethod]
        public void EventStop()
        {
            BattleEvent<string> handler = new();

            handler.Hook(BattleEventActions.AddE);
            handler.Hook(BattleEventActions.Stop);
            handler.Hook(BattleEventActions.AddQ);

            var args = new BattleEventArgs<string>() { Data = "" };
            handler.Raise(args);

            Assert.AreEqual("q", args.Data);
        }

        [TestMethod]
        public void EventFilter()
        {
            BattleEvent<KeyedArgs> handler = new();

            handler.Hook(BattleEventActions.F1);
            handler.Hook(BattleEventActions.F2);
            handler.Hook(BattleEventActions.F3);

            var args1 = new BattleEventArgs<KeyedArgs>() { Data = new KeyedArgs() { Key = "F1" } };
            handler.Raise(args1);
            Assert.AreEqual(1, args1.Data.Value);

            var args2 = new BattleEventArgs<KeyedArgs>() { Data = new KeyedArgs() { Key = "F2" } };
            handler.Raise(args2);
            Assert.AreEqual(2, args2.Data.Value);

            var args3 = new BattleEventArgs<KeyedArgs>() { Data = new KeyedArgs() { Key = "F4" } };
            handler.Raise(args3);
            Assert.AreEqual(0, args3.Data.Value);
        }

        private class KeyedArgs : IRPGKey
        {
            public string Key { get; set; }
            public int Value { get; set; }
        }

        private static class BattleEventActions
        {
            public static bool Stop(BattleEventArgs<string> args)
            {
                return true;
            }

            public static bool AddE(BattleEventArgs<string> args)
            {
                args.Data += "e";
                return false;
            }

            public static bool AddQ(BattleEventArgs<string> args)
            {
                args.Data += "q";
                return false;
            }

            [BattleEventAction(typeof(object), Priority = 50)]
            public static bool AddC(BattleEventArgs<string> args)
            {
                args.Data += "c";
                return false;
            }

            public static bool AddB(BattleEventArgs<string> args)
            {
                args.Data += "b";
                return false;
            }

            [BattleEventAction(typeof(object), Priority = 250)]
            public static bool AddA(BattleEventArgs<string> args)
            {
                args.Data += "a";
                return false;
            }

            [BattleEventAction(typeof(object), Priority = 50)]
            public static bool Add3(BattleEventArgs args)
            {
                args.Data += "3";
                return false;
            }

            public static bool Add2(BattleEventArgs args)
            {
                args.Data += "2";
                return false;
            }

            [BattleEventAction(typeof(object), Priority = 250)]
            public static bool Add1(BattleEventArgs args)
            {
                args.Data += "1";
                return false;
            }

            [BattleEventFilterAction("F1", typeof(KeyedArgs))]
            public static bool F1(BattleEventArgs<KeyedArgs> args)
            {
                args.Data.Value = 1;
                return false;
            }

            [BattleEventFilterAction("F2", typeof(KeyedArgs))]
            public static bool F2(BattleEventArgs<KeyedArgs> args)
            {
                args.Data.Value = 2;
                return false;
            }

            [BattleEventFilterAction("F3", typeof(KeyedArgs))]
            public static bool F3(BattleEventArgs<KeyedArgs> args)
            {
                args.Data.Value = 3;
                return false;
            }
        }
    }
}
