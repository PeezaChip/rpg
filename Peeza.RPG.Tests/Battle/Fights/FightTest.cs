﻿using Peeza.RPG.Base.Battle.Arguments.Damage;
using Peeza.RPG.Base.Battle.BattleEntities;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Base.Resources;
using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.Interfaces;
using Peeza.RPG.Core.Battle.Events.Helpers;
using Peeza.RPG.Core.Realizations.Battle;
using Peeza.RPG.Core.Realizations.BattleFields;
using Peeza.RPG.Core.Realizations.Fights;
using Peeza.RPG.Core.Realizations.Fights.RelationshipManagers;
using Peeza.RPG.Tests.Core;

namespace Peeza.RPG.Tests.Battle.Fights
{
    [TestClass]
    public class FightTest
    {
        [TestMethod]
        public async Task TestFightRoundLimit()
        {
            var random = new Random(111);

            var logger = new ConsoleBattleLogger();

            var fieldAccessor = new BattleFieldAccessor();
            var roundHandler = new BasicRoundHandler(new BasicTurnHandler(fieldAccessor), random, fieldAccessor, logger);
            var battleEventArgsMaker = new BasicBattleEventArgsMaker(roundHandler.GetQueueAccessor(), logger, random, new BattleFieldAccessor());
            var battleSidesAccessor = new BasicBattleSidesAccessor();
            var relationshipManager = new BrawlRelationshipManager(battleSidesAccessor);
            battleSidesAccessor.Add([GenerateBattleSide("Red", 2, battleEventArgsMaker, relationshipManager, false), GenerateBattleSide("Blue", 2, battleEventArgsMaker, relationshipManager, false)]);
            var fightResolver = new BasicFightResolver(battleSidesAccessor, roundHandler, fieldAccessor, 5, logger);

            var cts = new CancellationTokenSource();
            await fightResolver.ProccessFight(cts.Token);

            // this is correct since we have events, but no stats, so basically each attack results in a miss
        }

        [TestMethod]
        public async Task TestFight()
        {
            var random = new Random(111);

            var logger = new ConsoleBattleLogger();

            var fieldAccessor = new BattleFieldAccessor();
            var roundHandler = new BasicRoundHandler(new BasicTurnHandler(fieldAccessor), random, fieldAccessor, logger);
            var battleEventArgsMaker = new BasicBattleEventArgsMaker(roundHandler.GetQueueAccessor(), logger, random, new BattleFieldAccessor());
            var battleSidesAccessor = new BasicBattleSidesAccessor();
            var relationshipManager = new BrawlRelationshipManager(battleSidesAccessor);
            battleSidesAccessor.Add([GenerateBattleSide("Red", 2, battleEventArgsMaker, relationshipManager, true), GenerateBattleSide("Blue", 2, battleEventArgsMaker, relationshipManager, true)]);
            var fightResolver = new BasicFightResolver(battleSidesAccessor, roundHandler, fieldAccessor, logger: logger);

            var cts = new CancellationTokenSource();
            await fightResolver.ProccessFight(cts.Token);
        }

        private static IBattleSide GenerateBattleSide(string prefix, int count, IBattleEventArgsMaker battleEventArgsMaker, IBattleSideRelationshipManager relationshipManager, bool doStats)
        {
            var battleSide = new BattleSide();

            for (int i = 0; i < count; i++)
            {
                var be = new BasicBattleEntity($"{prefix} {(char)('A' + i)}", battleSide, battleEventArgsMaker, relationshipManager, null);
                battleSide.Add(be);

                if (i % 2 == 0)
                {
                    be.DefaultAttackFlags = DamageInstanceInfoFlags.Melee | DamageInstanceInfoFlags.Physical;
                }
                else
                {
                    be.DefaultAttackFlags = DamageInstanceInfoFlags.Ranged | DamageInstanceInfoFlags.Magic;
                }

                BattleEventLoader.LoadEventActionsFromType(typeof(BasicBattleEntityEventsImplementations), be.EventHandler);

                be.Resources.Add(BaseResourceScalings.CalculateHealth(1));
                if (doStats)
                {
                    be.Stats.Add(BaseStatScalings.CalculateAttack(1));
                    be.Stats.Add(BaseStatScalings.CalculateArmor(1));
                    be.Stats.Add(BaseStatScalings.CalculateMagicResistance(1));
                    be.Stats.Add(new AccuracyStat(0.75));
                    be.Stats.Add(new CritChanceStat(0.25));
                }
            }

            return battleSide;
        }
    }
}
