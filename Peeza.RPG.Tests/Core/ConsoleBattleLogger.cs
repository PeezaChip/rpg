﻿using Peeza.RPG.Core.Basic.Battle;

namespace Peeza.RPG.Tests.Core
{
    public class ConsoleBattleLogger : IBattleLogger
    {
        private readonly List<LogIndent> indents = [];

        public LogIndent IndentLog()
        {
            var loader = new LogIndent((l) =>
            {
                indents.Remove(l);
            });
            indents.Add(loader);
            return loader;
        }

        public void Log(string log, BattleLogVerbosity level = BattleLogVerbosity.Info)
        {
            if (indents.Count > 0 && !string.IsNullOrWhiteSpace(log)) Console.Write(new string(' ', indents.Count * 2));
            Console.WriteLine(log);
        }

        public string GetBattleLog() => "";
    }
}
