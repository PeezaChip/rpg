﻿using Peeza.RPG.Core.Basic.Auras;
using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.BattleFields;
using Peeza.RPG.Core.Basic.Cooldowns;

namespace Peeza.RPG.Tests.Core
{
    public class TestBattleField : IBattleField
    {
        private static int initCounter = 1;
        public string Name { get; set; } = $"TestField-{initCounter++}";

        public IBattleSidesAccessor AllBattleSides { get; set; }

        public bool SupportsAuras => Auras != null;
        public IAurasAccessor Auras { get; set; }

        public bool SupportsCooldowns => Cooldowns != null;
        public ICooldownAccessor Cooldowns { get; set; }

        public bool SupportsEvents => EventHandler != null;
        public IBattleFieldEventHandlersProvider EventHandler { get; set; }

        public void Hook(IBattleEntityEventHandlersProvider hookTo)
        {
            throw new NotImplementedException();
        }

        public void Unhook(IBattleEntityEventHandlersProvider hookTo)
        {
            throw new NotImplementedException();
        }
    }
}
