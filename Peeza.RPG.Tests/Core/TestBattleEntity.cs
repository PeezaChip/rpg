﻿using Peeza.RPG.Core.Basic.Ailments;
using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Cooldowns;
using Peeza.RPG.Core.Basic.Resources;
using Peeza.RPG.Core.Basic.Spells;
using Peeza.RPG.Core.Basic.Stats;
using Peeza.RPG.Core.Basic.Statuses;

namespace Peeza.RPG.Tests.Core
{
    public class TestBattleEntity : IQueableBattleEntity
    {
        private static int initCounter = 1;
        public string Name { get; set; } = $"TestEntity-{initCounter++}";

        public bool CanFight { get; set; } = true;

        public IBattleSide BattleSide { get; set; }
        public List<IBattleEntity> AllBattleEntities { get; set; }

        public bool SupportsEvents => EventHandler != null;
        public IBattleEntityEventHandlersProvider EventHandler { get; set; }

        public bool HaveMinions => Minions != null;
        public IMinionsAccessor Minions { get; set; }

        public bool SupportsCooldowns => Cooldowns != null;
        public ICooldownAccessor Cooldowns { get; set; }

        public bool SupportsAilments => Ailments != null;
        public IAilmentsAccessor Ailments { get; set; }

        public bool SupportsBuffs => Buffs != null;
        public IBuffsAccessor Buffs { get; set; }

        public bool SupportsStatuses => Statuses != null;
        public IStatusAccessor Statuses { get; set; }

        public bool SupportStats => Stats != null;
        public IStatsAccessor Stats { get; set; }

        public bool SupportResources => Resources != null;
        public IResourcesAccessor Resources { get; set; }

        public bool SupportSpells => Spells != null;
        public ISpellsAccessor Spells { get; set; }

        public void TurnAction()
        {
            throw new NotImplementedException();
        }
    }
}
