﻿namespace Peeza.RPG.Config
{
    public class RPGConfiguration
    {
        public double BaseDropChanceBonus { get; set; } = 0;

        public ulong ExpNeeded { get; set; } = 50;
        public double ExpCurve { get; set; } = 1.4;

        public uint MaxLevel { get; set; } = 50;

        public double BaseExpMultiplier { get; set; } = 1;

        public double DungeonEncountersRate { get; set; } = 0.05;
        public double DungeonEnemiesRate { get; set; } = 0.4;


        public uint MaxPartySize { get; set; } = 4;
        public uint MinLevelForParty { get; set; } = 5;
    }
}
