﻿using System;
using System.Reflection;

namespace Peeza.RPG.Loading
{
    [AttributeUsage(AttributeTargets.Method)]
    internal abstract class LoadMethodAttribute : Attribute
    {
        public abstract bool CanLoad(Type type);
        public abstract void Invoke(Type type, MethodInfo methodInfo);
    }
}
