﻿using Peeza.RPG.Core.Heroes.Races;
using Peeza.RPG.Core.Heroes.Specs;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;

namespace Peeza.RPG.Loading
{
    [SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "Used via reflection")]
    internal static class LoadRPG
    {
        public static void LoadAll()
        {
            var loadMethods = GetLoadMethods();
            foreach (var assembly in ExpansionLoadOrder.Expansions)
            {
                foreach (var type in assembly.GetExportedTypes())
                {
                    if (type.IsAbstract || type.IsInterface) continue;

                    foreach (var (method, meta) in loadMethods)
                    {
                        if (meta.CanLoad(type))
                        {
                            meta.Invoke(type, method);
                        }
                    }
                }
            }
        }

        [IsAssignableLoadMethod(typeof(RaceDeclaration))]
        private static void LoadRace(RaceDeclaration race)
        {
            RPG.RaceDeclarations.Add(race);
        }

        [IsAssignableLoadMethod(typeof(SpecDeclaration))]
        private static void LoadSpec(SpecDeclaration spec)
        {
            RPG.SpecDeclarations.Add(spec);
        }

        private static Dictionary<MethodInfo, LoadMethodAttribute> GetLoadMethods()
        {
            var result = new Dictionary<MethodInfo, LoadMethodAttribute>();
            var loadMethods = typeof(LoadRPG).GetMethods(BindingFlags.Static | BindingFlags.NonPublic).Where(m => m.GetCustomAttribute<LoadMethodAttribute>() != null);

            foreach (var method in loadMethods)
            {
                var attrubute = method.GetCustomAttribute<LoadMethodAttribute>();
                result.Add(method, attrubute);
            }

            return result;
        }

    }
}
