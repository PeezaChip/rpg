﻿using System.Collections.Generic;
using System.Reflection;

namespace Peeza.RPG.Loading
{
    internal static class ExpansionLoadOrder
    {
        static ExpansionLoadOrder()
        {
            Expansions = GetExpansionAssemblies();
        }

        private static List<Assembly> GetExpansionAssemblies()
        {
            return
            [
                typeof(Base.Expansion).Assembly
            ];
        }

        public static IEnumerable<Assembly> Expansions { get; }
    }
}
