﻿using System;
using System.Reflection;

namespace Peeza.RPG.Loading
{
    [AttributeUsage(AttributeTargets.Method)]
    internal class IsAssignableLoadMethodAttribute : LoadMethodAttribute
    {
        private readonly Type canLoadType;

        public override bool CanLoad(Type type)
        {
            return canLoadType.IsAssignableFrom(type);
        }

        public override void Invoke(Type type, MethodInfo methodInfo)
        {
            var instance = Activator.CreateInstance(type);
            methodInfo.Invoke(null, [instance]);
        }

        public IsAssignableLoadMethodAttribute(Type type)
        {
            canLoadType = type;
        }
    }
}
