﻿using Peeza.RPG.Config;
using Peeza.RPG.Core.Heroes.Races;
using Peeza.RPG.Core.Heroes.Specs;
using Peeza.RPG.Core.Realizations.Heroes.Races;
using Peeza.RPG.Core.Realizations.Heroes.Specs;
using Peeza.RPG.Loading;

namespace Peeza.RPG
{
    public static class RPG
    {
        public static RPGConfiguration Configuration { get; set; }

        public static IRaceDeclarationAccessor RaceDeclarations { get; } = new RacesDictionary();
        public static ISpecDeclarationAccessor SpecDeclarations { get; } = new SpecsDictionary();

        static RPG()
        {
            LoadRPG.LoadAll();
        }
    }
}
