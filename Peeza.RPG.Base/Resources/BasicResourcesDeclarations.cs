﻿using Peeza.RPG.Core.Basic.Resources;

namespace Peeza.RPG.Base.Resources
{
    public class HealthResourceDeclaration : ResourceDeclaration
    {
        public HealthResourceDeclaration() : base(100, true) { }
    }

    public class ShieldResourceDeclaration : ResourceDeclaration
    {
        public ShieldResourceDeclaration() : base(100, true) { }
    }

    public class ManaResourceDeclaration : ResourceDeclaration
    {
        public ManaResourceDeclaration() : base(100, true) { }
    }
}
