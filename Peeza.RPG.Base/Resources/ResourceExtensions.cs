﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Core.Basic.Resources;
using Peeza.RPG.Core.Battle.Events;

namespace Peeza.RPG.Base.Resources
{
    public static class ResourceExtensions
    {
        public static ResourceChangeInfo ChangeResource(this Resource resource, long value, object reason = null)
        {
            var oldResourceValue = resource.Value;
            resource.Value += value;

            return new ResourceChangeInfo(resource.Key, oldResourceValue, resource.Value, reason);
        }

        public static void ChangeAndSelfRaise<T>(this IResourcesAccessor resourceAccessor, string resourceKey, BattleEventArgs<T> args, long value, object reason = null)
        {
            var resource = resourceAccessor.GetByKey(resourceKey);
            if (resource == null) return;

            resource.ChangeAndSelfRaise(args, value, reason);
        }

        public static void ChangeAndSelfRaise(this IResourcesAccessor resourceAccessor, string resourceKey, BattleEventArgs args, long value, object reason = null)
        {
            var resource = resourceAccessor.GetByKey(resourceKey);
            if (resource == null) return;

            resource.ChangeAndSelfRaise(args, value, reason);
        }

        public static void ChangeAndSelfRaise<T>(this Resource resource, BattleEventArgs<T> args, long value, object reason = null)
        {
            resource.ChangeResource(value, reason).SelfRaiseChange(args);
        }

        public static void ChangeAndSelfRaise(this Resource resource, BattleEventArgs args, long value, object reason = null)
        {
            resource.ChangeResource(value, reason).SelfRaiseChange(args);
        }

        public static void SelfRaiseChange<T>(this ResourceChangeInfo resourceChangeInfo, BattleEventArgs<T> args)
        {
            args.Self.EventHandler.OnResourceChange(BattleEventArgs<ResourceChangeInfo>.CloneFrom(args, data: resourceChangeInfo));
        }

        public static void SelfRaiseChange(this ResourceChangeInfo resourceChangeInfo, BattleEventArgs args)
        {
            args.Self.EventHandler.OnResourceChange(BattleEventArgs<ResourceChangeInfo>.CloneFrom(args, data: resourceChangeInfo));
        }
    }
}
