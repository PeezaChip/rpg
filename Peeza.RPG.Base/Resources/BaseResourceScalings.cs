﻿using Peeza.RPG.Core.Basic.Buffs;

namespace Peeza.RPG.Base.Resources
{
    public class BaseResourceScalings
    {
        public static HealthResource CalculateHealth(uint level, IBuffsAccessor buffs = null)
        {
            return new HealthResource(220 + 48 * level, buffs);
        }

        public static ManaResource CalculateMana(uint level, IBuffsAccessor buffs = null)
        {
            return new ManaResource(90 + 10 * level, buffs);
        }
    }
}
