﻿using Peeza.RPG.Core.Basic.Interfaces;

namespace Peeza.RPG.Base
{
    public class Expansion : IExpansion
    {
        public string Name => "Base Expansion";
    }

}
