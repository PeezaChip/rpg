﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Base.Resources;
using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Cooldowns;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace Peeza.RPG.Base.Spells.Implementations
{
    public class DispellSpell : BaseSpell<OnTimeEnum>
    {
        public DispellSpell(IBuffsAccessor buffs = null) : base(BasicBattleSharedEventsDeclarationsExtensions.OnTurnDeclaration, [], BasicResourcesDeclarationsExtensions.ManaResourceKey, CooldownTypeFlags.OnTurnStart, 100)
        {
            Stats.Add(new SpellCooldownStat(Key, 3, buffs));
            Stats.Add(new SpellCostStat(Key, 30, buffs));
        }

        public override string Key => "Dispell";

        protected override string SpellDescription => "On start of the turn dispells single debuff from self or an ally";

        protected override bool BattleEventAction(BattleEventArgs<OnTimeEnum> args)
        {
            IBattleEntity entityToDispel;
            Buff buffToDispell;

            List<IBattleEntity> entities = args.Self.BattleSide == null || args.Self.BattleSide.BattleEntities == null ? [args.Self] : [.. args.Self.BattleSide.BattleEntities];

            entityToDispel = entities.Where(e => e.SupportsBuffs && e.SupportsEvents && GetDispellableDebuffs(e.Buffs).Any()).TakeRandom(args.Random);
            buffToDispell = GetDispellableDebuffs(entityToDispel.Buffs).TakeRandom(args.Random);

            args.Log($"{args.Self.Name} dispells {(args.Self == entityToDispel ? "self" : entityToDispel.Name)}");

            entityToDispel.EventHandler.OnAffectedWithStatus(BattleEventArgs<OnStatusData>.CloneFrom(args, entityToDispel, args.Self, data: new()
            {
                LiftedStatuses = [buffToDispell]
            }));

            return false;
        }

        protected override bool Precondition(BattleEventArgs<OnTimeEnum> args)
        {
            if (args.Data != OnTimeEnum.Start) return false;
            List<IBattleEntity> entities = args.Self.BattleSide == null || args.Self.BattleSide.BattleEntities == null ? [args.Self] : [.. args.Self.BattleSide.BattleEntities];
            return entities.Where(e => e.SupportsBuffs && e.SupportsEvents && GetDispellableDebuffs(e.Buffs).Any()).Any();
        }

        private static IEnumerable<Buff> GetDispellableDebuffs(IBuffsAccessor buffs) => buffs.GetAll(buff => buff.Flags.HasFlag(BuffFlags.Negative | BuffFlags.Dispellable));
    }
}
