﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Cooldowns;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Battle.Events.Attributes;

namespace Peeza.RPG.Base.Spells.Implementations
{
    public class WarriorShoutSpell : BaseSpell<OnTimeEnum>
    {
        public WarriorShoutSpell(IBuffsAccessor buffs = null) : base(BasicBattleSharedEventsDeclarationsExtensions.OnTurnDeclaration, [], null, CooldownTypeFlags.OnTurnStart, 100)
        {
            Stats.Add(new SpellCooldownStat(Key, 3, buffs));
            Stats.Add(new SpellCostStat(Key, 30, buffs));
            Stats.Add(new SpellDurationStat(Key, 2, buffs));
        }

        public override string Key => "Warrior Shout";

        protected override string SpellDescription => $"Shouts to increase aggro for `50%` for `{Stats.GetSpellDuration()}` rounds";

        [BattleEventAction(typeof(OnTurn), Priority = 100)]
        protected override bool BattleEventAction(BattleEventArgs<OnTimeEnum> args)
        {
            if (args.Data != OnTimeEnum.Start) return false;
            if (!args.Self.SupportsBuffs) return false;
            if (!args.Self.SupportsEvents) return false;

            args.Log($"{args.Self.Name} shouts");
            args.Self.EventHandler.OnAffectedWithStatus(BattleEventArgs<OnStatusData>.CloneFrom(args, false, new OnStatusData()
            {
                NewStatuses = [
                    new AggroBuff(args.Self, 1.5, BuffFlags.Positive | BuffFlags.Multiplicative, 2)
                ]
            }));

            return false;
        }
    }
}