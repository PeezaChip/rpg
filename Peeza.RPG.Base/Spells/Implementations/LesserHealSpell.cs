﻿using Peeza.RPG.Base.Battle.Arguments.Damage;
using Peeza.RPG.Base.Battle.Arguments.Heal;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Base.Resources;
using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Cooldowns;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace Peeza.RPG.Base.Spells.Implementations
{
    public class LesserHealSpell : BaseSpell<DamageInfo>
    {
        public LesserHealSpell(IBuffsAccessor buffs = null) : base(BasicBattleEntityEventsDeclarationsExtensions.OnCalculateDamageDeclaration, [], BasicResourcesDeclarationsExtensions.ManaResourceKey, CooldownTypeFlags.OnTurnStart, 106)
        {
            Stats.Add(new SpellCooldownStat(Key, 3, buffs));
            Stats.Add(new SpellCostStat(Key, 40, buffs));
            Stats.Add(new SpellPowerStat(Key, 1.2, buffs));
        }

        public override string Key => "Lesser Heal";

        protected override string SpellDescription => $"Instead of attacking, heal self or ally for {Stats.GetSpellPower():0%} of your attack";

        public override string Details => 
            "- Only targets units lower than `60%` HP\n" +
            "- If there are more than one target, targets the one with lower HP%";

        protected override bool BattleEventAction(BattleEventArgs<DamageInfo> args)
        {
            List<IBattleEntity> entities = args.Self.BattleSide == null || args.Self.BattleSide.BattleEntities == null ? [args.Self] : [.. args.Self.BattleSide.BattleEntities];
            var entityToHeal = entities.Where(e => e.SupportResources && e.SupportsEvents && e.Resources.GetHealth() != null && e.Resources.GetHealth().Percentage < 60).OrderBy(e => e.Resources.GetHealth().Percentage).FirstOrDefault();

            if (entityToHeal == null) return false;

            var healData = new HealInfo()
            {
                HealInstances = []
            };

            var healInstance = new HealInstanceInfo()
            {
                Source = this,
                Flags = HealInstanceInfoFlags.Health
            };

            healData.HealInstances.Add(healInstance);

            if (args.Random.Roll(args.Self.Stats.GetCritChance()))
            {
                healInstance.Modifiers |= HealInstanceInfoModifierFlags.Crit;
                healInstance.Heal = (long)(args.Self.Stats.GetAttack() * Stats.GetSpellPower() * args.Self.Stats.GetCritDamage());
            }
            else
            {
                healInstance.Heal = (long)(args.Self.Stats.GetAttack() * Stats.GetSpellPower());
            }

            args.Log($"{args.Self.Name} is casting {Key} on {entityToHeal.Name}");
            using var indent = args.Logger?.IndentLog();
            entityToHeal.EventHandler.OnGetHeal(BattleEventArgs<HealInfo>.CloneFrom(args, entityToHeal, args.Self, healData));

            return true;
        }

        protected override bool Precondition(BattleEventArgs<DamageInfo> args)
        {
            if (!args.Self.SupportStats) return false;

            List<IBattleEntity> entities = args.Self.BattleSide == null || args.Self.BattleSide.BattleEntities == null ? [args.Self] : [.. args.Self.BattleSide.BattleEntities];

            return entities.Where(e => e.SupportResources && e.SupportsEvents && e.Resources.GetHealth() != null).Any(e => e.Resources.GetHealth().Percentage < 60);
        }
    }
}
