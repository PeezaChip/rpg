﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Base.Resources;
using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Cooldowns;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace Peeza.RPG.Base.Spells.Implementations
{
    public class SelfDispellSpell : BaseSpell<OnTimeEnum>
    {
        public SelfDispellSpell(IBuffsAccessor buffs = null) : base(BasicBattleSharedEventsDeclarationsExtensions.OnTurnDeclaration, [], BasicResourcesDeclarationsExtensions.ManaResourceKey, CooldownTypeFlags.OnTurnStart, 100)
        {
            Stats.Add(new SpellCooldownStat(Key, 3, buffs));
            Stats.Add(new SpellCostStat(Key, 20, buffs));
        }

        public override string Key => "Self Dispell";

        protected override string SpellDescription => "On start of the turn dispells self from a single debuff";

        protected override bool BattleEventAction(BattleEventArgs<OnTimeEnum> args)
        {
            var buffToDispell = GetDispellableDebuffs(args.Self.Buffs).TakeRandom(args.Random);

            args.Log($"{args.Self.Name} dispells self");

            args.Self.EventHandler.OnAffectedWithStatus(BattleEventArgs<OnStatusData>.CloneFrom(args, data: new()
            {
                LiftedStatuses = [buffToDispell]
            }));

            return false;
        }

        protected override bool Precondition(BattleEventArgs<OnTimeEnum> args)
        {
            if (args.Data != OnTimeEnum.Start) return false;
            if (!args.Self.SupportsBuffs) return false;
            if (!args.Self.SupportsEvents) return false;

            var dispellableDebuffs = GetDispellableDebuffs(args.Self.Buffs);

            return dispellableDebuffs.Any();
        }

        private static IEnumerable<Buff> GetDispellableDebuffs(IBuffsAccessor buffs) => buffs.GetAll(buff => buff.Flags.HasFlag(BuffFlags.Negative | BuffFlags.Dispellable));
    }
}
