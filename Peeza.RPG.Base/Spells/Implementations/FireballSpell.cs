﻿using Peeza.RPG.Base.Battle.Arguments.Damage;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Base.Resources;
using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Cooldowns;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Extensions;

namespace Peeza.RPG.Base.Spells.Implementations
{
    public class FireballSpell : BaseSpell<DamageInfo>
    {
        public FireballSpell(IBuffsAccessor buffs = null) : base(BasicBattleEntityEventsDeclarationsExtensions.OnCalculateDamageDeclaration, [], BasicResourcesDeclarationsExtensions.ManaResourceKey, CooldownTypeFlags.OnTurnStart, 105)
        {
            Stats.Add(new SpellCooldownStat(Key, 3, buffs));
            Stats.Add(new SpellCostStat(Key, 50, buffs));
            Stats.Add(new SpellPowerStat(Key, 2, buffs));
        }

        public override string Key => "Fireball";

        protected override string SpellDescription => $"Instead of attacking, shoots a fireball that deals {Stats.GetSpellPower():0%} of your attack";

        protected override bool BattleEventAction(BattleEventArgs<DamageInfo> args)
        {
            args.Data = new DamageInfo()
            {
                DamageInstances = []
            };

            var dmgInstance = new DamageInstanceInfo()
            {
                Source = this,
                Flags = DamageInstanceInfoFlags.Ranged | DamageInstanceInfoFlags.Magic,
            };

            args.Data.DamageInstances.Add(dmgInstance);

            if (!args.Random.Roll(args.Self.Stats.GetAccuracy()))
            {
                dmgInstance.Result = DamageInstanceInfoResultFlags.Miss;
                return false;
            }

            if (args.Random.Roll(args.Self.Stats.GetCritChance()))
            {
                dmgInstance.Modifiers |= DamageInstanceInfoModifierFlags.Crit;
                dmgInstance.Damage = (long)(args.Self.Stats.GetAttack() * Stats.GetSpellPower() * args.Self.Stats.GetCritDamage());
            }
            else
            {
                dmgInstance.Damage = (long)(args.Self.Stats.GetAttack() * Stats.GetSpellPower());
            }

            return true;
        }

        protected override bool Precondition(BattleEventArgs<DamageInfo> args)
        {
            if (args.Target == null) return false;
            if (!args.Self.SupportStats) return false;

            return true;
        }
    }
}
