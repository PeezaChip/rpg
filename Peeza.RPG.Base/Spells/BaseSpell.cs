﻿using Peeza.RPG.Base.Resources;
using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Cooldowns;
using Peeza.RPG.Core.Basic.Resources;
using Peeza.RPG.Core.Basic.Spells;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Realizations.Stats;
using System.Text;

namespace Peeza.RPG.Base.Spells
{
    public abstract class BaseSpell<T> : BaseSpellShared
    {
        protected BattleEventDeclaration<T> BattleEventDeclaration { get; }
        protected abstract bool BattleEventAction(BattleEventArgs<T> args);
        protected virtual bool Precondition(BattleEventArgs<T> args) => true;

        public BaseSpell(BattleEventDeclaration<T> battleEventDeclaration, StatList stats, string resourceKey = null, CooldownTypeFlags? cooldownType = null, int? priority = null) : base(stats, resourceKey, cooldownType, priority)
        {
            BattleEventDeclaration = battleEventDeclaration;
        }

        private BattleEventAction<T> wrapped;
        private BattleEventAction<T> WrapCall(BattleEventAction<T> action, BattleEventAction<T> customPrecondition = null)
        {
            wrapped ??= (BattleEventArgs<T> args) =>
            {
                if (CooldownType.HasValue && !CooldownPrecondition(args.Self.Cooldowns)) return false;
                if (ResourceKey != null && !ResourcePrecondition(args.Self.Resources)) return false;
                if (!customPrecondition?.Invoke(args) ?? false) return false;

                PayCost(args);
                var returnValue = action(args);

                if (CooldownType.HasValue) ApplyCooldown(args.Self.Cooldowns);

                return returnValue;
            };

            return wrapped;
        }

        public override void Hook(IBattleEntityEventHandlersProvider hookTo)
        {
            hookTo.HookEvent(BattleEventDeclaration, WrapCall(BattleEventAction, Precondition));
        }

        public override void Unhook(IBattleEntityEventHandlersProvider hookTo)
        {
            hookTo.UnhookEvent(BattleEventDeclaration, WrapCall(BattleEventAction, Precondition));
        }
    }

    public abstract class BaseSpell : BaseSpellShared
    {
        protected BattleEventDeclaration BattleEventDeclaration { get; }
        protected abstract bool BattleEventAction(BattleEventArgs args);
        protected virtual bool Precondition(BattleEventArgs args) => true;

        public BaseSpell(BattleEventDeclaration battleEventDeclaration, StatList stats, string resourceKey = null, CooldownTypeFlags? cooldownType = null, int? priority = null) : base(stats, resourceKey, cooldownType, priority)
        {
            BattleEventDeclaration = battleEventDeclaration;
        }

        private BattleEventAction wrapped;
        private BattleEventAction WrapCall(BattleEventAction action, BattleEventAction customPrecondition = null)
        {
            wrapped ??= (BattleEventArgs args) =>
            {
                if (CooldownType.HasValue && !CooldownPrecondition(args.Self.Cooldowns)) return false;
                if (ResourceKey != null && !ResourcePrecondition(args.Self.Resources)) return false;
                if (!customPrecondition?.Invoke(args) ?? true) return false;

                PayCost(args);
                var returnValue = action(args);

                if (CooldownType.HasValue) ApplyCooldown(args.Self.Cooldowns);

                return returnValue;
            };

            return wrapped;
        }

        public override void Hook(IBattleEntityEventHandlersProvider hookTo)
        {
            hookTo.HookEvent(BattleEventDeclaration, WrapCall(BattleEventAction, Precondition));
        }

        public override void Unhook(IBattleEntityEventHandlersProvider hookTo)
        {
            hookTo.UnhookEvent(BattleEventDeclaration, WrapCall(BattleEventAction, Precondition));
        }
    }

    public abstract class BaseSpellShared : Spell
    {
        public override string Description => ConstructDescription();
        public override string Details => string.Empty;

        protected abstract string SpellDescription { get; }


        protected BaseSpellShared(StatList stats, string resourceKey = null, CooldownTypeFlags? cooldownType = null, int? priority = null) : base(stats, resourceKey, cooldownType, priority)
        {
        }

        private string ConstructDescription()
        {
            var sb = new StringBuilder();

            if (CooldownType != null)
            {
                string time = CooldownType switch
                {
                    CooldownTypeFlags.OnFightStart or CooldownTypeFlags.OnFightEnd or CooldownTypeFlags.OnFight => " fights",
                    CooldownTypeFlags.OnRoundStart or CooldownTypeFlags.OnRoundEnd or CooldownTypeFlags.OnRound => " rounds",
                    CooldownTypeFlags.OnTurnStart or CooldownTypeFlags.OnTurnEnd or CooldownTypeFlags.OnTurn => " turns",
                    _ => "",
                };
                sb.AppendLine($"- _Cooldown_: `{(CooldownType.Value == CooldownTypeFlags.Never ? "Infinite" : Stats.GetSpellCooldown())}`{time}");
            }
            else
            {
                sb.AppendLine("- _Cooldown_: None");
            }

            if (ResourceKey != null)
            {
                sb.AppendLine($"- _Cost_: `{Stats.GetSpellCost()}` {ResourceKey}");
            }
            else
            {
                sb.AppendLine("- _Cost_: None");
            }

            sb.AppendLine();

            sb.Append(SpellDescription);

            return sb.ToString();
        }

        protected bool CooldownPrecondition(ICooldownAccessor cooldowns)
        {
            if (CooldownType == null) return true;
            if (cooldowns == null) return false;

            return cooldowns.GetByKey(Key) == null;
        }

        protected bool ResourcePrecondition(IResourcesAccessor resources)
        {
            if (ResourceKey == null) return true;
            if (resources == null) return false;

            return resources.GetByKey(ResourceKey).Value > Stats.GetSpellCost();
        }

        protected void ApplyCooldown(ICooldownAccessor cooldowns)
        {
            if (CooldownType == null) return;
            if (cooldowns == null) return;

            cooldowns.Add(new Cooldown(Key, Stats.GetSpellCooldown(), CooldownType.Value));
        }

        protected void PayCost<T>(BattleEventArgs<T> args)
        {
            if (ResourceKey == null) return;
            if (args.Self.EventHandler == null) return;

            args.Self.Resources.ChangeAndSelfRaise(ResourceKey, args, -Stats.GetSpellCost(), this);
        }
    }
}
