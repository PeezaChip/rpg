﻿using Peeza.RPG.Base.Battle.Arguments.Damage;
using Peeza.RPG.Core.Heroes.Specs;

namespace Peeza.RPG.Base.Heroes.Specs
{
    public abstract class BaseSpecDeclaration : SpecDeclaration
    {
        public abstract DamageInstanceInfoFlags DefaultAttackFlags { get; }
    }
}
