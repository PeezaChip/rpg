﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Base.Battle.Arguments.Damage;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Statuses;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Battle.Events.Attributes;
using System.Linq;

namespace Peeza.RPG.Base.Heroes.Specs.Ability
{
    public class RogueAbilityFactory : BattleEntityAbilityFactory
    {
        public override void Hook(IBattleEntityEventHandlersProvider hookTo)
        {
            hookTo.HookEvent(BasicBattleEntityEventsDeclarationsExtensions.OnGetHitResultDeclaration, RogueCheckIfHit);
            hookTo.HookEvent(BasicBattleSharedEventsDeclarationsExtensions.OnRoundDeclaration, RogueApplyCritBuff);
        }

        public override void Unhook(IBattleEntityEventHandlersProvider hookTo)
        {
            throw new System.NotImplementedException();
        }

        [BattleEventAction(typeof(OnGetHitResult), Priority = 105)]
        public static bool RogueCheckIfHit(BattleEventArgs<DamageInfo> args)
        {
            if (!args.Self.SupportsStatuses) return false;

            if (args.Data.TotalDamageDealt > 0)
            {
                if (args.Self.Statuses.GetAll(nameof(RogueRecievedDamageStatus)).Any()) return false;

                args.Self.EventHandler.OnAffectedWithStatus(BattleEventArgs<OnStatusData>.CloneFrom(args, data: new()
                {
                    NewStatuses = [new RogueRecievedDamageStatus(args.Self)],
                    LiftedStatuses = [.. args.Self.Buffs.GetAll(buff => buff is RogueCritBuff)]
                }));
            }

            return false;
        }

        [BattleEventAction(typeof(OnRound), Priority = 100)]
        public static bool RogueApplyCritBuff(BattleEventArgs<OnTimeEnum> args)
        {
            if (args.Data != OnTimeEnum.End) return false;
            if (!args.Self.SupportsStatuses) return false;
            if (!args.Self.SupportsEvents) return false;

            if (args.Self.Statuses.GetAll(nameof(RogueRecievedDamageStatus)).Any())
            {
                args.Self.EventHandler.OnAffectedWithStatus(BattleEventArgs<OnStatusData>.CloneFrom(args, data: new()
                {
                    LiftedStatuses = [.. args.Self.Statuses.GetAll(nameof(RogueRecievedDamageStatus))]
                }));
            }
            else
            {
                if (args.Self.Buffs.GetAll(buff => buff is RogueCritBuff).Count() < 5)
                {
                    args.Self.EventHandler.OnAffectedWithStatus(BattleEventArgs<OnStatusData>.CloneFrom(args, data: new()
                    {
                        NewStatuses = [
                            new RogueCritBuff(args.Self)
                        ]
                    }));
                }
            }

            return false;
        }


        public class RogueRecievedDamageStatus(IBattleEntity battleEntity) : Status(nameof(RogueRecievedDamageStatus), battleEntity);
        public class RogueCritBuff(IBattleEntity battleEntity) : CritChanceBuff(battleEntity, 0.02, BuffFlags.InfiniteDuration | BuffFlags.Positive);
    }
}