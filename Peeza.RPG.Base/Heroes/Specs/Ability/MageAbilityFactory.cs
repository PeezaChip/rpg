﻿using Peeza.RPG.Base.Battle.Arguments.Damage;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Base.Resources;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Battle.Events.Attributes;
using System;

namespace Peeza.RPG.Base.Heroes.Specs.Ability
{
    public class MageAbilityFactory : BattleEntityAbilityFactory
    {
        public override void Hook(IBattleEntityEventHandlersProvider hookTo)
        {
            hookTo.HookEvent(BasicBattleEntityEventsDeclarationsExtensions.OnAttackResolveDeclaration, MageAfterAttackManaRegen);
        }

        public override void Unhook(IBattleEntityEventHandlersProvider hookTo)
        {
            throw new NotImplementedException();
        }

        [BattleEventAction(typeof(OnAttackResolve), Priority = 100)]
        public static bool MageAfterAttackManaRegen(BattleEventArgs<DamageInfo> args)
        {
            if (!args.Self.SupportResources) return false;

            args.Self.Resources.GetMana()?.ChangeAndSelfRaise(args, 10, nameof(MageAfterAttackManaRegen));

            return false;
        }
    }
}
