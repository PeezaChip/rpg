﻿using Peeza.RPG.Base.Battle.Arguments.Damage;
using Peeza.RPG.Base.Heroes.Specs.Spells;
using Peeza.RPG.Base.Heroes.Specs.Stats;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Interfaces;
using Peeza.RPG.Core.Heroes;
using Peeza.RPG.Core.Players;

namespace Peeza.RPG.Base.Heroes.Specs
{
    public class Warrior : BaseSpecDeclaration
    {
        public override string Key => "Warrior";

        public override string AbilityName => "Strength";

        public override string Description =>
            "- _Passive Bonus_: Receives a `10%` bonus to all defenses\n" +
            "- _Ability_: Can use following skills:\n" +
            "  - [Spell:Warrior Shout];";

        public override string ShortDescription => "Increased defenses, more aggro generation";

        public override string Icon => "<:Warrior:1309691236877209602>";

        public override DamageInstanceInfoFlags DefaultAttackFlags => DamageInstanceInfoFlags.Melee | DamageInstanceInfoFlags.Physical;

        public override IBattleEntityEventActionsProvider CreateAbilityFactory(Hero hero, IBuffsAccessor buffs)
        {
            return null;
        }

        public override IStatsFactory CreateStatsFactory(Hero hero, IBuffsAccessor buffs)
        {
            return new WarriorStatsFactory(buffs);
        }

        public override IResourcesFactory CreateResourcesFactory(Hero hero, IBuffsAccessor buffs)
        {
            return null;
        }

        public override ISpellsFactory CreateSpellsFactory(Hero hero, IBuffsAccessor buffs)
        {
            return new WarriorSpellsFactory(buffs);
        }

        public override bool IsAvailable(PlayerAccount playerAccount, Hero hero)
        {
            return true;
        }
    }
}
