﻿using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Stats;

namespace Peeza.RPG.Base.Heroes.Specs.Stats
{
    public class WarriorStatsFactory(IBuffsAccessor buffs) : BattleEntityStatsFactory(buffs)
    {
        public override void Calculate(IStatsAccessor stats)
        {
            Modify(stats.GetByKey<long>(BasicStatsDeclarationsExtensions.ArmorStatKey));
            Modify(stats.GetByKey<long>(BasicStatsDeclarationsExtensions.MagicResistanceStatKey));
        }

        private static void Modify(Stat<long> stat)
        {
            if (stat is IModifiableStat<long> modifiable)
            {
                modifiable.Modify((long)(stat.Value * 1.1));
            }
        }
    }
}
