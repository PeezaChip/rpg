﻿using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Stats;

namespace Peeza.RPG.Base.Heroes.Specs.Stats
{
    public class RogueStatsFactory(IBuffsAccessor buffs) : BattleEntityStatsFactory(buffs)
    {
        public override void Calculate(IStatsAccessor stats)
        {
            var dodge = stats.GetByKey<double>(BasicStatsDeclarationsExtensions.DodgeChanceStatKey);

            if (dodge == null)
            {
                stats.Add(new DodgeChanceStat(0.05, buffs));
            }
            else if (dodge is IModifiableStat<double> modifiable)
            {
                modifiable.Modify(dodge.BaseValue + 0.05);
            }
        }
    }
}