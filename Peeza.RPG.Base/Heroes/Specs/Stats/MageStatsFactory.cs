﻿using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Stats;

namespace Peeza.RPG.Base.Heroes.Specs.Stats
{
    public class MageStatsFactory(IBuffsAccessor buffs) : BattleEntityStatsFactory(buffs)
    {
        public override void Calculate(IStatsAccessor stats)
        {
            var critDamage = stats.GetByKey<double>(BasicStatsDeclarationsExtensions.CritDamageStatKey);

            if (critDamage == null)
            {
                critDamage = new CritDamageStat(buffs: buffs);
                stats.Add(critDamage);
            }
            
            if (critDamage is IModifiableStat<double> modifiable)
            {
                modifiable.Modify(critDamage.BaseValue + 0.1);
            }
        }
    }
}
