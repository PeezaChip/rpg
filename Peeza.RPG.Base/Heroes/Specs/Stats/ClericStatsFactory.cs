﻿using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Stats;

namespace Peeza.RPG.Base.Heroes.Specs.Stats
{
    public class ClericStatsFactory(IBuffsAccessor buffs) : BattleEntityStatsFactory(buffs)
    {
        public override void Calculate(IStatsAccessor stats)
        {
            var aggro = stats.GetByKey<double>(BasicStatsDeclarationsExtensions.AggroStatKey);
            
            if (aggro is IModifiableStat<double> modifiable)
            {
                modifiable.Modify(aggro * 0.9);
            }
        }
    }
}
