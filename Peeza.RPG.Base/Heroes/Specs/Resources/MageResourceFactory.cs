﻿using Peeza.RPG.Base.Resources;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Resources;

namespace Peeza.RPG.Base.Heroes.Specs.Resources
{
    public class MageResourceFactory(uint level, IBuffsAccessor buffs) : BattleEntityResourcesFactory(buffs)
    {
        public override void Calculate(IResourcesAccessor resources)
        {
            if (resources.GetMana() == null)
            {
                resources.Add(BaseResourceScalings.CalculateMana(level, buffs));
            }
        }
    }
}
