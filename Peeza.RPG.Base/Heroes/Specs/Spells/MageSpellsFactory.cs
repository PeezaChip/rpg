﻿using Peeza.RPG.Base.Spells.Implementations;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Interfaces;
using Peeza.RPG.Core.Basic.Spells;

namespace Peeza.RPG.Base.Heroes.Specs.Spells
{
    public class MageSpellsFactory(IBuffsAccessor buffs) : ISpellsFactory
    {
        public void Learn(ISpellsAccessor spells)
        {
            spells.Add(new SelfDispellSpell(buffs));
            spells.Add(new FireballSpell(buffs));
        }
    }
}
