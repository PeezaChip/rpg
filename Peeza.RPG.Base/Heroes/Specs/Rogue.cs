﻿using Peeza.RPG.Base.Battle.Arguments.Damage;
using Peeza.RPG.Base.Heroes.Specs.Ability;
using Peeza.RPG.Base.Heroes.Specs.Stats;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Interfaces;
using Peeza.RPG.Core.Heroes;
using Peeza.RPG.Core.Players;

namespace Peeza.RPG.Base.Heroes.Specs
{
    public class Rogue : BaseSpecDeclaration
    {
        public override string Key => "Rogue";

        public override string AbilityName => "Dexterity";

        public override string Description =>
            "- _Passive Bonus_: Receives a `5%` (Flat) bonus to dodge chance\n" +
            "- _Ability_: Every round without taking any damage gives stacking `2%` (Flat) (up to `10%`) bonus crit chance";

        public override string ShortDescription => "Increased dodge and crit chance";

        public override string Icon => "<:Rogue:1309691233932935288>";

        public override DamageInstanceInfoFlags DefaultAttackFlags => DamageInstanceInfoFlags.Melee | DamageInstanceInfoFlags.Physical;

        public override IBattleEntityEventActionsProvider CreateAbilityFactory(Hero hero, IBuffsAccessor buffs)
        {
            return new RogueAbilityFactory();
        }

        public override IStatsFactory CreateStatsFactory(Hero hero, IBuffsAccessor buffs)
        {
            return new RogueStatsFactory(buffs);
        }

        public override IResourcesFactory CreateResourcesFactory(Hero hero, IBuffsAccessor buffs)
        {
            return null;
        }

        public override ISpellsFactory CreateSpellsFactory(Hero hero, IBuffsAccessor buffs)
        {
            return null;
        }

        public override bool IsAvailable(PlayerAccount playerAccount, Hero hero)
        {
            return true;
        }
    }
}
