﻿using Peeza.RPG.Base.Battle.Arguments.Damage;
using Peeza.RPG.Base.Heroes.Specs.Ability;
using Peeza.RPG.Base.Heroes.Specs.Resources;
using Peeza.RPG.Base.Heroes.Specs.Spells;
using Peeza.RPG.Base.Heroes.Specs.Stats;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Interfaces;
using Peeza.RPG.Core.Heroes;
using Peeza.RPG.Core.Players;

namespace Peeza.RPG.Base.Heroes.Specs
{

    public class Mage : BaseSpecDeclaration
    {
        public override string Key => "Mage";

        public override string AbilityName => "Intelligence";

        public override string Description =>
            "- _Passive Bonus_: Receives a `10%` bonus to crit damage\n" +
            "- _Ability_: On each attack regenerates `10` mana, can use following spells:\n" +
            "  - [Spell:Self Dispell]\n" +
            "  - [Spell:Fireball]";

        public override string ShortDescription => "Increased crit damage bonus, learns self dispell and fireball spells";

        public override string Icon => "<:Mage:1309691230338416682>";

        public override DamageInstanceInfoFlags DefaultAttackFlags => DamageInstanceInfoFlags.Ranged | DamageInstanceInfoFlags.Magic;

        public override IBattleEntityEventActionsProvider CreateAbilityFactory(Hero hero, IBuffsAccessor buffs)
        {
            return new MageAbilityFactory();
        }

        public override IStatsFactory CreateStatsFactory(Hero hero, IBuffsAccessor buffs)
        {
            return new MageStatsFactory(buffs);
        }

        public override IResourcesFactory CreateResourcesFactory(Hero hero, IBuffsAccessor buffs)
        {
            return new MageResourceFactory(hero.Level, buffs);
        }

        public override ISpellsFactory CreateSpellsFactory(Hero hero, IBuffsAccessor buffs)
        {
            return new MageSpellsFactory(buffs);
        }

        public override bool IsAvailable(PlayerAccount playerAccount, Hero hero)
        {
            return true;
        }
    }
}
