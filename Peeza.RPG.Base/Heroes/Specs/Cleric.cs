﻿using Peeza.RPG.Base.Battle.Arguments.Damage;
using Peeza.RPG.Base.Heroes.Specs.Ability;
using Peeza.RPG.Base.Heroes.Specs.Resources;
using Peeza.RPG.Base.Heroes.Specs.Spells;
using Peeza.RPG.Base.Heroes.Specs.Stats;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Interfaces;
using Peeza.RPG.Core.Heroes;
using Peeza.RPG.Core.Players;

namespace Peeza.RPG.Base.Heroes.Specs
{
    public class Cleric : BaseSpecDeclaration
    {
        public override string Key => "Cleric";

        public override string AbilityName => "Mind";

        public override string Description =>
            "- _Passive Bonus_: Decreases aggro by `10%`\n" +
            "- _Ability_: On each attack regenerates `10` mana, can use following spells:\n" +
            "  - [Spell:Dispell]\n" +
            "  - [Spell:Lesser Heal]";

        public override string ShortDescription => "Decreased aggro, learns dispell and lesser heal spells";

        public override string Icon => "<:Cleric:1309691224554340374>";

        public override DamageInstanceInfoFlags DefaultAttackFlags => DamageInstanceInfoFlags.Ranged | DamageInstanceInfoFlags.Magic;

        public override IBattleEntityEventActionsProvider CreateAbilityFactory(Hero hero, IBuffsAccessor buffs)
        {
            return new MageAbilityFactory();
        }

        public override IStatsFactory CreateStatsFactory(Hero hero, IBuffsAccessor buffs)
        {
            return new ClericStatsFactory(buffs);
        }

        public override IResourcesFactory CreateResourcesFactory(Hero hero, IBuffsAccessor buffs)
        {
            return new MageResourceFactory(hero.Level, buffs);
        }

        public override ISpellsFactory CreateSpellsFactory(Hero hero, IBuffsAccessor buffs)
        {
            return new ClericSpellsFactory(buffs);
        }

        public override bool IsAvailable(PlayerAccount playerAccount, Hero hero)
        {
            return true;
        }
    }
}
