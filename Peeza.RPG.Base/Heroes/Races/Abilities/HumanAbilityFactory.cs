﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Battle.Events.Attributes;
using Peeza.RPG.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Peeza.RPG.Base.Heroes.Races.Abilities
{
    public class HumanAbilityFactory : BattleEntityAbilityFactory
    {
        public override void Hook(IBattleEntityEventHandlersProvider hookTo)
        {
            hookTo.HookEvent(BasicBattleSharedEventsDeclarationsExtensions.OnAnyDeclaration, EnsureHasHumanPassive);
            hookTo.HookEvent(BasicBattleSharedEventsDeclarationsExtensions.OnFightDeclaration, HumanOnFightBuff);
        }

        public override void Unhook(IBattleEntityEventHandlersProvider hookTo)
        {
            throw new NotImplementedException("Unhooking that is not supported");
        }

        [BattleEventAction(typeof(OnAny), Priority = 10000)]
        public static bool EnsureHasHumanPassive(BattleEventArgs args)
        {
            if (!args.Self.SupportsBuffs) return false;
            if (!args.Self.SupportsEvents) return false;
            if (!args.Self.CanFight) return false;

            if (args.Self.Buffs.Any(buff => buff is HumanBuff && buff.Inflicter == args.Self)) return false;

            args.Self.EventHandler.OnAffectedWithStatus(BattleEventArgs<OnStatusData>.CloneFrom(args, false, new OnStatusData() { NewStatuses = [new HumanBuff(args.Self)] }));

            return false;
        }

        [BattleEventAction(typeof(OnFight), Priority = 999)]
        public static bool HumanOnFightBuff(BattleEventArgs<OnTimeEnum> args)
        {
            if (args.Data != OnTimeEnum.Start) return false;
            if (!args.Self.SupportsBuffs) return false;
            if (!args.Self.SupportsEvents) return false;

            var buffInfo = humanFightBuffList.TakeRandom(args.Random);
            var buff = (Buff)buffInfo.BuffType.GetConstructors().First().Invoke([args.Self, buffInfo.BuffValue, BuffFlags.Positive, 3]);

            args.Self.EventHandler.OnAffectedWithStatus(BattleEventArgs<OnStatusData>.CloneFrom(args, false, new OnStatusData() { NewStatuses = [ buff ] }));

            return false;
        }

        private class HumanBuff(IBattleEntity inflicter) : Buff<double>("*", inflicter, 1.1, BuffFlags.Positive | BuffFlags.Multiplicative | BuffFlags.InfiniteDuration, 0) { }
        private readonly struct HumanFightBuffData(Type buffType, double buffValue)
        {
            public Type BuffType { get; } = buffType;
            public double BuffValue { get; } = buffValue;
        }

        private static readonly List<HumanFightBuffData> humanFightBuffList = [
            new(typeof(MultiplicativeAttackBuff), 1.2),
            new(typeof(CritChanceBuff), 0.05),
            new(typeof(CritDamageBuff), 0.2),
            new(typeof(DodgeChanceBuff), 0.05),
            new(typeof(BlockChanceBuff), 0.05),
            new(typeof(AccuracyBuff), 0.1),
            new(typeof(MultiplicativeArmorBuff), 1.1),
            new(typeof(MultiplicativeMagicResistanceBuff), 1.1),
        ];
    }
}
