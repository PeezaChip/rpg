﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Base.Battle.Arguments.Heal;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Battle.Events.Attributes;
using System;
using System.Linq;

namespace Peeza.RPG.Base.Heroes.Races.Abilities
{
    public class UndeadAbilityFactory : BattleEntityAbilityFactory
    {
        public override void Hook(IBattleEntityEventHandlersProvider hookTo)
        {
            hookTo.HookEvent(BasicBattleEntityEventsDeclarationsExtensions.OnGetHealDeclaration, UndeadRemoveHeals);
            hookTo.HookEvent(BasicBattleEntityEventsDeclarationsExtensions.OnKillDeclaration, UndeadOnKill);
        }

        public override void Unhook(IBattleEntityEventHandlersProvider hookTo)
        {
            throw new NotImplementedException("Unhooking that is not supported");
        }

        [BattleEventAction(typeof(OnGetHeal), Priority = 101)]
        public static bool UndeadRemoveHeals(BattleEventArgs<HealInfo> args)
        {
            var invalidHealInstances = args.Data.HealInstances.Where(healInstance => !healInstance.Modifiers.HasFlag(HealInstanceInfoModifierFlags.IsLifesteal)).ToList();
            foreach (var healInstance in invalidHealInstances)
            {
                healInstance.Result = HealInstanceInfoResultFlags.Miss;
            }

            if (invalidHealInstances.Count > 0) args.Log($"{args.Self.Name} cannot be healed by normal means");

            return false;
        }

        [BattleEventAction(typeof(OnKill), Priority = 100)]
        public static bool UndeadOnKill(BattleEventArgs<ResourceChangeInfo> args)
        {
            if (!args.Self.SupportsEvents) return false;

            args.Self.EventHandler.OnAffectedWithStatus(BattleEventArgs<OnStatusData>.CloneFrom(args, false, new OnStatusData()
            {
                NewStatuses = [
                    new LifestealBuff(args.Self, 0.1, BuffFlags.Positive, 3)
                ]
            }));

            return false;
        }
    }
}
