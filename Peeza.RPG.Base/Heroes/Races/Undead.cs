﻿using Peeza.RPG.Base.Heroes.Races.Abilities;
using Peeza.RPG.Base.Heroes.Races.Resources;
using Peeza.RPG.Base.Heroes.Races.Stats;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Interfaces;
using Peeza.RPG.Core.Heroes;
using Peeza.RPG.Core.Heroes.Races;
using Peeza.RPG.Core.Players;

namespace Peeza.RPG.Base.Heroes.Races
{
    public class Undead : RaceDeclaration
    {
        public override string Key => "Undead";

        public override string AbilityName => "Unholy Constitution";

        public override string Description =>
            "- _Passive Bonus_: Maximum HP is doubled, gains `20%` lifesteal, but cannot be healed by normal means\n" +
            "- _Ability_: On each kill gains stackable `10%` flat lifesteal bonus for `3` rounds";

        public override string ShortDescription => "Increased HP, gains lifesteal, but cannot be healed by normal means";

        public override string Icon => "<:Undead:1309691234809413675>";

        public override IBattleEntityEventActionsProvider CreateAbilityFactory(Hero hero, IBuffsAccessor buffs)
        {
            return new UndeadAbilityFactory();
        }

        public override IStatsFactory CreateStatsFactory(Hero hero, IBuffsAccessor buffs)
        {
            return new UndeadStatsFactory(buffs);
        }

        public override IResourcesFactory CreateResourcesFactory(Hero hero, IBuffsAccessor buffs)
        {
            return new UndeadResourcesFactory(buffs);
        }

        public override ISpellsFactory CreateSpellsFactory(Hero hero, IBuffsAccessor buffs)
        {
            return null;
        }

        public override bool IsAvailable(PlayerAccount playerAccount, Hero hero)
        {
            return true;
        }
    }
}
