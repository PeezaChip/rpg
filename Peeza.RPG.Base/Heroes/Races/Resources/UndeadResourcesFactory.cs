﻿using Peeza.RPG.Base.Resources;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Resources;

namespace Peeza.RPG.Base.Heroes.Races.Resources
{
    public class UndeadResourcesFactory : BattleEntityResourcesFactory
    {
        public UndeadResourcesFactory(IBuffsAccessor buffs) : base(buffs) { }

        public override void Calculate(IResourcesAccessor stats)
        {
            var health = stats.GetHealth();

            if (health is IModifiableResource modifiable)
            {
                modifiable.ModifyMax(health.MaxValue * 2);
            }
        }
    }
}
