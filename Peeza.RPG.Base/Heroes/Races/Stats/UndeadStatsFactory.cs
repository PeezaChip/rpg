﻿using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Stats;

namespace Peeza.RPG.Base.Heroes.Races.Stats
{
    public class UndeadStatsFactory : BattleEntityStatsFactory
    {
        public UndeadStatsFactory(IBuffsAccessor buffs) : base(buffs) { }

        public override void Calculate(IStatsAccessor stats)
        {
            stats.Add(new LifestealStat(0.2, buffs));
        }
    }
}
