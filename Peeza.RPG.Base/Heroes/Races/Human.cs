﻿using Peeza.RPG.Base.Heroes.Races.Abilities;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Interfaces;
using Peeza.RPG.Core.Heroes;
using Peeza.RPG.Core.Heroes.Races;
using Peeza.RPG.Core.Players;

namespace Peeza.RPG.Base.Heroes.Races
{
    public class Human : RaceDeclaration
    {
        public override string Key => "Human";

        public override string AbilityName => "Human's Adaptability";

        public override string Description =>
            "- _Passive Bonus_: Receives a `10%` bonus to all stats\n" +
            "- _Ability_: On the start of each battle receives a random buff for `3` rounds from the list below:\n" +
            "  - `20%` Attack\n" +
            "  - `5%` Crit Chance (Flat)\n" +
            "  - `20%` Crit Damage (Flat)\n" +
            "  - `5%` Dodge Chance (Flat)\n" +
            "  - `5%` Block Chance (Flat)\n" +
            "  - `10%` Accuracy (Flat)\n" +
            "  - `10%` Armor\n" +
            "  - `10%` Magic Resistance";

        public override string ShortDescription => "Receives an all stats bonus and a random buff at the start of each battle";

        public override string Icon => "<:Human:1309691229050634311>";

        public override IBattleEntityEventActionsProvider CreateAbilityFactory(Hero hero, IBuffsAccessor buffs)
        {
            return new HumanAbilityFactory();
        }

        public override IStatsFactory CreateStatsFactory(Hero hero, IBuffsAccessor buffs)
        {
            return null;
        }

        public override IResourcesFactory CreateResourcesFactory(Hero hero, IBuffsAccessor buffs)
        {
            return null;
        }

        public override ISpellsFactory CreateSpellsFactory(Hero hero, IBuffsAccessor buffs)
        {
            return null;
        }

        public override bool IsAvailable(PlayerAccount playerAccount, Hero hero)
        {
            return true;
        }
    }
}
