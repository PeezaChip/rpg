﻿using Peeza.RPG.Base.Battle.Fights;
using Peeza.RPG.Core.Basic.Auras;
using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.BattleFields;
using Peeza.RPG.Core.Basic.Cooldowns;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Realizations.Auras;
using Peeza.RPG.Core.Realizations.Cooldowns;

namespace Peeza.RPG.Base.Battle.BattleFields
{
    public class BasicBattleField : IBattleField
    {
        protected readonly BaseBattleEventArgsMaker argsMaker;

        public string Name { get; }

        public IBattleSidesAccessor AllBattleSides { get; }

        public bool SupportsAuras => Auras != null;
        private readonly AuraList _aurasList;
        public IAurasAccessor Auras => _aurasList;

        public bool SupportsCooldowns => true;
        private readonly CooldownList _cooldowns;
        public ICooldownAccessor Cooldowns => _cooldowns;

        public bool SupportsEvents => true;
        private readonly DictionaryEventHandlerProvider _eventHandler;
        public IBattleFieldEventHandlersProvider EventHandler => _eventHandler;

        public BasicBattleField(string name, BaseBattleEventArgsMaker argsMaker, IBattleSidesAccessor battleSidesAccessor = null)
        {
            this.argsMaker = argsMaker;

            Name = name;
            AllBattleSides = battleSidesAccessor;

            _aurasList = [];
            _cooldowns = [];
            _eventHandler = new DictionaryEventHandlerProvider();
        }

        public virtual void Hook(IBattleEntityEventHandlersProvider hookTo) { }

        public virtual void Unhook(IBattleEntityEventHandlersProvider hookTo) { }

        public override string ToString()
        {
            return Name;
        }
    }
}
