﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.BattleFields;
using Peeza.RPG.Core.Realizations.Fights;
using System.Threading;
using System.Threading.Tasks;

namespace Peeza.RPG.Base.Battle.Fights
{
    public class BaseTurnHandler : BasicTurnHandler
    {
        protected BaseBattleEventArgsMaker argsMaker;

        public BaseTurnHandler(BaseBattleEventArgsMaker argsMaker, IBattleFieldAccessor battleFieldAccessor)
            : base(battleFieldAccessor)
        {
            this.argsMaker = argsMaker;
        }

        public override async Task ProccessTurn(CancellationToken cancellationToken, IBattleQueable queueable)
        {
            InvokeOnTurn(OnTimeEnum.Start, queueable);
            await base.ProccessTurn(cancellationToken, queueable);
            InvokeOnTurn(OnTimeEnum.End, queueable);
        }

        private void InvokeOnTurn(OnTimeEnum time, IBattleQueable queable)
        {
            if (fieldAccessor.GetField()?.SupportsEvents == true)
            {
                fieldAccessor.GetField().EventHandler.OnTurn(argsMaker.FromData(time));
            }

            if (queable is IBattleEntity entity && entity.SupportsEvents && entity.CanFight)
            {
                entity.EventHandler.OnTurn(argsMaker.FromData(time, entity));
            }
        }
    }
}
