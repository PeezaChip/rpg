﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.BattleFields;
using Peeza.RPG.Core.Basic.Interfaces;
using Peeza.RPG.Core.Realizations.Fights;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Peeza.RPG.Base.Battle.Fights
{
    public class BaseRoundHandler : BasicRoundHandler
    {
        protected BaseBattleEventArgsMaker argsMaker;

        public BaseRoundHandler(BaseBattleEventArgsMaker argsMaker, ITurnHandler turnHandler, Random random, IBattleFieldAccessor battleFieldAccessor)
            : base(turnHandler, random, battleFieldAccessor)
        {
            this.argsMaker = argsMaker;
        }

        public override async Task ProccessRound(CancellationToken cancellationToken, IEnumerable<IBattleQueable> queables)
        {
            InvokeOnRound(OnTimeEnum.Start, queables);
            await base.ProccessRound(cancellationToken, queables);
            InvokeOnRound(OnTimeEnum.End, queables);
        }

        private void InvokeOnRound(OnTimeEnum time, IEnumerable<IBattleQueable> queables)
        {
            if (fieldAccessor.GetField()?.SupportsEvents == true)
            {
                fieldAccessor.GetField().EventHandler.OnRound(argsMaker.FromData(time));
            }

            var entities = queables.Where(queable => queable is IBattleEntity entity && entity.SupportsEvents && entity.CanFight).Cast<IBattleEntity>();
            foreach(var entity in entities)
            {
                entity.EventHandler.OnRound(argsMaker.FromData(time, entity));
            }
        }
    }
}
