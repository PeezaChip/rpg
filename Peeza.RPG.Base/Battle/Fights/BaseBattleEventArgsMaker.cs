﻿using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.BattleFields;
using Peeza.RPG.Core.Basic.Interfaces;
using Peeza.RPG.Core.Realizations.Fights;
using System;

namespace Peeza.RPG.Base.Battle.Fights
{
    public class BaseBattleEventArgsMaker : BasicBattleEventArgsMaker
    {
        public BaseBattleEventArgsMaker(IQueueAccessor queueAccessor, IBattleFieldAccessor fieldAccessor, IBattleLogger logger, Random random)
            : base(queueAccessor, logger, random, fieldAccessor) { }
    }
}
