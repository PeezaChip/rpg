﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.BattleFields;
using Peeza.RPG.Core.Basic.Interfaces;
using Peeza.RPG.Core.Realizations.Fights;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Peeza.RPG.Base.Battle.Fights
{
    public class BaseFightResolver : BasicFightResolver
    {
        protected BaseBattleEventArgsMaker argsMaker;

        public BaseFightResolver(BaseBattleEventArgsMaker argsMaker, IBattleSidesAccessor battleSides, IRoundHandler roundHandler, IBattleFieldAccessor battleFieldAccessor, int roundsHardLimit = 1000)
            : base(battleSides, roundHandler, battleFieldAccessor, roundsHardLimit)
        {
            this.argsMaker = argsMaker;
        }

        public override async Task ProccessFight(CancellationToken cancellationToken)
        {
            InvokeOnFight(OnTimeEnum.Start);
            await base.ProccessFight(cancellationToken);
            InvokeOnFight(OnTimeEnum.End);
        }

        private void InvokeOnFight(OnTimeEnum time)
        {
            if (fieldAccessor.GetField()?.SupportsEvents == true)
            {
                fieldAccessor.GetField().EventHandler.OnFight(argsMaker.FromData(time));
            }

            var entities = battleSides.GetAll().SelectMany(side => side.BattleEntities.Where(entity => entity.SupportsEvents && entity.CanFight)).Distinct().ToList();
            foreach (var entity in entities)
            {
                entity.EventHandler.OnFight(argsMaker.FromData(time, entity));
            }
        }
    }
}
