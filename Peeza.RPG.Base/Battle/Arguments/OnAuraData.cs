﻿using Peeza.RPG.Core.Basic.Auras;
using System.Collections.Generic;

namespace Peeza.RPG.Base.Battle.Arguments
{
    public class OnAuraData
    {
        public List<Aura> NewAuras { get; set; } = [];
        public List<Aura> LiftedAuras { get; set; } = [];
    }
}
