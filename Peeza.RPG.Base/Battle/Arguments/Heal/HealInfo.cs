﻿using System.Collections.Generic;
using System.Linq;

namespace Peeza.RPG.Base.Battle.Arguments.Heal
{
    public class HealInfo
    {
        public List<HealInstanceInfo> HealInstances { get; set; } = [];

        public long TotalHeal => HealInstances.Sum(i => i.Heal);

        public long TotalHealDealt => HealInstances.Sum(i => i.HealDealt);
    }
}
