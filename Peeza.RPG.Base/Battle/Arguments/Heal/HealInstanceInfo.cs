﻿namespace Peeza.RPG.Base.Battle.Arguments.Heal
{
    public class HealInstanceInfo
    {
        public HealInstanceInfoResultFlags Result { get; set; }

        private long? initialHeal;
        public long InitialHeal
        {
            get => initialHeal.GetValueOrDefault(0);
        }

        private long? heal;
        public long Heal
        {
            get => heal.GetValueOrDefault(0);
            set
            {
                heal = value;
                if (!initialHeal.HasValue)
                {
                    initialHeal = value;
                }
            }
        }

        public long HealDealt { get; set; }

        public long HealLeft
        {
            get
            {
                if (Result != HealInstanceInfoResultFlags.None) return 0;

                return Heal - HealDealt;
            }
        }

        public HealInstanceInfoFlags Flags { get; set; }
        public HealInstanceInfoModifierFlags Modifiers { get; set; }

        public object Source { get; set; }
    }
}
