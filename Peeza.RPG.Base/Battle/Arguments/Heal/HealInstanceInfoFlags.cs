﻿using System;

namespace Peeza.RPG.Base.Battle.Arguments.Heal
{
    [Flags]
    public enum HealInstanceInfoFlags
    {
        None = 0,

        Health = 1,
        Shield = 2,
    }
}
