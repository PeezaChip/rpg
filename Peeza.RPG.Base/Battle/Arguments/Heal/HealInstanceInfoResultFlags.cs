﻿using System;

namespace Peeza.RPG.Base.Battle.Arguments.Heal
{
    [Flags]
    public enum HealInstanceInfoResultFlags
    {
        None = 0,

        Miss = 1,
        //Reserved = 2,
        Block = 4,

        Done = 8,
    }
}
