﻿using System;

namespace Peeza.RPG.Base.Battle.Arguments.Heal
{
    [Flags]
    public enum HealInstanceInfoModifierFlags
    {
        None = 0,

        //Reserved = 1,
        IsLifesteal = 2,
        CantBlock = 4,

        Crit = 8,
    }
}
