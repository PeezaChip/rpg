﻿using Peeza.RPG.Core.Basic.Generic;

namespace Peeza.RPG.Base.Battle.Arguments
{
    public class ResourceChangeInfo : IRPGKey
    {
        public string Key { get; private set; }

        public long OldValue { get; private set; }
        public long NewValue { get; private set; }

        public object Reason { get; private set; }

        public ResourceChangeInfo(string key, long oldValue, long newValue, object reason = null)
        {
            Key = key;
            OldValue = oldValue;
            NewValue = newValue;
            Reason = reason;
        }
    }
}
