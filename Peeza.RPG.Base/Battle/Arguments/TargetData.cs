﻿using Peeza.RPG.Core.Basic.BattleEntities;
using System.Collections.Generic;

namespace Peeza.RPG.Base.Battle.Arguments
{
    public class TargetData
    {
        public List<IBattleEntity> PossibleTargets { get; set; } = new List<IBattleEntity>();
        public IBattleEntity Target { get; set; }
    }
}
