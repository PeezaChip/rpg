﻿using Peeza.RPG.Core.Basic.Minions;
using System.Collections.Generic;

namespace Peeza.RPG.Base.Battle.Arguments
{
    public class OnMinionData
    {
        public List<Minion> NewMinions { get; set; } = new List<Minion>();
        public List<Minion> DeadMinions { get; set; } = new List<Minion>();
    }
}
