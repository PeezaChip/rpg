﻿using Peeza.RPG.Core.Basic.BattleEntities;

namespace Peeza.RPG.Base.Battle.Arguments
{
    public class RessurectionData
    {
        public long HealthToRestore { get; set; }
        public IBattleEntity Source { get; set; }
    }
}
