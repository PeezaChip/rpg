﻿using Peeza.RPG.Core.Basic.BattleFields;

namespace Peeza.RPG.Base.Battle.Arguments
{
    public class EnvironmentChangeData
    {
        public IBattleField OldBattleField { get; }
        public IBattleField NewBattleField { get; }

        public EnvironmentChangeData(IBattleField oldBattleField, IBattleField newBattleField)
        {
            OldBattleField = oldBattleField;
            NewBattleField = newBattleField;
        }
    }
}
