﻿using Peeza.RPG.Core.Basic.Statuses;
using System.Collections.Generic;

namespace Peeza.RPG.Base.Battle.Arguments
{
    public class OnStatusData
    {
        public List<Status> NewStatuses { get; set; } = [];
        public List<Status> LiftedStatuses { get; set; } = [];
    }
}
