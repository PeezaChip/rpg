﻿using System.Collections.Generic;
using System.Linq;

namespace Peeza.RPG.Base.Battle.Arguments.Damage
{
    public class DamageInfo
    {
        public List<DamageInstanceInfo> DamageInstances { get; set; } = [];

        public long TotalAttack => DamageInstances.Sum(i => i.Damage);

        public long TotalDamageDealt => DamageInstances.Sum(i => i.DamageDealt);

        public long TotalMitigated => DamageInstances.Sum(i => i.DamageMitigated);
    }
}
