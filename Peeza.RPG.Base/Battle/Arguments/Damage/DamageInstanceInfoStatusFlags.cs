﻿using System;

namespace Peeza.RPG.Base.Battle.Arguments.Damage
{
    [Flags]
    public enum DamageInstanceInfoStatusFlags
    {
        None = 0,

        DefenseCalculated = 1,
        DamageDone = 2,
    }
}
