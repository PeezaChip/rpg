﻿namespace Peeza.RPG.Base.Battle.Arguments.Damage
{
    public class DamageInstanceInfo
    {
        public DamageInstanceInfoResultFlags Result { get; set; }
        public DamageInstanceInfoStatusFlags Status { get; set; }

        private long? initialDamage;
        public long InitialDamage
        {
            get => initialDamage.GetValueOrDefault(0);
        }

        private long? damage;
        public long Damage
        {
            get => damage.GetValueOrDefault(0);
            set
            {
                damage = value;
                if (!initialDamage.HasValue)
                {
                    initialDamage = value;
                }
            }
        }

        public long DamageDealt { get; set; }

        public long DamageLeft
        {
            get
            {
                if (Result != DamageInstanceInfoResultFlags.None) return 0;
                if (Status.HasFlag(DamageInstanceInfoStatusFlags.DamageDone)) return 0;

                return Damage - DamageDealt;
            }
        }

        public long DamageMitigated
        {
            get => InitialDamage - Damage;
        }

        public DamageInstanceInfoFlags Flags { get; set; }
        public DamageInstanceInfoModifierFlags Modifiers { get; set; }

        public object Source { get; set; }
    }
}
