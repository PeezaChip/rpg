﻿using System;

namespace Peeza.RPG.Base.Battle.Arguments.Damage
{
    [Flags]
    public enum DamageInstanceInfoModifierFlags
    {
        None = 0,

        CantReflect = 1,
        CantDodge = 2,
        CantBlock = 4,

        Crit = 8,
    }
}
