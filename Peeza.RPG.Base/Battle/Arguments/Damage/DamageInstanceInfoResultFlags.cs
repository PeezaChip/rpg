﻿using System;

namespace Peeza.RPG.Base.Battle.Arguments.Damage
{
    [Flags]
    public enum DamageInstanceInfoResultFlags
    {
        None = 0,

        Miss = 1,
        Dodge = 2,
        Block = 4,

        Done = 8,
    }
}
