﻿using System;

namespace Peeza.RPG.Base.Battle.Arguments.Damage
{
    [Flags]
    public enum DamageInstanceInfoFlags
    {
        None = 0,

        Melee = 1,
        Ranged = 2,

        Physical = 4,
        Magic = 8,
        Mixed = 12,
        Pure = 16,
    }
}
