﻿using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Realizations.Battle;
using System.Collections.Generic;

namespace Peeza.RPG.Base.Battle
{
    public class NamedBattleSide : BattleSide
    {
        public string Name { get; }

        public NamedBattleSide(string name) : base()
        {
            Name = name;
        }

        public NamedBattleSide(string name, IEnumerable<IBattleEntity> battleEntities) : base(battleEntities)
        {
            Name = name;
        }
    }
}
