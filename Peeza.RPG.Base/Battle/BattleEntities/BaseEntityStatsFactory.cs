﻿using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Stats;

namespace Peeza.RPG.Base.Battle.BattleEntities
{
    public class BaseEntityStatsFactory : BattleEntityStatsFactory
    {
        protected uint Level { get; }

        public BaseEntityStatsFactory(uint level, IBuffsAccessor buffs) : base(buffs)
        {
            Level = level;
        }

        public override void Calculate(IStatsAccessor stats)
        {
            stats.Add(
                new LevelStat((int)Level),

                new AggroStat(buffs: buffs),

                BaseStatScalings.CalculateAttack(Level, buffs),
                BaseStatScalings.CalculateArmor(Level, buffs),
                BaseStatScalings.CalculateMagicResistance(Level, buffs),

                new AccuracyStat(0.65, buffs),

                new CritChanceStat(buffs: buffs),
                new CritDamageStat(buffs: buffs),
                new DodgeChanceStat(buffs: buffs),
                new BlockChanceStat(buffs: buffs)
            );
        }
    }
}
