﻿using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.BattleFields;
using Peeza.RPG.Core.Basic.Interfaces;

namespace Peeza.RPG.Base.Battle.BattleEntities
{
    public class BaseBattleEntity : BasicBattleEntity
    {
        public BaseBattleEntity(string name, uint level, IBattleSide battleSide, IBattleEventArgsMaker eventArgsMaker, IBattleSideRelationshipManager relationshipManager, IBattleFieldAccessor battleFieldAccessor) : base(name, battleSide, eventArgsMaker, relationshipManager, battleFieldAccessor)
        {
            new BaseEntityAbilityFactory().Hook(EventHandler);
            new BaseEntityResourceFactory(level, Buffs).Calculate(Resources);
            new BaseEntityStatsFactory(level, Buffs).Calculate(Stats);
        }
    }
}
