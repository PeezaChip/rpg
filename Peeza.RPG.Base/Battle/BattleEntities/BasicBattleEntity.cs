﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Base.Battle.Arguments.Damage;
using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Base.Resources;
using Peeza.RPG.Core.Basic.Ailments;
using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.BattleFields;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Cooldowns;
using Peeza.RPG.Core.Basic.Interfaces;
using Peeza.RPG.Core.Basic.Resources;
using Peeza.RPG.Core.Basic.Spells;
using Peeza.RPG.Core.Basic.Stats;
using Peeza.RPG.Core.Basic.Statuses;
using Peeza.RPG.Core.Realizations.Ailments;
using Peeza.RPG.Core.Realizations.BattleEntities;
using Peeza.RPG.Core.Realizations.Buffs;
using Peeza.RPG.Core.Realizations.Cooldowns;
using Peeza.RPG.Core.Realizations.Resources;
using Peeza.RPG.Core.Realizations.Spells;
using Peeza.RPG.Core.Realizations.Stats;
using Peeza.RPG.Core.Realizations.Statuses;
using System.Linq;
using System.Text;

namespace Peeza.RPG.Base.Battle.BattleEntities
{
    public class BasicBattleEntity : IQueableBattleEntity
    {
        private readonly IBattleEventArgsMaker eventArgsMaker;
        private readonly IBattleSideRelationshipManager relationshipManager;
        private readonly IBattleFieldAccessor battleFieldAccessor;

        public string Name { get; }

        public virtual bool CanFight => !Resources.GetHealth().IsEmpty;

        public virtual DamageInstanceInfoFlags DefaultAttackFlags { get; set; }

        public IBattleSide BattleSide { get; }

        public bool SupportsEvents => true;
        private readonly BaseEventHandlerProvider _eventHandler;
        public IBattleEntityEventHandlersProvider EventHandler => _eventHandler;

        public bool HaveMinions => true;
        private readonly MinionList _minions;
        public IMinionsAccessor Minions => _minions;

        public bool SupportsCooldowns => true;
        private readonly CooldownList _cooldowns;
        public ICooldownAccessor Cooldowns => _cooldowns;

        public bool SupportsAilments => true;
        private readonly AilmentList _ailments;
        public IAilmentsAccessor Ailments => _ailments;

        public bool SupportsBuffs => true;
        private readonly BuffList _buffs;
        public IBuffsAccessor Buffs => _buffs;

        public bool SupportsStatuses => true;
        private readonly StatusesList _statuses;
        public IStatusAccessor Statuses => _statuses;

        public bool SupportStats => true;
        private readonly StatList _stats;
        public IStatsAccessor Stats => _stats;

        public bool SupportResources => true;
        private readonly ResourceList _resources;
        public IResourcesAccessor Resources => _resources;

        public bool SupportSpells => true;
        private readonly SpellList _spells;
        public ISpellsAccessor Spells => _spells;

        public virtual void TurnAction()
        {
            if (!CanFight) return;
            var hostileEntities = relationshipManager.GetHostiles(BattleSide).SelectMany(side => side.BattleEntities).Distinct().ToList();

            var targetArgs = eventArgsMaker.FromData(new TargetData() { PossibleTargets = hostileEntities }, this);
            EventHandler.OnPickTarget(targetArgs);

            if (targetArgs.Data.Target != null)
            {
                var damageArgs = eventArgsMaker.FromData<DamageInfo>(null, this, targetArgs.Data.Target);
                EventHandler.OnCalculateDamage(damageArgs);
                if (damageArgs.Data == null) return;
                EventHandler.OnDoAttack(damageArgs);
            }
        }

        public BasicBattleEntity(string name, IBattleSide battleSide, IBattleEventArgsMaker eventArgsMaker, IBattleSideRelationshipManager relationshipManager, IBattleFieldAccessor battleFieldAccessor)
        {
            this.eventArgsMaker = eventArgsMaker;
            this.relationshipManager = relationshipManager;
            this.battleFieldAccessor = battleFieldAccessor;

            Name = name;
            BattleSide = battleSide;

            _eventHandler = new();
            _minions = [];
            _cooldowns = [];
            _ailments = [];
            _buffs = battleFieldAccessor == null ? new BuffList() : new FieldAwareBuffList(battleSide, battleFieldAccessor);
            _statuses = [];
            _stats = [];
            _resources = [];
            _spells = [];
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append(Name);

            if (Resources.GetHealth() != null)
            {
                var hp = Resources.GetHealth();
                sb.Append($" ({hp.Value}/{hp.MaxValue} HP)");
            }

            if (Resources.GetShield() != null)
            {
                var sh = Resources.GetShield();
                sb.Append($" ({sh.Value}/{sh.MaxValue} SH)");
            }

            return sb.ToString();
        }
    }
}
