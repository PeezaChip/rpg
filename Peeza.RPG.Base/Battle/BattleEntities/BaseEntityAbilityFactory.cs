﻿using Peeza.RPG.Base.Battle.Events;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Battle.Events.Helpers;

namespace Peeza.RPG.Base.Battle.BattleEntities
{
    public class BaseEntityAbilityFactory : BattleEntityAbilityFactory
    {
        public override void Hook(IBattleEntityEventHandlersProvider hookTo)
        {
            BattleEventLoader.LoadEventActionsFromType(typeof(BasicBattleEntityEventsImplementations), hookTo);
        }

        public override void Unhook(IBattleEntityEventHandlersProvider hookTo)
        {
            BattleEventLoader.LoadEventActionsFromType(typeof(BasicBattleEntityEventsImplementations), hookTo);
        }
    }
}
