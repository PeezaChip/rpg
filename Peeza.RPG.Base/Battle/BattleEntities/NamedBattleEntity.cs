﻿using Peeza.RPG.Core.Basic.Battle;

namespace Peeza.RPG.Base.Battle.BattleEntities
{
    public class NamedBattleEntity : AbstractBattleEntity
    {
        public NamedBattleEntity(string name, IBattleSide battleSide = null) : base(name, battleSide) { }
    }
}
