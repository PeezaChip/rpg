﻿using Peeza.RPG.Core.Basic.Ailments;
using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Cooldowns;
using Peeza.RPG.Core.Basic.Resources;
using Peeza.RPG.Core.Basic.Spells;
using Peeza.RPG.Core.Basic.Stats;
using Peeza.RPG.Core.Basic.Statuses;

namespace Peeza.RPG.Base.Battle.BattleEntities
{
    public abstract class AbstractBattleEntity : IBattleEntity
    {
        public string Name { get; }

        public virtual bool CanFight => false;

        public IBattleSide BattleSide { get; }

        public bool SupportsEvents => EventHandler != null;
        public IBattleEntityEventHandlersProvider EventHandler { get; protected set; }

        public bool HaveMinions => Minions != null;
        public IMinionsAccessor Minions { get; protected set; }

        public bool SupportsCooldowns => Cooldowns != null;
        public ICooldownAccessor Cooldowns { get; protected set; }

        public bool SupportsAilments => Ailments != null;
        public IAilmentsAccessor Ailments { get; protected set; }

        public bool SupportsBuffs => Buffs != null;
        public IBuffsAccessor Buffs { get; protected set; }

        public bool SupportsStatuses => Statuses != null;
        public IStatusAccessor Statuses { get; protected set; }

        public bool SupportStats => Stats != null;
        public IStatsAccessor Stats { get; protected set; }

        public bool SupportResources => Resources != null;
        public IResourcesAccessor Resources { get; protected set; }

        public bool SupportSpells => Spells != null;
        public ISpellsAccessor Spells { get; protected set; }

        public AbstractBattleEntity(string name, IBattleSide battleSide = null)
        {
            Name = name;
            BattleSide = battleSide;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
