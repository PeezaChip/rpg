﻿using Peeza.RPG.Base.Heroes.Specs;
using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.BattleFields;
using Peeza.RPG.Core.Basic.Interfaces;
using Peeza.RPG.Core.Heroes;
using Peeza.RPG.Core.Heroes.Races;
using Peeza.RPG.Core.Heroes.Specs;

namespace Peeza.RPG.Base.Battle.BattleEntities
{
    public class HeroBattleEntity : BaseBattleEntity
    {
        public HeroBattleEntity(Hero hero, RaceDeclaration raceDeclaration, SpecDeclaration specDeclaration, IBattleSide battleSide, IBattleEventArgsMaker eventArgsMaker, IBattleSideRelationshipManager relationshipManager, IBattleFieldAccessor battleFieldAccessor) : base(hero.Name, hero.Level, battleSide, eventArgsMaker, relationshipManager, battleFieldAccessor)
        {
            raceDeclaration?.CreateAbilityFactory(hero, Buffs)?.Hook(EventHandler);
            raceDeclaration?.CreateStatsFactory(hero, Buffs)?.Calculate(Stats);
            raceDeclaration?.CreateResourcesFactory(hero, Buffs)?.Calculate(Resources);
            raceDeclaration?.CreateSpellsFactory(hero, Buffs)?.Learn(Spells);

            specDeclaration?.CreateAbilityFactory(hero, Buffs)?.Hook(EventHandler);
            specDeclaration?.CreateStatsFactory(hero, Buffs)?.Calculate(Stats);
            specDeclaration?.CreateResourcesFactory(hero, Buffs)?.Calculate(Resources);
            specDeclaration?.CreateSpellsFactory(hero, Buffs)?.Learn(Spells);

            if (specDeclaration != null && specDeclaration is BaseSpecDeclaration baseSpec)
            {
                DefaultAttackFlags = baseSpec.DefaultAttackFlags;
            }

            foreach(var spell in Spells)
            {
                spell.Hook(EventHandler);
            }
        }
    }
}
