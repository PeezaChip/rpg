﻿using Peeza.RPG.Base.Resources;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Resources;

namespace Peeza.RPG.Base.Battle.BattleEntities
{
    public class BaseEntityResourceFactory : BattleEntityResourcesFactory
    {
        protected uint Level { get; }

        public BaseEntityResourceFactory(uint level, IBuffsAccessor buffs) : base(buffs)
        {
            Level = level;
        }

        public override void Calculate(IResourcesAccessor resources)
        {
            resources.Add(
                BaseResourceScalings.CalculateHealth(Level, buffs),
                new ShieldResource(BaseResourceScalings.CalculateHealth(Level).MaxValue * 2, buffs, setCurrentToMax: false)
            );
        }
    }
}
