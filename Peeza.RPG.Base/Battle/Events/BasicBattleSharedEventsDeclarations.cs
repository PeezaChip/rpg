﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Core.Battle.Events;

namespace Peeza.RPG.Base.Battle.Events
{
    #region GlobalEvents

    /// <summary>
    /// <para>Event gets called when another event is called</para>
    /// <para>Unless called directly will always receieve a copy of parameters (data is still real)</para>
    /// </summary>
    public class OnAny : BattleEventDeclaration { }

    /// <summary>
    /// <para>Event gets called when fight starts or ends</para>
    /// <para>Takes <see cref="OnTimeEnum"/> as parameter</para>
    /// </summary>
    public class OnFight : BattleEventDeclaration<OnTimeEnum> { }

    /// <summary>
    /// <para>Event gets called when round starts or ends</para>
    /// <para>Takes <see cref="OnTimeEnum"/> as parameter</para>
    /// </summary>
    public class OnRound : BattleEventDeclaration<OnTimeEnum> { }

    /// <summary>
    /// <para>Event gets called when turn starts or ends</para>
    /// <para>Takes <see cref="OnTimeEnum"/> as parameter</para>
    /// </summary>
    public class OnTurn : BattleEventDeclaration<OnTimeEnum> { }

    #endregion

    #region AffectedWith

    /// <summary>
    /// <para>Event gets called when a new aura is applied or removed</para>
    /// <para>Takes <see cref="OnAuraData"/> as parameter</para>
    /// </summary>
    public class OnAuraApplied : BattleEventDeclaration<OnAuraData> { }

    #endregion
}
