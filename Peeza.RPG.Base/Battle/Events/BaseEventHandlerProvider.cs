﻿using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Extensions;

namespace Peeza.RPG.Base.Battle.Events
{
    public class BaseEventHandlerProvider : DictionaryEventHandlerProvider
    {
        public override void RaiseEvent(string key, BattleEventArgs args)
        {
            RaiseOnAny(args.Clone(false, true));
            base.RaiseEvent(key, args);
        }

        public override void RaiseEvent<T>(string key, BattleEventArgs<T> args)
        {
            RaiseOnAny(BattleEventArgs.CloneFrom(args, false, true));
            base.RaiseEvent(key, args);
        }

        private bool callingOnAny = false;
        private void RaiseOnAny(BattleEventArgs<object> args)
        {
            if (callingOnAny) return;
            callingOnAny = true;
            base.RaiseEvent(BasicBattleSharedEventsDeclarationsExtensions.OnAnyDeclaration.Key, args);
            callingOnAny = false;
        }
    }
}
