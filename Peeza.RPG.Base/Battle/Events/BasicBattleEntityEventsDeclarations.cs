﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Base.Battle.Arguments.Damage;
using Peeza.RPG.Base.Battle.Arguments.Heal;
using Peeza.RPG.Core.Basic.BattleEntities;

namespace Peeza.RPG.Base.Battle.Events
{
    #region GlobalEvents

    /// <summary>
    /// <para>Event gets called when dungeon starts or ends</para>
    /// <para>Takes <see cref="OnTimeEnum"/> as parameter</para>
    /// </summary>
    public class OnDungeon : BattleEntityEventDeclaration<OnTimeEnum> { }

    /// <summary>
    /// <para>Event gets called when environment changes</para>
    /// <para>Takes <see cref="EnvironmentChangeData"> as parameter</para>
    /// </summary>
    public class OnEnvironmentChange : BattleEntityEventDeclaration<EnvironmentChangeData> { }

    #endregion

    #region AffectedBy

    /// <summary>
    /// <para>Event gets called when a new ailment or buff or status is applied or removed</para>
    /// <para>Takes <see cref="OnStatusData"/> as parameter</para>
    /// </summary>
    public class OnAffectedWithStatus : BattleEntityEventDeclaration<OnStatusData> { }

    /// <summary>
    /// <para>Event gets called when a minion is summoned or dead</para>
    /// <para>Takes <see cref="OnMinionData"/> as parameter</para>
    /// </summary>
    public class OnMinionSummoned : BattleEntityEventDeclaration<OnMinionData> { }

    #endregion

    #region BattleOffense

    /// <summary>
    /// <para>Events gets called when entity needs to select a target</para>
    /// <para>Calls <see cref="OnSelectedAsTarget"/> of the selected target if any</para>
    /// <para>Takes <see cref="TargetData"/> as parameter</para>
    /// </summary>
    public class OnPickTarget : BattleEntityEventDeclaration<TargetData> { }

    /// <summary>
    /// <para>Events gets called when entity needs to calculate a damage</para>
    /// <para>Takes <see cref="DamageInfo"/> as parameter</para>
    /// </summary>
    public class OnCalculateDamage : BattleEntityEventDeclaration<DamageInfo> { }

    /// <summary>
    /// <para>Events gets called when entity needs to attack</para>
    /// <para>Calls <see cref="OnGetHit"/> of the selected target if any</para>
    /// <para>Takes <see cref="DamageInfo"/> as parameter</para>
    /// </summary>
    public class OnDoAttack : BattleEntityEventDeclaration<DamageInfo> { }

    /// <summary>
    /// <para>Events gets called after attack is resolved</para>
    /// <para>Takes <see cref="DamageInfo"/> as parameter</para>
    /// </summary>
    public class OnAttackResolve : BattleEntityEventDeclaration<DamageInfo> { }

    /// <summary>
    /// <para>Events gets called after heal is resolved</para>
    /// <para>Takes <see cref="HealInfo"/> as parameter</para>
    /// </summary>
    public class OnHealResolve : BattleEntityEventDeclaration<HealInfo> { }

    #endregion

    #region BattleDefense

    /// <summary>
    /// <para>Events gets called when entity selected as a target</para>
    /// <para>Takes <see cref="TargetData"/> as parameter</para>
    /// </summary>
    public class OnSelectedAsTarget : BattleEntityEventDeclaration<TargetData> { }

    /// <summary>
    /// <para>Events gets called when entity is hit</para>
    /// <para>Calls self <see cref="OnProcessDamageInstance"/></para>
    /// <para>Calls self <see cref="OnGetHitResult"/></para>
    /// <para>Takes <see cref="DamageInfo"/> as parameter</para>
    /// </summary>
    public class OnGetHit : BattleEntityEventDeclaration<DamageInfo> { }

    /// <summary>
    /// <para>Events gets called when entity should process a damageinstance</para>
    /// <para>Calls self <see cref="OnCalculateDefenses"/></para>
    /// <para>Calls self <see cref="OnDoDamage"/></para>
    /// <para>Takes <see cref="DamageInstanceInfo"/> as parameter</para>
    /// </summary>
    public class OnProcessDamageInstance : BattleEntityEventDeclaration<DamageInstanceInfo> { }

    /// <summary>
    /// <para>Events gets called when entity needs to calculate defenses</para>
    /// <para>Takes <see cref="DamageInfo"/> as parameter</para>
    /// </summary>
    public class OnCalculateDefenses : BattleEntityEventDeclaration<DamageInstanceInfo> { }

    /// <summary>
    /// <para>Event gets called when entity should apply damage</para>
    /// <para>Calls self <see cref="OnResourceChange"/> if needed</para>
    /// <para>Takes <see cref="DamageInstanceInfo"/> as parameter</para>
    /// </summary>
    public class OnDoDamage : BattleEntityEventDeclaration<DamageInstanceInfo> { }

    /// <summary>
    /// <para>Events gets called after attack is resolved</para>
    /// <para>Calls target <see cref="OnAttackResolve"/></para>
    /// <para>Takes <see cref="DamageInfo"/> as parameter</para>
    /// </summary>
    public class OnGetHitResult : BattleEntityEventDeclaration<DamageInfo> { }

    /// <summary>
    /// <para>Events gets called when entity is healed</para>
    /// <para>Calls self <see cref="OnProcessHealInstance"/></para>
    /// <para>Calls self <see cref="OnGetHealResult"/></para>
    /// <para>Takes <see cref="HealInfo"/> as parameter</para>
    /// </summary>
    public class OnGetHeal : BattleEntityEventDeclaration<HealInfo> { }

    /// <summary>
    /// <para>Events gets called when entity should process a healinstance</para>
    /// <para>Calls self <see cref="OnDoHeal"/></para>
    /// <para>Takes <see cref="HealInstanceInfo"/> as parameter</para>
    /// </summary>
    public class OnProcessHealInstance : BattleEntityEventDeclaration<HealInstanceInfo> { }

    /// <summary>
    /// <para>Event gets called when entity should apply heal</para>
    /// <para>Calls self <see cref="OnResourceChange"/> if needed</para>
    /// <para>Takes <see cref="HealInstanceInfo"/> as parameter</para>
    /// </summary>
    public class OnDoHeal : BattleEntityEventDeclaration<HealInstanceInfo> { }

    /// <summary>
    /// <para>Events gets called after heal is resolved</para>
    /// <para>Calls target <see cref="OnHealResolve"/></para>
    /// <para>Takes <see cref="HealInfo"/> as parameter</para>
    /// </summary>
    public class OnGetHealResult : BattleEntityEventDeclaration<HealInfo> { }

    #endregion

    #region BattleOther

    /// <summary>
    /// <para>Events gets called when entity dies</para>
    /// <para>Calls target <see cref="OnKill"/></para>
    /// <para>Takes <see cref="ResourceChangeInfo"/> as parameter</para>
    /// </summary>
    public class OnDeath : BattleEntityEventDeclaration<ResourceChangeInfo> { }

    /// <summary>
    /// <para>Events gets called when entity ressurects</para>
    /// <para>Takes <see cref="RessurectionData"/> as parameter</para>
    /// </summary>
    public class OnRessurection : BattleEntityEventDeclaration<RessurectionData> { }

    /// <summary>
    /// <para>Events gets called when entity kills</para>
    /// <para>Takes <see cref="ResourceChangeInfo"/> as parameter</para>
    /// </summary>
    public class OnKill : BattleEntityEventDeclaration<ResourceChangeInfo> { }

    /// <summary>
    /// <para>Events gets called when entity's resource is changed</para>
    /// <para>Takes <see cref="ResourceChangeInfo"/> as parameter</para>
    /// </summary>
    public class OnResourceChange : BattleEntityEventDeclaration<ResourceChangeInfo> { }

    #endregion
}
