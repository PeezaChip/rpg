﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Base.Battle.Arguments.Damage;
using Peeza.RPG.Base.Battle.Arguments.Heal;
using Peeza.RPG.Base.Battle.BattleEntities;
using Peeza.RPG.Base.Resources;
using Peeza.RPG.Base.Stats;
using Peeza.RPG.Core.Basic.Ailments;
using Peeza.RPG.Core.Basic.Battle;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Cooldowns;
using Peeza.RPG.Core.Basic.Minions;
using Peeza.RPG.Core.Basic.Resources;
using Peeza.RPG.Core.Basic.Statuses;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Battle.Events.Attributes;
using Peeza.RPG.Core.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace Peeza.RPG.Base.Battle.Events
{
    public static class BasicBattleEntityEventsImplementations
    {
        #region GlobalEvents

        [BattleEventAction(typeof(OnDungeon), Priority = 2000)]
        public static bool OnDungeonClearMinions(BattleEventArgs<OnTimeEnum> args)
        {
            if (!args.Self.HaveMinions) return false;
            args.Self.Minions.Clear();
            return false;
        }

        [BattleEventAction(typeof(OnDungeon), Priority = 1000)]
        public static bool OnDungeonStartClearCooldowns(BattleEventArgs<OnTimeEnum> args)
        {
            if (args.Data != OnTimeEnum.Start) return false;
            if (!args.Self.SupportsCooldowns) return false;
            args.Self.Cooldowns.DecreaseCooldownsOfType(CooldownTypeFlags.OnDungeonStart);

            return false;
        }

        [BattleEventAction(typeof(OnDungeon), Priority = 1000)]
        public static bool OnDungeonEndClearCooldowns(BattleEventArgs<OnTimeEnum> args)
        {
            if (args.Data != OnTimeEnum.End) return false;
            if (!args.Self.SupportsCooldowns) return false;
            args.Self.Cooldowns.DecreaseCooldownsOfType(CooldownTypeFlags.OnDungeonEnd);

            return false;
        }

        [BattleEventAction(typeof(OnFight), Priority = 1000)]
        public static bool OnFightStartClearCooldowns(BattleEventArgs<OnTimeEnum> args)
        {
            if (args.Data != OnTimeEnum.Start) return false;
            if (!args.Self.SupportsCooldowns) return false;
            args.Self.Cooldowns.DecreaseCooldownsOfType(CooldownTypeFlags.OnFightStart);

            return false;
        }

        [BattleEventAction(typeof(OnFight), Priority = 1000)]
        public static bool OnFightEndClearCooldowns(BattleEventArgs<OnTimeEnum> args)
        {
            if (args.Data != OnTimeEnum.End) return false;
            if (!args.Self.SupportsCooldowns) return false;
            args.Self.Cooldowns.DecreaseCooldownsOfType(CooldownTypeFlags.OnFightEnd);

            return false;
        }

        [BattleEventAction(typeof(OnRound), Priority = 1000)]
        public static bool OnRoundStartClearCooldowns(BattleEventArgs<OnTimeEnum> args)
        {
            if (args.Data != OnTimeEnum.Start) return false;
            if (!args.Self.SupportsCooldowns) return false;
            args.Self.Cooldowns.DecreaseCooldownsOfType(CooldownTypeFlags.OnRoundStart);

            return false;
        }

        [BattleEventAction(typeof(OnRound), Priority = 1000)]
        public static bool OnRoundEndClearCooldowns(BattleEventArgs<OnTimeEnum> args)
        {
            if (args.Data != OnTimeEnum.End) return false;
            if (!args.Self.SupportsCooldowns) return false;
            args.Self.Cooldowns.DecreaseCooldownsOfType(CooldownTypeFlags.OnRoundEnd);

            return false;
        }

        [BattleEventAction(typeof(OnRound), Priority = 900)]
        public static bool OnRoundProcessMinions(BattleEventArgs<OnTimeEnum> args)
        {
            if (args.Data != OnTimeEnum.Start) return false;
            if (!args.Self.HaveMinions) return false;

            var expired = new List<Minion>();
            foreach (var minion in args.Self.Minions)
            {
                minion.TimeLeft--;
                if (minion.TimeLeft <= 0)
                {
                    expired.Add(minion);
                }
            }

            var removeFromQueue = expired.Where(e => e.BattleEntity is IBattleQueable).Select(e => e.BattleEntity as IBattleQueable);
            args.BattleQueue?.RemoveRange(removeFromQueue);

            if (expired.Any() && args.Self.SupportsEvents)
            {
                var newArgs = BattleEventArgs<OnMinionData>.CloneFrom(args, data: new OnMinionData()
                {
                    DeadMinions = expired.ToList()
                });
                args.Self.EventHandler.OnMinionSummoned(newArgs);
            }

            return false;
        }

        [BattleEventAction(typeof(OnRound), Priority = 900)]
        public static bool OnRoundProcessStatuses(BattleEventArgs<OnTimeEnum> args)
        {
            if (args.Data != OnTimeEnum.Start) return false;

            var statuses = new List<DecayingStatus>();
            if (args.Self.SupportsStatuses)
            {
                statuses.AddRange(args.Self.Statuses.GetAll(status => status is DecayingStatus decayingStatus).Cast<DecayingStatus>());
            }
            if (args.Self.SupportsBuffs)
            {
                statuses.AddRange(args.Self.Buffs.GetAll(buff => !buff.Flags.HasFlag(BuffFlags.InfiniteDuration)));
            }
            if (args.Self.SupportsAilments)
            {
                statuses.AddRange(args.Self.Ailments.GetAll(ailment => !ailment.Flags.HasFlag(AilmentFlags.InfiniteDuration)));
            }

            if (!statuses.Any()) return false;

            var expired = new List<DecayingStatus>();
            foreach (var status in statuses)
            {
                status.TurnsLeft--;
                if (status.TurnsLeft <= 0)
                {
                    expired.Add(status);
                }
            }

            if (expired.Any() && args.Self.SupportsEvents)
            {
                var newArgs = BattleEventArgs<OnStatusData>.CloneFrom(args, data: new OnStatusData()
                {
                    LiftedStatuses = expired.Cast<Status>().ToList()
                });
                args.Self.EventHandler.OnAffectedWithStatus(newArgs);
            }

            return false;
        }

        [BattleEventAction(typeof(OnTurn), Priority = 1000)]
        public static bool OnTurnStartClearCooldowns(BattleEventArgs<OnTimeEnum> args)
        {
            if (args.Data != OnTimeEnum.Start) return false;
            if (!args.Self.SupportsCooldowns) return false;
            args.Self.Cooldowns.DecreaseCooldownsOfType(CooldownTypeFlags.OnTurnStart);

            return false;
        }

        [BattleEventAction(typeof(OnTurn), Priority = 1000)]
        public static bool OnTurnEndClearCooldowns(BattleEventArgs<OnTimeEnum> args)
        {
            if (args.Data != OnTimeEnum.End) return false;
            if (!args.Self.SupportsCooldowns) return false;
            args.Self.Cooldowns.DecreaseCooldownsOfType(CooldownTypeFlags.OnTurnEnd);

            return false;
        }

        [BattleEventAction(typeof(OnEnvironmentChange), Priority = 1000)]
        public static bool OnEnvironmentChangeHookEvents(BattleEventArgs<EnvironmentChangeData> args)
        {
            if (!args.Self.SupportsEvents) return false;

            args.Data.OldBattleField?.Unhook(args.Self.EventHandler);
            args.Data.NewBattleField?.Hook(args.Self.EventHandler);

            return false;
        }

        #endregion

        #region AffectedBy

        [BattleEventAction(typeof(OnAuraApplied), Priority = 200)]
        public static bool OnAuraApplied(BattleEventArgs<OnAuraData> args)
        {
            foreach (var aura in args.Data.NewAuras)
            {
                args.Log($"{args.Self.Name} is now under effect of '{aura.Key}' aura");

                using var indent = args.Logger?.IndentLog();
                foreach (var buff in aura.Buffs)
                {
                    args.Log(buff.ToString(), BattleLogVerbosity.Technical);
                }
            }

            foreach (var aura in args.Data.LiftedAuras)
            {
                args.Log($"{args.Self.Name} is no longer under effect of '{aura.Key}' aura");

                using var indent = args.Logger?.IndentLog();
                foreach (var buff in aura.Buffs)
                {
                    args.Log(buff.ToString(), BattleLogVerbosity.Technical);
                }
            }

            return false;
        }

        [BattleEventAction(typeof(OnAffectedWithStatus), Priority = 100)]
        public static bool OnAffectedWithStatusBase(BattleEventArgs<OnStatusData> args)
        {
            foreach (var status in args.Data.NewStatuses)
            {
                ProcessStatus(status, args.Self, true);
                string statusTypeString = status switch
                {
                    Buff => " buff",
                    Ailment => " ailment",
                    _ => "",
                };
                switch (status)
                {
                    case DecayingStatus decaying:
                        args.Log($"{(status.Inflicter != null ? $"{status.Inflicter.Name} inflicts {args.Self.Name}" : $"{args.Self.Name} is inflicted")} with {status.Key}{statusTypeString}{(decaying.TurnsLeft > 0 ? $" for {decaying.TurnsLeft} turns" : "")}", status.StatusFlags.HasFlag(StatusFlags.Silent) ? BattleLogVerbosity.Technical : BattleLogVerbosity.Info);
                        if (decaying is Buff buff)
                        {
                            using var indent = args.Logger?.IndentLog();
                            args.Log(buff.ToString(), BattleLogVerbosity.Technical);
                        }
                        break;
                    default:
                        args.Log($"{(status.Inflicter != null ? $"{status.Inflicter.Name} inflicts {args.Self.Name}" : $"{args.Self.Name} is inflicted")}{statusTypeString} with {status.Key}", status.StatusFlags.HasFlag(StatusFlags.Silent) ? BattleLogVerbosity.Technical : BattleLogVerbosity.Info);
                        break;
                }
            }

            foreach (var status in args.Data.LiftedStatuses)
            {
                ProcessStatus(status, args.Self, false);
                args.Log($"{args.Self.Name} no longer inflicted with {status.Key}", status.StatusFlags.HasFlag(StatusFlags.Silent) ? BattleLogVerbosity.Technical : BattleLogVerbosity.Info);
                if (status is Buff buff)
                {
                    using var indent = args.Logger?.IndentLog();
                    args.Log(buff.ToString(), BattleLogVerbosity.Technical);
                }
            }

            return false;
        }

        [BattleEventAction(typeof(OnMinionSummoned), Priority = 100)]
        public static bool OnMinionSummoned(BattleEventArgs<OnMinionData> args)
        {
            if (!args.Self.HaveMinions) return false;

            if (args.Data.NewMinions != null && args.Data.NewMinions.Any())
            {
                foreach (var minion in args.Data.NewMinions)
                {
                    args.Self.Minions.Add(minion);
                    minion.BattleEntity.BattleSide?.BattleEntities.Add(minion.BattleEntity);
                }
            }

            if (args.Data.DeadMinions != null && args.Data.DeadMinions.Any())
            {
                foreach (var minion in args.Data.DeadMinions)
                {
                    args.Self.Minions.Remove(minion);
                    minion.BattleEntity.BattleSide?.BattleEntities.Remove(minion.BattleEntity);
                }
            }

            return false;
        }

        #endregion

        #region BattleOffense

        [BattleEventAction(typeof(OnPickTarget), Priority = 100)]
        public static bool OnPickTarget(BattleEventArgs<TargetData> args)
        {
            var canTarget = args.Data.PossibleTargets.Where(t => t.CanFight && t.SupportStats);
            if (!canTarget.Any()) return false;

            args.Data.Target = canTarget.RandomByWeight(e => e.Stats.GetAggro(), args.Random);

            return false;
        }

        [BattleEventAction(typeof(OnPickTarget), Priority = 10)]
        public static bool OnPickTargetCallOnSelected(BattleEventArgs<TargetData> args)
        {
            if (args.Data.Target?.SupportsEvents ?? false)
            {
                IBattleEntity target = null;
                while (target != args.Data.Target)
                {
                    target = args.Data.Target;
                    args.Data.Target?.EventHandler.OnSelectedAsTarget(BattleEventArgs<TargetData>.CloneFrom(args, target, args.Self, args.Data));
                }
            }

            return false;
        }

        [BattleEventAction(typeof(OnCalculateDamage), Priority = 100)]
        public static bool OnCalculateDamage(BattleEventArgs<DamageInfo> args)
        {
            if (!args.Self.SupportStats) return false;

            args.Data = new DamageInfo()
            {
                DamageInstances = []
            };

            var dmgInstance = new DamageInstanceInfo()
            {
                Source = DamageInstanceInfoCommonSourcesEnum.BasicAttack,
                Flags = args.Self is BasicBattleEntity entiity ? entiity.DefaultAttackFlags : DamageInstanceInfoFlags.None
            };

            args.Data.DamageInstances.Add(dmgInstance);

            if (!args.Random.Roll(args.Self.Stats.GetAccuracy()))
            {
                dmgInstance.Result = DamageInstanceInfoResultFlags.Miss;
                return false;
            }

            if (args.Random.Roll(args.Self.Stats.GetCritChance()))
            {
                dmgInstance.Modifiers |= DamageInstanceInfoModifierFlags.Crit;
                dmgInstance.Damage = (long)(args.Self.Stats.GetAttack() * args.Self.Stats.GetCritDamage());
            }
            else
            {
                dmgInstance.Damage = args.Self.Stats.GetAttack();
            }

            return false;
        }

        [BattleEventAction(typeof(OnDoAttack), Priority = 10)]
        public static bool OnDoAttack(BattleEventArgs<DamageInfo> args)
        {
            if (args.Target != null && args.Target.SupportsEvents)
            {
                args.Log($"{args.Self.Name} is attacking {args.Target.Name}");
                using var indent = args.Logger?.IndentLog();
                args.Target.EventHandler.OnGetHit(args.Clone(true, true));
            }

            return false;
        }

        [BattleEventAction(typeof(OnAttackResolve), Priority = 100)]
        public static bool OnAttackResolve(BattleEventArgs<DamageInfo> args)
        {
            foreach (var damageInstance in args.Data.DamageInstances)
            {
                args.Log($"{args.Self.Name} dealt {args.Data.TotalDamageDealt} ({args.Data.TotalMitigated} mitigated) damage");
                using var indent = args.Logger?.IndentLog();
                args.Log($"{damageInstance.Source} ({damageInstance.Flags}) dealt {damageInstance.DamageDealt}" +
                    $" {(damageInstance.Modifiers.HasFlag(DamageInstanceInfoModifierFlags.Crit) ? "(crit) " : "")}" +
                    $"{(damageInstance.DamageMitigated > 0 ? $"({damageInstance.DamageMitigated} mitigated) " : "")}damage" +
                    $"{(damageInstance.DamageDealt == 0 ? $" ({damageInstance.Result})" : "")}" +
                    $"{(damageInstance.DamageLeft > 0 ? $" ({damageInstance.DamageLeft} overkill)" : "")}", BattleLogVerbosity.Verbose);
            }

            return false;
        }

        [BattleEventAction(typeof(OnAttackResolve), Priority = 90)]
        public static bool OnAttackResolveLifesteal(BattleEventArgs<DamageInfo> args)
        {
            if (!args.Self.SupportsEvents || !args.Self.SupportStats) return false;

            if (args.Data.TotalDamageDealt > 0 && args.Self.Stats.GetLifesteal() > 0)
            {
                args.Self.EventHandler.OnGetHeal(BattleEventArgs<HealInfo>.CloneFrom(args, args.Self, args.Self, new()
                {
                    HealInstances = [new() {
                        Flags = HealInstanceInfoFlags.Health,
                        Heal = (long)(args.Data.TotalDamageDealt * args.Self.Stats.GetLifesteal()),
                        Modifiers = HealInstanceInfoModifierFlags.IsLifesteal,
                        Source = HealInstanceInfoCommonSourcesEnum.Lifesteal,
                    }]
                }));
            }

            return false;
        }

        #endregion

        #region BattleDefense

        [BattleEventAction(typeof(OnGetHit), Priority = 100)]
        public static bool OnGetHit(BattleEventArgs<DamageInfo> args)
        {
            if (!args.Self.SupportsEvents) return false;

            foreach (var damageInstance in args.Data.DamageInstances)
            {
                var newArgs = BattleEventArgs<DamageInstanceInfo>.CloneFrom(args, data: damageInstance);
                args.Self.EventHandler.OnProcessDamageInstance(newArgs);
            }

            args.Self.EventHandler.OnGetHitResult(args);

            return false;
        }

        [BattleEventAction(typeof(OnGetHitResult), Priority = 100)]
        public static bool OnGetHitResult(BattleEventArgs<DamageInfo> args)
        {
            if (args.Target == null || !args.Target.SupportsEvents) return false;

            args.Target.EventHandler.OnAttackResolve(args.Clone(true, true));

            return false;
        }

        [BattleEventAction(typeof(OnProcessDamageInstance), Priority = 100)]
        public static bool OnProcessDamageInstanceCheckResult(BattleEventArgs<DamageInstanceInfo> args)
        {
            if (args.Data.Result != DamageInstanceInfoResultFlags.None) return true;

            return false;
        }

        [BattleEventAction(typeof(OnProcessDamageInstance), Priority = 90)]
        public static bool OnProcessDamageInstanceTryDodge(BattleEventArgs<DamageInstanceInfo> args)
        {
            if (!args.Self.SupportStats) return false;

            if (!args.Data.Modifiers.HasFlag(DamageInstanceInfoModifierFlags.CantDodge) && args.Random.Roll(args.Self.Stats.GetDodgeChance()))
            {
                args.Data.Result |= DamageInstanceInfoResultFlags.Dodge;
                args.Data.Damage = 0;
                return true;
            }

            return false;
        }

        [BattleEventAction(typeof(OnProcessDamageInstance), Priority = 80)]
        public static bool OnProcessDamageInstanceTryBlock(BattleEventArgs<DamageInstanceInfo> args)
        {
            if (!args.Self.SupportStats) return false;

            if (!args.Data.Modifiers.HasFlag(DamageInstanceInfoModifierFlags.CantBlock) && args.Random.Roll(args.Self.Stats.GetBlockChance()))
            {
                args.Data.Result |= DamageInstanceInfoResultFlags.Block;
                args.Data.Damage = 0;
                return true;
            }

            return false;
        }

        [BattleEventAction(typeof(OnProcessDamageInstance), Priority = 70)]
        public static bool OnProcessDamageInstanceCallCalculateDefense(BattleEventArgs<DamageInstanceInfo> args)
        {
            if (!args.Self.SupportsEvents) return false;

            args.Self.EventHandler.OnCalculateDefenses(args);

            return false;
        }

        [BattleEventAction(typeof(OnProcessDamageInstance), Priority = 60)]
        public static bool OnProcessDamageInstanceCallDoDamage(BattleEventArgs<DamageInstanceInfo> args)
        {
            if (!args.Self.SupportsEvents) return false;

            args.Self.EventHandler.OnDoDamage(args);

            return false;
        }

        [BattleEventAction(typeof(OnCalculateDefenses), Priority = 100)]
        public static bool OnCalculateDefenses(BattleEventArgs<DamageInstanceInfo> args)
        {
            if (!args.Self.SupportStats) return false;
            if (args.Data.Status.HasFlag(DamageInstanceInfoStatusFlags.DefenseCalculated)) return true;

            var damageMultiplier = 1d;

            if (args.Data.Flags.HasFlag(DamageInstanceInfoFlags.Magic))
            {
                damageMultiplier *= CalculateMagicMitigation(args.Self.Stats.GetMagicResistance(), args.Self.Stats.GetLevel(), args.Target.Stats.GetLevel());
            }
            if (args.Data.Flags.HasFlag(DamageInstanceInfoFlags.Physical))
            {
                damageMultiplier *= CalculateMagicMitigation(args.Self.Stats.GetMagicResistance(), args.Self.Stats.GetLevel(), args.Target.Stats.GetLevel());
            }

            args.Data.Damage = (long)(args.Data.Damage * damageMultiplier);
            args.Data.Status |= DamageInstanceInfoStatusFlags.DefenseCalculated;

            return false;
        }

        [BattleEventAction(typeof(OnDoDamage), Priority = 100)]
        public static bool OnDoDamageCheckIfNeedProcessing(BattleEventArgs<DamageInstanceInfo> args)
        {
            if (args.Data.Result != DamageInstanceInfoResultFlags.None) return true;
            if (args.Data.Status.HasFlag(DamageInstanceInfoStatusFlags.DamageDone)) return true;
            if (args.Data.Damage <= 0)
            {
                args.Data.Status |= DamageInstanceInfoStatusFlags.DamageDone;
                args.Data.Result |= DamageInstanceInfoResultFlags.Done;
                return true;
            }

            return false;
        }

        [BattleEventAction(typeof(OnDoDamage), Priority = 90)]
        public static bool OnDoDamageToShields(BattleEventArgs<DamageInstanceInfo> args)
        {
            if (!args.Self.SupportResources) return false;

            var shields = args.Self.Resources.GetShield();
            if (shields == null || shields.Value <= shields.MinValue) return false;

            var resourceChangeInfo = DealDamage(shields, args.Data);

            if (args.Self.SupportsEvents && resourceChangeInfo != null)
            {
                args.Self.EventHandler.OnResourceChange(BattleEventArgs<ResourceChangeInfo>.CloneFrom(args, data: resourceChangeInfo));
            }

            return args.Data.DamageLeft <= 0;
        }

        [BattleEventAction(typeof(OnDoDamage), Priority = 80)]
        public static bool OnDoDamageToHealth(BattleEventArgs<DamageInstanceInfo> args)
        {
            if (!args.Self.SupportResources) return false;

            var health = args.Self.Resources.GetHealth();
            if (health == null || health.Value <= health.MinValue) return false;

            var resourceChangeInfo = DealDamage(health, args.Data);

            if (args.Self.SupportsEvents && resourceChangeInfo != null)
            {
                args.Self.EventHandler.OnResourceChange(BattleEventArgs<ResourceChangeInfo>.CloneFrom(args, data: resourceChangeInfo));
            }

            return args.Data.DamageLeft <= 0;
        }

        [BattleEventAction(typeof(OnGetHeal), Priority = 100)]
        public static bool OnGetHeal(BattleEventArgs<HealInfo> args)
        {
            if (!args.Self.SupportsEvents) return false;

            foreach (var healInstance in args.Data.HealInstances)
            {
                var newArgs = BattleEventArgs<HealInstanceInfo>.CloneFrom(args, data: healInstance);
                args.Self.EventHandler.OnProcessHealInstance(newArgs);
            }

            args.Self.EventHandler.OnGetHealResult(args);

            return false;
        }

        [BattleEventAction(typeof(OnGetHealResult), Priority = 100)]
        public static bool OnGetHealResult(BattleEventArgs<HealInfo> args)
        {
            if (args.Target == null || !args.Target.SupportsEvents) return false;

            args.Target.EventHandler.OnHealResolve(args.Clone(true, true));

            return false;
        }

        [BattleEventAction(typeof(OnProcessHealInstance), Priority = 100)]
        public static bool OnProcessHealInstanceCheckResult(BattleEventArgs<HealInstanceInfo> args)
        {
            if (args.Data.Result != HealInstanceInfoResultFlags.None) return true;

            return false;
        }

        [BattleEventAction(typeof(OnProcessHealInstance), Priority = 60)]
        public static bool OnProcessHealInstanceCallDoHeal(BattleEventArgs<HealInstanceInfo> args)
        {
            if (!args.Self.SupportsEvents) return false;

            args.Self.EventHandler.OnDoHeal(args);

            return false;
        }

        [BattleEventAction(typeof(OnDoHeal), Priority = 100)]
        public static bool OnDoHealCheckIfNeedProcessing(BattleEventArgs<HealInstanceInfo> args)
        {
            if (args.Data.Result != HealInstanceInfoResultFlags.None) return true;
            if (args.Data.Heal <= 0)
            {
                args.Data.Result |= HealInstanceInfoResultFlags.Done;
                return true;
            }

            return false;
        }

        [BattleEventAction(typeof(OnDoHeal), Priority = 90)]
        public static bool OnDoHealToHealth(BattleEventArgs<HealInstanceInfo> args)
        {
            if (!args.Self.SupportResources) return false;
            if (!args.Data.Flags.HasFlag(HealInstanceInfoFlags.Health)) return false;

            var health = args.Self.Resources.GetHealth();
            if (health == null || health.Value >= health.MaxValue) return false;

            var resourceChangeInfo = DealHeal(health, args.Data);

            if (args.Self.SupportsEvents && resourceChangeInfo != null)
            {
                args.Self.EventHandler.OnResourceChange(BattleEventArgs<ResourceChangeInfo>.CloneFrom(args, data: resourceChangeInfo));
            }

            return args.Data.HealLeft <= 0;
        }

        [BattleEventAction(typeof(OnDoHeal), Priority = 80)]
        public static bool OnDoHealToShields(BattleEventArgs<HealInstanceInfo> args)
        {
            if (!args.Self.SupportResources) return false;
            if (!args.Data.Flags.HasFlag(HealInstanceInfoFlags.Shield)) return false;

            var shields = args.Self.Resources.GetShield();
            if (shields == null || shields.Value <= shields.MinValue) return false;

            var resourceChangeInfo = DealHeal(shields, args.Data);

            if (args.Self.SupportsEvents && resourceChangeInfo != null)
            {
                args.Self.EventHandler.OnResourceChange(BattleEventArgs<ResourceChangeInfo>.CloneFrom(args, data: resourceChangeInfo));
            }

            return args.Data.HealLeft <= 0;
        }

        [BattleEventAction(typeof(OnHealResolve), Priority = 100)]
        public static bool OnHealResolve(BattleEventArgs<HealInfo> args)
        {
            foreach (var healInstance in args.Data.HealInstances)
            {
                args.Log($"{args.Self.Name} healed {args.Data.TotalHealDealt} HP");
                using var indent = args.Logger?.IndentLog();
                args.Log($"{healInstance.Source} ({healInstance.Flags}) healed {healInstance.HealDealt}" +
                    $" {(healInstance.Modifiers.HasFlag(HealInstanceInfoModifierFlags.Crit) ? "(crit) " : "")}HP" +
                    $"{(healInstance.HealDealt == 0 ? $" ({healInstance.Result})" : "")}" +
                    $"{(healInstance.HealLeft > 0 ? $" ({healInstance.HealLeft} overheal)" : "")}", BattleLogVerbosity.Verbose);
            }

            return false;
        }

        #endregion

        #region BattleOther

        [BattleEventAction(typeof(OnResourceChange), Priority = 150)]
        public static bool OnResourceChangeLog(BattleEventArgs<ResourceChangeInfo> args)
        {
            args.Log($"{args.Self.Name} {args.Data.Key} has changed from {args.Data.OldValue} to {args.Data.NewValue}{(args.Data.Reason != null ? $" due to {args.Data.Reason}" : "")}", BattleLogVerbosity.Technical);

            return false;
        }

        [BattleEventFilterAction(BasicResourcesDeclarationsExtensions.HealthResourceKey, typeof(OnResourceChange), Priority = 100)]
        public static bool OnHealthChange(BattleEventArgs<ResourceChangeInfo> args)
        {
            if (!args.Self.SupportResources) return false;

            var health = args.Self.Resources.GetHealth();
            if (health == null) return false;

            if (health.Value == 0 && args.Self.SupportsEvents)
            {
                args.Self.EventHandler.OnDeath(args);
            }

            return false;
        }

        [BattleEventAction(typeof(OnDeath), Priority = 100)]
        public static bool OnDeath(BattleEventArgs<ResourceChangeInfo> args)
        {
            if (!args.Self.SupportResources) return false;

            var shields = args.Self.Resources.GetShield();
            var health = args.Self.Resources.GetHealth();

            if (shields != null) shields.Value = 0;
            if (health != null) health.Value = 0;

            if (args.Self.SupportsStatuses)
            {
                args.Self.EventHandler.OnAffectedWithStatus(BattleEventArgs<OnStatusData>.CloneFrom(args, false, new OnStatusData()
                {
                    LiftedStatuses = args.Self.Statuses.GetAll().ToList()
                }));
            }

            if (args.Self.HaveMinions)
            {
                args.Self.EventHandler.OnMinionSummoned(BattleEventArgs<OnMinionData>.CloneFrom(args, false, new OnMinionData()
                {
                    NewMinions = args.Self.Minions.GetAll().ToList()
                }));
            }

            args.Log($"{args.Self.Name} died");

            if (args.Target?.SupportsEvents ?? false)
            {
                args.Target.EventHandler.OnKill(BattleEventArgs<ResourceChangeInfo>.CloneFrom(args, true, true));
            }

            return true;
        }

        [BattleEventAction(typeof(OnRessurection), Priority = 100)]
        public static bool OnRessurection(BattleEventArgs<RessurectionData> args)
        {
            if (!args.Self.SupportResources) return false;

            var health = args.Self.Resources.GetHealth();
            if (health != null) health.Value = args.Data.HealthToRestore;

            args.Log($"{args.Self.Name} ressurected");

            return true;
        }

        #endregion

        #region HelperFunctions

        public static ResourceChangeInfo DealHeal(Resource resource, HealInstanceInfo healInstanceInfo)
        {
            if (resource.Value >= resource.MaxValue) return null;
            if (healInstanceInfo.HealLeft <= 0) return null;

            var oldResourceValue = resource.Value;
            resource.Value += healInstanceInfo.HealLeft;
            healInstanceInfo.HealDealt += resource.Value - oldResourceValue;

            if (healInstanceInfo.HealLeft <= 0)
            {
                healInstanceInfo.Result |= HealInstanceInfoResultFlags.Done;
            }

            return new ResourceChangeInfo(resource.Key, oldResourceValue, resource.Value, healInstanceInfo);
        }

        public static ResourceChangeInfo DealDamage(Resource resource, DamageInstanceInfo damageInstanceInfo)
        {
            if (resource.Value <= resource.MinValue) return null;
            if (damageInstanceInfo.DamageLeft <= 0) return null;

            var oldResourceValue = resource.Value;
            resource.Value -= damageInstanceInfo.DamageLeft;
            damageInstanceInfo.DamageDealt += oldResourceValue - resource.Value;

            if (damageInstanceInfo.DamageLeft <= 0)
            {
                damageInstanceInfo.Status |= DamageInstanceInfoStatusFlags.DamageDone;
                damageInstanceInfo.Result |= DamageInstanceInfoResultFlags.Done;
            }

            return new ResourceChangeInfo(resource.Key, oldResourceValue, resource.Value, damageInstanceInfo);
        }

        public static double CalculateArmorMitigation(long armor, int level, int attackerLevel)
        {
            var levelDifference = level - attackerLevel;
            if (levelDifference > 0)
            {
                armor += levelDifference * 10;
            }
            else if (levelDifference < 0)
            {
                armor += levelDifference * 5;
            }

            if (armor >= 0)
            {
                return 150d / (150 + armor);
            }
            else
            {
                return 2 - (300d / (300 - armor));
            }
        }

        public static double CalculateMagicMitigation(long mr, int level, int attackerLevel)
        {
            var levelDifference = level - attackerLevel;
            if (levelDifference > 0)
            {
                mr += levelDifference * 12;
            }
            else if (levelDifference < 0)
            {
                mr += levelDifference * 6;
            }

            if (mr >= 0)
            {
                return 250d / (250 + mr);
            }
            else
            {
                return 2 - (200d / (200 - mr));
            }
        }

        private static void ProcessStatus<T>(T status, IBattleEntity entity, bool add) where T : Status
        {
            switch (status)
            {
                case Ailment ailment:
                    if (!entity.SupportsAilments) break;
                    if (add)
                    {
                        entity.Ailments.Add(ailment);
                    }
                    else
                    {
                        entity.Ailments.Remove(ailment);
                    }
                    break;

                case Buff buff:
                    if (!entity.SupportsBuffs) break;
                    if (add)
                    {
                        entity.Buffs.Add(buff);
                    }
                    else
                    {
                        entity.Buffs.Remove(buff);
                    }
                    break;

                default:
                    if (!entity.SupportsStatuses) break;
                    if (add)
                    {
                        entity.Statuses.Add(status);
                    }
                    else
                    {
                        entity.Statuses.Remove(status);
                    }
                    break;
            }
        }

        #endregion
    }
}
