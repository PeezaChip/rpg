﻿using Peeza.RPG.Base.Battle.Arguments;
using Peeza.RPG.Core.Basic.Auras;
using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Cooldowns;
using Peeza.RPG.Core.Battle.Events;
using Peeza.RPG.Core.Battle.Events.Attributes;
using System.Collections.Generic;
using System.Linq;

namespace Peeza.RPG.Base.Battle.Events
{
    public static class BasicBattleFieldEventsImplementations
    {
        #region GlobalEvents

        [BattleEventAction(typeof(OnFight), Priority = 1000)]
        public static bool OnFightStartClearCooldowns(BattleEventArgs<OnTimeEnum> args)
        {
            if (args.Data != OnTimeEnum.Start) return false;
            if (!args.Field.SupportsCooldowns) return false;
            args.Field.Cooldowns.DecreaseCooldownsOfType(CooldownTypeFlags.OnFightStart);

            return false;
        }

        [BattleEventAction(typeof(OnFight), Priority = 1000)]
        public static bool OnFightEndClearCooldowns(BattleEventArgs<OnTimeEnum> args)
        {
            if (args.Data != OnTimeEnum.End) return false;
            if (!args.Field.SupportsCooldowns) return false;
            args.Field.Cooldowns.DecreaseCooldownsOfType(CooldownTypeFlags.OnFightEnd);

            return false;
        }

        [BattleEventAction(typeof(OnRound), Priority = 1000)]
        public static bool OnRoundStartClearCooldowns(BattleEventArgs<OnTimeEnum> args)
        {
            if (args.Data != OnTimeEnum.Start) return false;
            if (!args.Field.SupportsCooldowns) return false;
            args.Field.Cooldowns.DecreaseCooldownsOfType(CooldownTypeFlags.OnRoundStart);

            return false;
        }

        [BattleEventAction(typeof(OnRound), Priority = 1000)]
        public static bool OnRoundEndClearCooldowns(BattleEventArgs<OnTimeEnum> args)
        {
            if (args.Data != OnTimeEnum.End) return false;
            if (!args.Field.SupportsCooldowns) return false;
            args.Field.Cooldowns.DecreaseCooldownsOfType(CooldownTypeFlags.OnRoundEnd);

            return false;
        }

        [BattleEventAction(typeof(OnTurn), Priority = 1000)]
        public static bool OnTurnStartClearCooldowns(BattleEventArgs<OnTimeEnum> args)
        {
            if (args.Data != OnTimeEnum.Start) return false;
            if (!args.Field.SupportsCooldowns) return false;
            args.Field.Cooldowns.DecreaseCooldownsOfType(CooldownTypeFlags.OnTurnStart);

            return false;
        }

        [BattleEventAction(typeof(OnTurn), Priority = 1000)]
        public static bool OnTurnEndClearCooldowns(BattleEventArgs<OnTimeEnum> args)
        {
            if (args.Data != OnTimeEnum.End) return false;
            if (!args.Field.SupportsCooldowns) return false;
            args.Field.Cooldowns.DecreaseCooldownsOfType(CooldownTypeFlags.OnTurnEnd);

            return false;
        }

        [BattleEventAction(typeof(OnTurn), Priority = 900)]
        public static bool OnTurnCheckAuras(BattleEventArgs<OnTimeEnum> args)
        {
            if (args.Data != OnTimeEnum.Start) return false;
            if (!args.Field.SupportsAuras || !args.Field.SupportsEvents) return false;
            var expiredAuras = args.Field.Auras.Where(aura => !aura.Owner.CanFight);
            if (expiredAuras.Any())
            {
                args.Field.EventHandler.OnAuraApplied(BattleEventArgs<OnAuraData>.CloneFrom(args, data: new OnAuraData() { LiftedAuras = expiredAuras.ToList() }));
            }

            return false;
        }

        #endregion

        #region AffectedBy

        [BattleEventAction(typeof(OnAuraApplied), Priority = 100)]
        public static bool OnAuraApplied(BattleEventArgs<OnAuraData> args)
        {
            foreach (var aura in args.Data.NewAuras)
            {
                if (!args.Field.SupportsAuras) break;
                ProcessAura(args, aura, true);
            }

            foreach (var aura in args.Data.LiftedAuras)
            {
                ProcessAura(args, aura, false);
            }

            return false;
        }

        private static void ProcessAura(BattleEventArgs<OnAuraData> args, Aura aura, bool add)
        {
            var field = args.Field;

            if (!field.SupportsAuras) return;

            List<IBattleEntity> affectedEntities;
            if (!aura.Filter.Any())
            {
                affectedEntities = field.AllBattleSides.SelectMany(s => s.BattleEntities).Distinct().ToList();
            }
            else
            {
                affectedEntities = field.AllBattleSides.Intersect(aura.Filter).SelectMany(s => s.BattleEntities).Distinct().ToList();
            }

            if (add)
            {
                field.Auras.Add(aura);
            }
            else
            {
                field.Auras.Remove(aura);
            }

            foreach (var affectedEntity in affectedEntities)
            {
                if (!affectedEntity.SupportsEvents) continue;

                var newArgs = new BattleEventArgs<OnAuraData>(args.BattleQueue, args.Logger, args.Random, affectedEntity, null, field)
                {
                    Data = add
                    ? new OnAuraData() { NewAuras = [aura] }
                    : new OnAuraData() { LiftedAuras = [aura] }
                };
                affectedEntity.EventHandler.OnAuraApplied(newArgs);
            }
        }

        #endregion
    }
}
