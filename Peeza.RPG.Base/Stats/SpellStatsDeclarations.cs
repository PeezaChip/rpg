﻿using Peeza.RPG.Core.Basic.Stats;

namespace Peeza.RPG.Base.Stats
{
    public class SpellCooldownStatDeclaration : StatDeclaration<int> // prefix
    {
        public SpellCooldownStatDeclaration() : base(1, true) { }
    }

    public class SpellCostStatDeclaration : StatDeclaration<long> // prefix
    {
        public SpellCostStatDeclaration() : base(1, true) { }
    }

    public class SpellPowerStatDeclaration : StatDeclaration<double> // prefix
    {
        public SpellPowerStatDeclaration() : base(1, true) { }
    }

    public class SpellDurationStatDeclaration : StatDeclaration<int> // prefix
    {
        public SpellDurationStatDeclaration() : base(1, true) { }
    }
}
