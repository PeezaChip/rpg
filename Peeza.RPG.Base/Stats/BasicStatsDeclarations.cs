﻿using Peeza.RPG.Core.Basic.Stats;

namespace Peeza.RPG.Base.Stats
{
    public class LevelStatDeclaration : StatDeclaration<int>
    {
        public LevelStatDeclaration() : base(1, false) { }
    }

    public class AggroStatDeclaration : StatDeclaration<double>
    {
        public AggroStatDeclaration() : base(10, true) { }
    }

    public class AttackStatDeclaration : StatDeclaration<long>
    {
        public AttackStatDeclaration() : base(0, true) { }
    }


    public class AccuracyStatDeclaration : StatDeclaration<double>
    {
        public AccuracyStatDeclaration() : base(0, true) { }
    }

    public class CritChanceStatDeclaration : StatDeclaration<double>
    {
        public CritChanceStatDeclaration() : base(0, true) { }
    }

    public class CritDamageStatDeclaration : StatDeclaration<double>
    {
        public CritDamageStatDeclaration() : base(2, true) { }
    }


    public class DodgeChanceStatDeclaration : StatDeclaration<double>
    {
        public DodgeChanceStatDeclaration() : base(0, true) { }
    }

    public class BlockChanceStatDeclaration : StatDeclaration<double>
    {
        public BlockChanceStatDeclaration() : base(0, true) { }
    }


    public class ArmorStatDeclaration : StatDeclaration<long>
    {
        public ArmorStatDeclaration() : base(0, true) { }
    }

    public class MagicResistanceStatDeclaration : StatDeclaration<long>
    {
        public MagicResistanceStatDeclaration() : base(0, true) { }
    }

    public class LifestealStatDeclaration : StatDeclaration<double>
    {
        public LifestealStatDeclaration() : base(0, true) { }
    }
}
