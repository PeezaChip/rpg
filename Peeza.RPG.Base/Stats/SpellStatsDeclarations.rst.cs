// <auto-generated />
// <generate-token>gIL0QP3YfeCg0LSegYTrHrPmIVlKpLAKaBMMYnBxcIQ=-8pjKgrT+YmoPPTRgYTT/RrrMLVSj1M3GvpPpucpMrKU=</generate-token>

using Peeza.RPG.Core.Basic.BattleEntities;
using Peeza.RPG.Core.Basic.Buffs;
using Peeza.RPG.Core.Basic.Stats;

namespace Peeza.RPG.Base.Stats
{
    public static class SpellStatsDeclarationsExtensions
    {
        public const string SpellCooldownStatKey = "SpellCooldown";
        public static int GetSpellCooldown(this IStatsAccessor stats)
        {
            var stat = stats.GetByKey<int>(SpellCooldownStatKey);
            if (stat != null)
            {
                return stat.Value;
            }
            else
            {
                return 1;
            }
        }
        public static int GetBaseSpellCooldown(this IStatsAccessor stats)
        {
            var stat = stats.GetByKey<int>(SpellCooldownStatKey);
            if (stat != null)
            {
                return stat.BaseValue;
            }
            else
            {
                return 1;
            }
        }

        public const string SpellCostStatKey = "SpellCost";
        public static long GetSpellCost(this IStatsAccessor stats)
        {
            var stat = stats.GetByKey<long>(SpellCostStatKey);
            if (stat != null)
            {
                return stat.Value;
            }
            else
            {
                return 1;
            }
        }
        public static long GetBaseSpellCost(this IStatsAccessor stats)
        {
            var stat = stats.GetByKey<long>(SpellCostStatKey);
            if (stat != null)
            {
                return stat.BaseValue;
            }
            else
            {
                return 1;
            }
        }

        public const string SpellPowerStatKey = "SpellPower";
        public static double GetSpellPower(this IStatsAccessor stats)
        {
            var stat = stats.GetByKey<double>(SpellPowerStatKey);
            if (stat != null)
            {
                return stat.Value;
            }
            else
            {
                return 1;
            }
        }
        public static double GetBaseSpellPower(this IStatsAccessor stats)
        {
            var stat = stats.GetByKey<double>(SpellPowerStatKey);
            if (stat != null)
            {
                return stat.BaseValue;
            }
            else
            {
                return 1;
            }
        }

        public const string SpellDurationStatKey = "SpellDuration";
        public static int GetSpellDuration(this IStatsAccessor stats)
        {
            var stat = stats.GetByKey<int>(SpellDurationStatKey);
            if (stat != null)
            {
                return stat.Value;
            }
            else
            {
                return 1;
            }
        }
        public static int GetBaseSpellDuration(this IStatsAccessor stats)
        {
            var stat = stats.GetByKey<int>(SpellDurationStatKey);
            if (stat != null)
            {
                return stat.BaseValue;
            }
            else
            {
                return 1;
            }
        }

    }

    public class FlatSpellCooldownBuff(IBattleEntity inflicter, int value, BuffFlags flags = BuffFlags.None, int turnsLeft = 0) : Buff<int>(SpellStatsDeclarationsExtensions.SpellCooldownStatKey, inflicter, value, flags, turnsLeft);
    public class MultiplicativeSpellCooldownBuff(IBattleEntity inflicter, double value, BuffFlags flags = BuffFlags.None, int turnsLeft = 0) : Buff<double>(SpellStatsDeclarationsExtensions.SpellCooldownStatKey, inflicter, value, flags | BuffFlags.Multiplicative, turnsLeft);
    public class SpellCooldownStat(string prefix, int value = 1, IBuffsAccessor buffs = null, int minValue = 0) : PrefixedBuffableStat<int>(SpellStatsDeclarationsExtensions.SpellCooldownStatKey, prefix, buffs, value, minValue);

    public class FlatSpellCostBuff(IBattleEntity inflicter, long value, BuffFlags flags = BuffFlags.None, int turnsLeft = 0) : Buff<long>(SpellStatsDeclarationsExtensions.SpellCostStatKey, inflicter, value, flags, turnsLeft);
    public class MultiplicativeSpellCostBuff(IBattleEntity inflicter, double value, BuffFlags flags = BuffFlags.None, int turnsLeft = 0) : Buff<double>(SpellStatsDeclarationsExtensions.SpellCostStatKey, inflicter, value, flags | BuffFlags.Multiplicative, turnsLeft);
    public class SpellCostStat(string prefix, long value = 1, IBuffsAccessor buffs = null, long minValue = 0) : PrefixedBuffableStat<long>(SpellStatsDeclarationsExtensions.SpellCostStatKey, prefix, buffs, value, minValue);

    public class SpellPowerBuff(IBattleEntity inflicter, double value, BuffFlags flags = BuffFlags.None, int turnsLeft = 0) : Buff<double>(SpellStatsDeclarationsExtensions.SpellPowerStatKey, inflicter, value, flags, turnsLeft);
    public class SpellPowerStat(string prefix, double value = 1, IBuffsAccessor buffs = null, double minValue = 0) : PrefixedBuffableStat<double>(SpellStatsDeclarationsExtensions.SpellPowerStatKey, prefix, buffs, value, minValue);

    public class FlatSpellDurationBuff(IBattleEntity inflicter, int value, BuffFlags flags = BuffFlags.None, int turnsLeft = 0) : Buff<int>(SpellStatsDeclarationsExtensions.SpellDurationStatKey, inflicter, value, flags, turnsLeft);
    public class MultiplicativeSpellDurationBuff(IBattleEntity inflicter, double value, BuffFlags flags = BuffFlags.None, int turnsLeft = 0) : Buff<double>(SpellStatsDeclarationsExtensions.SpellDurationStatKey, inflicter, value, flags | BuffFlags.Multiplicative, turnsLeft);
    public class SpellDurationStat(string prefix, int value = 1, IBuffsAccessor buffs = null, int minValue = 0) : PrefixedBuffableStat<int>(SpellStatsDeclarationsExtensions.SpellDurationStatKey, prefix, buffs, value, minValue);
}
