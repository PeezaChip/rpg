﻿using Peeza.RPG.Core.Basic.Buffs;
using System;

namespace Peeza.RPG.Base.Stats
{
    public static class BaseStatScalings
    {
        public static AttackStat CalculateAttack(uint level, IBuffsAccessor buffs = null)
        {
            return new AttackStat(60 + 2 * level, buffs);
        }

        public static ArmorStat CalculateArmor(uint level, IBuffsAccessor buffs = null)
        {
            return new ArmorStat(25 + (long)Math.Round(1.5 * level), buffs);
        }

        public static MagicResistanceStat CalculateMagicResistance(uint level, IBuffsAccessor buffs = null)
        {
            return new MagicResistanceStat(25 + (long)Math.Round(1.4 * level), buffs);
        }
    }
}
