﻿using Peeza.RPG.Core.Basic.Stats;

namespace Peeza.RPG.Base.Stats
{
    public class ItemLevelStatDeclaration : StatDeclaration<int>
    {
        public ItemLevelStatDeclaration() : base(1, false) { }
    }
}
