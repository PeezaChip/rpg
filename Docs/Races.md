﻿* **Naga** - Serpent's Guile
	* Bonus: Attacks have a chance to apply poison
	* Ability: Has a chance to redirect an enemy attack to another target or nullify it completely

---

* **Demon** - Infernal Dominance
	* Bonus: On each kill receives a temporary bonus to attack, applies all stat debuff on each party member
	* Ability: Once per battle creates a portal that spawns minions (portal is targetable and will create minions every turn)

---

* **Undead** - Unholy Constitution
	* Bonus: HP is doubled, gains lifesteal, cannot be healed
	* Ability: On each kill lifesteal for a percentage of enemy HP

---

* **Dwarf** - 
	* Bonus: Can't dodge, crit chance is lowered but crit damage is significantly increased
	* Ability: Each crit has a chance to stun a target

---

* **Elf** - Grace of the Eternal
	* Bonus: Increased dodge chance, accuracy aura
	* Ability: - On taking a hit decreases aggro

---

* **Fairy** - Michevious Trickster
	* Bonus: Greatly increased dodge chance, reduced damage
	* Ability: Once per battle causes enemies to attack their friends

---

* **Human** - 
	* Bonus: All stats bonus
	* Ability: Receives a random buff on start of each battle

---

* **Orc** - Berserker's Fury
	* Bonus: Gains increased attack damage bonus when below 50% HP
	* Ability: One time per battle when HP reaches 50% enters in a state of rage for three turns that doubles attack damage but is lowering defense and magic resistance

---

