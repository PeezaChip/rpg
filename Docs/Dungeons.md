﻿## Initializing

```cs
int difficulty;
double encountersRate;
double enemiesRate;
int enemiesStrength;
int enemiesCount;
```

heroes etc.

Encounters will scale based on `difficulty`. Enemies will scale HP, Atk, Def, and after certain threshold they will be ranged, cast abilites etc.
`encountersRate` will determine chance of getting an encounter instead of nothing. `enemiesRate` will determine a chance of getting a battle encounter;
`bossRate` will be based on `enemiesRate` and `difficulty`. `enemiesStrength` will be added to `difficulty` when generating them.
`enemiesCount` will multiply their count.
XP and points reward will be based on `difficulty`.

Only battle events awards XP, so that you can't abuse high level dungeons. XP and damage is also scaled based on level difference.

## Dungeon

Minimum dungeon time is 10m. Each minute encounter is rolled. Each five the dungeon is saved.
The legacy version generated reward on dungeon completition only. And it was based on difficulty only.
Now reward will be generated for each encounter. If dungeon is completed successfuly a bonus added. If not reward is divided by half.
Reward IS splitted between a party.

Legacy dungeon was processed when full time passed. Now the dungeon is processed in realtime. So as soon as all members of a party is dead. It is over and can be completed.
(might consider auto-completing a dungeon and starting a new one with same settings).
For a fee you could always end a dungeon early losing all rewards.