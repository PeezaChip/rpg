﻿There are 7 item types:
```cs
public enum ItemType
{
    Weapon,
    Helmet,
    Armor,
    Cape,
    Necklace,
    Ring,
    Trinket
}
```

And 7 item rarities:
```cs
[Flags]
public enum ItemRarity
{
    Common = 0, // #FFFFFF
    Uncommon = 1, // #0094FF
    Rare = 2, // #4800FF
    Unique = 4, // #D8B400
    Legendary = 8, // #4CFF00
    Charged = 16, //
    Legacy = 32 // #FF0000
}
```

`Charged` is currenlty unused.
`Legacy` these are presetted items, currently cannot be dropped.

There are number of things that can be generated on an item.
* Tier 1 stats
  * VIT, TEC, LUK
* Tier 2 stats
  * INT, DEX, STR
* Tier 3 Stats
  * Acc, CrC, CrD, Dge, Blk
* Tier 4 stats
  * Atk, Def
* Tier 5 stats
  * Aggro
* Item abilites

Items are generated based on iLvl and drop %.
First roll is for item rarity.

* Common 76%
* Uncommon 15%
* Rare 5%
* Unique 3%
* Legendary 1%

Each % in drop rate will reduce chance of getting common item by 4%. Each % after 10 will reduce chance of getting uncommon item by 3%.

* Common items can only get T1 stats. Only 1 T1 stat can be present on an item.
* Uncommon items adds T2 stats to the pool. Up to 2 T1, T2 different stats can present on an item.
* Rare items can contain negative T1, T2 stats. If they do, up to 2 T1, T2 different stats can present on an item.
  * Chance of getting negative stats
    * Common 0%
    * Uncommon 0%
    * Rare 5%
    * Unique 10%
    * Legendary 25%
* Rare items can also have an ability.
  * Chance of getting an ability
    * Common 0%
    * Uncommon 0%
    * Rare 1%
    * Unique 10%
    * Legendary 20%
* Unique items can contain T3 stats. Up to 1 T3 stat can be present on an item.
* Legendary items can contain T4 stats. Up to 2 T3, T4 different stats can present on an item.
* If an item have a VIT stat it can also roll a chance for a T5 stat
 * Chance of getting a T5 stat
    * Common 5%
    * Uncommon 10%
    * Rare 20%
    * Unique 35%
    * Legendary 50%

iLvl increases stats amount and ability power value.

Positive stats amount is 1-10.
Negative stats amount is 1-5.
(each negative stat point will also increase another stat by one point)