import numpy as np
import matplotlib.pyplot as pyplot

def graph(func, x, configure_plot = None, configure_line = None, subplot = 0):
   x = np.arange(*x)
   y = func(x)

   if subplot > 0:
      pyplot.subplot(subplot)

   if configure_plot != None:
      configure_plot(pyplot)

   line, = pyplot.plot(x, y)
   if configure_line != None:
      configure_line(line)
      pyplot.legend()

def split():
   pyplot.show()

def proxy(configure):
   configure(pyplot)