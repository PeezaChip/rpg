﻿```mermaid
flowchart
    Warrior --> Guardian
    Warrior --> Berserk
    Warrior --> BM[Battle Mage]
    Guardian --> Crusader
    Guardian --> Vanguard
    Guardian --> Paladin
    Berserk --> Juggernaut
    Berserk --> Gladiator

    Mage --> BM
    Mage --> Wizard
    Mage --> Elementalist
    BM --> DK[Death Knight]
    BM --> Templar
    Wizard --> Necromancer
    Wizard --> AW[Arch Wizard]
    Elementalist --> ES[Elemental Summoner]
    Elementalist --> EM[Elemental Master]

    Rogue --> Archer
    Rogue --> Assasin
    Archer --> Hunter
    Archer --> Ranger
    Assasin --> NB[Night Blade]
    Assasin --> CA[Cult Assassin]

    Cleric --> Enchanter
    Cleric --> Priest
    Enchanter --> SM[Spell Master]
    Enchanter --> BR[Blood Ritualist]
    Priest --> HP[Holy Priest]
    Priest --> UP[Unholy Priest]
```
```

---
---
---

* **Warrior** - Strength
	* Bonus: Increased defenses
	* Ability: Every few turns can shout to attract enemy attention

---
---

* **Guardian** - Protector's Oath
	* Bonus: Increased aggro
	* Ability: Once per battle can intervene if a party member is about to receive a lethal hit

---

* **Berserk** - Fear of Nothing
	* Bonus: Gains increased attack damage bonus and decreased defenses when below 50% HP
	* Ability: Gain HP regeneration when below 25% HP

---
---

* **Crusader** - Holy Land Defender
	* Bonus: Upon receieving heals deals magic damage to a random enemy
	* Ability: Every few turns swings his weapon imbued with holy powers that blinds enemies

---

* **Vanguard** - Unyielding Advance
	* Bonus: Defense Aura
	* Ability: Every few turns greatly incresases party's defenses at the cost of your ability to attack

---

* **Paladin** - Sacred Bulwark
	* Bonus: Every attack heals a party member with lowest HP% for a portion of that attack damage
	* Ability: Every N HP healed this way creates a shield for each party member

---
---

* **Juggernaut** - ???
	* Bonus: ???
	* Ability: ???

---

* **Gladiator** - ???
	* Bonus: ???
	* Ability: ???

---
---
---

* **Mage** - ???
	* Bonus: ???
	* Ability: ???

---
---

* **Battle Mage** - ???
	* Bonus: ???
	* Ability: ???

---

* **Wizard** - ???
	* Bonus: ???
	* Ability: ???

---
---

* **Death Knight** - ???
	* Bonus: ???
	* Ability: ???

---

* **Templar** - ???
	* Bonus: ???
	* Ability: ???

---
---

* **Necromancer** - ???
	* Bonus: ???
	* Ability: ???

---

* **Arch Wizard** - ???
	* Bonus: ???
	* Ability: ???

---
---
---

* **Rogue** - ???
	* Bonus: ???
	* Ability: ???

---
---

* **Archer** - ???
	* Bonus: ???
	* Ability: ???

---

* **Assassin** - ???
	* Bonus: ???
	* Ability: ???

---
---

* **Hunter** - ???
	* Bonus: ???
	* Ability: ???

---

* **Ranger** - ???
	* Bonus: ???
	* Ability: ???

---
---

* **Shadow Blade** - ???
	* Bonus: ???
	* Ability: ???

---

* **Cult Assassin** - ???
	* Bonus: ???
	* Ability: ???

---
---
---

* **Cleric** - ???
	* Bonus: ???
	* Ability: ???

---
---

* **Enchanter** - ???
	* Bonus: ???
	* Ability: ???

---

* **Priest** - ???
	* Bonus: ???
	* Ability: ???

---
---

* **???** - ???
	* Bonus: ???
	* Ability: ???

---

* **Ritualist** - ???
	* Bonus: ???
	* Ability: ???

---
---

* **Holy Priest** - ???
	* Bonus: ???
	* Ability: ???

---

* **Unholy Priest** - ???
	* Bonus: ???
	* Ability: ???

---
---
---

```
Warrior - More aggro
    Guardian - More aggro
        Crusader - Deals damage to a random enemy when healed
        Vanguard - Much more aggro
        Paladin - Tank with heal 
    Berserk - More damage
        Juggernaut - Enrages - while on low hp deals much more damage
        Gladiator - AoE

Mage - None
    BattleMage - Damage
        Death Knight - Reflects damage (before applying to himself, if target dies of reflect dk mitigates all of it)
        Templar - Can ressurect himself
    Wizard - Damage
        Necromancer - Ressurects dead enemies to fight by his side
        Arch Wizard - Converts damage to magic damage
    Cleric - Heals, Less aggro
        Enchanter - Shields
        Priest - AoE Heals

Rogue - None
    Archer - Damage
        Hunter - Traps that disables enemies, trying to attack target with lowest aggro
        Ranger - Attacks multiple times
    Assassin - Can attack back on dodge
        Night Blade - Can backstab resulting in instant enemy death
        Cult Assassin - Gains health when kills enemies
```