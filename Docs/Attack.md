﻿# OnPreAttack
- Checks if target is valid
- Accuracy check

# OnMiss
- Miss

# OnAttack
- Calls OnAttackDamage to calculate Crit Damage and Elemental Damage
- Calls enemy OnGetAttack

# OnGetAttack
- Dodge check
- Block check
- Dodge (Block) completely skips getting damage
  - Calls attacker OnAfterAttack
- Magic damage reduced by magical defense
- Calculates damage to shield
- Physical damage reduced by physcial defense
- Calculates damage to health
- Calls self OnHealthChange
- Calls attacker OnAfterAttack

# OnGetAttack
- If health is zero calls self OnDeath

# OnDeath
- Calls attacker OnKill

Elements
- Fire, Nature, Elec, Water
- Light, Dark

Any elemental damage counts as a magical damage and thus affected by Magic Defense
