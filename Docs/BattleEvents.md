﻿# ProcessAilments
- Flag - If true will not call OnPreAttack
- Data - Not used
- Will call OnPreAttack (if not stunned, killed, etc)
- Either way will call ProcessBuffs (after OnPreAttack)
- Call to ProcessAilments counts as a turn
# ProcessBuffs
- Flag - Not used
- Data - Not used
# OnPreAttack
- Flag - Not used
- Data - Not used
- Will call target's OnPreGetAttack
- Will call OnAttack if Accuracy check is passed, OnMiss otherwise
# OnAttack
- Flag - Not used
- Data - Not used
- Will call OnAttackDamage
# OnAttackDamage
- Flag - Not used
- Data - DamageInfoData
- Will call target's OnGetAttack
# OnAfterAttack
- Flag - Not used
- Data - DamageInfoData
# OnPreGetAttack
- Flag - Not used
- Data - Not used
# OnGetAttack
- Flag - Not used
- Data - DamageInfoData
- Will call attacker's OnAfterAttack
- Will call OnDodge if Dodge check is passed
- Will call OnBlock if Block check is passed
    ### OnGetAttackShield
    - Flag - Not used
    - Data - DamageInfoData
    - Will reduce Data by shield capacity
    ### OnGetAttackHealth
    - Flag - Not used
    - Data - DamageInfoData
    - Will call OnHealthChange
# OnHealthChange
- Flag - Not used
- Data - HealthChangeData
- Will call OnDeath if health is 0
# OnMiss
- Flag - Not used
- Data - Not used
# OnDodge
- Flag - Not used
- Data - DamageInfoData
# OnBlock
- Flag - Not used
- Data - DamageInfoData
# OnGetHeal
- Flag - Not used
- Data - HealInfoData
- Will call OnHealthChange
# OnDeath
- Flag - if true won't call onKill
- Data - DamageInfoData
# OnRessurection
- Flag - Not used
- Data - How much health should be restored
# OnKill
- Flag - Not used
- Data - DamageInfoData
# OnFightStart
- Flag - Not used
- Data - Not used
# OnFightEnd
- Flag - Not used
- Data - Not used