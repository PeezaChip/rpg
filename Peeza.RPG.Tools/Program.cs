﻿using Peeza.RPG.Tools.Core;
using Peeza.RPG.Tools.Generators;

Console.WriteLine("Peeza.RPG.Tools");

var pathPrefix = @"..\..\..\..\";
var map = new Dictionary<BaseGenerator, List<string>>() {
    {
        new BattleEventDeclarationExtensionsGenerator(pathPrefix + @"Peeza.RPG.Tools\Generators\BattleEventDeclarationExtensionsGenerator.cs"), new List<string>() {
            @"Peeza.RPG.Tests\Battle\BattleEventHooksTest.cs",
            @"Peeza.RPG.Base\Battle\Events\BasicBattleEntityEventsDeclarations.cs",
            @"Peeza.RPG.Base\Battle\Events\BasicBattleSharedEventsDeclarations.cs",
        }
    },
    {
        new StatDeclarationExtensionsGenerator(pathPrefix + @"Peeza.RPG.Tools\Generators\StatDeclarationExtensionsGenerator.cs"), new List<string>()
        {
            @"Peeza.RPG.Base\Stats\BasicStatsDeclarations.cs",
            @"Peeza.RPG.Base\Stats\ItemStatsDeclarations.cs",
            @"Peeza.RPG.Base\Stats\SpellStatsDeclarations.cs",
        }
    },
    {
        new ResourceDeclarationExtensionsGenerator(pathPrefix + @"Peeza.RPG.Tools\Generators\ResourceDeclarationExtensionsGenerator.cs"), new List<string>()
        {
            @"Peeza.RPG.Base\Resources\BasicResourcesDeclarations.cs",
        }
    }
};

foreach(var (generator, files) in map)
{
    foreach(var file in files)
    {
        var filePath = Path.Combine(pathPrefix, file);
        var (source, token) = generator.CheckIfNeedToRegenerate(filePath);
        if (source == null) continue;

        Console.WriteLine($"Generating {file} using {generator.GetType().Name}");
        var generatedSource = generator.Generate(Path.GetFileNameWithoutExtension(filePath), source, token);
        if (generatedSource == null) continue;

        var generatedPath = Path.Combine(Path.GetDirectoryName(filePath), Path.GetFileNameWithoutExtension(filePath) + generator.Extension);
        File.WriteAllText(generatedPath, generatedSource);
    }
}