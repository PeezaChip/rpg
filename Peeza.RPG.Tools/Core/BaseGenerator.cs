﻿using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace Peeza.RPG.Tools.Core
{
    public abstract partial class BaseGenerator
    {
        protected string generatorToken;

        public abstract string Extension { get; }

        public abstract string Generate(string fileName, string source, string token);

        public BaseGenerator(string pathToGenerator)
        {
            var source = File.ReadAllText(pathToGenerator);
            generatorToken = GenerateToken(source);
        }

        public virtual (string, string) CheckIfNeedToRegenerate(string pathToFile)
        {
            var fileName = Path.GetFileNameWithoutExtension(pathToFile);

            var generatedPath = Path.Combine(Path.GetDirectoryName(pathToFile), fileName + Extension);

            var source = File.ReadAllText(pathToFile);
            var token = GenerateToken(source);

            if (!File.Exists(generatedPath)) return (source, token);

            var generatedTokenMatch = GenerateTokenRegex().Match(File.ReadAllText(generatedPath));

            if (generatedTokenMatch.Success && $"{token}-{generatorToken}" == generatedTokenMatch.Groups[1].Value) return (null, null);

            return (source, token);
        }

        private static string GenerateToken(string source)
        {
            return Convert.ToBase64String(SHA256.HashData(Encoding.UTF8.GetBytes(source)));
        }

        [GeneratedRegex(@"\/\/ <generate-token>(.*)<\/generate-token>")]
        private static partial Regex GenerateTokenRegex();
    }
}
