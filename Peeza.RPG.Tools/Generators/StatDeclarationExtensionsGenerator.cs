﻿using Peeza.RPG.Tools.Core;
using System.Text.RegularExpressions;

namespace Peeza.RPG.Tools.Generators
{
    public partial class StatDeclarationExtensionsGenerator : BaseGenerator
    {
        [GeneratedRegex(@" +public class ([\w\d]+) ?: ?StatDeclaration(<[\w\d]+>)?( *\/\/([\s\w\d,]+)| *)")]
        private static partial Regex declarationsRegex();

        [GeneratedRegex(@"using ([\w\d\.]+)")]
        private static partial Regex usingRegex();

        [GeneratedRegex(@"namespace ([\w\d\.]+)")]
        private static partial Regex namespaceRegex();

        public override string Extension => ".rst.cs";

        public StatDeclarationExtensionsGenerator(string pathToGenerator) : base(pathToGenerator) { }

        public override string Generate(string fileName, string source, string token)
        {
            var usingMatches = usingRegex().Matches(source);
            var namespaceMatch = namespaceRegex().Match(source);
            var declarationMatches = declarationsRegex().Matches(source);

            if (!usingMatches.Any() || !declarationMatches.Any() || !namespaceMatch.Success) return null;

            var sourceBuilder = new SourceBuilder();
            sourceBuilder.AppendHeader($"{token}-{generatorToken}");

            foreach (var match in usingMatches.Cast<Match>())
            {
                sourceBuilder.AppendLine("using Peeza.RPG.Core.Basic.BattleEntities;");
                sourceBuilder.AppendLine("using Peeza.RPG.Core.Basic.Buffs;");
                sourceBuilder.AppendLine($"using {match.Groups[1].Value};");
            }

            sourceBuilder.AppendLine();
            sourceBuilder.AppendLine($"namespace {namespaceMatch.Groups[1].Value}");
            sourceBuilder.BeginBlock();

            sourceBuilder.AppendLine($"public static class {fileName}Extensions");
            sourceBuilder.BeginBlock();

            var sourceClassBuilder = new List<string>();

            foreach (var match in declarationMatches.Cast<Match>())
            {
                var prefix = false;
                if (!string.IsNullOrWhiteSpace(match.Groups[4].Value))
                {
                    var modifiers = match.Groups[4].Value.Split(',').Select(s => s.Trim());
                    if (modifiers.Contains("prefix"))
                    {
                        prefix = true;
                    }
                }

                var statName = match.Groups[1].Value;
                if (statName.EndsWith("StatDeclaration"))
                {
                    statName = statName[..^"StatDeclaration".Length];
                }

                var typeName = match.Groups[2].Value[1..^1];

                var constructorRegex = new Regex($@" +public {match.Groups[1].Value}\(\) ?: ?base\(([\w\d]+), ?([\w]+)\) ?{{ ?}}");
                var constructorMatches = constructorRegex.Matches(source);
                if (constructorMatches.Count != 1)
                {
                    throw new NotImplementedException("There should be atleast and only 1 resource constructor");
                }

                sourceBuilder.AppendLine($"public const string {statName}StatKey = \"{statName}\";");

                sourceClassBuilder.Add("");
                var classString = $"public class {statName}Stat({(prefix ? "string prefix, " : "")}{typeName} value = {constructorMatches[0].Groups[1].Value}";
                if (constructorMatches[0].Groups[2].Value.ToLower() == "true")
                {
                    classString += $", IBuffsAccessor buffs = null, {typeName} minValue = 0) : {(prefix ? "Prefixed" : "")}BuffableStat<{typeName}>({fileName}Extensions.{statName}StatKey, {(prefix ? "prefix, " : "")}buffs, value, minValue);";
                    if (typeName == "double")
                    {
                        sourceClassBuilder.Add($"public class {statName}Buff(IBattleEntity inflicter, {typeName} value, BuffFlags flags = BuffFlags.None, int turnsLeft = 0) : Buff<{typeName}>({fileName}Extensions.{statName}StatKey, inflicter, value, flags, turnsLeft);");
                    }
                    else
                    {
                        sourceClassBuilder.Add($"public class Flat{statName}Buff(IBattleEntity inflicter, {typeName} value, BuffFlags flags = BuffFlags.None, int turnsLeft = 0) : Buff<{typeName}>({fileName}Extensions.{statName}StatKey, inflicter, value, flags, turnsLeft);");
                        sourceClassBuilder.Add($"public class Multiplicative{statName}Buff(IBattleEntity inflicter, double value, BuffFlags flags = BuffFlags.None, int turnsLeft = 0) : Buff<double>({fileName}Extensions.{statName}StatKey, inflicter, value, flags | BuffFlags.Multiplicative, turnsLeft);");
                    }
                }
                else
                {
                    classString += $", {typeName} minValue = 0) : Stat<{typeName}>({fileName}Extensions.{statName}StatKey, value, minValue);";
                }
                sourceClassBuilder.Add(classString);

                sourceBuilder.AppendLine($"public static {typeName} Get{statName}(this IStatsAccessor stats)");
                sourceBuilder.BeginBlock();
                sourceBuilder.AppendLine($"var stat = stats.GetByKey<{typeName}>({statName}StatKey);");
                sourceBuilder.AppendLine($"if (stat != null)");
                sourceBuilder.BeginBlock();
                sourceBuilder.AppendLine($"return stat.Value;");
                sourceBuilder.EndBlock();
                sourceBuilder.AppendLine($"else");
                sourceBuilder.BeginBlock();
                sourceBuilder.AppendLine($"return {constructorMatches[0].Groups[1].Value};");
                sourceBuilder.EndBlock();
                sourceBuilder.EndBlock();

                sourceBuilder.AppendLine($"public static {typeName} GetBase{statName}(this IStatsAccessor stats)");
                sourceBuilder.BeginBlock();
                sourceBuilder.AppendLine($"var stat = stats.GetByKey<{typeName}>({statName}StatKey);");
                sourceBuilder.AppendLine($"if (stat != null)");
                sourceBuilder.BeginBlock();
                sourceBuilder.AppendLine($"return stat.BaseValue;");
                sourceBuilder.EndBlock();
                sourceBuilder.AppendLine($"else");
                sourceBuilder.BeginBlock();
                sourceBuilder.AppendLine($"return {constructorMatches[0].Groups[1].Value};");
                sourceBuilder.EndBlock();
                sourceBuilder.EndBlock();

                sourceBuilder.AppendLine();
            }

            sourceBuilder.EndBlock();
            foreach (var classLine in sourceClassBuilder)
            {
                sourceBuilder.AppendLine(classLine);
            }
            sourceBuilder.EndBlock();

            return sourceBuilder.ToString();
        }
    }
}
