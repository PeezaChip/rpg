﻿using Peeza.RPG.Tools.Core;
using System.Text.RegularExpressions;

namespace Peeza.RPG.Tools.Generators
{
    public partial class BattleEventDeclarationExtensionsGenerator : BaseGenerator
    {
        [GeneratedRegex(@" +public class ([\w\d]+) ?: ?Battle(\w+)?EventDeclaration(<[\w\d]+>)?")]
        private static partial Regex declarationsRegex();

        [GeneratedRegex(@"using ([\w\d\.]+)")]
        private static partial Regex usingRegex();

        [GeneratedRegex(@"namespace ([\w\d\.]+)")]
        private static partial Regex namespaceRegex();

        public static readonly string[] mandatoryUsings = ["Peeza.RPG.Core.Battle.Events.Interfaces", "Peeza.RPG.Core.Battle.Events"];

        public override string Extension => ".rev.cs";

        public BattleEventDeclarationExtensionsGenerator(string pathToGenerator) : base(pathToGenerator) { }

        public override string Generate(string fileName, string source, string token)
        {
            var usingMatches = usingRegex().Matches(source);
            var namespaceMatch = namespaceRegex().Match(source);
            var declarationMatches = declarationsRegex().Matches(source);

            if (!usingMatches.Any() || !declarationMatches.Any() || !namespaceMatch.Success) return null;

            var sourceBuilder = new SourceBuilder();
            sourceBuilder.AppendHeader($"{token}-{generatorToken}");

            foreach (var mandatoryUsing in mandatoryUsings)
            {
                sourceBuilder.AppendLine($"using {mandatoryUsing};");
            }

            foreach (var match in usingMatches.Cast<Match>())
            {
                if (mandatoryUsings.Contains(match.Groups[1].Value)) continue;
                sourceBuilder.AppendLine($"using {match.Groups[1].Value};");
            }

            sourceBuilder.AppendLine();
            sourceBuilder.AppendLine($"namespace {namespaceMatch.Groups[1].Value}");
            sourceBuilder.BeginBlock();

            sourceBuilder.AppendLine($"public static class {fileName}Extensions");
            sourceBuilder.BeginBlock();

            foreach (var match in declarationMatches.Cast<Match>())
            {
                sourceBuilder.AppendLine($"public static {match.Groups[1].Value} {match.Groups[1].Value}Declaration {{ get; }} = new {match.Groups[1].Value}();");
                sourceBuilder.AppendLine("/// <summary>");
                sourceBuilder.AppendLine($"/// <inheritdoc cref=\"{namespaceMatch.Groups[1].Value}.{match.Groups[1].Value}\"/>");
                sourceBuilder.AppendLine("/// </summary>");
                sourceBuilder.AppendLine($"public static void {match.Groups[1].Value}(this IBattle{match.Groups[2].Value}EventHandlersProvider eventHandlerProvider, BattleEventArgs{match.Groups[3].Value} args)");
                sourceBuilder.BeginBlock();

                sourceBuilder.AppendLine($"eventHandlerProvider.RaiseEvent({match.Groups[1].Value}Declaration, args);");

                sourceBuilder.EndBlock();

                sourceBuilder.AppendLine();
            }

            sourceBuilder.EndBlock();
            sourceBuilder.EndBlock();

            return sourceBuilder.ToString();
        }
    }
}
