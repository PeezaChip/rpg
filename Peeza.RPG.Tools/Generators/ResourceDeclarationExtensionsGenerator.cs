﻿using Peeza.RPG.Tools.Core;
using System.Text.RegularExpressions;

namespace Peeza.RPG.Tools.Generators
{
    public partial class ResourceDeclarationExtensionsGenerator : BaseGenerator
    {
        [GeneratedRegex(@" +public class ([\w\d]+) ?: ?ResourceDeclaration")]
        private static partial Regex declarationsRegex();

        [GeneratedRegex(@"using ([\w\d\.]+)")]
        private static partial Regex usingRegex();

        [GeneratedRegex(@"namespace ([\w\d\.]+)")]
        private static partial Regex namespaceRegex();

        public override string Extension => ".rres.cs";

        public ResourceDeclarationExtensionsGenerator(string pathToGenerator) : base(pathToGenerator) { }

        public override string Generate(string fileName, string source, string token)
        {
            var usingMatches = usingRegex().Matches(source);
            var namespaceMatch = namespaceRegex().Match(source);
            var declarationMatches = declarationsRegex().Matches(source);

            if (!usingMatches.Any() || !declarationMatches.Any() || !namespaceMatch.Success) return null;

            var sourceBuilder = new SourceBuilder();
            sourceBuilder.AppendHeader($"{token}-{generatorToken}");

            foreach (var match in usingMatches.Cast<Match>())
            {
                sourceBuilder.AppendLine("using Peeza.RPG.Core.Basic.Buffs;");
                sourceBuilder.AppendLine($"using {match.Groups[1].Value};");
            }

            sourceBuilder.AppendLine();
            sourceBuilder.AppendLine($"namespace {namespaceMatch.Groups[1].Value}");
            sourceBuilder.BeginBlock();

            sourceBuilder.AppendLine($"public static class {fileName}Extensions");
            sourceBuilder.BeginBlock();

            var sourceClassBuilder = new List<string>();

            foreach (var match in declarationMatches.Cast<Match>())
            {
                var statName = match.Groups[1].Value;
                if (statName.EndsWith("ResourceDeclaration"))
                {
                    statName = statName[..^"ResourceDeclaration".Length];
                }

                var constructorRegex = new Regex($@" +public {match.Groups[1].Value}\(\) ?: ?base\(([\w\d]+), ?([\w]+)\) ?{{ ?}}");
                var constructorMatches = constructorRegex.Matches(source);
                if (constructorMatches.Count != 1)
                {
                    throw new NotImplementedException("There should be atleast and only 1 resource constructor");
                }

                sourceBuilder.AppendLine($"public const string {statName}ResourceKey = \"{statName}\";");

                //sourceBuilder.AppendLine($"public static {match.Groups[1].Value} {match.Groups[1].Value}Declaration {{ get; }} = new {match.Groups[1].Value}();");

                var classString = $"public class {statName}Resource(long defaultValue = {constructorMatches[0].Groups[1].Value}";
                if (constructorMatches[0].Groups[2].Value.ToLower() == "true")
                {
                    classString += $", IBuffsAccessor buffs = null, long min = 0, bool setCurrentToMax = true) : BuffableResource({fileName}Extensions.{statName}ResourceKey, buffs, defaultValue, min, setCurrentToMax);";
                }
                else
                {
                    classString += $", long min = 0, bool setCurrentToMax = true) : SimpleResource({fileName}Extensions.{statName}ResourceKey, defaultValue, min, setCurrentToMax);";
                }
                sourceClassBuilder.Add(classString);

                sourceBuilder.AppendLine($"public static Resource Get{statName}(this IResourcesAccessor stats) => stats.GetByKey({statName}ResourceKey);");

                sourceBuilder.AppendLine();
            }

            sourceBuilder.EndBlock();
            sourceBuilder.AppendLine();
            foreach (var classLine in sourceClassBuilder)
            {
                sourceBuilder.AppendLine(classLine);
            }
            sourceBuilder.EndBlock();

            return sourceBuilder.ToString();
        }
    }
}
